this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var RouletteTableStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_RouletteTableStream();
    };

    var p = createjs.extend(RouletteTableStream, alteastream.ABSVideoStream);
// static properties:
// events:

// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_RouletteTableStream = function (){
    };

    p.setPlayer = function(address){
        /* // local ver
        var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
        this.webrtcPlayer.setPreview(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, streamPreviewParams.machineName);
        */ // local ver
        this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver

        this._local_activate();// local ver
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            that.scene.socketCommunicator.disposeCommunication();//quitPlay() if started
            that.scene.exitMachineGameplay();
        });
    };

    p.concreteActivate = function(){
        if(is.safari()){
            //var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
            //if(is.ipad() || is.ios() || isiPadOS){
            this.scene.setIosOverlay();
            //}
        }
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;

        if(this.scene.streamVideoSmall){
            this.scene.streamVideoSmall.showVideoStream(true);
        }
    };

    p.addSpinner = function(){
        var spinnerBackground = this.spinnerBackground = alteastream.Assets.getImage(alteastream.Assets.images.liveStreamConnecting);
        spinnerBackground.regX = spinnerBackground.image.width*0.5;
        spinnerBackground.regY = spinnerBackground.image.height*0.5;
        spinnerBackground.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinnerBackground.y = alteastream.AbstractScene.GAME_HEIGHT *0.5;

        var spinner = this.spinner = new alteastream.MockLoader();
        spinner.customScaleGfx(0.4);
        this.scene.addChild(spinnerBackground, spinner);
        //spinner.setLabel("Connecting to stream..");
        spinner.x = spinnerBackground.x + 196;
        spinner.y = spinnerBackground.y - 80;
        spinner.runPreload(true);
    };

    p.setMedia = function(){
        window.parent.switchSource("background_music");

        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            soundMachineIsToggled = Boolean(soundMachineIsToggled);
        }else{
            soundMachineIsToggled = false;
        }

        if(soundMachineIsToggled === false){
            this.videoElement.setMuted(false);
            this.videoElement.setVolume(0.1);
            console.log("AUDIO ON:::::");
        }

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
                this.scene.addChild(poster);
                poster.mouseEnabled = false;*/
    };

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.onStreamFailed = function(){
        var _this = this;
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed. Exiting game",function(){_this.scene.onQuit();});
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.RouletteTableStream = createjs.promote(RouletteTableStream,"ABSVideoStream");
})();this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var RouletteWheelStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_RouletteWheelStream();
    };

    var p = createjs.extend(RouletteWheelStream, alteastream.ABSVideoStream);

// static properties:
// events:
// VideoStreamSmall was here
// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinnerWheel = null;
    p.spinnerWheelBackground = null;
// constructor:
    p.initialize_RouletteWheelStream = function (){
    };

    p.setPlayer = function(address){
        /* // local ver
        var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
        //temp HC "testroulette" instead of streamPreviewParams.machineName
        //this.webrtcPlayer.setPreview(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, "testroulette");
        this.webrtcPlayer.setPreview2(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, "testroulette");//setPreview2
        */ // local ver
        this.webrtcPlayer.setPreview(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver

        var videoStream = this.videoStream = new createjs.DOMElement(this.videoElement.htmlVideo);
        alteastream.Roulette.getInstance().addChildAt(videoStream,0);
        this.videoStream.scaleX = 0.32;
        this.videoStream.scaleY = 0.32;
        this.videoStream.mouseEnabled = false;

        this.showVideoStream(false);
        this.setDynamicPosition(-0.053,0.605);

        var _this = this;
        window.addEventListener("resize",function(){
            _this._resize();
        });

        this._local_activate();// local ver
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
        });
    }

    p.concreteActivate = function(){
        if(this.spinnerWheel){
            this.spinnerWheel.runPreload(false);
            this.scene.removeChild(this.spinnerWheel);
            this.scene.removeChild(this.spinnerWheelBackground);
            this.spinnerWheel = null;
        }
    };

    p.addSpinner = function () {};//temp toggle visibility

    p.appendSpinner = function () {//temp toggle visibility
        if(!this.streamMonitor._streamEstablished){
            var spinnerWheelBackground = this.spinnerWheelBackground = new createjs.Shape();
            spinnerWheelBackground.graphics.beginFill("#000000").drawRect(0, 0, 344, 344);
            spinnerWheelBackground.x = 29;
            spinnerWheelBackground.y = 656;

            var spinner = this.spinnerWheel = new alteastream.MockLoader();
            this.scene.addChild(spinnerWheelBackground, spinner);
            spinner.x = spinnerWheelBackground.x + 172;
            spinner.y = spinnerWheelBackground.y + 96;
            spinner.runPreload(true);
        }
    };

    p.setMedia = function(){

    }

    p.setPreloadPoster = function(bool){

    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.onStreamFailed = function(){
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed");
    }

    p.showVideoStream = function (show) {
        this.videoStream.visible = show;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._resize = function(){
        var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var h = (window.innerHeight > 0) ? window.innerHeight : screen.width;

        //this.SPACING_LEFT
        //this.SPACING_TOP
        var spacingX = 2*(w/1000);
        var spacingY = 2*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);
        this.showVideoStream(true);

        // x:69 y:430
        /*var that = this;
        stage.addEventListener("stagemousedown", function(){
            var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            var xPos = w/4;
            TweenLite.to(that.videoStream,0.3,{x:xPos,y:0,scaleX:0.5,scaleY:0.5,onComplete:function(that){
                    that.setDynamicPosition(0.252,-0.004);
                    setTimeout(function(){
                        that.videoStream.scaleX = 0.32;
                        that.videoStream.scaleY = 0.32;
                        that.setDynamicPosition(-0.053,0.605);},1000);
                },onCompleteParams:[that]});
        })*/

    };

    alteastream.RouletteWheelStream = createjs.promote(RouletteWheelStream,"ABSVideoStream");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSBetField = function(name,label,imageName) {
        this.Container_constructor();
        this.initialize_ABSBetField(name,label,imageName);
    };

    var p = createjs.extend(ABSBetField, createjs.Container);
    var _instance = null;

// static properties:
// events:
// public properties:
// private properties:
    p._normalFontColor = "#ffffff";
    p._highlitedFontColor = "#000000";
    p._disabledFontColor = "#b4b4b4";

// constructor:
// static methods:
// public methods:
    p.initialize_ABSBetField = function(name,label,imageName) {

        _instance = this;
        this.name = name;

        this.highlightState = alteastream.Assets.getImage(alteastream.Assets.images[imageName+"H"]);
        this.normalState = alteastream.Assets.getImage(alteastream.Assets.images[imageName]);
        this.addChild(this.highlightState,this.normalState);
        this.highlightState.visible = false;

        var w = this._fieldWidth =  this.normalState.image.width,h = this._fieldHeight = this.normalState.image.height;

        var fontSize = 30;
        //var bitmapCachedY = this.bitmapCachedY=6;
        //var fontSmall = "bold "+fontSize+"px HurmeGeometricSans3";
        var fontSmall = fontSize+"px RobotoCondensed";

        if(label!==null){
            this.label = new alteastream.BitmapText(String(label),fontSmall, this._normalFontColor,{x:w/2,y:(h/2-fontSize/2),textAlign:"center"/*,yCacheAdd:bitmapCachedY*/});
            this.label.mouseEnabled = false;
            this.addChild(this.label);
            if(label ==="red" || label ==="black"){
                this.label.visible = false;
                var decal = this.decal = alteastream.Assets.getImage(alteastream.Assets.images["label_"+label]);
                this.addChild(decal);
                decal.x = w/2-decal.image.width/2;
                decal.y = h/2-decal.image.height/2;
            }
        }

        this.chipsArr = [];
    };

    p.getWidth = function(){
        return  this._fieldWidth;
    };

    p.getHeight = function(){
        return  this._fieldHeight;
    };

    p.setColor = function(color,imageName){
        this.normalState.image = alteastream.Assets.getImage(alteastream.Assets.images[imageName+"_"+color]).image;
    }

    p.setFont = function(size,fontName){// bitmapText, temp srediti
        var fontSize = size;
        var fontSmall = fontSize+"px "+ fontName;

        var labelTxt = this.label.text;
        var childIndx = this.getChildIndex(this.label);
        this.removeChild(this.label);

        this.label = new alteastream.BitmapText(labelTxt,fontSmall, this._normalFontColor,{x:this._fieldWidth/2,y:this._fieldHeight/2-fontSize/2,textAlign:"center",yCacheAdd:this.bitmapCachedY});
        this.addChildAt(this.label,childIndx);
    };

    p.setFontColors = function(normal,highlight){
        this._normalFontColor = normal;
        this._highlitedFontColor = highlight;
    }

    p.setHighlighted = function(bool){
        this.highlightState.visible = bool;
        this.normalState.visible = !bool;
        //this.label.color = bool === true? this._highlitedFontColor : this._normalFontColor;// normal text
        var color = bool ===true?this._highlitedFontColor : this._normalFontColor;// bitmap text
        this.label.updateText("color",color);// bitmap text
    };

    p.setDisabled = function(bool){
        //this.highlightState.visible = !bool;
        this.normalState.visible = true;
        //this.label.color = bool === true? this._highlitedFontColor : this._normalFontColor;// normal text
        var color = bool ===true?this._disabledFontColor : this._normalFontColor;// bitmap text
        this.label.updateText("color",color);// bitmap text
        this.alpha = bool ===true? 0.7:1;
        this.mouseEnabled = !bool;
    }

    alteastream.ABSBetField = createjs.promote(ABSBetField,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSBetTable = function(game,betmatrix) {
        this.Container_constructor();
        this.initialize_ABSBetTable(game,betmatrix);
    };

    var p = createjs.extend(ABSBetTable, createjs.Container);
    var _instance = null;

// static properties:

// events:
// public properties:
    p.BETMATRIX = [];
    p.TOTAL_NUMBERS = 37;
    p.FINALS_NUMBERS = 10;
    p.STREET_POSITIONS = 12;
    p.LINES_POSITIONS = 11;
    p._currentBet = [];
    p._selectedBetValuesArr = [];
    p.finalUserBet = [];
    p._currentBetAmount = 0.00;
    p.minStake = 0;
    p.maxStake = 0;
    p.minStakeReached = false;
    p.maxStakeReached = false;
    p.minStakeOn = false;
    p.maxStakeOn = false;
    p.minStakeOff = false;
    p.maxStakeOff = false;
// private properties:
    
// constructor:
// static methods:
// public methods:
    p.initialize_ABSBetTable = function(game,betmatrix) {
        _instance = this;
        this.game = game;//gamecore[gamecore.AbstractScene.NAME].getInstance();
        this.scene = this.game/*.scene*/;
        this.BETMATRIX = betmatrix;
        this._createLookUpTable(this.BETMATRIX);
    };

    p.setStakeMinMax = function(minStake,maxStake){
        this.minStake = minStake;
        this.maxStake = maxStake;
    };

    p.getMinStake = function(){
        return this.minStake;
    };

    p.getMaxStake = function(){
        return this.maxStake;
    };

    p._checkStake = function(){
        this.minStakeReached = this._currentBetAmount>=this.minStake;
        this.maxStakeReached = this._currentBetAmount>=this.maxStake;
        if(this.minStakeReached){
            if(!this.minStakeOn){
                this.minStakeOn = true;
                this.minStakeOff = false;
                alteastream.Roulette.getInstance().onMinStake(true);
            }
        }else{
            this.minStakeOn = false;
            if(!this.minStakeOff){
                this.minStakeOff = true;
                alteastream.Roulette.getInstance().onMinStake(false);
            }
        }
        if(this.maxStakeReached){
            if(!this.maxStakeOn){
                this.maxStakeOn = true;
                this.maxStakeOff = false;
                alteastream.Roulette.getInstance().onMaxStake(true);
            }
        }else{
            this.maxStakeOn = false;
            if(!this.maxStakeOff){
                this.maxStakeOff = true;
                alteastream.Roulette.getInstance().onMaxStake(false);
            }
        }
    };

    p.pushBet = function(chosenBet,amount){
        this._currentBet.push([chosenBet,amount]);
        var targetBetType = this.BETMATRIX[this._lookUpTable[chosenBet]];
        targetBetType.bet = Math.round((targetBetType.bet + amount) * 100) / 100;
        //console.log("chosenBet:: "+chosenBet);
        //console.log("pushBet:: "+targetBetType.bet);
        this._currentBetAmount = Math.round((this._currentBetAmount + amount) * 100) / 100;
        //console.log("currentBetAmount:: "+this._currentBetAmount);
        this._checkStake();
    };

    p.popBet = function(){
        var lastBetType = this._currentBet.pop();
        var targetBetType = this.BETMATRIX[this._lookUpTable[lastBetType[0]]];
        targetBetType.bet = Math.round((targetBetType.bet - lastBetType[1]) * 100) / 100;
        this._currentBetAmount = Math.round((this._currentBetAmount - lastBetType[1]) * 100) / 100;
        //console.log("currentBetAmount--:: "+this._currentBetAmount);
        this._checkStake();
    };

    p.clearBets = function(){
        this._currentBet = [];
        this._selectedBetValuesArr = [];
        this._currentBetAmount = 0.00;
        this.finalUserBet = [];
        this.highlightUserBet = [];//correctionHighlight new

        this.minStakeReached = false;
        this.maxStakeReached = false;
        this.minStakeOn = false;
        this.maxStakeOn = false;
        this.minStakeOff = false;
        this.maxStakeOff = false;
        alteastream.Roulette.getInstance().onMinStake(false);
        alteastream.Roulette.getInstance().onMaxStake(false);

        for(var i = 0,j = this.BETMATRIX.length; i<j; i++){
            this.BETMATRIX[i].bet = 0.00;
        }
    };

    p.getFinalUserBet = function(){
        //var cb = JSON.parse(JSON.stringify( this._currentBet ));
        //this.highlightUserBet = this.mergeBets(cb);// only jeuZ voisins...

        var finalBet = [];
        for(var i = 0,j = this.BETMATRIX.length; i<j; i++){
            var betName = this.BETMATRIX[i].name;
            var specials = this.BETMATRIX[i].special || null;
            var betAmount = this.BETMATRIX[i].bet;
            if(betAmount > 0.00){
                if(!specials){
                    finalBet.push([betName,betAmount]);
                }
            }
        }
        this.highlightUserBet = this.mergeBets(finalBet);
        this.finalUserBet = this.mergeBets(finalBet);
        return this.finalUserBet;
    };

    p.mergeBets = function(bets){
        var merged = [];
        var helper = [];
        var index;

        for(var i = 0,j = bets.length; i < j; i++) {
            index = helper.indexOf(bets[i][0]);
            if(index !== -1) {
                var sum = merged[index][1]+bets[i][1];
                merged[index][1] = Math.floor(sum*100)/100;
            }
            else {
                helper.push(bets[i][0]);
                merged.push(bets[i]);
            }
        }
        //console.log("MERGED "+merged);
        return merged;
    };

    p.getBetField = function(chosenBet){
        return this.BETMATRIX[this._lookUpTable[chosenBet]];
    };

    p.setWinningBets = function(winningNumber){
        this.wonBetsArray = [];
        var betMatrix = this.BETMATRIX;
        var finalUserBet = this.highlightUserBet;//correctionHighlight new
        var isSplit;
        var isNumber;
        var hasSplitOrDirect = false;
        for(var i = 0,j = finalUserBet.length; i<j; i++){
            var playedBets = finalUserBet[i][0];
            var betType = betMatrix[this._lookUpTable[playedBets]];
            isSplit = betType.name.substr(0,5)==="split" && betType.numbers.indexOf(winningNumber)>-1;
            isNumber = betType.name ==="num_"+String(winningNumber);
            if(isSplit === true || isNumber ===true){
                hasSplitOrDirect = true;
            }
            else{
                if(betType.numbers.indexOf(winningNumber)>-1){
                     this.wonBetsArray.push([betType.name]);
                }
            }
        }
        if(hasSplitOrDirect){
            var number = "num_"+String(winningNumber);
            this.wonBetsArray.unshift([number]);
        }
    };

// private methods:
    p._createLookUpTable = function(betMatrix){
        this._lookUpTable = {};
        for(var i = 0,j = betMatrix.length; i<j; i++)
            this._lookUpTable[betMatrix[i].name] = i;
    };

    alteastream.ABSBetTable = createjs.promote(ABSBetTable,"Container");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSChip = function(value,assetID) {
        this.Container_constructor();
        this.initialize_ABSChip(value,assetID);
    };

    var p = createjs.extend(ABSChip, createjs.Container);
    var _instance = null;

// static properties:

// events:
// public properties:
// private properties:
    
// constructor:
// static methods:
// public methods:
    p.initialize_ABSChip = function(value,assetID) {
        _instance = this;

        var fontSize = 16;
        var fontSmall = fontSize+"px RobotoCondensed";
        var white = "#ffffff";

        this.skin = alteastream.Assets.getImage(alteastream.Assets.images["chip"+assetID]);
        this.addChild(this.skin);

        var w = this.skin.image.width,h = this.skin.image.height;

        //if(value!==null)
        this.label = new alteastream.BitmapText(String(value),fontSmall, white,{x:w/2,y:h/2-fontSize/2,textAlign:"center"});
        this.addChild(this.label);

        this.regX = this.regY = this.skin.image.width/2;
        this.mouseEnabled = false;
    };

    alteastream.ABSChip = createjs.promote(ABSChip,"Container");
})();this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var TableMatrix = function() {};

    TableMatrix.FRENCH = [
            {name:"num_0", bet:0.00,numbers:[0],color:"green"},  {name:"num_1", bet:0.00,numbers:[1],color:"red"},  {name:"num_2", bet:0.00,numbers:[2],color:"black"},  {name:"num_3", bet:0.00,numbers:[3],color:"red"},  {name:"num_4", bet:0.00,numbers:[4],color:"black"},  {name:"num_5", bet:0.00,numbers:[5],color:"red"},  {name:"num_6", bet:0.00,numbers:[6],color:"black"},  {name:"num_7", bet:0.00,numbers:[7],color:"red"},  {name:"num_8", bet:0.00,numbers:[8],color:"black"},
            {name:"num_9", bet:0.00,numbers:[9],color:"red"},  {name:"num_10",bet:0.00,numbers:[10],color:"black"}, {name:"num_11",bet:0.00,numbers:[11],color:"black"}, {name:"num_12",bet:0.00,numbers:[12],color:"red"}, {name:"num_13",bet:0.00,numbers:[13],color:"black"}, {name:"num_14",bet:0.00,numbers:[14],color:"red"}, {name:"num_15",bet:0.00,numbers:[15],color:"black"}, {name:"num_16",bet:0.00,numbers:[16],color:"red"}, {name:"num_17",bet:0.00,numbers:[17],color:"black"},
            {name:"num_18",bet:0.00,numbers:[18],color:"red"}, {name:"num_19",bet:0.00,numbers:[19],color:"red"}, {name:"num_20",bet:0.00,numbers:[20],color:"black"}, {name:"num_21",bet:0.00,numbers:[21],color:"red"}, {name:"num_22",bet:0.00,numbers:[22],color:"black"}, {name:"num_23",bet:0.00,numbers:[23],color:"red"}, {name:"num_24",bet:0.00,numbers:[24],color:"black"}, {name:"num_25",bet:0.00,numbers:[25],color:"red"}, {name:"num_26",bet:0.00,numbers:[26],color:"black"},
            {name:"num_27",bet:0.00,numbers:[27],color:"red"}, {name:"num_28",bet:0.00,numbers:[28],color:"black"}, {name:"num_29",bet:0.00,numbers:[29],color:"black"}, {name:"num_30",bet:0.00,numbers:[30],color:"red"}, {name:"num_31",bet:0.00,numbers:[31],color:"black"}, {name:"num_32",bet:0.00,numbers:[32],color:"red"}, {name:"num_33",bet:0.00,numbers:[33],color:"black"}, {name:"num_34",bet:0.00,numbers:[34],color:"red"}, {name:"num_35",bet:0.00,numbers:[35],color:"black"},
            {name:"num_36",bet:0.00,numbers:[36],color:"red"},

            {name:"1st 12", bet:0.00,numbers:[1,2,3,4,5,6,7,8,9,10,11,12]},
            {name:"2nd 12", bet:0.00,numbers:[13,14,15,16,17,18,19,20,21,22,23,24]},
            {name:"3rd 12", bet:0.00,numbers:[25,26,27,28,29,30,31,32,33,34,35,36]},
            {name:"EVEN",   bet:0.00,numbers:[2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36]},
            {name:"red",    bet:0.00,numbers:[32,19,21,25,34,27,36,30,23,5,16,1,14,9,18,7,12,3]},
            {name:"black",  bet:0.00,numbers:[26,15,4,2,17,6,13,11,8,10,24,33,20,31,22,29,28,35]},
            {name:"ODD",    bet:0.00,numbers:[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]},
            {name:"19 - 36",bet:0.00,numbers:[19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]},
            {name:"1 - 18", bet:0.00,numbers:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]},

            {name:"c1", bet:0.00,numbers:[3,6,9,12,15,18,21,24,27,30,33,36]},
            {name:"c2", bet:0.00,numbers:[2,5,8,11,14,17,20,23,26,29,32,35]},
            {name:"c3", bet:0.00,numbers:[1,4,7,10,13,16,19,22,25,28,31,34]},

            {name:"split0_1", bet:0.00,numbers:[0,1]},
            {name:"split0_2", bet:0.00,numbers:[0,2]},
            {name:"split0_3", bet:0.00,numbers:[0,3]},
            {name:"split0_4", bet:0.00,numbers:[0,1,2]},
            {name:"split0_5", bet:0.00,numbers:[0,2,3]},

            {name:"split2H_1",   bet:0.00,numbers:[1,4]},
            {name:"split2H_2",   bet:0.00,numbers:[2,5]},
            {name:"split2H_3",   bet:0.00,numbers:[3,6]},
            {name:"split2H_4",   bet:0.00,numbers:[4,7]},
            {name:"split2H_5",   bet:0.00,numbers:[5,8]},
            {name:"split2H_6",   bet:0.00,numbers:[6,9]},
            {name:"split2H_7",   bet:0.00,numbers:[7,10]},
            {name:"split2H_8",   bet:0.00,numbers:[8,11]},
            {name:"split2H_9",   bet:0.00,numbers:[9,12]},
            {name:"split2H_10",  bet:0.00,numbers:[10,13]},
            {name:"split2H_11",  bet:0.00,numbers:[11,14]},
            {name:"split2H_12",  bet:0.00,numbers:[12,15]},
            {name:"split2H_13",  bet:0.00,numbers:[13,16]},
            {name:"split2H_14",  bet:0.00,numbers:[14,17]},
            {name:"split2H_15",  bet:0.00,numbers:[15,18]},
            {name:"split2H_16",  bet:0.00,numbers:[16,19]},
            {name:"split2H_17",  bet:0.00,numbers:[17,20]},
            {name:"split2H_18",  bet:0.00,numbers:[18,21]},
            {name:"split2H_19",  bet:0.00,numbers:[19,22]},
            {name:"split2H_20",  bet:0.00,numbers:[20,23]},
            {name:"split2H_21",  bet:0.00,numbers:[21,24]},
            {name:"split2H_22",  bet:0.00,numbers:[22,25]},
            {name:"split2H_23",  bet:0.00,numbers:[23,26]},
            {name:"split2H_24",  bet:0.00,numbers:[24,27]},
            {name:"split2H_25",  bet:0.00,numbers:[25,28]},
            {name:"split2H_26",  bet:0.00,numbers:[26,29]},
            {name:"split2H_27",  bet:0.00,numbers:[27,30]},
            {name:"split2H_28",  bet:0.00,numbers:[28,31]},
            {name:"split2H_29",  bet:0.00,numbers:[29,32]},
            {name:"split2H_30",  bet:0.00,numbers:[30,33]},
            {name:"split2H_31",  bet:0.00,numbers:[31,34]},
            {name:"split2H_32",  bet:0.00,numbers:[32,35]},
            {name:"split2H_33",  bet:0.00,numbers:[33,36]},

            {name:"split2V_1",  bet:0.00,numbers:[1,2]},
            {name:"split2V_2",  bet:0.00,numbers:[2,3]},
            {name:"split2V_4",  bet:0.00,numbers:[4,5]},
            {name:"split2V_5",  bet:0.00,numbers:[5,6]},
            {name:"split2V_8",  bet:0.00,numbers:[8,9]},
            {name:"split2V_7",  bet:0.00,numbers:[7,8]},
            {name:"split2V_11", bet:0.00,numbers:[11,12]},
            {name:"split2V_10", bet:0.00,numbers:[10,11]},
            {name:"split2V_13", bet:0.00,numbers:[13,14]},
            {name:"split2V_14", bet:0.00,numbers:[14,15]},
            {name:"split2V_17", bet:0.00,numbers:[17,18]},
            {name:"split2V_16", bet:0.00,numbers:[16,17]},
            {name:"split2V_19", bet:0.00,numbers:[19,20]},
            {name:"split2V_20", bet:0.00,numbers:[20,21]},
            {name:"split2V_22", bet:0.00,numbers:[22,23]},
            {name:"split2V_23", bet:0.00,numbers:[23,24]},
            {name:"split2V_25", bet:0.00,numbers:[25,26]},
            {name:"split2V_26", bet:0.00,numbers:[26,27]},
            {name:"split2V_28", bet:0.00,numbers:[28,29]},
            {name:"split2V_29", bet:0.00,numbers:[29,30]},
            {name:"split2V_31", bet:0.00,numbers:[31,32]},
            {name:"split2V_32", bet:0.00,numbers:[32,33]},
            {name:"split2V_34", bet:0.00,numbers:[34,35]},
            {name:"split2V_35", bet:0.00,numbers:[35,36]},

            {name:"split4_1",  bet:0.00,numbers:[1,2,4,5]},
            {name:"split4_2",  bet:0.00,numbers:[2,3,5,6]},
            {name:"split4_4",  bet:0.00,numbers:[4,5,7,8]},
            {name:"split4_5",  bet:0.00,numbers:[5,6,8,9]},
            {name:"split4_7",  bet:0.00,numbers:[7,8,10,11]},
            {name:"split4_8",  bet:0.00,numbers:[8,9,11,12]},
            {name:"split4_10", bet:0.00,numbers:[10,11,13,14]},
            {name:"split4_11", bet:0.00,numbers:[11,12,14,15]},
            {name:"split4_13", bet:0.00,numbers:[13,14,16,17]},
            {name:"split4_14", bet:0.00,numbers:[14,15,17,18]},
            {name:"split4_16", bet:0.00,numbers:[16,17,19,20]},
            {name:"split4_17", bet:0.00,numbers:[17,18,20,21]},
            {name:"split4_19", bet:0.00,numbers:[19,20,22,23]},
            {name:"split4_20", bet:0.00,numbers:[20,21,23,24]},
            {name:"split4_22", bet:0.00,numbers:[22,23,25,26]},
            {name:"split4_23", bet:0.00,numbers:[23,24,26,27]},
            {name:"split4_25", bet:0.00,numbers:[25,26,28,29]},
            {name:"split4_26", bet:0.00,numbers:[26,27,29,30]},
            {name:"split4_28", bet:0.00,numbers:[28,29,31,32]},
            {name:"split4_29", bet:0.00,numbers:[29,30,32,33]},
            {name:"split4_31", bet:0.00,numbers:[31,32,34,35]},
            {name:"split4_32", bet:0.00,numbers:[32,33,35,36]},

            //0-spiel:
            {name:"jeuZ",      bet:0.00,numbers:[0,3,12,15,26,32,35],special:["split0_3", "split2H_12", "split2H_32", "num_26"]},
            //serie 0/2/3:
            {name:"voisins",   bet:0.00,numbers:[0,3,2,4,7,12,15,18,21,19,22,25,26,28,29,32,35],special:["split0_5", "split0_5", "split2H_4", "split2H_12", "split2H_18", "split2H_19", "split2H_32", "split4_25"]},
            //orphelins:
            {name:"orphelins", bet:0.00,numbers:[1,6,9,14,17,20,31,34],special:["num_1", "split2H_6", "split2H_14", "split2H_17", "split2H_31"]},
            //serie 5/8:
            {name:"tiers",     bet:0.00,numbers:[5,8,11,10,13,16,23,24,27,30,33,36],special:["split2H_5", "split2V_10", "split2H_13", "split2V_23", "split2H_27", "split2H_33"]},

            {name:"street_0",  bet:0.00,numbers:[1,2,3],special:["num_1", "num_2", "num_3"]},
            {name:"street_1",  bet:0.00,numbers:[4,5,6],special:["num_4", "num_5", "num_6"]},
            {name:"street_2",  bet:0.00,numbers:[7,8,9],special:["num_7", "num_8", "num_9"]},
            {name:"street_3",  bet:0.00,numbers:[10,11,12],special:["num_10", "num_11", "num_12"]},
            {name:"street_4",  bet:0.00,numbers:[13,14,15],special:["num_13", "num_14", "num_15"]},
            {name:"street_5",  bet:0.00,numbers:[16,17,18],special:["num_16", "num_17", "num_18"]},
            {name:"street_6",  bet:0.00,numbers:[19,20,21],special:["num_19", "num_20", "num_21"]},
            {name:"street_7",  bet:0.00,numbers:[22,23,24],special:["num_22", "num_23", "num_24"]},
            {name:"street_8",  bet:0.00,numbers:[25,26,27],special:["num_25", "num_26", "num_27"]},
            {name:"street_9",  bet:0.00,numbers:[28,29,30],special:["num_28", "num_29", "num_30"]},
            {name:"street_10", bet:0.00,numbers:[31,32,33],special:["num_31", "num_32", "num_33"]},
            {name:"street_11", bet:0.00,numbers:[34,35,36],special:["num_34", "num_35", "num_36"]},

            {name:"line_0",  bet:0.00,numbers:[1,2,3,4,5,6],special:["num_1", "num_2", "num_3","num_4", "num_5", "num_6"]},
            {name:"line_1",  bet:0.00,numbers:[4,5,6,7,8,9],special:["num_4", "num_5", "num_6","num_7", "num_8", "num_9"]},
            {name:"line_2",  bet:0.00,numbers:[7,8,9,10,11,12],special:["num_7", "num_8", "num_9","num_10", "num_11", "num_12"]},
            {name:"line_3",  bet:0.00,numbers:[10,11,12,13,14,15],special:["num_10", "num_11", "num_12","num_13", "num_14", "num_15"]},
            {name:"line_4",  bet:0.00,numbers:[13,14,15,16,17,18],special:["num_13", "num_14", "num_15","num_16", "num_17", "num_18"]},
            {name:"line_5",  bet:0.00,numbers:[16,17,18,19,20,21],special:["num_16", "num_17", "num_18","num_19", "num_20", "num_21"]},
            {name:"line_6",  bet:0.00,numbers:[19,20,21,22,23,24],special:["num_19", "num_20", "num_21","num_22", "num_23", "num_24"]},
            {name:"line_7",  bet:0.00,numbers:[22,23,24,25,26,27],special:["num_22", "num_23", "num_24","num_25", "num_26", "num_27"]},
            {name:"line_8",  bet:0.00,numbers:[25,26,27,28,29,30],special:["num_25", "num_26", "num_27","num_28", "num_29", "num_30"]},
            {name:"line_9", bet:0.00,numbers:[28,29,30,31,32,33],special:["num_28", "num_29", "num_30","num_31", "num_32", "num_33"]},
            {name:"line_10", bet:0.00,numbers:[31,32,33,34,35,36],special:["num_31", "num_32", "num_33","num_34", "num_35", "num_36"]},

            // finals
            {name:"finals_0",  bet:0.00,numbers:[0,10,20,30],special:["num_0", "num_10", "num_20","num_30"]},
            {name:"finals_1",  bet:0.00,numbers:[1,11,21,31],special:["num_1", "num_11", "num_21","num_31"]},
            {name:"finals_2",  bet:0.00,numbers:[2,12,22,32],special:["num_2", "num_12", "num_22","num_32"]},
            {name:"finals_3",  bet:0.00,numbers:[3,13,23,33],special:["num_3", "num_13", "num_23","num_33"]},
            {name:"finals_4",  bet:0.00,numbers:[4,14,24,34],special:["num_4", "num_14", "num_24","num_34"]},
            {name:"finals_5",  bet:0.00,numbers:[5,15,25,35],special:["num_5", "num_15", "num_25","num_35"]},
            {name:"finals_6",  bet:0.00,numbers:[6,16,26,36],special:["num_6", "num_16", "num_26","num_36"]},
            {name:"finals_7",  bet:0.00,numbers:[7,17,27],special:["num_7", "num_17", "num_27"]},
            {name:"finals_8",  bet:0.00,numbers:[8,18,28],special:["num_8", "num_18", "num_28"]},
            {name:"finals_9",  bet:0.00,numbers:[9,19,29],special:["num_9", "num_19", "num_29"]},

            // 4 neighbours:
            {name:"neighbours_0",  bet:0.00,numbers:[3,26,0,32,15],special:["num_3", "num_26", "num_0", "num_32", "num_15"]},
            {name:"neighbours_1",  bet:0.00,numbers:[16,33,1,20,14],special:["num_16", "num_33", "num_1", "num_20", "num_14"]},
            {name:"neighbours_2",  bet:0.00,numbers:[4,21,2,25,17],special:["num_4", "num_21", "num_2", "num_25", "num_17"]},
            {name:"neighbours_3",  bet:0.00,numbers:[12,35,3,26,0],special:["num_12", "num_35", "num_3", "num_26", "num_0"]},
            {name:"neighbours_4",  bet:0.00,numbers:[15,19,4,21,2],special:["num_15", "num_19", "num_4", "num_21", "num_2"]},
            {name:"neighbours_5",  bet:0.00,numbers:[23,10,5,24,16],special:["num_23", "num_10", "num_5", "num_24", "num_16"]},
            {name:"neighbours_6",  bet:0.00,numbers:[17,34,6,27,13],special:["num_17", "num_34", "num_6", "num_27", "num_13"]},
            {name:"neighbours_7",  bet:0.00,numbers:[18,29,7,28,12],special:["num_18", "num_29", "num_7", "num_28", "num_12"]},
            {name:"neighbours_8",  bet:0.00,numbers:[11,30,8,23,10],special:["num_11", "num_30", "num_8", "num_23", "num_10"]},
            {name:"neighbours_9",  bet:0.00,numbers:[14,31,9,22,18],special:["num_14", "num_31", "num_9", "num_22", "num_18"]},
            {name:"neighbours_10", bet:0.00,numbers:[8,23,10,5,24],special:["num_8", "num_23", "num_10", "num_5", "num_24"]},
            {name:"neighbours_11", bet:0.00,numbers:[13,36,11,30,8],special:["num_13", "num_36", "num_11", "num_30", "num_8"]},
            {name:"neighbours_12", bet:0.00,numbers:[7,28,12,35,3],special:["num_7", "num_28", "num_12", "num_35", "num_3"]},
            {name:"neighbours_13", bet:0.00,numbers:[6,27,13,36,11],special:["num_6", "num_27", "num_13", "num_36", "num_11"]},
            {name:"neighbours_14", bet:0.00,numbers:[1,20,14,31,9],special:["num_1", "num_20", "num_14", "num_31", "num_9"]},
            {name:"neighbours_15", bet:0.00,numbers:[0,32,15,19,4],special:["num_0", "num_32", "num_15", "num_19", "num_4"]},
            {name:"neighbours_16", bet:0.00,numbers:[5,24,16,33,1],special:["num_5", "num_24", "num_16", "num_33", "num_1"]},
            {name:"neighbours_17", bet:0.00,numbers:[2,25,17,34,6],special:["num_2", "num_25", "num_17", "num_34", "num_6"]},
            {name:"neighbours_18", bet:0.00,numbers:[9,22,18,29,7],special:["num_9", "num_22", "num_18", "num_29", "num_7"]},
            {name:"neighbours_19", bet:0.00,numbers:[32,15,19,4,21],special:["num_32", "num_15", "num_19", "num_4", "num_21"]},
            {name:"neighbours_20", bet:0.00,numbers:[33,1,20,14,31],special:["num_33", "num_1", "num_20", "num_14", "num_31"]},
            {name:"neighbours_21", bet:0.00,numbers:[19,4,21,2,25],special:["num_19", "num_4", "num_21", "num_2", "num_25"]},
            {name:"neighbours_22", bet:0.00,numbers:[31,9,22,18,29],special:["num_31", "num_9", "num_22", "num_18", "num_29"]},
            {name:"neighbours_23", bet:0.00,numbers:[30,8,23,10,5],special:["num_30", "num_8", "num_23", "num_10", "num_5"]},
            {name:"neighbours_24", bet:0.00,numbers:[10,5,24,16,33],special:["num_10", "num_5", "num_24", "num_16", "num_33"]},
            {name:"neighbours_25", bet:0.00,numbers:[21,2,25,17,34],special:["num_21", "num_2", "num_25", "num_17", "num_34"]},
            {name:"neighbours_26", bet:0.00,numbers:[35,3,26,0,32],special:["num_35", "num_3", "num_26", "num_0", "num_32"]},
            {name:"neighbours_27", bet:0.00,numbers:[34,6,27,13,36],special:["num_34", "num_6", "num_27", "num_13", "num_36"]},
            {name:"neighbours_28", bet:0.00,numbers:[29,7,28,12,35],special:["num_29", "num_7", "num_28", "num_12", "num_35"]},
            {name:"neighbours_29", bet:0.00,numbers:[22,18,29,7,28],special:["num_22", "num_18", "num_29", "num_7", "num_28"]},
            {name:"neighbours_30", bet:0.00,numbers:[36,11,30,8,23],special:["num_36", "num_11", "num_30", "num_8", "num_23"]},
            {name:"neighbours_31", bet:0.00,numbers:[20,14,31,9,22],special:["num_20", "num_14", "num_31", "num_9", "num_22"]},
            {name:"neighbours_32", bet:0.00,numbers:[26,0,32,15,19],special:["num_26", "num_0", "num_32", "num_15", "num_19"]},
            {name:"neighbours_33", bet:0.00,numbers:[24,16,33,1,20],special:["num_24", "num_16", "num_33", "num_1", "num_20"]},
            {name:"neighbours_34", bet:0.00,numbers:[25,17,34,6,27],special:["num_25", "num_17", "num_34", "num_6", "num_27"]},
            {name:"neighbours_35", bet:0.00,numbers:[28,12,35,3,26],special:["num_28", "num_12", "num_35", "num_3", "num_26"]},
            {name:"neighbours_36", bet:0.00,numbers:[27,13,36,11,30],special:["num_27", "num_13", "num_36", "num_11", "num_30"]}
        ];

    alteastream.TableMatrix = TableMatrix;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PreloaderComponentMachine = function(stage, canv) {
        this.initialize(stage, canv);
    }
    var p = PreloaderComponentMachine.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage, canv) {
        this.PreloaderComponent_initialize(stage);

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRect(0, 0, canv.width, canv.height);
        shape.cache(0, 0, canv.width, canv.height);
        this.addChild(shape);

        //var that = this;
        /* this.addOnProgressChangeAction(function() {
             image.alpha = that.getProgress();
        });*/
    };

// static methods:
// public methods:
// private methods:
    p._getSaturation = function() {
        return (this.getProgress() - 1) * 100;
    };

    alteastream.PreloaderComponentMachine = PreloaderComponentMachine;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var RouletteAssets = function() {};
    var Assets = alteastream.Assets;

    RouletteAssets.videoManifest = [
        'assets/sprite_sheets/drum.mp4'
    ];

    RouletteAssets.count = 0;

    RouletteAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        var language = Assets.getImageLang();
        var localePath = "assets/images/locale/" + language + "/";
        return [
            // Images
            //{id:Assets.images.preloadingPoster, src:"assets/images/webrtc.png"},
            {id:Assets.images.mainBackgroundWithHole, src:"assets/images/cbMainBgWithHole.png"},
            //{id:Assets.images.backgroundCB, src:"assets/images/control_board_bg.jpg"},
            //{id:Assets.images.btnMockSS, src:"assets/images/buttons/btnMockSS.png"},
            {id:Assets.images.fieldSpec, src:"assets/images/buttons/fieldSpec.png"},
            {id:Assets.images.fieldSpecH, src:"assets/images/buttons/fieldSpecH.png"},
            {id:Assets.images.fieldMini, src:"assets/images/buttons/fieldMini.png"},
            {id:Assets.images.fieldMiniH, src:"assets/images/buttons/fieldMiniH.png"},
            {id:Assets.images.fieldSmall, src:"assets/images/buttons/fieldSmall.png"},
            {id:Assets.images.fieldSmall_red, src:"assets/images/buttons/fieldSmall_red.png"},
            {id:Assets.images.fieldSmall_black, src:"assets/images/buttons/fieldSmall_black.png"},
            {id:Assets.images.fieldSmall_green, src:"assets/images/buttons/fieldSmall_green.png"},
            {id:Assets.images.fieldMedium, src:"assets/images/buttons/fieldMedium.png"},
            {id:Assets.images.fieldLarge, src:"assets/images/buttons/fieldLarge.png"},
            {id:Assets.images.fieldZero, src:"assets/images/buttons/fieldZero.png"},
            {id:Assets.images.fieldSplit, src:"assets/images/buttons/fieldSplit.png"},
            {id:Assets.images.fieldSplitTest, src:"assets/images/buttons/fieldSplitTest.png"},//temp dev visibility test only
            {id:Assets.images.fieldSmallH, src:"assets/images/buttons/fieldSmallH.png"},
            {id:Assets.images.fieldMediumH, src:"assets/images/buttons/fieldMediumH.png"},
            {id:Assets.images.fieldLargeH, src:"assets/images/buttons/fieldLargeH.png"},
            {id:Assets.images.fieldZeroH, src:"assets/images/buttons/fieldZeroH.png"},
            {id:Assets.images.label_red, src:"assets/images/buttons/label_red.png"},
            {id:Assets.images.label_black, src:"assets/images/buttons/label_black.png"},
            {id:Assets.images.fieldTrack, src:"assets/images/buttons/fieldTrack.png"},
            {id:Assets.images.fieldTrack_black, src:"assets/images/buttons/fieldTrack_black.png"},
            {id:Assets.images.fieldTrack_red, src:"assets/images/buttons/fieldTrack_red.png"},
            {id:Assets.images.fieldTrack_green, src:"assets/images/buttons/fieldTrack_green.png"},
            {id:Assets.images.fieldTrackH, src:"assets/images/buttons/fieldTrackH.png"},
            {id:Assets.images.winningBg_red, src:"assets/images/buttons/winningBg_red.png"},
            {id:Assets.images.winningBg_black, src:"assets/images/buttons/winningBg_black.png"},
            {id:Assets.images.winningBg_green, src:"assets/images/buttons/winningBg_green.png"},
            {id:Assets.images.btnFullScreen_On,src:"assets/images/buttons/btnFullScreen_On.png"},
            {id:Assets.images.btnFullScreen_Off,src:"assets/images/buttons/btnFullScreen_Off.png"},

            {id:Assets.images.spinner, src:"assets/images/spinner.png"},
            {id:Assets.images.liveStreamConnecting, src:"assets/images/liveStreamConnecting.png"},
            //{id:Assets.images.btnIncSS, src:"assets/images/buttons/count_btn_plus.png"},
            //{id:Assets.images.btnDecSS, src:"assets/images/buttons/count_btn_minus.png"},
           // {id:Assets.images.btnMediumSS, src:"assets/images/buttons/middle_btn.png"},
            //{id:Assets.images.btnArrowLeftSS, src:"assets/images/buttons/left_btn.png"},
            //{id:Assets.images.btnArrowRightSS, src:"assets/images/buttons/right_btn.png"},
            //{id:Assets.images.btnMedium2SS, src:"assets/images/buttons/yello_btn.png"},

            //btns
            {id:Assets.images.btnQuitSS,src:"assets/images/buttons/quitSS.png"},
            {id:Assets.images.btnInfoSS,src:"assets/images/buttons/infoSS.png"},
            {id:Assets.images.btnStepBackSS,src:"assets/images/buttons/backBtn.png"},
            {id:Assets.images.btnClearSS,src:"assets/images/buttons/clearBtn.png"},
            {id:Assets.images.soundOn,src:"assets/images/buttons/sound_icon_default.png"},
            {id:Assets.images.soundOff,src:"assets/images/buttons/sound_icon_disabled.png"},
            {id:Assets.images.btnStart,src:"assets/images/buttons/start_btn.png"},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"},

            {id:Assets.images.chipBtn0,src:"assets/images/buttons/chipBtn0.png"},
            {id:Assets.images.chipBtn1,src:"assets/images/buttons/chipBtn1.png"},
            {id:Assets.images.chipBtn2,src:"assets/images/buttons/chipBtn2.png"},
            {id:Assets.images.chipBtn3,src:"assets/images/buttons/chipBtn3.png"},
            {id:Assets.images.chipBtn4,src:"assets/images/buttons/chipBtn4.png"},
            {id:Assets.images.chipBtn5,src:"assets/images/buttons/chipBtn5.png"},
            {id:Assets.images.chipBtn6,src:"assets/images/buttons/chipBtn6.png"},
            {id:Assets.images.chipBtn7,src:"assets/images/buttons/chipBtn7.png"},
           // {id:Assets.images.chipBlank,src:"assets/images/buttons/chipBlank.png"},

            {id:Assets.images.chip0,src:"assets/images/buttons/chip0.png"},
            {id:Assets.images.chip1,src:"assets/images/buttons/chip1.png"},
            {id:Assets.images.chip2,src:"assets/images/buttons/chip2.png"},
            {id:Assets.images.chip3,src:"assets/images/buttons/chip3.png"},
            {id:Assets.images.chip4,src:"assets/images/buttons/chip4.png"},
            {id:Assets.images.chip5,src:"assets/images/buttons/chip5.png"},
            {id:Assets.images.chip6,src:"assets/images/buttons/chip6.png"},
            {id:Assets.images.chip7,src:"assets/images/buttons/chip7.png"},


            //{id:Assets.images.singleShotTextImg,src:"assets/images/buttons/single.png"},
            //{id:Assets.images.burstShotTextImg,src:"assets/images/buttons/burst.png"},


           // {id:Assets.sounds.shooter,src:"assets/sounds/shooter.ogg"},
            //{id:Assets.sounds.shooter2,src:"assets/sounds/shooter2.ogg"},
            //{id:Assets.sounds.shooterStop,src:"assets/sounds/shootStop.ogg"},
           // {id:Assets.sounds.btnUp,src:"assets/sounds/btnUp.ogg"},
           // {id:Assets.sounds.btnDown,src:"assets/sounds/btnUp.ogg"},
           // {id:Assets.sounds.btnDownDouble,src:"assets/sounds/btnDownDouble.ogg"},
            //{id:Assets.sounds.bgMusicMachine,src:"assets/sounds/bgMusicMachine.ogg"},
            {id:Assets.sounds.error1,src:"assets/sounds/error1.ogg"},
            //{id:Assets.sounds.bgMusic, src:"assets/sounds/background_music.mp3"},
            {id:Assets.sounds.buttonClick, src:"assets/sounds/buttonClick.mp3"},
            {id:Assets.sounds.winSound, src:"assets/sounds/winSound.mp3"},
            {id:Assets.sounds.winCheer, src:"assets/sounds/winCheer.mp3"},
            {id:Assets.sounds.buttonCollect, src:"assets/sounds/buttonCollect.mp3"},
            {id:Assets.sounds.countLoop, src:"assets/sounds/countLoop.mp3"},
            {id:Assets.sounds.chipDown, src:"assets/sounds/chipDown.mp3"},

            {id:Assets.sounds.spoken_0, src:"assets/sounds/0.mp3"},
            {id:Assets.sounds.spoken_1, src:"assets/sounds/1.mp3"},
            {id:Assets.sounds.spoken_2, src:"assets/sounds/2.mp3"},
            {id:Assets.sounds.spoken_3, src:"assets/sounds/3.mp3"},
            {id:Assets.sounds.spoken_4, src:"assets/sounds/4.mp3"},
            {id:Assets.sounds.spoken_5, src:"assets/sounds/5.mp3"},
            {id:Assets.sounds.spoken_6, src:"assets/sounds/6.mp3"},
            {id:Assets.sounds.spoken_7, src:"assets/sounds/7.mp3"},
            {id:Assets.sounds.spoken_8, src:"assets/sounds/8.mp3"},
            {id:Assets.sounds.spoken_9, src:"assets/sounds/9.mp3"},
            {id:Assets.sounds.spoken_10, src:"assets/sounds/10.mp3"},
            {id:Assets.sounds.spoken_11, src:"assets/sounds/11.mp3"},
            {id:Assets.sounds.spoken_12, src:"assets/sounds/12.mp3"},
            {id:Assets.sounds.spoken_13, src:"assets/sounds/13.mp3"},
            {id:Assets.sounds.spoken_14, src:"assets/sounds/14.mp3"},
            {id:Assets.sounds.spoken_15, src:"assets/sounds/15.mp3"},
            {id:Assets.sounds.spoken_16, src:"assets/sounds/16.mp3"},
            {id:Assets.sounds.spoken_17, src:"assets/sounds/17.mp3"},
            {id:Assets.sounds.spoken_18, src:"assets/sounds/18.mp3"},
            {id:Assets.sounds.spoken_19, src:"assets/sounds/19.mp3"},
            {id:Assets.sounds.spoken_20, src:"assets/sounds/20.mp3"},
            {id:Assets.sounds.spoken_21, src:"assets/sounds/21.mp3"},
            {id:Assets.sounds.spoken_22, src:"assets/sounds/22.mp3"},
            {id:Assets.sounds.spoken_23, src:"assets/sounds/23.mp3"},
            {id:Assets.sounds.spoken_24, src:"assets/sounds/24.mp3"},
            {id:Assets.sounds.spoken_25, src:"assets/sounds/25.mp3"},
            {id:Assets.sounds.spoken_26, src:"assets/sounds/26.mp3"},
            {id:Assets.sounds.spoken_27, src:"assets/sounds/27.mp3"},
            {id:Assets.sounds.spoken_28, src:"assets/sounds/28.mp3"},
            {id:Assets.sounds.spoken_29, src:"assets/sounds/29.mp3"},
            {id:Assets.sounds.spoken_30, src:"assets/sounds/30.mp3"},
            {id:Assets.sounds.spoken_31, src:"assets/sounds/31.mp3"},
            {id:Assets.sounds.spoken_32, src:"assets/sounds/32.mp3"},
            {id:Assets.sounds.spoken_33, src:"assets/sounds/33.mp3"},
            {id:Assets.sounds.spoken_34, src:"assets/sounds/34.mp3"},
            {id:Assets.sounds.spoken_35, src:"assets/sounds/35.mp3"},
            {id:Assets.sounds.spoken_36, src:"assets/sounds/36.mp3"},

            {id:Assets.sounds.spoken_welcome, src:"assets/sounds/welcome.mp3"},
            {id:Assets.sounds.spoken_youWin_1, src:"assets/sounds/youWin_1.mp3"},
            {id:Assets.sounds.spoken_youWin_2, src:"assets/sounds/youWin_2.mp3"},
            {id:Assets.sounds.spoken_youWin_3, src:"assets/sounds/youWin_3.mp3"},
            {id:Assets.sounds.spoken_youWin_4, src:"assets/sounds/youWin_4.mp3"},
            {id:Assets.sounds.spoken_noWin_1, src:"assets/sounds/noWin_1.mp3"},
            {id:Assets.sounds.spoken_noWin_2, src:"assets/sounds/noWin_2.mp3"},
            {id:Assets.sounds.spoken_placeBet, src:"assets/sounds/placeBet.mp3"},
            {id:Assets.sounds.spoken_placeBet_2, src:"assets/sounds/placeBet_2.mp3"},
            {id:Assets.sounds.spoken_doSpin_1, src:"assets/sounds/doSpin_1.mp3"},
            {id:Assets.sounds.spoken_doSpin_2, src:"assets/sounds/doSpin_2.mp3"},
            {id:Assets.sounds.spoken_doSpin_3, src:"assets/sounds/doSpin_3.mp3"},
            {id:Assets.sounds.spoken_maxReached, src:"assets/sounds/maxReached.mp3"},
            {id:Assets.sounds.spoken_clearedBets, src:"assets/sounds/clearBets.mp3"},
            //{id:Assets.sounds.spoken_last5, src:"assets/sounds/last5Drawn.mp3"},
            {id:Assets.sounds.spoken_winningNumber_1, src:"assets/sounds/winningNumber_1.mp3"},
            {id:Assets.sounds.spoken_winningNumber_2, src:"assets/sounds/winningNumber_2.mp3"}

            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"},
             */

            //images big
            //{id:Assets.images.mainBackground,src:"assets/images/mainBackground.jpg"},//1

            //sounds
        ];
    };

    RouletteAssets.loadFontManifest = function(){
        return [
            //"css/BebasNeue-Regular.ttf",
            "css/RobotoCondensed-Bold.ttf",
            "css/HurmeGeometricSans3.otf"
        ];
    };


    RouletteAssets.atlasImage = {};
    //var aI = RouletteAssets.atlasImage = {};

    //aI[Assets.atlasImage.ss] = "ss";

    RouletteAssets.texts = {};
    var t = RouletteAssets.texts["en"] = {};
    t[Assets.texts.stake] = "BET";
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.crd] = "Cash:";
    t[Assets.texts.bet] = "Bet:";

    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";


    t = RouletteAssets.texts["sr"] = {};

    RouletteAssets.loadVideo = function(show,progress){
        show();

    };

    alteastream.RouletteAssets = RouletteAssets;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Roulette = function() {
        this.AbstractScene_constructor();
        this.initialize_Roulette();
    };
    var p = createjs.extend(Roulette, alteastream.AbstractScene);

// static properties:
    //statuses::
    Roulette.START = 1;
    Roulette.PLACE_BET = 2;
    Roulette.BALL_IN_RIM = 3;
    Roulette.NO_MORE_BETS = 4;
    Roulette.WINNING_NUMBER = 5;
    Roulette.TABLE_CLOSED = 6;
    Roulette.DEALER_LOCK = 7;
// events:
// public properties:
// private properties:

    var _this = null;
    p.active = false;
    p.okToShowBalance = true;
    p._gameResult = {};

// constructor:
    p.initialize_Roulette = function() {
        _this = this;
        this._local_activate();// local ver
        this._callInitMethods();

        console.log("initialize_Roulette..");
    };

    Roulette.getInstance = function(){
        return _this;
    };

    p._callInitMethods = function() {
        var machineParams = window.localStorage.getItem('machineParameters');
        this.machineParameters = JSON.parse(machineParams);

        // todo until backend sets this 4 parameters
        /*if(!this.machineParameters.chipValues){this.machineParameters.chipValues = [1000,500,100,50,25,10,5,1];}
        if(!this.machineParameters.playedNumbers){this.machineParameters.playedNumbers = [19,33,16,5,0,6,24,31,9,7];}
        if(!this.machineParameters.minStake){this.machineParameters.minStake = 0.20;}
        if(!this.machineParameters.maxStake){this.machineParameters.maxStake = 5.00;}*/

        this.setHttpCommunication();

        this.setSocketCommunication("Roulette");
        this.socketCommunicator.activate(this.machineParameters);

        //this._addInfo();
        this.addGiveUpButton(true);//giveUpButton
        //this.monitorNetworkStatus();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
    };

    //new socket
    p.onSocketConnect = function(frame){ // invalid token fix new
        console.log("onSocketConnect::::::::::::::::::: ");
        _this.socketCommunicator.onGameConnect(frame);

        var machineParams = _this.machineParameters;

        _this._setControlBoard(machineParams);
        _this._setBetTable(machineParams.chipValues);
        _this._setWinningNumberApplet();

        _this._setStreamVideo(machineParams);// new stream load comment
        _this._setStreamVideoSmall(machineParams);
        localStorage.removeItem("streamPreviewParams");// stream preview roulette

        _this.setGameParameters(machineParams);
        //_this.setPayout(machineParams.payout);//NE RULET
        //new stakes
        //_this.setCurrency();
        //_this.setMonetaryValueView(_this.controlBoard._toValueView);//NE RULET
        //_this.checkStreamLoad();// new stream load uncomment

        // new stream load comment block start
        _this.streamVideoSmall.activate(function(){});
        _this.streamVideo.activate(function(){
            _this.socketCommunicator.confirmGameInit();
            _this.streamVideoSmall.appendSpinner();
        });
        // new stream load comment block end

        //_this._setInfo();
        _this.showGame(false);
        _this.setRenderer();

        window.localStorage.removeItem('machineParameters');
    };

    p._setStreamVideo = function(address){
        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"50%",height:"50%",marginLeft:"25%"}};
        this.streamVideo = new alteastream.RouletteTableStream(this,options,address);
        //this.streamVideo = new alteastream.VideoStream(this,"videoOutput",address);//old
    };

    p._setStreamVideoSmall = function(address){
        var options = {id:"videoOutputSmall",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideoSmall = new alteastream.RouletteWheelStream(this,options,address);
        //this.streamVideoSmall = new alteastream.VideoStreamSmall(this,"videoOutputSmall",address);//old
    };

    p._setControlBoard = function(params){
        console.log("_setControlBoard::::::::::::::::::: ");
        this.controlBoard = new alteastream.RouletteControlBoard(params);
        this.controlBoard.plugin(this);
        this.controlBoard.initialSetup();
        this.addChild(this.controlBoard);
        this.controlBoard.visible = false;
    };

    p._setBetTable = function(chipValues){
        console.log("_setBetTable::::::::::::::::::: ");
        this.betTable = new alteastream.BetTable(this,alteastream.TableMatrix.FRENCH,chipValues);
        this.addChild(this.betTable);
        this.betTable.visible = false;
        //this.betTable.y=730;
        //this.betTable.x=85;
    };

    p._setWinningNumberApplet = function(){
        this.winningNumberApplet = new alteastream.WinningNumberApplet();
        this.addChild(this.winningNumberApplet);
        this.winningNumberApplet.x = alteastream.AbstractScene.GAME_WIDTH*0.5 - this.winningNumberApplet.getWidth()*0.5;
        this.winningNumberApplet.y = alteastream.AbstractScene.GAME_HEIGHT*0.5- this.winningNumberApplet.getHeight()*0.5-280;
        this.winningNumberApplet.mouseEnabled = false;
        this.winningNumberApplet.mouseChildren = false;
    };

    p.setGameParameters = function(response){
        console.log("setGameParameters:: ");
        console.log("balance:: "+response.balance);

       ///////////////////////////////
        this.setCrd(response.balance);

        console.log("COMM:: gameParameters");
        this.minStake = response.minStake;
        this.maxStake = response.maxStake || 20;

        this.controlIdleTime = response.controlIdleTime;
        this._kickOutPing = this.controlIdleTime/3;

        this.currency = response.currency || "N/A";
        //decimal
        //var code = 8364;//resp.user.currency
        //this.currency = String.fromCharCode( code);

        //hex, css
        //var code = '20AC';//20A4 resp.user.currency
        //this.currency = String.fromCharCode( parseInt(code, 16) );

        this.betTable.setStakeMinMax(this.minStake,this.maxStake);
        this.controlBoard.setCurrency(this.currency);
        this.controlBoard.setStakesInfo(this.minStake,this.maxStake);
        this.controlBoard.setLastPlayedNumbers(response.playedNumbers);

        this.betTable.setAvailableBets();
        this.betTable.setAvailableCoins();
        this.betTable.removeAllBets();

        this.disableAllBets(true);
    };

    p.showGame = function(bool){
        if(bool === true){
            this.addGiveUpButton(false);
            //this.controlBoard.soundBtnHandler();
            this.streamVideo.setMedia();

           /* if(is.safari()){
                var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                if(is.not.ipad() && is.not.ios() && !isiPadOS){
                    alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
                }
            }else{
                alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
            }*/
        }

        this.controlBoard.visible = bool;
        this.betTable.visible = bool;
    };

    p.disableAllBets = function(bool){
        if(bool === true){
            this.controlBoard.disableAllChips(true);
            this.betTable.disableBetting();
        }else{
            this.controlBoard.disableAllChips(false);
            this.betTable.enableBetting();
            //this.betTable.setAvailableBets();
            //this.betTable.setAvailableCoins();
            //this.betTable.removeAllBets();
        }
    }

    //treba neki StatusHandler class
    // inicirati tek posle CONFIRM_GAME_INIT
    p.statusChange = function(status){
        console.log("statusChange:: "+status);
        var assets = alteastream.Assets;
        switch (status) {
            case Roulette.START:
                //clear winnings
                //this.disableAllBets(true);
                this.disableAllBets(false);// temp insted of PLACE_BET not comming at this moment
                //this.betTable.onCollectedWin();// oncollect?
                this.betTable.resetAnimations();
                this.winningNumberApplet.show(false);
                this.controlBoard.showUI(true);
                this.controlBoard.updateStatus("PLACE YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet_2);
                break;
            case Roulette.PLACE_BET:
                //this.disableAllBets(false);// temp PLACE_BET not comming at this moment
                this.controlBoard.updateStatus("PLACE YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet);
                break;
            case Roulette.BALL_IN_RIM:
                this.onSpin();
                this.controlBoard.updateStatus("FINISH YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet);
                break;
            case Roulette.NO_MORE_BETS:
                this.disableAllBets(true);
                this.sendFinalBet();
                this.controlBoard.updateStatus("NO MORE BETS");
                assets.playSound(assets.sounds.spoken_doSpin_1);
                break;
            case Roulette.WINNING_NUMBER:
                this.controlBoard.updateStatus("WINNING NUMBER");
                break;
            case Roulette.TABLE_CLOSED:
                this.disableAllBets(true);
                this.controlBoard.updateStatus("TABLE CLOSED");
                break;
            case Roulette.DEALER_LOCK:
                this.disableAllBets(true);
                this.controlBoard.updateStatus("DEALER LOCK");
                break;
        }
    };

    p.onSpin = function(){
        console.log("onSpin ");
        //this.controlBoard.onSpin();
        //this.betTable.onSpin();
        //this.sendFinalBet();
    };

    p.onPrizeDetection = function(number){
        this.controlBoard.updateStatus("WINNING NUMBER"); //temp maybe no status sent
        this.setGameResult(number);
    };

    p.sendFinalBet = function(){
        console.log("COMM:: sendBet");
        this.okToShowBalance = false;

        var take = this.credit - this.betTable._currentBetAmount;
        this.controlBoard.setCrd(take);
        var finalUserBet = this.betTable.getFinalUserBet();

        //temp loop
        var betToSend = [];
        for(var i=0,j=finalUserBet.length;i<j;i++){
            betToSend.push(finalUserBet[i]);
            betToSend[i][1]*=100;
        }
        console.log("betToSend:::::::::::::::::: "+betToSend);
        /*// local ver
        alteastream.SocketCommunicatorRoulette.getInstance().sendBet(betToSend);
        */// local ver
    };

    p.setGameResult = function(response){
        console.log("COMM:: gameResult");
        var gameResult = this._gameResult;
        gameResult.winAmount = response.win;
        gameResult.newBalance = response.balance;
        gameResult.winNumber = response.drawnNo;
        gameResult.winColor = this.betTable.getBetField("num_"+gameResult.winNumber).color;

        this.betTable.hideSpecials(false);

        if(gameResult.winAmount>0.00){
            this.betTable.setWinningBets(gameResult.winNumber);
            this.winResult(gameResult);
        }else{
            this.noWinResult(gameResult);
        }

        this.winningNumberApplet.show(true);
        this.controlBoard.updateLastPlayedNumbers(gameResult.winNumber);
        this.okToShowBalance = true;
        this.setCrd(gameResult.newBalance);
        this._resultSpoken(gameResult.winAmount>0.00);
    };

    p.winResult = function(gameResult){
        console.log("winResult :: ");
        console.log("AMOUNT:: "+gameResult.winAmount);
        console.log("NUMBER:: "+gameResult.winNumber);
        console.log("COLOR:: "+gameResult.winColor);
        this.controlBoard.onSpinWin();
        this.winningNumberApplet.setResult(gameResult,true);

        this.betTable.highlightWin(gameResult.winNumber);
    };

    p.noWinResult = function(gameResult){
        console.log("noWinResult:: ");
        this.controlBoard.onSpinLost(gameResult);
        //after setSpinToBet
        //this.betTable.enableButtons(true);
        this.winningNumberApplet.setResult(gameResult,false);
        //this.betTable.setAvailableCoins();//LATEST
    };

    p._resultSpoken = function(win){
        var assets = alteastream.Assets;
        var spokenNumber = "spoken_"+this._gameResult.winNumber;
        assets.playSoundChain(spokenNumber,1,function(){
            var rnd;
            var phrase;
            if(win===true){
                rnd = gamecore.Utils.NUMBER.randomRange(1,4);
                phrase = "spoken_youWin_"+rnd;
                assets.playSound(phrase,1);

                assets.playSoundChain(phrase,1,function(){
                    assets.playSound("winSound",0.8);
                    assets.playSound("winCheer",0.7);
                });
            }else{
                rnd = gamecore.Utils.NUMBER.randomRange(1,2);
                phrase = "spoken_noWin_"+rnd;
                assets.playSound(phrase,1);
            }
        });
    };

    p.setCrd = function(amount){
        var crdAmountCents = Math.min(amount/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this.credit = crdAmountCents;
        if(this.okToShowBalance)
            this.controlBoard.setCrd(amount);
    };

    p.getGameResult = function(){
        return this._gameResult;
    };

    p.infoBtnHandler = function() {
        this.onInfo();
    };

    p.onInfo = function(){
       console.log("INFO");
    };

    p.quitBtnHandler = function(){
        _this._setQuitPanel();
    };

    p.quitConfirmed = function(){
        //if(_this.socketCommunicator)
        //_this.socketCommunicator.disposeCommunication();
        _this.disableAllBets(true);
        _this.onQuit();
    };

    p.onQuit = function(){
        //alteastream.VideoStreamPreload.stop();// new stream load uncomment
        //alteastream.VideoStreamPreload.removeVideoElement();// new stream load uncomment
        this.streamVideo.stop();// new stream load comment
        this.socketCommunicator.quitPlay();
    };

    //exitMachineGame::
    p.exitMachineGameplay = function(){
        this.killGame();
        this.closeGame();
    };

    //new socket
    p.closeGame = function(){
        window.top.manageBgMusic("pause");
        //window.localStorage.setItem('backgroundMusicIsMuted', String(_this.controlBoard.soundToggled));
        window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
    };

    p.addGiveUpButton = function(bool){
        if(bool === true){
            var giveUpButton = this.giveUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
            var width = giveUpButton.width;
            var spacing = width/2;
            giveUpButton.x = alteastream.AbstractScene.GAME_WIDTH - width - spacing;
            giveUpButton.y = 10;
            this.addChild(giveUpButton);
            giveUpButton.addEventListener("click", function () {
                _this.quitBtnHandler();
                //_this.streamVideo.stop();
                //_this.exitMachineGameplay();// nije otvorio soket, ne moze da dobije 85, token itd..mozda http poziv u ovom slucaju
            });
        }else{
            this.removeChild(this.giveUpButton);
            this.giveUpButton = null;
        }
    };

    p._local_activate = function() {
       /* this._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
              //return "yes";
              return "not";
        };*/
        this._callInitMethods = function () {};
        //var machineParams = "ws://87.130.112.6:8083/ws";

        this.setHttpCommunication();

        this.setSocketCommunication("Roulette");

        this.addGiveUpButton(true);
        this.giveUpButton.y+=50;

        this.machineParameters = {"state":0,"key":"79a2fb56-7c2c-46cf-97d5-35db5661425e","controlIdleTime":60000,"streamendpoint":"ws://87.130.112.6:8889","webrtcurl":"ws://87.130.112.6:8083/ws","chipValues":[1000,500,100,50,25,10,5,1],"playedNumbers":[19,33,16,5,0,6,24,31,9,7],"balance":999800,"minStake":0.20,"maxStake":5.00,"tokenValue":100.0,"tokens":0,"maxTokens":60,"currency":"RSD","payout":{"1":5,"2":10,"3":15,"4":45,"101":0.25},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus"};

        this._setControlBoard(this.machineParameters);
        this._setBetTable(this.machineParameters.chipValues);

        this._setStreamVideo(this.machineParameters.webrtcurl);
        this._setStreamVideoSmall(this.machineParameters.webrtcurl);

        this.setGameParameters(this.machineParameters);
        //this.setCurrency();
        //this._addInfo();

        //this._setInfo();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
        //this.setMonetaryValueView(this.controlBoard._toValueView);//NE RULET

        this._setWinningNumberApplet();
        this.showGame(true);
        setTimeout(function(){
            _this.addGiveUpButton(false);
            if(_this._hiddenMenuButton){
                _this._hiddenMenuButton.visible = true;
                //_this._showHiddenMenu(true);
            }
        },2000);

        setTimeout(function(){_this.statusChange(Roulette.START);},3000);
        //setTimeout(function(){_this.statusChange(Roulette.PLACE_BET);},1000)
        setTimeout(function(){_this.statusChange(Roulette.BALL_IN_RIM);},8000);
        setTimeout(function(){_this.statusChange(Roulette.NO_MORE_BETS);},13000);
        setTimeout(function(){
            _this.statusChange(Roulette.WINNING_NUMBER);
            _this.onPrizeDetection({"user_bets":5.0,"win":250,"drawnNo":28,"amount":0,"money":0,"balance":862206});
        },18000);
        setTimeout(function(){_this.statusChange(Roulette.START);},23000);
        //setTimeout(function(){_this.statusChange(Roulette.PLACE_BET);},18000)
    };

    p.onMinStake = function(bool){
        console.log("min stake true:::::::::::::");
        this.controlBoard.enableSpin(bool);
    };

    p.onMaxStake = function(bool){
        this.betTable.enableButtons(!bool);
        if(bool ===true){
            console.log("max stake true:::::::::::::");
            this.betTable.onlyHoverAllowed();
            //gamecore.Assets.playSound("spoken_maxReached",1);
        }
    };
    

// static methods:
// public methods:

    alteastream.Roulette = createjs.promote(Roulette,"AbstractScene");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var WinningNumberApplet = function() {
        this.Container_constructor();
        this.initialize_WinningNumberApplet();
    };

    var p = createjs.extend(WinningNumberApplet, createjs.Container);
    var _instance = null;

// static properties:
// events:
// public properties:
// private properties:
    p._normalFontColor = "#ffffff";
    p._highlitedFontColor = "#000000";

// constructor:
// static methods:
// public methods:
    p.initialize_WinningNumberApplet = function() {

        _instance = this;

        this._resultBg = alteastream.Assets.getImage(alteastream.Assets.images.winningBg_black);
        this.addChild(this._resultBg);

        this._fieldWidth =  this._resultBg.image.width;
        this._fieldHeight = this._resultBg.image.height;
        var fontSmall = "24px RobotoCondensed";
        var fontBig = "60px RobotoCondensed";
        var colorWhite = "#ffffff";

        var _resultLabel = this._resultLabel = new createjs.Text("Winning Number",fontSmall,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_resultLabel);
        _resultLabel.x = this._fieldWidth*0.5;
        _resultLabel.y = -30;

        var _resultNumber = this._resultNumber = new createjs.Text("30",fontBig,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_resultNumber);
        _resultNumber.x = this._fieldWidth*0.5;
        _resultNumber.y = this._fieldHeight*0.5+5;

        var _winLabel = this._winLabel = new createjs.Text("YOU WIN:",fontSmall,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_winLabel);
        _winLabel.x = this._fieldWidth*0.5;
        _winLabel.y = this._fieldHeight+30;

        var _winAmount = this._winAmount = new createjs.Text("0.20",fontBig,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_winAmount);
        _winAmount.x = this._fieldWidth*0.5;
        _winAmount.y = this._fieldHeight+80;

        this.show(false);
    };

    p.getWidth = function(){
        return  this._fieldWidth;
    };

    p.getHeight = function(){
        return  this._fieldHeight;
    };

    p.setResult = function(result,win){
        this._resultBg.image = alteastream.Assets.getImage(alteastream.Assets.images["winningBg_"+result.winColor]).image;
        this._resultNumber.text = result.winNumber;
        this._winAmount.text = win === true? Math.min(result.winAmount/100).toFixed(2):"";
        this._winLabel.text = win === true?"You Win":"No Win";
    }

    p.show = function(bool){
       this.visible = bool;
    };

    alteastream.WinningNumberApplet = createjs.promote(WinningNumberApplet,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BetTable = function(game,betmatrix,chipValues) {
        this.ABSBetTable_constructor(game,betmatrix,chipValues);
        this.initialize_BetTable(chipValues);
    };

    var p = createjs.extend(BetTable, alteastream.ABSBetTable);
    var _this = null;

// static properties:
// events:
// public properties:
    p.BETCHIPS = [];
    p.selectedChipValue = 0;
    p.chipsOnTable = 0;
    p._betButons = [];
    p._chipButons = [];
    p.runAnimations = false;
    p._animationsCounter = 0;
    p._noCliksAllowed = false;

// private properties:
// constructor:
// static methods:
    BetTable.getInstance = function(){
        return _this;
    };

// public methods:
    p.initialize_BetTable = function(chipValues) {
        _this = this;
        console.log("initialize_BetTable ");
        var startingY = 670;
        this.BETCHIPS = chipValues;
        for(var i = 0;i<this.BETCHIPS.length;i++){//all chips to be cloned
            var chip =  new alteastream.ABSChip(this.BETCHIPS[i],i);
            chip.name = this.BETCHIPS[i];
            this.addChild(chip);
            chip.visible = false;
        }
        //var hoverAction = alteastream.AbstractScene.HAS_TOUCH === true? null: this.highlightBet;

        var assets = alteastream.Assets;

        var fieldHeight = assets.getImage(assets.images.fieldSmall).image.height;
        var splitFieldDim = assets.getImage(assets.images.fieldSplit).image.height;
        var smallFieldWidth = assets.getImage(assets.images.fieldSmall).image.width;
        var mediumFieldWidth = assets.getImage(assets.images.fieldMedium).image.width;
        var largeFieldWidth = assets.getImage(assets.images.fieldLarge).image.width;
        var zeroFieldWidth = assets.getImage(assets.images.fieldZero).image.width;
        var fieldMiniHeight = assets.getImage(assets.images.fieldMini).image.height;
        var fieldMiniWidth = assets.getImage(assets.images.fieldMini).image.width;
        var startingX = (alteastream.AbstractScene.GAME_WIDTH/2)-(6*smallFieldWidth);

        // betField border
        var betFieldBorder = new createjs.Shape();
        this.addChild(betFieldBorder);

        var _borderThickness = 2;
        var betFieldBorderGfx = betFieldBorder.graphics;
        betFieldBorderGfx.setStrokeStyle(_borderThickness).beginStroke("#ffffff");
        betFieldBorderGfx.moveTo(startingX+smallFieldWidth*6, startingY)
        .lineTo((startingX+smallFieldWidth*6)+7*smallFieldWidth, startingY)
        .lineTo((startingX+smallFieldWidth*6)+7*smallFieldWidth, startingY+3*fieldHeight)
        .lineTo((startingX+smallFieldWidth*6)+6*smallFieldWidth, startingY+3*fieldHeight)
        .lineTo((startingX+smallFieldWidth*6)+6*smallFieldWidth, startingY+5*fieldHeight)
        .lineTo(startingX+smallFieldWidth*6, startingY+5*fieldHeight)
        .endStroke();
        betFieldBorder.cache(startingX+smallFieldWidth*6,startingY-_borderThickness,(startingX+smallFieldWidth*6)+7*smallFieldWidth,startingY+5*fieldHeight);
        betFieldBorder.mouseEnabled = false;

        var betFieldBorderLeft = new createjs.Bitmap(betFieldBorder.cacheCanvas);
        this.addChild(betFieldBorderLeft);
        betFieldBorderLeft.x=startingX+smallFieldWidth*6;
        betFieldBorderLeft.y=startingY-_borderThickness;
        betFieldBorderLeft.scaleX*=-1;
        betFieldBorderLeft.mouseEnabled = false;
        // betField border

        // raceTrack borders
        var raceTrackBorderOuter = new createjs.Shape();
        this.addChild(raceTrackBorderOuter);
        raceTrackBorderOuter.x=1715;
        raceTrackBorderOuter.y=830;
        raceTrackBorderOuter.graphics.setStrokeStyle(3).beginStroke("#ffffff").drawCircle(0,0,165);
        raceTrackBorderOuter.cache(-168,-168,335,335);

        var raceTrackBorderInner = new createjs.Shape();
        this.addChild(raceTrackBorderInner);
        raceTrackBorderInner.x=1715;
        raceTrackBorderInner.y=830;
        raceTrackBorderInner.graphics.setStrokeStyle(3).beginStroke("#ffffff").drawCircle(0,0,130);
        raceTrackBorderInner.cache(-132,-132,264,264);
        // raceTrack borders


        //inner and splits
        this._setInnerBets([3,6,9,12,15,18,21,24,27,30,33,36],startingX,startingY,"fieldSmall");
        this._setInnerBets([2,5,8,11,14,17,20,23,26,29,32,35],startingX,startingY+fieldHeight/*-1*/,"fieldSmall");
        this._setInnerBets([1,4,7,10,13,16,19,22,25,28,31,34],startingX,startingY+fieldHeight*2/*-2*/,"fieldSmall");
        this._setInnerBets([0],startingX-zeroFieldWidth,startingY,"fieldZero");

        var splitsHY = startingY+fieldHeight/2-splitFieldDim/2;
        var splitsHX = startingX+smallFieldWidth-splitFieldDim/2;
        this._setSplitBets([3,6,9,12,15,18,21,24,27,30,33,36],splitsHX,splitsHY,"split2H_","fieldSplit");
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32,35],splitsHX,splitsHY+fieldHeight,"split2H_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31,34],splitsHX,splitsHY+fieldHeight*2,"split2H_","fieldSplit");

        var splitsVX = startingX+smallFieldWidth/2-splitFieldDim/2;
        var splitsVY = startingY+fieldHeight-splitFieldDim/2;
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32,35],splitsVX,splitsVY,"split2V_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31,34],splitsVX,splitsVY+fieldHeight,"split2V_","fieldSplit");

        var splits4X = startingX+smallFieldWidth-splitFieldDim/2;
        var splits4Y = startingY+fieldHeight-splitFieldDim/2;
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32],splits4X,splits4Y,"split4_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31],splits4X,splits4Y+fieldHeight,"split4_","fieldSplit");

        //zero splits
        var splitsZeroX = startingX-splitFieldDim/2;
        var splitsZeroY = startingY+3*fieldHeight+fieldHeight/2-splitFieldDim/2;
        var splitsZeroY2 = startingY+6*fieldHeight-splitFieldDim/2;
        for(var j =1;j<6;j++){
            var yPos = j<4?splitsZeroY:splitsZeroY2;
            this._setSplitBets([j],splitsZeroX,yPos-(j*fieldHeight),"split0_","fieldSplit");
        }

        //streets and 6-lines
        var streetX = startingX+smallFieldWidth/2-splitFieldDim/2;
        var streetY = startingY-splitFieldDim/2;
        var streetAndLinePositions = [3,6,9,12,15,18,21,24,27,30,33,36];
        for(var s=0;s<streetAndLinePositions.length;s++){
            var betBtn = new alteastream.ABSBetField("street_"+s,null,"fieldSplit");
            this.addChild(betBtn);
            betBtn.x=streetX+(s*(betBtn.getWidth()*4));// fieldSmall is 4 times smaller than regular field
            betBtn.y=streetY;
            this._setAsButton(betBtn,this.placeBet,this.highlightBet);
            this._betButons.push(betBtn);
        }

        var lineX = startingX+smallFieldWidth-splitFieldDim/2;
        var lineY = startingY-splitFieldDim/2;
        for(var l=0;l<streetAndLinePositions.length-1;l++){
            var betBtnLine = new alteastream.ABSBetField("line_"+l,null,"fieldSplit");
            this.addChild(betBtnLine);
            betBtnLine.x=lineX+(l*(betBtnLine.getWidth()*4));// fieldSmall is 4 times smaller than regular field
            betBtnLine.y=lineY;
            this._setAsButton(betBtnLine,this.placeBet,this.highlightBet);
            this._betButons.push(betBtnLine);
        }

        //outer and neighbours
        var cPosX = startingX+(12*smallFieldWidth);
        this._setOuterBets("c1",cPosX,startingY,"fieldSmall");// decal here?
        this._setOuterBets("c2",cPosX,startingY+fieldHeight,"fieldSmall");
        this._setOuterBets("c3",cPosX,startingY+fieldHeight*2,"fieldSmall");

        this._setOuterBets("1st 12",startingX,startingY+fieldHeight*3,"fieldLarge");
        this._setOuterBets("2nd 12",startingX+largeFieldWidth,startingY+fieldHeight*3,"fieldLarge");
        this._setOuterBets("3rd 12",startingX+largeFieldWidth*2,startingY+fieldHeight*3,"fieldLarge");

        this._setOuterBets("1 - 18",startingX,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("EVEN",startingX+mediumFieldWidth,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("red",startingX+mediumFieldWidth*2,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("black",startingX+mediumFieldWidth*3,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("ODD",startingX+mediumFieldWidth*4,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("19 - 36",startingX+mediumFieldWidth*5,startingY+fieldHeight*4,"fieldMedium");

        var xPosSpec = 1636;
        var yPosSpec = 767;
        this._setSpecialBets("jeuZ",xPosSpec,yPosSpec,"fieldSpec");
        this._setSpecialBets("voisins",xPosSpec,yPosSpec+fieldMiniHeight,"fieldSpec");
        this._setSpecialBets("orphelins",xPosSpec,yPosSpec+fieldMiniHeight*2,"fieldSpec");
        this._setSpecialBets("tiers",xPosSpec,yPosSpec+fieldMiniHeight*3,"fieldSpec");

        //temp::
        var wheelOrder = [0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26];
        for(var n=0;n<wheelOrder.length;n++){
            var num = wheelOrder[n];
            this._setNeighbourBets("neighbours_"+num,1715,830,"fieldTrack",num,n);
        }

        for(var f=0;f<10;f++){
            var brk = f<5?f:f-5;
            var xPF = 1555+(brk*fieldMiniWidth);
            var yPF = f<5?570:570+fieldMiniHeight;
            this._setFinalsBets("finals_"+f,xPF,yPF,"fieldMini",f);
        }

        this.toDefaultCoin();
        this.setSpecialBetProps();
        //this._setLastPlayedPanel();
    };

    p._setInnerBets = function(row,posX,posY,fieldSmall){
        for(var i =0,j =this.BETMATRIX.length;i<j;i++) {
            var matrix =this.BETMATRIX[i];
            for (var k = 0, l = row.length; k < l; k++) {
                if (matrix.name === "num_" +row[k]) {
                    var betBtn = new alteastream.ABSBetField(matrix.name,row[k],fieldSmall);
                    this.addChild(betBtn);
                    betBtn.x=posX+(k*betBtn.getWidth())/*-(k+1)*/;
                    betBtn.y=posY;
                    this._setAsButton(betBtn,this.placeBet,this.highlightBet);
                    this._betButons.push(betBtn);
                    if(matrix.name !== "num_0")
                        betBtn.setColor(matrix.color,fieldSmall);
                }
            }
        }
    };

    p._setOuterBets = function(name,posX,posY,fieldString){
        var field = new alteastream.ABSBetField(name,name,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        this._setAsButton(field,this.placeBet,this.highlightBet);
        this._betButons.push(field);
    };

    p._setSplitBets = function(row,posX,posY,splitType,fieldSmall){
        for(var i =0,j =this.BETMATRIX.length;i<j;i++) {
            var matrix =this.BETMATRIX[i];
            for (var k = 0, l = row.length; k < l; k++) {
                if (matrix.name === splitType+row[k]) {
                    var betBtn = new alteastream.ABSBetField(matrix.name,null,fieldSmall);
                    this.addChild(betBtn);
                    betBtn.x=posX+(k*(betBtn.getWidth()*4));// fieldSmall is 4 times smaller than regular field
                    betBtn.y=posY;
                    this._setAsButton(betBtn,this.placeBet,this.highlightBet);
                    this._betButons.push(betBtn);
                }
            }
        }
    };

    p._setSpecialBets = function(name,posX,posY,fieldString){
        var field = new alteastream.ABSBetField(name,name,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.setFont(20,"RobotoCondensed");//bitmap font
        //this._setAsButton(field,this.placeBet,this.highlightBet);
        this._setAsButton(field,this.placeBet,this.highlightSpecials);
        this._betButons.push(field);
    };

    p._setNeighbourBets = function(name,posX,posY,fieldString,num,n){
        var field = new alteastream.ABSBetField(name,num,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.regX = field.getWidth() * 0.5;
        field.regY = 163;//radius
        field.rotation = n*(360/37);
        field.setFont(18,"RobotoCondensed");//bitmap font
        this._setAsButton(field,this.placeBet,this.highlightNeighbours);
        this._betButons.push(field);
        var color = num>0?this.BETMATRIX[num].color:"green";
        field.setColor(color,fieldString);
    };

    p._setFinalsBets = function(name,posX,posY,fieldString,ind){
        var field = new alteastream.ABSBetField(name,ind,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.setFont(20,"RobotoCondensed");//bitmap font
        this._setAsButton(field,this.placeBet,this.highlightFinals);
        this._betButons.push(field);
    };

    p.setSpecialBetProps = function(){
        var _setSpecial = function(type,length){
            for(var i=0;i<length;i++){
                _this[type+i+"Chips"] = _this.getBetField(type+i).special.length;
            }
        }

        _setSpecial("street_",this.STREET_POSITIONS);
        _setSpecial("line_",this.LINES_POSITIONS);
        _setSpecial("neighbours_",this.TOTAL_NUMBERS);
        _setSpecial("finals_",this.FINALS_NUMBERS);

        this.jeuzChips = this.getBetField("jeuZ").special.length;
        this.tiersChips = this.getBetField("tiers").special.length;
        this.orphelinsChips = this.getBetField("orphelins").special.length;
        this.voisinsChips = this.getBetField("voisins").special.length;
    };

    p.setChipValue = function(chip){
        var value = parseInt(chip.name);
        _this.selectedChipValue = value;
        _this.selectedBetValue = value / 100;
        console.log("selected chip "+ _this.selectedChipValue+", bet value "+ _this.selectedBetValue);
        _this.setAvailableBets();
        //_this.setHoverImage(_this.selectedChipValue);

        //alteastream.Assets.playSound("buttonClick");
    };

    p.highlightNeighbours = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
         _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightSpecials = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
        _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightFinals = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        var finalN = numbers[0];
        var name = "finals_"+String(finalN);
        var mesh = _this.getChildByName(name);
        mesh.setHighlighted(bool);
        _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightBet = function(betFieldMesh,bool){
        //just highlight
        /*if(_this.getBetField(betFieldMesh.name).bet>0.00){
            console.log("bet: "+  _this.getBetField(betFieldMesh.name).bet);
        }*/
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        for(var i = 0,j = numbers.length;i<j;i++){
            var name = "num_"+String(numbers[i]);
            var mesh = _this.getChildByName(name);
            mesh.setHighlighted(bool);
        }
        var prefix = String(betFieldMesh.name.substr(0,4));
        if(prefix !=="spli" && prefix!=="num_" && prefix!=="stre" && prefix!=="line")
            _this.getChildByName(String(betFieldMesh.name)).setHighlighted(bool);

        if(_this.getBetField(betFieldMesh.name).bet>0.00){
        //if(_this.getChildByName(betFieldMesh.name).chipsArr.length>1){
            _this._revealChipStack(betFieldMesh,bool);
            _this._showToolTip(betFieldMesh,bool);
        }
    };

    p.highlightRaceTrack = function(betFieldMesh,bool){
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        for(var i = 0,j = numbers.length;i<j;i++){
            var name = "neighbours_"+String(numbers[i]);
            var mesh = _this.getChildByName(name);
            mesh.setHighlighted(bool);
        }
    };

    p._revealChipStack = function(betFieldMesh,bool){
        var betField = _this.getChildByName(betFieldMesh.name);
        var betFieldHeight = betFieldMesh.normalState.image.height;
        var chipsArrLength = betField.chipsArr.length;
        var spacing = chipsArrLength>1?28:0;
        var orIndex = this.getChildIndex(betField.chipsArr[betField.chipsArr.length-1]);

        for(var i = 0,j = chipsArrLength;i<j;i++){
            var chip = betField.chipsArr[i];
            var index = bool === true? this.numChildren-1 : orIndex;
            this.setChildIndex(chip,index);
            var revealedY = (chipsArrLength*i)+spacing-(i*spacing)-spacing;
            var defaultY = betFieldMesh.y + (betFieldHeight/2) - (4*i);
            chip.y = bool === true? chip.y+revealedY : defaultY;
            chip.scaleX = chip.scaleY = bool === true? 1+0.15*i : 1;
        }
        //console.log("POS "+betField.chipsArr[chipsArrLength-1].y);
    };

    p._showToolTip = function(betFieldMesh,bool){
        console.log("bet: "+  _this.getBetField(betFieldMesh.name).bet);
        /*if(bool){
            var height = 30;//betFieldMesh.normalState.image.height/2;//40*_this.getChildByName(betFieldMesh.name).chipsArr.length;
            var width = 40;//betFieldMesh.normalState.image.width/2;
            var radius = 20;
            var rect = this.rect = new createjs.Shape();
            //rect.graphics.setStrokeStyle(2).beginStroke("white").beginFill("black").drawRect(0, 0, width, height);
            rect.graphics.setStrokeStyle(2).beginStroke("white").beginFill("black").drawCircle(0, 0, radius);
            this.addChild(rect);
            rect.alpha = 0.6;
            //rect.regX = width;
           // rect.regY = height;
            rect.x = betFieldMesh.x + (betFieldMesh.normalState.image.width/2)+20;
            rect.y = betFieldMesh.y + (betFieldMesh.normalState.image.height/2)+10;
        }else{
            if(this.rect){
                this.removeChild(this.rect);
            }
        }*/

    };

    p.onSpin = function(){
        this.disableBetting();
    };

    p.disableBetting = function(){
        console.log("disableBetting: ");
        this.enableButtons(false);
        this.resetAnimations();
        this.hideSpecials(true);
    };

    p.enableBetting = function(){
        this.enableButtons(true);
        //this.resetAnimations();
        this.setAvailableBets();
        this.setAvailableCoins();
        this.hideSpecials(false);
    };

    p.highlightWin = function (winNumber) {
        console.log("highlightWin:::::::::::::::::::::::::::::");// temp all:::
        //console.log("WIN BETS (exclude splits from highlighting!!!):: "+wonBetsArray);
        //this.winningNumber = this.scene.getChildByName("num_"+String(winNumber));
        //this.winningNumber.position.y = fieldHighY;
        var wonBetsArray = this.wonBetsArray;
        var meshName = String(wonBetsArray[0]);
        var field = this.getChildByName(meshName);
        this.runAnimations = true;
        this._animateWin(field);

        console.log("on collect or some end game end event:: test!!!!");// temp all local:::
        /*setTimeout(function(){
            _this.onCollectedWin();
            alteastream.RouletteControlBoard.getInstance().showUI(true);
            alteastream.RouletteControlBoard.getInstance().setWin(0);
            alteastream.Roulette.getInstance().winningNumberApplet.show(false);
        },5000);*/
    };

    p._animateWin = function(field){
        if(this.runAnimations){
            field.setHighlighted(true);
            var that = this;
            setTimeout(function(){
                field.setHighlighted(false);
                that._animationsCounter = (that._animationsCounter < that.wonBetsArray.length-1)? that._animationsCounter+1: 0;

                var meshName = String(that.wonBetsArray[that._animationsCounter]);
                var next = that.getChildByName(meshName);
                setTimeout(function(){that._animateWin(next)},250);
            },250);
        }
    };

    p.onCollectedWin = function(){
        console.log("onCollectedWin");
        this.resetAnimations();
        //this.enableButtons(true);
        //this.setAvailableCoins();
    };

    p.resetAnimations = function(){
        if(this.runAnimations){
            this.runAnimations = false;
            //this.winningNumber.position.y = fieldStartY;
            for(var i = 0,j = this.wonBetsArray.length;i<j;i++){
                var meshName = String(this.wonBetsArray[i]);
                this.getChildByName(meshName).setHighlighted(false);
            }
        }
    };

    p.toDefaultCoin = function() {
        var _defaultCoinName = String(this.BETCHIPS[this.BETCHIPS.length-1]);
        this.setChipValue(this.getChildByName(_defaultCoinName));
        alteastream.RouletteControlBoard.getInstance().setDefaultChip(_defaultCoinName);
    };

    p.placeBet = function(betFieldMesh){
        console.log("PLACE BET "+betFieldMesh.name);
        if(_this.getBetField(betFieldMesh.name).special){
            var specialBet = _this.getBetField(betFieldMesh.name).special;
            _this._selectedBetValuesArr.push(_this.selectedBetValue);
            _this.pushBet(betFieldMesh.name,_this.selectedBetValue*specialBet.length);
            for(var i=0,j=specialBet.length;i<j;i++){
                var specialBetName = specialBet[i];
                var specialBetMesh = _this.getChildByName(specialBetName);
                var targetBetType = _this.BETMATRIX[_this._lookUpTable[specialBetName]];
                targetBetType.bet = Math.round((targetBetType.bet + _this.selectedBetValue) * 100) / 100;
                _this.recalculateChips(_this.recalculate(targetBetType.bet),specialBetMesh);
                _this.chipsOnTable+=i;
            }
        }
        else{
            _this.pushBet(betFieldMesh.name,_this.selectedBetValue);
            _this._selectedBetValuesArr.push(_this.selectedBetValue);
            var currentBetOnField = _this.getBetField(betFieldMesh.name).bet;
            _this.recalculateChips(_this.recalculate(currentBetOnField),betFieldMesh);
            _this.chipsOnTable++;
        }
        _this.setAvailableBets();
        _this.setAvailableCoins();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,true);
    };

    p.removeBet = function(){
        var betFieldMesh = this._currentBet[this._currentBet.length-1][0];
        if(_this.getBetField(betFieldMesh).special){
            var specialBet = _this.getBetField(betFieldMesh).special;
            _this.popBet();
            var ind = _this._selectedBetValuesArr.pop();
            for(var i=0,j=specialBet.length;i<j;i++){
                var specialBetName = specialBet[i];
                var specialBetMesh = _this.getChildByName(specialBetName);
                //specialBetMesh.chipsArr.pop();
                //specialBetMesh.chipsArr.splice(i,1);
                var targetBetType = _this.BETMATRIX[_this._lookUpTable[specialBetName]];
                targetBetType.bet = Math.round((targetBetType.bet - ind) * 100) / 100;
                //console.log("targetBetType.bet "+targetBetType.bet);
                //console.log("_this.selectedBetValue "+_this.selectedBetValue);
                _this.recalculateChips(_this.recalculate(targetBetType.bet),specialBetMesh);
                _this.chipsOnTable-=i;
            }
        }
        else{
            var betFieldMesh1 = this.getChildByName(this._currentBet[this._currentBet.length-1][0]);
            this.popBet();
            _this._selectedBetValuesArr.pop();
            var currentBetOnField = _this.getBetField(betFieldMesh1.name).bet;
            _this.recalculateChips(_this.recalculate(currentBetOnField),betFieldMesh1);
            _this.chipsOnTable--;
        }

        _this.setAvailableBets();
        _this.setAvailableCoins();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,_this.chipsOnTable>0);
        //_this.testArrayLength("removeBet");
    };

    p.removeAllBets = function(){
        for(var i = 0, j = _this._currentBet.length;i<j;i++ ){
            var betFieldMesh = _this.getChildByName(_this._currentBet[i][0]).name;
            if(_this.getBetField(betFieldMesh).special){
                var specialBet = _this.getBetField(betFieldMesh).special;
                for(var k=0,l=specialBet.length;k<l;k++){
                    var specialBetName = specialBet[k];
                    var specialBetMesh = _this.getChildByName(specialBetName);
                    for(var m=0,n=specialBetMesh.chipsArr.length;m<n;m++){
                        this.removeChild(specialBetMesh.chipsArr[m]);
                        //specialBetMesh.chipsArr.splice(m,1);
                    }
                }
            }
            else {
                for(var o = 0, p = _this._currentBet.length;o<p;o++ ){
                    var betFieldMesh1 = _this.getChildByName(_this._currentBet[o][0]);
                    _this.clearChipsPerField(betFieldMesh1);
                }
            }
        }
        _this.clearBets();
        _this.chipsOnTable = 0;

        //_this.toDefaultCoin();
        _this.setAvailableBets();
        _this.setAvailableCoins();
        _this.resetAnimations();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,false);
        //_this.testArrayLength("removeAllBets");
    };

    p.onStepBack = function(){
        this.removeBet();
        this.resetAnimations();
    };

    p.enableButtons = function(bool){
        if(bool === true){
            if(!this.maxStakeReached){
                this._pickableButtons(true);
                this._noCliksAllowed = false;
            }else{
                this.onlyHoverAllowed();
            }

            this.setAvailableBets();
        }else
            this._pickableButtons(false);
    };

    p.onlyHoverAllowed = function(){//LATEST
        console.log("NOOOOOOOOOO CLICK, only hover maybe");
        //this.scene.hoverCursor = "auto";
        this._pickableButtons(true);
        this._noCliksAllowed = true;
    };

    p._pickableButtons = function(bool){//LATEST
        for(var i =0,j = this._betButons.length;i<j;i++)
            this._betButons[i].mouseEnabled = bool;
    };

    p._setAsButton = function(mesh,onClick,onHover){
        mesh.on("click",function(e){
            if(_this._noCliksAllowed){
                return;
            }
            onClick(e.currentTarget);
        });
        if(onHover !== null){
            mesh.on("mouseover",function(e){
                onHover(e.currentTarget,true);
            });

            mesh.on("mouseout",function(e){
                onHover(e.currentTarget,false);
            });
        }
    };

    p.recalculate = function(currentBet){
        currentBet = currentBet*100;
        var BETCHIPS = this.BETCHIPS;
        var amounts = [];

        for(var i = 0,j=BETCHIPS.length-1;i<j;i++){
            var chip =  Math.floor(currentBet/BETCHIPS[i]);
            currentBet -= chip*BETCHIPS[i];
            amounts[i]=chip;
        }
        amounts[BETCHIPS.length-1]=Math.floor(currentBet);
        return amounts;
    };

    p.clearChipsPerField = function(betFieldMesh){
        if(betFieldMesh.chipsArr.length>0){
            for(var i = 0,j = betFieldMesh.chipsArr.length;i<j;i++ ){
                this.removeChild(betFieldMesh.chipsArr[i]);
               // betFieldMesh.chipsArr[i].dispose();
            }
        }
        betFieldMesh.chipsArr = [];
    };

    p.recalculateChips = function(amounts,betFieldMesh) {
        this.clearChipsPerField(betFieldMesh);
        //for (var i= 0,j = amounts.length; i < j ; i++) {
        for (var i= amounts.length-1,j = 0; i >= j ; i--) {// reversed, now its from lowest to highest chip value placement
            if(amounts[i]>0){
                //for(var k = 0,l = amounts[i];k<l;k++){
                for(var k = amounts[i]-1,l = 0;k>=l;k--){// reversed
                    this.deployChips(this.BETCHIPS[i],betFieldMesh);
                }
            }
        }
    };

    p.deployChips = function(chip,betFieldMesh){
        var selectedChipName = String(chip);
        var selectedChip = this.getChildByName(selectedChipName).clone(true);
        console.log("selectedChipName "+selectedChipName);
        this.addChild(selectedChip);
        selectedChip.visible = true;
        selectedChip.mouseChildren = false;

        var meshPositionX = (betFieldMesh.x +betFieldMesh.normalState.image.width/2);
        var yPos = (betFieldMesh.y + (betFieldMesh.normalState.image.height/2)) - (4*betFieldMesh.chipsArr.length);

        selectedChip.x = meshPositionX/*+gamecore.Utils.NUMBER.randomRange(-2,2)*/;
        selectedChip.y = yPos;
        selectedChip.rotation = gamecore.Utils.NUMBER.randomRange(-30,30);

        betFieldMesh.chipsArr.push(selectedChip);
    };

    //checkAvail
    p.setAvailableBets = function() {
        console.log("specialBets here:::::")
        var chipValue = this.selectedBetValue;
        var currentBet = this._currentBetAmount;
        var maxStake = this.maxStake;
        var attemptedCredit = alteastream.Roulette.getInstance().credit - currentBet;

        //console.log("CHIP SELECTED "+chipValue);
        //if (attemptedCredit < alteastream.Roulette.getInstance().credits)//CHECK!!!

        var _setAvailable = function(betType,betName){
            if (attemptedCredit < (betType * chipValue) || (betType * chipValue + currentBet) > maxStake) {
                _this.getChildByName(betName).setDisabled(true);
            }else {
                _this.getChildByName(betName).setDisabled(false);
            }
        }

        _setAvailable(this.jeuzChips,"jeuZ");
        _setAvailable(this.tiersChips,"tiers");
        _setAvailable(this.orphelinsChips,"orphelins");
        _setAvailable(this.voisinsChips,"voisins");

        for(var i = 0;i<this.TOTAL_NUMBERS;i++){
            _setAvailable(this.neighbours_0Chips,"neighbours_"+i);
        }
        for(var j = 0;j<this.FINALS_NUMBERS;j++){
            _setAvailable(this["finals_"+j+"Chips"],"finals_"+j);
        }

        for(var k = 0;k<this.STREET_POSITIONS;k++){
            var betNameStreet = "street_"+k;
            _this.getChildByName(betNameStreet).mouseEnabled = !(attemptedCredit < (this.street_0Chips * chipValue) || (this.street_0Chips * chipValue + currentBet) > maxStake);
        }

        for(var l = 0;l<this.LINES_POSITIONS;l++){
            var betNameLine = "line_"+l;
            _this.getChildByName(betNameLine).mouseEnabled = !(attemptedCredit < (this.line_0Chips * chipValue) || (this.line_0Chips * chipValue + currentBet) > maxStake);
        }

        var controlBoard = alteastream.RouletteControlBoard.getInstance();
        controlBoard.updateStreetBetLabels( _this.getChildByName("street_0").mouseEnabled);
        controlBoard.updateSixLineBetLabels(_this.getChildByName("line_0").mouseEnabled);
        //staviti za split image (fieldSplit )mozda da bude mala tacka, menja avail sa "x" i "o"
        //_this.getChildByName(betNameLine).image = bool === true? o:x:
    };

    p.hideSpecials = function(bool){
        console.log("hide specialBets here::::: "+bool)
       /* var scene = this.scene;
        scene.getChildByName("orphelins_gray").visible = !bool;
        scene.getChildByName("tiers_gray").visible = !bool;
        scene.getChildByName("voisins_gray").visible = !bool;
        scene.getChildByName("jeuZ_gray").visible = !bool;*/
    };

    p.setAvailableCoins = function() {
        var betChips = this.BETCHIPS;
        var maxStake = this.maxStake;
        var currentBetAmount = this._currentBetAmount;
        var currentCredit = alteastream.Roulette.getInstance().credit;
        var controlBoard = alteastream.RouletteControlBoard.getInstance();
        var availCoins = [];

        for (var i = 0,j = betChips.length; i<j; i++ ) {
            var betChip = betChips[i];
            var betChipValue = betChips[i]/100;

            if ((currentBetAmount + betChipValue) > currentCredit || betChipValue > currentCredit || betChipValue > maxStake || currentBetAmount > 0 && (currentBetAmount + betChipValue) > maxStake) {
                controlBoard.disableChip(String(betChip),true);
            }
            else {
                controlBoard.disableChip(String(betChip),false);
                availCoins.push(betChip);
            }
        }

        if(availCoins.length>0){
            if(availCoins.indexOf(_this.selectedChipValue)>-1){
                var chip = String(availCoins[availCoins.indexOf(_this.selectedChipValue)]);
                //var chip = String(availCoins[0]);
                controlBoard.highlightChip(chip,true);
            }else{_this.toDefaultCoin();}
        }
    };

    //local test
    /*p.testArrayLength = function(onBet){
        var betmatrix = _this.BETMATRIX;
        for(var i =0,j = betmatrix.length;i<j;i++){
            var betField = _this.scene.getChildByName(betmatrix[i].name);
            console.log(onBet+" testArrayLength chipsArr.length " +betField.chipsArr.length);
        }
    };*/

    alteastream.BetTable = createjs.promote(BetTable, "ABSBetTable");
})();// namespace:
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var RouletteControlBoard = function(params) {
        this.AbstractControlBoard_constructor();
        this.initialize_RouletteControlBoard(params);
    };
    var p = createjs.extend(RouletteControlBoard, alteastream.AbstractControlBoard);
    var _this = null;

// static properties:
// events:
// public properties:
// private properties:
    p._color_highlighted_text = "";
    p._color_disabled_text = "";
    p._color_enabled_text = "";

// constructor:

    p.initialize_RouletteControlBoard = function(params) {
        _this = this;

        this._color_highlighted_text = "green";
        this._color_disabled_text = "#0c1828";
        this._color_enabled_text = "white";

        this._setTopButtons();
        this._setValuesPanel();
        this._setChipsButtonsPanel(params.chipValues);
        this._setPlayButtonsPanel();
    };

// static methods:
    RouletteControlBoard.getInstance = function(){
        return _this;
    };
// public methods:

    p.setBetAmount = function(amount) {
        //this._textStakeAmount.text = amount;
        this._textStakeAmount.setText(amount);
    };

// private methods:
    p._setChipsButtonsPanel = function(chipValues){
        console.log("COMM:: chipsButtonsPanel");
        var assets = alteastream.Assets;
        this.chipsPanel = new createjs.Container();
        this.addChild(this.chipsPanel);

        var numOfButtons = chipValues.length;
        var buttonWidth = assets.getImage(assets.images.chipBtn0).image.width/3;
        var spacingX = Math.ceil(buttonWidth+(buttonWidth/10));
        var middlePoint = alteastream.AbstractScene.GAME_WIDTH/2-spacingX/2;

        var startX = Math.ceil(middlePoint+spacingX/2+(((numOfButtons/2)-1)*spacingX));// from right to left from highest value
        var startY = 600;

        //var positions = [];
        for(var i = 0;i<chipValues.length;i++){
            var valueTxt = new createjs.Text(String(chipValues[i]),"20px RobotoCondensed","#ffffff").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
            var chip = new alteastream.ImageButton(assets.getImageURI(assets.images["chipBtn"+i]),3,valueTxt);
            chip.name = chipValues[i];
            this.chipsPanel.addChild(chip);

            var posX = Math.ceil(startX-(i*spacingX));
            chip.x = Math.ceil(posX+buttonWidth/2);
            chip.y = startY+5;
            chip.setClickHandler(this.onChipButton);
            //positions.push(posX);
        }

        this.mainBg = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundWithHole);
        this.addChildAt(this.mainBg,0);
        //this.mainBg.y = 535;
        //this.mainBg.alpha = 0.4;
        this.mainBg.mouseEnabled = false;

        //this.selectedChip = "1";//when all
        //this.selectedChip = String(response.values[response.values.length-1]);//when all
       // console.log("this.selectedChip "+this.selectedChip);
    };

    //chipsPanel === this now
    p.onChipButton = function(e){
        var chipName = e.currentTarget.name;
        if(chipName === undefined || _this.selectedChip === chipName)
            return;
        _this.chipsPanel.getChildByName(_this.selectedChip).setDisabled(false);
        _this.highlightChip(_this.selectedChip,false);
        _this.selectedChip = chipName;
        //chipName.setHighlighted(true);
        _this.highlightChip(chipName,true);

        alteastream.BetTable.getInstance().setChipValue(e.currentTarget);
    };

    p._setTopButtons = function(){
        var spacingTop = 5;
        var btnQuit = this.btnQuit = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
        btnQuit.x = 1865;
        btnQuit.y = spacingTop;
        this.addChild(btnQuit);

        var width = this.btnQuit.width;
        var spacing = (width/2)-10;

        //var btnSound = this.btnSound = alteastream.Assets.getImage(alteastream.Assets.images.soundOn);
        var btnSound = this.btnSound = alteastream.Assets.getImage(alteastream.Assets.images.soundOff);
        this.soundToggled = true;
        btnSound.x = this.btnQuit.x - width - spacing;
        btnSound.y = spacingTop;
        this.addChild(btnSound);

        var btnInfo = this.btnInfo = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnInfoSS),3);
        btnInfo.x = this.btnSound.x - width - spacing;
        btnInfo.y = spacingTop;
        btnInfo.highlightedTextColor  = "#fff";
        this.addChild(btnInfo);

        var scene = alteastream.Roulette.getInstance();
        var btnFs = scene.btnFs = alteastream.Assets.getImage(alteastream.Assets.images.btnFullScreen_On);
        btnFs.x = this.btnInfo.x - width - spacing;
        btnFs.y = this.btnInfo.y;
        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", scene[method]);
        this.addChild(btnFs);
    };

    p._setPlayButtonsPanel = function(){
        console.log("_setPlayButtonsPanel:::::::::::::::::::::");

        var assets = alteastream.Assets;
        var buttonsPanel = this.buttonsPanel = new createjs.Container();

        var fontSize = 16;
        var fontSmall = fontSize+"px RobotoCondensed";
        var white = "#ffffff";

        //var text = new alteastream.BitmapText("back",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnStepBack = this.btnStepBack = new alteastream.ImageButton(assets.getImageURI(assets.images.btnStepBackSS),3/*,text*/);
        buttonsPanel.addChild(btnStepBack);
        btnStepBack.setClickHandler(this.onStepBackBtn);

        //text = new alteastream.BitmapText("clear",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnClearAll = this.btnClearAll = new alteastream.ImageButton(assets.getImageURI(assets.images.btnClearSS),3/*,text*/);
        buttonsPanel.addChild(btnClearAll);
        btnClearAll.x= 723;
        btnClearAll.setClickHandler(this.onClearAllBtn);

        var textTemp = new alteastream.BitmapText("TEMP",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnSpin = this.btnSpin = new alteastream.ImageButton(assets.getImageURI(assets.images["chipBtn2"]),3,textTemp);
        //buttonsPanel.addChild(btnSpin);
        btnSpin.x= 905;
        btnSpin.y= 25;
        btnSpin.setClickHandler(this.onSpin);

        this.addChild(buttonsPanel);
        buttonsPanel.x = 570;
        buttonsPanel.y = 580;

        this.enableSpin(false);
    };

    /////////////////////////////////////ABS ROULETTE//////////////////////////////////

    p._setValuesPanel = function(){
        console.log("_setValuesPanel:::::::::::::::::::::");
        var valuesPanel = this.valuesPanel = new createjs.Container();
        this.addChild(valuesPanel);
        this.valuesPanel.y = 1005;
        var roboto14 ="14px RobotoCondensed";
        var robotoBold24 ="bold 24px RobotoCondensed";
        var colorGreen ="#4bdc0a";
        var colorGray ="#b8b8b8";
        var colorWhite ="#ffffff";
        var bottomLine1 = 25;
        var bottomLine2 = 50;

        this.statusText = new createjs.Text("GETTING STATUS..",roboto14,colorWhite).set({x:960,y:554,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this.statusText);

        var _textFinalsLabel = new createjs.Text("FINALS",roboto14,colorWhite).set({x:1715,y:554,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(_textFinalsLabel);

        this._textCrd = new createjs.Text("BALANCE",roboto14,colorGray).set({x:540,y:bottomLine1,textAlign:"right",textBaseline:"middle",mouseEnabled:false});//975
        this._textCurrencyValue = new createjs.Text("",roboto14,colorGray).set({x:this._textCrd.x+5,y:bottomLine1,textAlign:"left",textBaseline:"middle",mouseEnabled:false});
        this._textCrdAmount = new createjs.Text("0.00",robotoBold24,colorGreen).set({x:this._textCrd.x-10,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});

        this._textMinStakeLabel = new createjs.Text("MIN. STAKE",roboto14,colorGray).set({x:850,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1170
        this.minBetAmountText = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:850,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1170

        this._textCurrentStakeLabel = new createjs.Text("CURRENT STAKE",roboto14,colorGray).set({x:960,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1280
        this._textStakeAmount = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:960,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1280

        this._textMaxStakeLabel = new createjs.Text("MAX. STAKE",roboto14,colorGray).set({x:1070,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1400
        this.maxBetAmountText = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:1070,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1400

        valuesPanel.addChild(
            this._textCurrencyValue,
            this._textCrd,
            this._textCrdAmount,
            this._textCurrentStakeLabel,
            this._textStakeAmount,
            this._textWinAmount,
            this._textMinStakeLabel,
            this.minBetAmountText,
            this._textMaxStakeLabel,
            this.maxBetAmountText
        );

        this._streetBetsLabel = new createjs.Text("STREET BETS",roboto14,colorGray).set({x:1300,y:1030,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._streetBetsLabel);
        this._streetBetsAvail = new createjs.Text("AVAILABLE",roboto14,colorWhite).set({x:1300,y:1055,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._streetBetsAvail);

        this._sixLineBetsLabel = new createjs.Text("SIX LINE BETS",roboto14,colorGray).set({x:1400,y:1030,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._sixLineBetsLabel);
        this._sixLineBetsAvail = new createjs.Text("AVAILABLE",roboto14,colorWhite).set({x:1400,y:1055,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._sixLineBetsAvail);
    };

    p.setLastPlayedNumbers = function(nums){
        console.log("COMM:: playedNumbersPanel");
        var playedNumbersPanel = this.playedNumbersPanel = new createjs.Container();
        this.addChild(playedNumbersPanel);
        playedNumbersPanel.x = 1572;
        playedNumbersPanel.y = 1055;

        var _textPlayedNumbers = new createjs.Text("LATEST NUMBERS","14px RobotoCondensed","#b8b8b8").set({x:150,y:-25,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.numsArray = [];
        var startNumsArray = nums;
        playedNumbersPanel.addChild(_textPlayedNumbers);

        for (var i = 0,j=startNumsArray.length;i<j; i++) {
            var color = alteastream.BetTable.getInstance().getBetField("num_"+startNumsArray[i]).color;
            if(color === "black")//temp
                color = "#ffffff";
            var number = new createjs.Text(String(startNumsArray[i]),"20px RobotoCondensed",color).set({textAlign:"center",textBaseline:"middle"});
            number.x = 32*i;
            number.mouseEnabled = false;
            playedNumbersPanel.addChild(number);
            this.numsArray.push(number);
        }
        this.numsArray[0].font = "28px RobotoCondensed";
        playedNumbersPanel.mouseEnabled = false;
        playedNumbersPanel.mouseChildren = false;
    };

    p.updateLastPlayedNumbers = function(num){
        console.log("COMM:: updateLastPlayed");
        var numsArray = this.numsArray;
        for (var i = numsArray.length-1,j = 0; i > j; i--) {
            numsArray[i].text = numsArray[i-1].text;
            numsArray[i].color = numsArray[i-1].color;
            numsArray[i].x = 34*i;
        }
        var color = alteastream.BetTable.getInstance().getBetField("num_"+num).color;
        if(color === "black")//temp
            color = "#ffffff";
        numsArray[0].font = "bold 28px Roboto Condensed";
        numsArray[0].color = color;
        numsArray[0].text = String(num);
    };

    p.updateStatus = function(status){
        console.log("status:: "+status);
        this.statusText.text = status;
    };

    p.updateStreetBetLabels = function(bool){
        this._streetBetsAvail.color = bool===true?"#4bdc0a":"#ff0000";
    };

    p.updateSixLineBetLabels = function(bool){
        this._sixLineBetsAvail.color = bool===true?"#4bdc0a":"#ff0000";
    };

    p.setCurrency = function(c){
        this._textCurrencyValue.text = "("+c+")";
    };

    p.setStakesInfo = function(min,max){
        this.minBetAmountText.text = min.toFixed(2);
        this.maxBetAmountText.text = max.toFixed(2);
    };

    p.onClearAllBtn = function(){
        alteastream.BetTable.getInstance().removeAllBets();
        var assets = alteastream.Assets;
        assets.playSound(assets.sounds.buttonClick);
        assets.playSound(assets.sounds.spoken_clearedBets);
        assets.playSoundChain(assets.sounds.spoken_clearedBets,1,function(){
            assets.playSound(assets.sounds.spoken_placeBet_2);
        });
    };

    p.onStepBackBtn = function(){
        alteastream.BetTable.getInstance().onStepBack();
        alteastream.Assets.playSound(alteastream.Assets.sounds.buttonClick);
    };

    p.onSpin = function(){
        _this.resetUI(false);
        _this.showUI(false);
    };

    p.onSpinWin = function(){
        this.resetUI(true);
    };

    p.onSpinLost = function(gameResult){
        this.resetUI(true);
        this.showUI(true);
        console.log("winningNumber.text here:::: "+gameResult.winNumber);
        /*this.winningNumber.text = String(gameResult.winNumber);
        this.winningNumberLabel.color = "#ffffff";
        this.winningNumber.color = "#ffffff";
        this.currentWinAmount.color = "#ffffff";*/
        this.win = false;
    };

    p.resetUI = function(bool){
        console.log("resetUI:::: "+bool);
        //this.enableSpin(bool);
        //this.enableQuit(bool);

        /*this.enableStepBack(bool);
        this.resetBallFrame();
        */
    };

    p.showUI = function(bool){
        console.log("showUI:::: "+bool);
        //this.background.visible = bool;
        //this.chipsPanel.visible = bool;
        //this.infoButtonsPanel.visible = bool;
        //this.buttonsPanel.visible = bool;
    };

    p.setDefaultChip = function(defaultCoinName){
        this.selectedChip = defaultCoinName;
        this.highlightChip(defaultCoinName,true);
        console.log("setDefaultChip "+defaultCoinName);
    };

    p.highlightChip = function(chipName,bool){
        var chip = this.chipsPanel.getChildByName(chipName);
        chip.setHighlighted(bool);
        //chip.scaleX = chip.scaleY =bool ===true?1.25:1;
    };

    p.disableChip = function(chip,bool){
        this.chipsPanel.getChildByName(chip).setDisabled(bool);
    };

    p.disableAllChips = function(bool){
        var children = this.chipsPanel.children;
        for(var i = 0,j=children.length;i<j;i++){
            this.chipsPanel.getChildByName(children[i].name).setDisabled(bool);
        }
        if(bool ===true){
            this.enableStepBack(false);
        }else{
            this.enableStepBack(alteastream.BetTable.getInstance().chipsOnTable>0);
        }
    };

    p.onBetAction = function(currentBetAmount,bool){
        this.setBetAmountText(currentBetAmount);
        this.enableStepBack(bool);
        alteastream.Assets.playSound(alteastream.Assets.sounds.chipDown);
    };

    p.setBetAmountText = function(betAmount){
        var color = betAmount<alteastream.BetTable.getInstance().minStake?"#bd0000":"#2dff00";
        var bet = parseFloat(String(betAmount));
        this._textStakeAmount.text = bet.toFixed(2);
        this._textStakeAmount.color = color;
    };

    p.enableStepBack = function(bool){
        this.btnStepBack.setDisabled(!bool);
        this.btnClearAll.setDisabled(!bool);
    };

    p.enableSpin = function(bool){
        this.btnSpin.setDisabled(!bool);
    };

    p.enableQuit = function(bool){
        this.btnQuit.setDisabled(!bool);
    };

    p.onSpin = function(bool){
        //this.enableSpin(!bool);
        this.enableQuit(!bool);
        this.enableStepBack(!bool);
        //this.resetBallFrame();
        this.showUI(!bool);
        /*if(bool)
            this.storeButtonStates();
        else{
            this.restoreButtonStates();
        }*/
    };

    p.soundBtnHandler = function(){
        var assets = alteastream.Assets;
        this.soundToggled = !this.soundToggled;
        this.btnSound.image = this.soundToggled === true? assets.getImage(assets.images.soundOff).image: assets.getImage(assets.images.soundOn).image;
        //assets.setMute(this.soundToggled);

        /* // local ver
        var action = this.soundToggled===true?"pause":"play";
        window.parent.manageBgMusic(action);
        */ // local ver
    };

    p.restoreMachineSoundState = function (){
        console.log("restoreMachineSoundState Here::::: ");
        /*var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            this.soundMachineToggled = soundMachineIsToggled === "false";
            this.soundMachineBtnHandler();
        }*/
    };

    p.restoreMusicSoundState = function (){
        console.log("restoreMusicSoundState Here::::: ");
        /*var gameplaySoundIsToggled = window.localStorage.getItem("gameplaySoundIsToggled");
        if(gameplaySoundIsToggled !== null) {
            this.soundToggled = gameplaySoundIsToggled === "false";
            this.soundBtnHandler();
        }*/
    };


    alteastream.RouletteControlBoard = createjs.promote(RouletteControlBoard,"AbstractControlBoard");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponent = function(stage) {
    this.initialize(stage);
}
var p = PreloaderComponent.prototype = new createjs.Container();

// static properties:
// events:
// public properties:
// private properties:
    p._percent = null;
    p._actions = null;

// constructor:
    p.Container_initialize = p.initialize;

    p.initialize = function(stage) {
        this.Container_initialize();
        this._percent = new alteastream.Percent();
        this._actions = [];
        if(stage){
            stage.register(this);
            stage.addChild(this);
            // debug log for iphone
            //stage.setChildIndex(logText,stage.numChildren-1);
        }
    };

// static methods:
// public methods:
    p.getProgress = function() {
        return this._percent.getDecimal();
    };
    p.getProgressPercent = function() {
        return this._percent.getPercent();
    }
    p.setProgress = function(percent) {
        this._percent.set(percent);
        this._performActions();
    };
    p.isPreloaderComponent = function () {
        return true;
    };
    p.addOnProgressChangeAction = function(action) {
        this._actions.push(action);
    };

// private methods:
    p._performActions = function() {
        for(var index in this._actions)
            this._actions[index]();
    };

alteastream.PreloaderComponent = PreloaderComponent;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var Percent = function() {
    this.initialize();
}
var p = Percent.prototype;

// static properties:
// events:
// public properties:
// private properties:
    p._percent = 0;

// constructor:
    p.initialize = function() {}

// static methods:
// public methods:
    p.getPercent = function() {
        return this._percent * 100;
    }
    p.getDecimal = function() {
        return this._percent;
    }
    p.toString = function() {
        return this.getPercent() + " %";
    }
    p.set = function(percent) {
        if(percent < 0) throw "Percent can not be negative";
        if(percent > 1) percent = 1;

        this._percent = percent;
    }

// private methods:

alteastream.Percent = Percent;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PreloaderComponentBar = function(stage,isDesktop,addPercentageText) {
        this.initialize(stage,isDesktop,addPercentageText);
    };

    var p = PreloaderComponentBar.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage,isDesktop,addPercentageText) {
        this.PreloaderComponent_initialize(stage);
        // sredi koordinate
        var props = isDesktop === true ?
            {frameWidth:596, frameHeight:13, frameRoundness:6, font:"14px Arial", txtY:22, txt2Y:50, barWidth:592, barHeight:11, barMargin:2} :
            {frameWidth:298, frameHeight:7, frameRoundness:3, font:"10px Arial", txtY:13, txt2Y:30, barWidth:296, barHeight:6, barMargin:1};

        var shape1 = new createjs.Shape();
        shape1.graphics.beginFill("#ffffff").drawRoundRect(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);
        shape1.cache(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#ffcb05").drawRect(0, 0, 0, 0);
        mask.cache(0, 0, 0, 0);

        var text = new createjs.Text("LOADING...", props.font, "white");
        text.x = props.frameWidth/2;
        text.y = props.txtY;
        text.textAlign = "center";
        var cacheBounds = text.getBounds();
        text.cache(cacheBounds.x, cacheBounds.y, cacheBounds.width, cacheBounds.height);

        var r = 255;
        var g = 203;
        var b = 4;
        var start = 120;

        this.addChild(shape1);
        this.addChild(mask);
        this.addChild(text);

        var that = this;

        if(addPercentageText) {
            var textPercentage = new createjs.Text("%", props.font, "white");
            textPercentage.x = props.frameWidth/2 + 2;
            textPercentage.y = props.txt2Y;
            textPercentage.textAlign = "left";

            var textPercentageNumber = new createjs.Text("0", props.font, "white");
            textPercentageNumber.x = props.frameWidth/2 - 2;
            textPercentageNumber.y = props.txt2Y;
            textPercentageNumber.textAlign = "right";

            this.addChild(textPercentageNumber,textPercentage);

            this.addOnProgressChangeAction(function() {
                textPercentageNumber.text = Math.round(that.getProgressPercent());
            });
        }

        this.addOnProgressChangeAction(function() {
            mask.uncache();
            mask.graphics.clear();
            var progress = props.barWidth*that.getProgress();
            var tmpR = Math.round(start + (r-start) * that.getProgress());
            var tmpG = Math.round(start + (g-start) * that.getProgress());
            var tmpB = Math.round(start + (b-start) * that.getProgress());
            mask.graphics.beginFill("rgb(" + tmpR + "," + tmpG + "," + tmpB + ")").drawRect(props.barMargin, props.barMargin, progress, props.barHeight);
            mask.cache(0,0,progress,props.barHeight);
        });
    };

// static methods:
// public methods:
// private methods:

    alteastream.PreloaderComponentBar = PreloaderComponentBar;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponentImage = function(stage, logo, canv) {
    this.initialize(stage, logo, canv);
};

var p = PreloaderComponentImage.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage, logo, canv) {
        this.PreloaderComponent_initialize(stage);

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRect(0, 0, canv.width, canv.height);
        shape.cache(0, 0, canv.width, canv.height);
        this.addChild(shape);

        this.addChild(logo);
        logo.mouseEnabled = false;

        /*var that = this;
        this.addOnProgressChangeAction(function() {
            image2.alpha = that.getProgress();
        });*/
    };

// static methods:
// public methods:
// private methods:
    p._getSaturation = function() {
        return (this.getProgress() - 1) * 100;
    };

     alteastream.PreloaderComponentImage = PreloaderComponentImage;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
var PreloaderStage = function(stage) {
    this.Stage_constructor(stage);
    this.initialize();
}
var p = PreloaderStage.prototype = createjs.extend(PreloaderStage,createjs.Stage);

// static properties:
// events:
// public properties:

// private properties:
    p._percent = null;
    p._registrants = null;
// constructor:
    p.initialize = function() {
        this._percent = new alteastream.Percent();
        this._registrants = [];
    };

// static methods:
// public methods:
    p.getProgress = function() {
        return this._percent.getDecimal();
    };

    p.setProgress = function(percent) {
        this._percent.set(percent);
        this.propagate();
    };

    p.register = function(observer) {
        if(!typeof observer.isPreloaderComponent === 'function'
            || !observer.isPreloaderComponent())
            throw "Can not register non PreloaderStageComponet object instance";

        this._registrants.push(observer);
    };

    p.propagate = function() {
        for(var index in this._registrants)
            this._registrants[index].setProgress(this._percent.getDecimal());
    };

    p.render = function(){
        var that = this;
        createjs.Ticker.timingMode = createjs.Ticker.RAF;
        that.enableMouseOver(20);
        createjs.Ticker.addEventListener("tick", function(event) {
            that.update(event);
        });
    };

// private methods:

alteastream.PreloaderStage = createjs.promote(PreloaderStage,"Stage");
})();

this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var CoinPusherInfo = function(){
        this.Container_constructor();
        this.initCoinPusherInfo();
    };

    var p = createjs.extend(CoinPusherInfo,createjs.Container);

    // public properties
    p.prizeImages = [];
    p.prizeValues = [];
    p.prizeDescriptions = [];
    p.labels = [];
    p.gameDescriptions = [];
    p.chips = [];

    // constructor
    p.initCoinPusherInfo = function () {};

    // public methods
    p.setInfo = function(payoutManager, hasBonus, multiplier) {
        // if hasBonus can be true uncomment this two lines block
/*        var backgroundImage = hasBonus === true ? "infoBackground" : "infoBackgroundNoBonus";
        var background = alteastream.Assets.getImage(alteastream.Assets.images[backgroundImage]);*/
        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackgroundNoBonus);
        this.addChild(background);

        var chipsY = 165;
        var chipsYSpacing = 140;
        var chipsXSpacing = 0;

        var payouts = payoutManager.getPayouts();
        var tags = Object.keys(payouts);
        var maxChipsInColumn = 4;
        var yPosMultiplier = 0;
        var startXPos = tags.length > maxChipsInColumn ? 306 : 375;
        for(var i = 0; i < tags.length; i++) {
            var tag = tags[i];
            var prize = payouts[tag];

            if(yPosMultiplier === maxChipsInColumn){
                yPosMultiplier = 0;
                chipsXSpacing = 140;
            }

            yPosMultiplier++;

            var chip = new alteastream.Chip(prize, tag, multiplier);
            chip.x = startXPos + chipsXSpacing;
            chip.y = chipsY + (chipsYSpacing * yPosMultiplier);
            this.addChild(chip);
            chip.updateChip(true, false);
            this.chips.push(chip);
        }

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText2);
        var gameplayDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:1540, y:264, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        gameplayDescription.lineWidth = 270;
        gameplayDescription.lineHeight = 40;
        this.gameDescriptions.push(gameplayDescription);

        this.addChild(gameplayDescription);
    };

/*    p.setKeyboardLabels = function() {
        var ctrl = new createjs.Text("KEY CTRL","18px HurmeGeometricSans3","#cccccc").set({x:62,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var left = new createjs.Text("KEY LEFT","18px HurmeGeometricSans3","#cccccc").set({x:760,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var space = new createjs.Text("KEY SPACE","18px HurmeGeometricSans3","#cccccc").set({x:960,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var right = new createjs.Text("KEY RIGHT","18px HurmeGeometricSans3","#cccccc").set({x:1155,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var down = new createjs.Text("KEY DOWN","18px HurmeGeometricSans3","#cccccc").set({x:1736-87,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var up = new createjs.Text("KEY UP","18px HurmeGeometricSans3","#cccccc").set({x:1857-87,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(ctrl,left,space,right,down,up);
    };*/

    p.setNonPlayingTimeInfo = function(time) {
        var seconds = time/1000;
        var textSrc = seconds >= 120? Math.floor(seconds/60) + "\nminutes": seconds + "\nseconds";

        //var textSrc = time/1000 + "\nseconds";
        var nonPlayingTimeSeconds = new createjs.Text(textSrc,"32px Impact","#e2d631").set({x:960, y:658, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        nonPlayingTimeSeconds.lineHeight = 30;

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var nonPlayingTimeDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:960, y:795, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        nonPlayingTimeDescription.lineWidth = 300;
        nonPlayingTimeDescription.lineHeight = 30;

        this.gameDescriptions.push(nonPlayingTimeDescription,nonPlayingTimeSeconds);
        this.addChild(nonPlayingTimeDescription,nonPlayingTimeSeconds);
    };

    p.setCountedTokensInfo = function(refundValue) {
        var coinImage = this.coinImage = alteastream.Assets.getImage(alteastream.Assets.images.metalCoin);
        coinImage.regX = coinImage.image.width*0.5;
        coinImage.regY = coinImage.image.height*0.5;
        coinImage.x = 960;
        coinImage.y = 290;

        textSrc = refundValue + " " + "\n"+this.parent.currency;
        var refundValueText = new createjs.Text(textSrc,"32px Impact","#000000").set({x:coinImage.x+2, y:coinImage.y-17, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        refundValueText.lineHeight = 35;

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText3);
        var countedTokensDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:960, y:412, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        countedTokensDescription.lineWidth = 440;
        countedTokensDescription.lineHeight = 30;

        this.gameDescriptions.push(countedTokensDescription,refundValueText);

        this.addChild(coinImage,countedTokensDescription,refundValueText);
    };

    p.setSunLogo = function() {
        var sunLogo = this.coinImage = alteastream.Assets.getImage(alteastream.Assets.images.sunLogo);
        sunLogo.regX = sunLogo.image.width*0.5;
        sunLogo.regY = sunLogo.image.height*0.5;
        sunLogo.x = 960;
        sunLogo.y = 360;
        this.addChild(sunLogo);
    };

    p.setMaxStakeInfo = function(maxStake) {
        //var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var yPos = 736;
        var textSrc = "Max Stake: ";
        var maxStakeDescription = new createjs.Text(textSrc,"29px Impact","#ffffff").set({x:1560, y:yPos, textAlign:"right", textBaseline:"middle", mouseEnabled:false});
        maxStakeDescription.lineWidth = 440;
        maxStakeDescription.lineHeight = 35;
        var stakeType = alteastream.AbstractScene.GAME_TYPE === "pearlpusher" ? " pearls" : " coins";
        var maxStakeText = new createjs.Text(maxStake + stakeType,"29px Impact","#e2d631").set({x:1560, y:yPos, textAlign:"left", textBaseline:"middle", mouseEnabled:false});
        this.gameDescriptions.push(maxStakeDescription,maxStakeText);

        this.addChild(maxStakeDescription,maxStakeText);
    };

    p.setBonusInfo = function(neededForBonus) {
        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText1);
        var bonusWheelDescription = new createjs.Text(textSrc,"29px Impact","#ffffff").set({x:960, y:460, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        bonusWheelDescription.lineWidth = 440;
        bonusWheelDescription.lineHeight = 40;
        this.gameDescriptions.push(bonusWheelDescription);

        var numOfCoinsNeededForBonus = new createjs.Text(neededForBonus,"29px Impact","#fcff00").set({x:830, y:460, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        this.gameDescriptions.push(numOfCoinsNeededForBonus);

        this.addChild(bonusWheelDescription, numOfCoinsNeededForBonus);
    };

    p.updatePrizes = function(isEur){
        for(var i = 0; i < this.chips.length; i++){
            this.chips[i].flipCoin(isEur);
        }
    };

    alteastream.CoinPusherInfo = createjs.promote(CoinPusherInfo,"Container");
})();this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var CoinPusherStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_CoinPusherStream();
    };

    var p = createjs.extend(CoinPusherStream, alteastream.ABSVideoStream);
// static properties:
// events:
// public vars:
// private vars:
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_CoinPusherStream = function (){
    };

    p.setPlayer = function(address){
        this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);
        this._local_activate();// local ver
    };

    p.handleError = function(error){
        //{"id":"error","code":-7,"txt":"User session does not exist"}
        //{"id":"error","code":13,"txt":"User token is expired or does not exist"}
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            that.scene.socketCommunicator.disposeCommunication();//quitPlay() if started
            that.scene.exitMachineGameplay();
        });
    }

    p.concreteActivate = function(){
        if(is.safari()){
            //var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
            //if(is.ipad() || is.ios() || isiPadOS){
            this.scene.setIosOverlay();
            //}
        }
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;
    };

    p.addSpinner = function(){
        var spinnerBackground = this.spinnerBackground = alteastream.Assets.getImage(alteastream.Assets.images.liveStreamConnecting);
        spinnerBackground.regX = spinnerBackground.image.width*0.5;
        spinnerBackground.regY = spinnerBackground.image.height*0.5;
        spinnerBackground.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinnerBackground.y = alteastream.AbstractScene.GAME_HEIGHT *0.5;

        var spinner = this.spinner = new alteastream.MockLoader();
        spinner.customScaleGfx(0.4);
        this.scene.addChild(spinnerBackground, spinner);
        //spinner.setLabel("Connecting to stream..");
        spinner.x = spinnerBackground.x + 196;
        spinner.y = spinnerBackground.y - 80;
        spinner.runPreload(true);
    }

    p.setMedia = function(){
        window.parent.switchSource("bgMusic");
        //that.scene.controlBoard.soundToggled = backgroundMusicIsMuted === "false";
        //that.scene.controlBoard.soundBtnHandler();

        // hajkova zelja, ako se skloni ovaj blok igra se ponasa tako sto pamti stanje backgroung muzike
        // i na osnovu toga pusta ili ne pusta zvuk kada se udje u masinu ili se vrati u lobi iz masine
        //if(that.scene.controlBoard.soundToggled === false){
        //that.scene.controlBoard.soundBtnHandler();
        //}

        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            soundMachineIsToggled = Boolean(soundMachineIsToggled);
        }else{
            soundMachineIsToggled = false;
        }

        if(soundMachineIsToggled === false){
            this.videoElement.setMuted(false);
            this.videoElement.setVolume(0.1);
            console.log("AUDIO ON:::::");
        }

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    }

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
        this.scene.addChild(poster);
        poster.mouseEnabled = false;*/
    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.startStreaming = function(){
        this.webrtcPlayer.startGameplayStream();//gameplay
    };

    p.onStreamFailed = function(){
        var _this = this;
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed. Exiting game",function(){_this.scene.onQuit();});
    }

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.CoinPusherStream = createjs.promote(CoinPusherStream,"ABSVideoStream");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PayoutManager = function(payout,refund) {
        this.initialize_PayoutManager(payout,refund);
    };

    var p = PayoutManager.prototype;
    var _payouts = null;
    // private properties:
    p._refund = null;

    // static properties:
    // events:
    // public properties:
    // private methods:
    // constructor:
    p.initialize_PayoutManager = function(payout,refund) {
        _payouts = payout;
        this._refund = refund;
    };
    // static methods:
    // public methods:
    //{"state":0,"key":"468356ef-484f-4b7e-b774-f871416fa2b4","controlIdleTime":60000,"streamendpoint":"ws://139.59.137.63:8889","webrtcurl":"ws://84.199.144.157:8083/ws","balance":1000000,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{"1":1,"2":5,"3":10,"4":25,"5":50,"6":1,"7":1,"8":1,"9":1,"10":1,"11":1,"12":1},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"}
    //{""payout":{"1":5,"2":10,"3":15,"4":45},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"}
    //multiplier = 0.1
    p.getPayoutByPrize = function(prize){
         return _payouts[prize];
         //(Math.round(Math.floor(5 * 10)) / 100).toFixed(2);
    };

    p.getPayouts = function() {
        return _payouts;
    };

    p.getRefundValue = function() {
        return this._refund;
    };

    alteastream.PayoutManager = PayoutManager;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusherAssets = function() {};
    var Assets = alteastream.Assets;

/*    CoinPusherAssets.videoManifest = [
        'assets/sprite_sheets/drum.mp4'
    ];*/

    CoinPusherAssets.count = 0;

    CoinPusherAssets.loadManifest = function(lang,gameCode){
        Assets.setImageLang(lang);
        //var language = Assets.getImageLang();
        //var localePath = typeImages+"/locale/" + language + "/";
        var noCache = "?ver="+Assets.VERSION;

        var typeImages = "assets/"+gameCode+"/images";
        var typeSounds = "assets/"+gameCode+"/sounds";
        //return [
        var manifest =  [
            // Images
            //{id:Assets.images.bounsWonBgImage,src:typeImages+"/bounsWonBgImage.png"+noCache},
            //{id:Assets.images.bounsWonBgImageRing,src:typeImages+"/bounsWonBgImageRing.png"+noCache},
            //{id:Assets.images.wheelBg,src:typeImages+"/wheelBg.png"+noCache},
            //{id:Assets.images.wheel_8_bonus_fields_highlight,src:typeImages+"/wheel_8_bonus_fields_highlight.png"+noCache},
            //{id:Assets.images.lampOn,src:typeImages+"/lampOn.png"+noCache},
            //{id:Assets.images.lampOff,src:typeImages+"/lampOff.png"+noCache},
            //{id:Assets.images.arrow,src:typeImages+"/arrow.png"+noCache},
            //{id:Assets.images.backgroundCB, src:typeImages+"/control_board_bg.png"+noCache},
            {id:Assets.images.preloadingPoster, src:typeImages+"/webrtc.png"+noCache},
            //{id:Assets.images.metalCoin, src:typeImages+"/metalCoin.png"+noCache},
            {id:Assets.images.coinWin, src:typeImages+"/coinWin.png"+noCache},
            {id:Assets.images.chip_glow, src:typeImages+"/chips/chip_glow.png"+noCache},
            {id:Assets.images.chip_1, src:typeImages+"/chips/chip_1.png"+noCache},
            {id:Assets.images.chip_2, src:typeImages+"/chips/chip_2.png"+noCache},
            {id:Assets.images.chip_3, src:typeImages+"/chips/chip_3.png"+noCache},
            {id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache},
            {id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache},
            {id:Assets.images.chip_6, src:typeImages+"/chips/chip_6.png"+noCache},
            {id:Assets.images.chip_7, src:typeImages+"/chips/chip_7.png"+noCache},
            {id:Assets.images.chip_8, src:typeImages+"/chips/chip_8.png"+noCache},
            {id:Assets.images.chip_9, src:typeImages+"/chips/chip_9.png"+noCache},
            {id:Assets.images.chip_10, src:typeImages+"/chips/chip_10.png"+noCache},
            {id:Assets.images.chip_11, src:typeImages+"/chips/chip_11.png"+noCache},
            {id:Assets.images.chip_12, src:typeImages+"/chips/chip_12.png"+noCache},
            {id:Assets.images.chip_0, src:typeImages+"/chips/chip_0.png"+noCache},
            {id:Assets.images.chip_1_back, src:typeImages+"/chips/chip_1_back.png"+noCache},
            {id:Assets.images.chip_2_back, src:typeImages+"/chips/chip_2_back.png"+noCache},
            {id:Assets.images.chip_3_back, src:typeImages+"/chips/chip_3_back.png"+noCache},
            {id:Assets.images.chip_4_back, src:typeImages+"/chips/chip_4_back.png"+noCache},
            {id:Assets.images.chip_5_back, src:typeImages+"/chips/chip_5_back.png"+noCache},
            {id:Assets.images.chip_6_back, src:typeImages+"/chips/chip_6_back.png"+noCache},
            {id:Assets.images.chip_7_back, src:typeImages+"/chips/chip_7_back.png"+noCache},
            {id:Assets.images.chip_8_back, src:typeImages+"/chips/chip_8_back.png"+noCache},
            {id:Assets.images.chip_9_back, src:typeImages+"/chips/chip_9_back.png"+noCache},
            {id:Assets.images.chip_10_back, src:typeImages+"/chips/chip_10_back.png"+noCache},
            {id:Assets.images.chip_11_back, src:typeImages+"/chips/chip_11_back.png"+noCache},
            {id:Assets.images.chip_12_back, src:typeImages+"/chips/chip_12_back.png"+noCache},
            {id:Assets.images.chip_0_back, src:typeImages+"/chips/chip_0_back.png"+noCache},
            //btns
            {id:Assets.images.btnIncSS, src:typeImages+"/buttons/count_btn_plus.png"+noCache},
            {id:Assets.images.btnDecSS, src:typeImages+"/buttons/count_btn_minus.png"+noCache},
            {id:Assets.images.btnFixedBet, src:typeImages+"/buttons/btnFixedBet.png"+noCache},
            {id:Assets.images.btnMediumSS, src:typeImages+"/buttons/middle_btn.png"+noCache},
            {id:Assets.images.btnArrowLeftSS, src:typeImages+"/buttons/left_btn.png"+noCache},
            {id:Assets.images.btnArrowRightSS, src:typeImages+"/buttons/right_btn.png"+noCache},
            {id:Assets.images.btnBetView, src:typeImages+"/buttons/switch_btn.png"+noCache},
            // common
            //{id:Assets.images.poster,src:"assets/common/images/poster.jpg"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/common/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/common/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/common/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/common/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.soundMachineOn,src:"assets/common/images/buttons/sound_icon_machine_default.png"+noCache},
            {id:Assets.images.soundMachineOff,src:"assets/common/images/buttons/sound_icon_machine_disabled.png"+noCache},
            {id:Assets.images.btnFullScreen_On,src:"assets/common/images/buttons/btnFullScreen_On.png"+noCache},
            {id:Assets.images.btnFullScreen_Off,src:"assets/common/images/buttons/btnFullScreen_Off.png"+noCache},
            {id:Assets.images.infoBackground, src:"assets/common/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoBackgroundNoBonus, src:"assets/common/images/infoBackgroundNoBonus.jpg"+noCache},
            {id:Assets.images.spinner, src:"assets/common/images/spinner.png"+noCache},
            {id:Assets.images.liveStreamConnecting, src:"assets/common/images/liveStreamConnecting.png"+noCache},
            {id:Assets.images.btnHiddenMenu, src:"assets/common/images/buttons/hiddenmenu_btn.png"+noCache},
            {id:Assets.images.controlsBg, src:"assets/common/images/controlsBg.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/common/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/common/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.metalCoin,src:"assets/common/images/metalCoin.png"+noCache},
            {id:Assets.images.sunLogo,src:"assets/common/images/sunLogo.png"+noCache},
            // sounds
            {id:Assets.sounds.shooter,src:typeSounds+"/shooter.ogg"+noCache},
            {id:Assets.sounds.shooter2,src:typeSounds+"/shooter2.ogg"+noCache},
            {id:Assets.sounds.insertCoin,src:typeSounds+"/coin_in.ogg"+noCache},
            {id:Assets.sounds.refund,src:typeSounds+"/refund.ogg"+noCache},
            {id:Assets.sounds.shooterStop,src:typeSounds+"/shootStop.ogg"+noCache},
            {id:Assets.sounds.btnUp,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDown,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDownDouble,src:typeSounds+"/btnDownDouble.ogg"+noCache},
            {id:Assets.sounds.bgMusicMachine,src:typeSounds+"/bgMusicMachine.ogg"+noCache},
            {id:Assets.sounds.win1,src:typeSounds+"/win1.ogg"+noCache},
            {id:Assets.sounds.error,src:typeSounds+"/error.ogg"+noCache},
            {id:Assets.sounds.info,src:typeSounds+"/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:typeSounds+"/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:typeSounds+"/exception.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:typeSounds+"/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.continuePlayingSpoken,src:typeSounds+"/do_we_continue_playing_01.ogg"+noCache},
            {id:Assets.sounds.byeSpoken,src:typeSounds+"/bye_01.ogg"+noCache},
            {id:Assets.sounds.youHaveWonSpoken,src:typeSounds+"/you_have_won_01.ogg"+noCache},
            {id:Assets.sounds.prizeDetect4,src:typeSounds+"/prize_detection4_01.ogg"+noCache},
            {id:Assets.sounds.shootCoinsSpoken,src:typeSounds+"/press_button_to_shoot_coins_01.ogg"+noCache},
            {id:Assets.sounds.keepGoingSpoken,src:typeSounds+"/keep_going_we_are_in_a_winners_mood_01.ogg"+noCache},
            {id:Assets.sounds.feelDizzySpoken,src:typeSounds+"/wauw_you_make_me_dizzy_01.ogg"+noCache},
            {id:Assets.sounds.reallyGoodSpoken,src:typeSounds+"/this_is_really_good_01.ogg"+noCache},
            {id:Assets.sounds.seeYouSpoken,src:typeSounds+"/see_you_later_01.ogg"+noCache},
            {id:Assets.sounds.prize_1,src:typeSounds+"/prize_1.ogg"+noCache},
            {id:Assets.sounds.prize_2,src:typeSounds+"/prize_2.ogg"+noCache},
            {id:Assets.sounds.prize_3,src:typeSounds+"/prize_3.ogg"+noCache},
            {id:Assets.sounds.prize_5,src:typeSounds+"/prize_5.ogg"+noCache},
            {id:Assets.sounds.prize_10,src:typeSounds+"/prize_10.ogg"+noCache},
            {id:Assets.sounds.prize_15,src:typeSounds+"/prize_15.ogg"+noCache},
            {id:Assets.sounds.prize_20,src:typeSounds+"/prize_20.ogg"+noCache},
            {id:Assets.sounds.prize_25,src:typeSounds+"/prize_25.ogg"+noCache},
            {id:Assets.sounds.prize_30,src:typeSounds+"/prize_30.ogg"+noCache},
            {id:Assets.sounds.prize_35,src:typeSounds+"/prize_35.ogg"+noCache},
            {id:Assets.sounds.prize_40,src:typeSounds+"/prize_40.ogg"+noCache},
            {id:Assets.sounds.prize_45,src:typeSounds+"/prize_45.ogg"+noCache},
            {id:Assets.sounds.prize_50,src:typeSounds+"/prize_50.ogg"+noCache},
            {id:Assets.sounds.prize_55,src:typeSounds+"/prize_55.ogg"+noCache},
            {id:Assets.sounds.prize_60,src:typeSounds+"/prize_60.ogg"+noCache},
            {id:Assets.sounds.prize_65,src:typeSounds+"/prize_65.ogg"+noCache},
            {id:Assets.sounds.prize_70,src:typeSounds+"/prize_70.ogg"+noCache},
            {id:Assets.sounds.prize_75,src:typeSounds+"/prize_75.ogg"+noCache},
            {id:Assets.sounds.prize_80,src:typeSounds+"/prize_80.ogg"+noCache},
            {id:Assets.sounds.prize_85,src:typeSounds+"/prize_85.ogg"+noCache},
            {id:Assets.sounds.prize_90,src:typeSounds+"/prize_90.ogg"+noCache},
            {id:Assets.sounds.prize_95,src:typeSounds+"/prize_95.ogg"+noCache},
            {id:Assets.sounds.prize_100,src:typeSounds+"/prize_100.ogg"+noCache},
            {id:Assets.sounds.bonusInc,src:typeSounds+"/extra_dot.ogg"+noCache},
            //{id:Assets.sounds.bonusSpinSound,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_1,src:typeSounds+"/super_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_2,src:typeSounds+"/yes_we_have_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusSpinLoop,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWord,src:typeSounds+"/bonusWord.ogg"+noCache}//my cut, does not exist

            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"+noCache},
             */

            //images big
            //{id:Assets.images.mainBackground,src:typeImages+"/mainBackground.jpg"+noCache},//1

            //sound
        ];

        var typeMethod = gameCode + "Assets";
        var typeAssets = CoinPusherAssets[typeMethod](typeImages,typeSounds);
        manifest = manifest.concat(typeAssets);
        return manifest;
    };

    CoinPusherAssets.ticketcircusAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache}
        ]
    };

    CoinPusherAssets.pearlpusherAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache}
        ]
    };

    CoinPusherAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/Technology-Bold.ttf",
            "css/impact.ttf",
            "css/Lato-Bold.ttf"
        ];
    };


    /*CoinPusherAssets.atlasImage = {};
    var aI = CoinPusherAssets.atlasImage = {};

    aI[Assets.atlasImage.ss] = "ss";*/

    CoinPusherAssets.texts = {};
    var t = CoinPusherAssets.texts["en"] = {};
    t[Assets.texts.stake] = "BET";
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.crd] = "Cash:";
    t[Assets.texts.bet] = "Bet:";
    t[Assets.texts.infoText1] = "With         coins/marbles fired bonus wheel feature will start and reward you with random prize.\nWhen bonus wheel feature is active playing buttons are disabled.";
    t[Assets.texts.infoText2] = "Use left/right arrows to move shooter.\nUse bet buttons to set your bet amount.\nFire number of coins/marbles set by bet using single/burst button.\nWhile dispensing coins/marbles, playing buttons are disabled.";
    t[Assets.texts.infoText3] = "COINS WON\nWith every metal coin\npushed over the edge\n you also win!  ";
    t[Assets.texts.infoText4] = "of non playing time is allowed\nAfter that your session will be terminated";

    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";

    //t = CoinPusherAssets.texts["sr"] = {};

    alteastream.CoinPusherAssets = CoinPusherAssets;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusherMobileAssets = function() {};
    var Assets = alteastream.Assets;

/*    CoinPusherMobileAssets.videoManifest = [
        'assets/sprite_sheets/drum.mp4'
    ];*/

    CoinPusherMobileAssets.count = 0;

    CoinPusherMobileAssets.loadManifest = function(lang,gameCode){
        Assets.setImageLang(lang);
        //var language = Assets.getImageLang();
        //var localePath = typeImages+"/locale/" + language + "/";
        var noCache = "?ver="+Assets.VERSION;

        var typeImages = "assets/"+gameCode+"/mobile/images";
        var typeSounds = "assets/"+gameCode+"/sounds";
        //return [
        var manifest =  [
            // Images
            //{id:Assets.images.bounsWonBgImage,src:typeImages+"/bounsWonBgImage.png"+noCache},
            //{id:Assets.images.bounsWonBgImageRing,src:typeImages+"/bounsWonBgImageRing.png"+noCache},
            //{id:Assets.images.wheelBg,src:typeImages+"/wheelBg.png"+noCache},
            //{id:Assets.images.wheel_8_bonus_fields_highlight,src:typeImages+"/wheel_8_bonus_fields_highlight.png"+noCache},
            //{id:Assets.images.lampOn,src:typeImages+"/lampOn.png"+noCache},
            //{id:Assets.images.lampOff,src:typeImages+"/lampOff.png"+noCache},
            //{id:Assets.images.arrow,src:typeImages+"/arrow.png"+noCache},
            {id:Assets.images.preloadingPoster, src:typeImages+"/webrtc.png"+noCache},
            //{id:Assets.images.metalCoin, src:typeImages+"/metalCoin.png"+noCache},
            {id:Assets.images.coinWin, src:typeImages+"/coinWin.png"+noCache},
            {id:Assets.images.chip_glow, src:typeImages+"/chips/chip_glow.png"+noCache},
            {id:Assets.images.chip_1, src:typeImages+"/chips/chip_1.png"+noCache},
            {id:Assets.images.chip_2, src:typeImages+"/chips/chip_2.png"+noCache},
            {id:Assets.images.chip_3, src:typeImages+"/chips/chip_3.png"+noCache},
            {id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache},
            {id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache},
            {id:Assets.images.chip_6, src:typeImages+"/chips/chip_6.png"+noCache},
            {id:Assets.images.chip_7, src:typeImages+"/chips/chip_7.png"+noCache},
            {id:Assets.images.chip_8, src:typeImages+"/chips/chip_8.png"+noCache},
            {id:Assets.images.chip_9, src:typeImages+"/chips/chip_9.png"+noCache},
            {id:Assets.images.chip_10, src:typeImages+"/chips/chip_10.png"+noCache},
            {id:Assets.images.chip_11, src:typeImages+"/chips/chip_11.png"+noCache},
            {id:Assets.images.chip_12, src:typeImages+"/chips/chip_12.png"+noCache},
            {id:Assets.images.chip_0, src:typeImages+"/chips/chip_0.png"+noCache},
            {id:Assets.images.chip_1_back, src:typeImages+"/chips/chip_1_back.png"+noCache},
            {id:Assets.images.chip_2_back, src:typeImages+"/chips/chip_2_back.png"+noCache},
            {id:Assets.images.chip_3_back, src:typeImages+"/chips/chip_3_back.png"+noCache},
            {id:Assets.images.chip_4_back, src:typeImages+"/chips/chip_4_back.png"+noCache},
            {id:Assets.images.chip_5_back, src:typeImages+"/chips/chip_5_back.png"+noCache},
            {id:Assets.images.chip_6_back, src:typeImages+"/chips/chip_6_back.png"+noCache},
            {id:Assets.images.chip_7_back, src:typeImages+"/chips/chip_7_back.png"+noCache},
            {id:Assets.images.chip_8_back, src:typeImages+"/chips/chip_8_back.png"+noCache},
            {id:Assets.images.chip_9_back, src:typeImages+"/chips/chip_9_back.png"+noCache},
            {id:Assets.images.chip_10_back, src:typeImages+"/chips/chip_10_back.png"+noCache},
            {id:Assets.images.chip_11_back, src:typeImages+"/chips/chip_11_back.png"+noCache},
            {id:Assets.images.chip_12_back, src:typeImages+"/chips/chip_12_back.png"+noCache},
            {id:Assets.images.chip_0_back, src:typeImages+"/chips/chip_0_back.png"+noCache},
            // btns
            // common
            //{id:Assets.images.poster,src:"assets/common/mobile/poster.jpg"+noCache},
            {id:Assets.images.btnHiddenMenu,src:"assets/common/mobile/buttons/hiddenmenu_btn.png"+noCache},
            {id:Assets.images.arrowButtonsBackground, src:"assets/common/mobile/control_arm_bg.png"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/common/mobile/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/common/mobile/buttons/infoSS.png"+noCache},
            {id:Assets.images.btnFixedBet, src:"assets/common/mobile/buttons/btnFixedBet.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/common/mobile/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/common/mobile/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.soundMachineOn,src:"assets/common/mobile/buttons/sound_icon_machine_default.png"+noCache},
            {id:Assets.images.soundMachineOff,src:"assets/common/mobile/buttons/sound_icon_machine_disabled.png"+noCache},
            {id:Assets.images.infoBackground,src:"assets/common/mobile/infoBackground.jpg"+noCache},
            {id:Assets.images.infoBackgroundNoBonus,src:"assets/common/mobile/infoBackgroundNoBonus.jpg"+noCache},
            {id:Assets.images.spinner, src:"assets/common/mobile/spinner.png"+noCache},
            {id:Assets.images.btnIncSS, src:"assets/common/mobile/buttons/count_btn_plus.png"+noCache},
            {id:Assets.images.btnDecSS, src:"assets/common/mobile/buttons/count_btn_minus.png"+noCache},
            {id:Assets.images.btnArrowLeftSS, src:"assets/common/mobile/buttons/left_btn.png"+noCache},
            {id:Assets.images.btnArrowRightSS, src:"assets/common/mobile/buttons/right_btn.png"+noCache},
            {id:Assets.images.btnMediumSS, src:"assets/common/mobile/buttons/middle_btn.png"+noCache},
            {id:Assets.images.btnBetView, src:"assets/common/mobile/buttons/switch_btn.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/common/mobile/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/common/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.liveStreamConnecting, src:"assets/common/mobile/liveStreamConnecting.png"+noCache},
            {id:Assets.images.metalCoin,src:"assets/common/mobile/metalCoin.png"+noCache},
            {id:Assets.images.sunLogo,src:"assets/common/mobile/sunLogo.png"+noCache},
            // sounds
            {id:Assets.sounds.shooter,src:typeSounds+"/shooter.ogg"+noCache},
            {id:Assets.sounds.shooter2,src:typeSounds+"/shooter2.ogg"+noCache},
            {id:Assets.sounds.insertCoin,src:typeSounds+"/coin_in.ogg"+noCache},
            {id:Assets.sounds.refund,src:typeSounds+"/refund.ogg"+noCache},
            {id:Assets.sounds.shooterStop,src:typeSounds+"/shootStop.ogg"+noCache},
            {id:Assets.sounds.btnUp,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDown,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDownDouble,src:typeSounds+"/btnDownDouble.ogg"+noCache},
            {id:Assets.sounds.bgMusicMachine,src:typeSounds+"/bgMusicMachine.ogg"+noCache},
            {id:Assets.sounds.win1,src:typeSounds+"/win1.ogg"+noCache},
            {id:Assets.sounds.error,src:typeSounds+"/error.ogg"+noCache},
            {id:Assets.sounds.info,src:typeSounds+"/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:typeSounds+"/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:typeSounds+"/exception.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:typeSounds+"/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.continuePlayingSpoken,src:typeSounds+"/do_we_continue_playing_01.ogg"+noCache},
            {id:Assets.sounds.byeSpoken,src:typeSounds+"/bye_01.ogg"+noCache},
            {id:Assets.sounds.youHaveWonSpoken,src:typeSounds+"/you_have_won_01.ogg"+noCache},
            {id:Assets.sounds.prizeDetect4,src:typeSounds+"/prize_detection4_01.ogg"+noCache},
            {id:Assets.sounds.shootCoinsSpoken,src:typeSounds+"/press_button_to_shoot_coins_01.ogg"+noCache},
            {id:Assets.sounds.keepGoingSpoken,src:typeSounds+"/keep_going_we_are_in_a_winners_mood_01.ogg"+noCache},
            {id:Assets.sounds.feelDizzySpoken,src:typeSounds+"/wauw_you_make_me_dizzy_01.ogg"+noCache},
            {id:Assets.sounds.reallyGoodSpoken,src:typeSounds+"/this_is_really_good_01.ogg"+noCache},
            {id:Assets.sounds.seeYouSpoken,src:typeSounds+"/see_you_later_01.ogg"+noCache},
            {id:Assets.sounds.prize_1,src:typeSounds+"/prize_1.ogg"+noCache},
            {id:Assets.sounds.prize_2,src:typeSounds+"/prize_2.ogg"+noCache},
            {id:Assets.sounds.prize_3,src:typeSounds+"/prize_3.ogg"+noCache},
            {id:Assets.sounds.prize_5,src:typeSounds+"/prize_5.ogg"+noCache},
            {id:Assets.sounds.prize_10,src:typeSounds+"/prize_10.ogg"+noCache},
            {id:Assets.sounds.prize_15,src:typeSounds+"/prize_15.ogg"+noCache},
            {id:Assets.sounds.prize_20,src:typeSounds+"/prize_20.ogg"+noCache},
            {id:Assets.sounds.prize_25,src:typeSounds+"/prize_25.ogg"+noCache},
            {id:Assets.sounds.prize_30,src:typeSounds+"/prize_30.ogg"+noCache},
            {id:Assets.sounds.prize_35,src:typeSounds+"/prize_35.ogg"+noCache},
            {id:Assets.sounds.prize_40,src:typeSounds+"/prize_40.ogg"+noCache},
            {id:Assets.sounds.prize_45,src:typeSounds+"/prize_45.ogg"+noCache},
            {id:Assets.sounds.prize_50,src:typeSounds+"/prize_50.ogg"+noCache},
            {id:Assets.sounds.prize_55,src:typeSounds+"/prize_55.ogg"+noCache},
            {id:Assets.sounds.prize_60,src:typeSounds+"/prize_60.ogg"+noCache},
            {id:Assets.sounds.prize_65,src:typeSounds+"/prize_65.ogg"+noCache},
            {id:Assets.sounds.prize_70,src:typeSounds+"/prize_70.ogg"+noCache},
            {id:Assets.sounds.prize_75,src:typeSounds+"/prize_75.ogg"+noCache},
            {id:Assets.sounds.prize_80,src:typeSounds+"/prize_80.ogg"+noCache},
            {id:Assets.sounds.prize_85,src:typeSounds+"/prize_85.ogg"+noCache},
            {id:Assets.sounds.prize_90,src:typeSounds+"/prize_90.ogg"+noCache},
            {id:Assets.sounds.prize_95,src:typeSounds+"/prize_95.ogg"+noCache},
            {id:Assets.sounds.prize_100,src:typeSounds+"/prize_100.ogg"+noCache},
            {id:Assets.sounds.bonusInc,src:typeSounds+"/extra_dot.ogg"+noCache},
            //{id:Assets.sounds.bonusSpinSound,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_1,src:typeSounds+"/super_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_2,src:typeSounds+"/yes_we_have_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusSpinLoop,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWord,src:typeSounds+"/bonusWord.ogg"+noCache}//my cut, does not exist

            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"+noCache},
             */

            //images big
            //{id:Assets.images.mainBackground,src:typeImages+"/mainBackground.jpg"+noCache},//1

            //sound
        ];

        var typeMethod = gameCode + "Assets";
        var typeAssets = CoinPusherMobileAssets[typeMethod](typeImages,typeSounds);
        manifest = manifest.concat(typeAssets);
        return manifest;
    };

    CoinPusherMobileAssets.ticketcircusAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache}
        ]
    };

    CoinPusherMobileAssets.pearlpusherAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache}
        ]
    };

    CoinPusherMobileAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/Technology-Bold.ttf",
            "css/impact.ttf",
            "css/Lato-Bold.ttf"
        ];
    };


    /*CoinPusherMobileAssets.atlasImage = {};
    var aI = CoinPusherMobileAssets.atlasImage = {};

    aI[Assets.atlasImage.ss] = "ss";*/

    CoinPusherMobileAssets.texts = {};
    var t = CoinPusherMobileAssets.texts["en"] = {};
    t[Assets.texts.stake] = "BET";
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.crd] = "Cash:";
    t[Assets.texts.bet] = "Bet:";
    t[Assets.texts.infoText1] = "With         coins/marbles fired bonus wheel feature will start and reward you with random prize.\nWhen bonus wheel feature is active playing buttons are disabled.";
    t[Assets.texts.infoText2] = "Use left/right arrows to move shooter.\nUse bet buttons to set your bet amount.\nFire number of coins/marbles set by bet using single/burst button.\nWhile dispensing coins/marbles, playing buttons are disabled.";
    t[Assets.texts.infoText3] = "COINS WON\nWith every metal coin\npushed over the edge\n you also win!  ";
    t[Assets.texts.infoText4] = "of non playing time is allowed\nAfter that your session will be terminated";
    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";

    //t = CoinPusherMobileAssets.texts["sr"] = {};

    alteastream.CoinPusherMobileAssets = CoinPusherMobileAssets;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponentMachine = function(stage, canv) {
    this.initialize(stage, canv);
}
var p = PreloaderComponentMachine.prototype = new alteastream.PreloaderComponent();

    // static properties:
    // events:
    // public properties:
    // private properties:
    // constructor:
    p.PreloaderComponent_initialize = p.initialize;
    p.initialize = function(stage, canv) {
        this.PreloaderComponent_initialize(stage);

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRect(0, 0, canv.width, canv.height);
        shape.cache(0, 0, canv.width, canv.height);
        this.addChild(shape);
    };

    // static methods:
    // public methods:
    // private methods:
    p._getSaturation = function() {
        return (this.getProgress() - 1) * 100;
    };

alteastream.PreloaderComponentMachine = PreloaderComponentMachine;
})();/*http://139.59.147.230:82/games/rules*/
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusher = function() {
        this.AbstractScene_constructor();
        this.initialize_CoinPusher();
    };
    var p = createjs.extend(CoinPusher, alteastream.AbstractScene);
    // static properties:
    // events:
    var _instance = null;
    // private properties:
    p.active = false;
    p._info = null;
    p._stakeIncrements = [];
    p._selectedStake = 0;
    p._refundCoinsCounter = 0;
    p._refundAnimationInterval = null;
    // public properties:
    p.bonusWon = 0;
    p.bonusActive = false;
    p.hasBonus = false;
    p.minStake = 0;
    p.maxStake = 0;
    p.stakeInc = 0;
    p.credit = 0;
    p.betManager = null;
    p.prizeQueue = [];
    p.queueCount = 0;
    p.queuePlaying = false;
    p.okToShowBalance = true;
    p.currentTokensCounted = 0;
    p.currentRefundValue = 0;
    p.autoCounter = 0;
    p.coinsCounter = null;
    p.prizeCounter = null;
    p.prizeWinLabel = null;
    p.prizeNumber = null;
    p.prizeEurNumber = null;
    p.currentPrize = 0;
    p.currentPrizeEur = 0;
    p.kickOutTimer = null;

    // constructor:
    p.initialize_CoinPusher = function() {
        stage.enableMouseOver(0);
        _instance = this;

        this._local_activate();// local ver
        this._callInitMethods();

        console.log("initialize_CoinPusher..");
    };
    // static methods
    CoinPusher.getInstance = function(){
        return _instance;
    };
    // private methods
    // new socket
    p._callInitMethods = function() {
        alteastream.Assets.setMute(true);
        this.setHttpCommunication();
        this.setSocketCommunication("Game");

        alteastream.Requester.getInstance().startGameStream(function(response){// todo OVAJ RESPONSE JE machineParameters
            _instance.machineParameters = response;
            _instance.socketCommunicator.activate(_instance.machineParameters);
        });

        /*var machineParams = window.localStorage.getItem('machineParameters');
        this.machineParameters = JSON.parse(machineParams);*/

        this._setControlBoard();
        this._addInfo();
        //this.setQueueInfo();
        //this.setCoinCounter();
        this.setPrizeWinCounter();
        //this.addGiveUpButton(true);//giveUpButton
        this.addControlBoard();// samo 
        this.monitorNetworkStatus();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);

        this.setOverlay();
        //this.setMachineNameText(); // show machine name , uncomment
    };

    p.setOverlay = function() { //todo mozda zvati ovo iz communicatora u CONFIRM_GAME_INIT umestop showGame
        _instance.controlBoard.mouseEnabled = false;
        _instance.showGame(false);

        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var bg = new createjs.Shape();
        bg.graphics.beginFill("#000000").drawRect(0, 0, w, h);

        var qFont = "90px Lato";
        var qY = 125;
        var question = new createjs.Text("Ready To Play?",qFont,"#ffffff").set({x:w/2, y:qY, textAlign:"center",textBaseline:"middle"});

        var buttonFont = "bold 20px Lato";
        var buttonFontY = 65;
        var buttonYCorrector = 145;
        var txt = new createjs.Text("PLAY NOW",buttonFont,"#fcfcfc");
        var playButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,txt);
        playButton.centerText(0,buttonFontY);
        playButton.x = w/2;
        playButton.y = h - buttonYCorrector;

        var overlayClickCallback = function () {
            //_instance.streamVideo.videoElement.setMuted(false);
            //_instance.streamVideo.videoElement.setVolume(0.1);
            //_instance.streamVideo.videoElement.play();
            //_instance.streamVideo.setMedia();
            _instance.controlBoard.mouseEnabled = true;
            _instance.showGame(true);
            _instance.activateFullscreen();
        };

        var overlayOptions = {
            background:bg,
            button:playButton,
            graphics:[],
            texts:[question],
            clickCallback:overlayClickCallback
        };

        this.setStartOverlay(overlayOptions);
    };

    p.activateFullscreen = function () {
        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        _instance[method]();
    };

    p.normalBrowsersFullScreenToggle = function(e) {// CHROME, OPERA, FIREFOX
        var element = window.parent.document.documentElement;
        if(!window.parent.document.fullscreenElement){
            element.requestFullscreen();
        }else{
            window.parent.document.exitFullscreen();
        }
    };

    p.otherBrowsersFullScreenToggle = function(e) {// EDGE, SAFARI
        var element = window.parent.document.documentElement;
        if(!window.parent.document.webkitFullscreenElement){ // ovde je drugacija provera posto ovi browseri izadju iz full moda kada krene da se ucitava masina
            element.webkitRequestFullScreen();
        }else{
            window.parent.document.webkitExitFullscreen();
        }
    };

    p.fullScreenManager = function(){};

    p._addInfo = function() {
        this._info = new alteastream.CoinPusherInfo();
        this._info.visible = false;
        this.addChild(this._info);
    };

    p._setInfo = function() {
        this._info.setInfo(this.payoutManager, this.hasBonus, this.betManager.getMultiplier());
        if(this.hasBonus === true){
            this._info.setBonusInfo(this.powerUp.neededForBonus)
        }
        var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && refundValue > 0){
        //if(refundValue > 0){
            //var refundValue = (Math.round(((this.payoutManager.getRefundValue() * 100)/100) * 1e12) / 1e12).toFixed(3);
            //var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
            this._info.setCountedTokensInfo(refundValue);//param metal coinvalue + slicica
        }else{
            this._info.setSunLogo();
        }
        this._info.setMaxStakeInfo(this.maxStake);
        this._info.setNonPlayingTimeInfo(this.controlIdleTime);//param from response
        //this._info.setKeyboardLabels();
    };

    p._switchInfoAndWheelChildIndexes = function() {
        this.swapChildren(this._info, this.bonusContainer);
    };

/*    p._setStreamVideo = function(address){
        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideo = new alteastream.CoinPusherStream(this,options,address);
        //this.streamVideo = new alteastream.VideoStream(this,"videoOutput",address);//old
    };*/

    p._setControlBoard = function(){
        var stakeIncrements = [0, 0, 0, 0, 0, 0, 0];
        this.setStakeIncrements(stakeIncrements);
        this.setBetTotal();

        this.controlBoard = new alteastream.CoinPusherControlBoard();
        this.controlBoard.plugin(this);
        this.controlBoard.initialSetup();

        /*var isMobile = localStorage.getItem("isMobile"); // verzija ako hocemo da se ne instancira keyboard listener za mobilni
        if(isMobile === "not"){
            var controller = new alteastream.Controller(this);
            controller.activate();
            this.controlBoard.twinComponent = controller;
        }*/
    };

    p._setBonusApplet = function(options){
        this.hasBonus = true;
        this.bonusContainer = new createjs.Container();

        var wheel = this.bonusWheel = new alteastream.PusherBonusWheel(this,options.bonus);
        this.bonusContainer.addChild(wheel);

        var highlightImg = "wheel_" + wheel.bonusesReceived.length * 2 + "_bonus_fields_highlight";
        var bonusHighlightField = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images[highlightImg]);
        bonusHighlightField.regX = bonusHighlightField.image.width * 0.5;
        bonusHighlightField.regY = bonusHighlightField.image.height;
        bonusHighlightField.visible = false;
        this.bonusContainer.addChild(bonusHighlightField);

        var ring = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImageRing);
        this.bonusContainer.addChild(ring);
        ring.regX = ring.image.width*0.5;
        ring.regY = ring.image.width*0.5;
        ring.mouseEnabled = false;

        var powerUp = this.powerUp = new alteastream.PowerUpWheel(this);
        this.bonusContainer.addChild(powerUp);
        var radius = 366;
        powerUp.build("lampOff","lampOn",radius,options.bonus_limit);

        var bounsWonBgImage = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImage);
        this.bonusContainer.addChild(bounsWonBgImage);
        bounsWonBgImage.regX = bounsWonBgImage.image.width*0.5;
        bounsWonBgImage.regY = bounsWonBgImage.image.height*0.5;

        var bonusWonText = this.bonusWonText = new createjs.Text("", '96px Technology', "#ed2224").set({textBaseline:"middle",textAlign:"right"});
        this.bonusContainer.addChild(bonusWonText);
        bonusWonText.x = 66;
        bonusWonText.y = 9;

        var marker = this.marker = alteastream.Assets.getImage(alteastream.Assets.images.arrow);
        this.bonusContainer.addChild(marker);
        marker.regX = marker.image.width*0.5;
        marker.x = 0;
        marker.y = -420;
        marker.mouseEnabled = false;

        this.addChild(this.bonusContainer);
        this.bonusContainer.x = this.bonusContainer.startX = 250;
        this.bonusContainer.y = this.bonusContainer.startY = 250;
        this.bonusContainer.xPosForAnimation = 400;
        this.bonusContainer.yPosForAnimation = 400;
        this.bonusContainer.xPosForInfo = 960;
        this.bonusContainer.yPosForInfo = 230;
        this.bonusContainer.mouseEnabled = false;
        this.bonusContainer.scaleX = this.bonusContainer.scaleY = 0.5;

        this.bonusWheel.setSkin(function () {
            //_instance.streamVideo.activate(function(){_instance.showGame(true);});
        });

        // new stream load
        /*this.bonusWheel.setSkin(function () {
            _instance.socketCommunicator.confirmGameInit();
        });*/
    };

    p._bonusCoinSpawn = function(n){
        var cnt = 0;
        for(var i = 0; i < n; ++i) {
            setTimeout(function(){
                var coin = new alteastream.SpringAnim('coinWin');
                coin.x = _instance.bonusContainer.xPosForAnimation;
                coin.y = _instance.bonusContainer.yPosForAnimation;
                _instance.addChild(coin);

                coin.setVR(-4,4);
                coin.setVX(-4,4);
                coin.setVY(-15,-30);
                coin.dispose = function(){
                    coin.removeAllChildren();
                    _instance.removeChild(coin);
                    cnt++;
                    if(cnt === n){
                        _instance.endBonusFeature();
                    }
                };
                coin.animate();
                alteastream.Assets.playSound("bonusInc",0.3);
            },i*100);
        }
    };

    p._coinSpawnAnimation = function(){
        var coin = new alteastream.SpringAnim('coinWin');
        coin.x = _instance.coinsCounter.x+_instance.coinsCounter.pivot;
        coin.y = alteastream.AbstractScene.GAME_HEIGHT+coin.pivot;
        _instance.addChild(coin);
        _instance._coinSpawnAnimationParams(coin);
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(1.5);
        coin.setVR(-10,10);
        coin.setVX(-1.5,1.5);
        coin.setVY(-28,-32);
        coin.animate();
    };

    // new stream load
    p._checkIfBonusGame = function(){
        if(Object.keys(_instance.machineParameters.bonus).length === 0){
            _instance.socketCommunicator.confirmGameInit();
        }else{
            _instance._setBonusApplet(_instance.machineParameters);
        }
    };

    p._setBonuswheelPositionAndForInfo = function(infoIsActive) {
        if(infoIsActive === true){
            this.bonusContainer.x = this.bonusContainer.xPosForInfo;
            this.bonusContainer.y = this.bonusContainer.yPosForInfo;
        }else{
            this.bonusContainer.x = this.bonusContainer.startX;
            this.bonusContainer.y = this.bonusContainer.startY;
        }
    };

    p._highlightCoinsWin = function(bool){// just a temp marker for some highlights
        this.coinsCounter.getChildAt(0).alpha =
            this.controlBoard.balanceBackground.alpha = bool === true? 1: 0.7;
    };

    p._highlightPrizeWin = function(bool){// just a temp marker for some highlights
        this.prizeCounter.getChildAt(0).alpha =
            this.controlBoard.balanceBackground.alpha = bool === true? 1: 0.7;
    };

    p._resetTimeout = function(){
        clearTimeout(this.timer);
        this.timer = setTimeout(function(){
                _instance.currentTokensCounted =
                    _instance.currentRefundValue = 0;
                _instance.eurNumber.text =
                    _instance.coinsNumber.text = "0.00";
                _instance._highlightCoinsWin(false);
            }
            ,1200);//adjustable
    };
    // public methods

    // show machine name , replace current methods with these methods
/*    p.setQueueInfo = function(){
        var queueInfoContainer = this.queueInfoContainer = new createjs.Container();
        var font = 'bold 16px HurmeGeometricSans3';
        var white = "#ffffff";

        var queueInfoBg  = new createjs.Shape();
        queueInfoBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 44, 5);
        queueInfoBg.cache(0, 0, 190, 44);

        var queueInfoText = this.queueInfoText = new createjs.Text("CO-PLAY: 0", font, white).set({textBaseline:"middle",textAlign:"center"});
        queueInfoText.x = 95;
        queueInfoText.y = 23;

        queueInfoContainer.addChild(queueInfoBg, queueInfoText);
        this.addChild(queueInfoContainer);
        queueInfoContainer.x = 260;
        queueInfoContainer.y = 10;
        queueInfoContainer.mouseEnabled = false;
        queueInfoContainer.mouseChildren = false;
        queueInfoContainer.visible = false;
    };

    p.updateQueueInfo = function(queue){
        this.queueInfoText.text = "CO-PLAY: " + queue || "CO-PLAY: 0";
    };*/

        // show machine name , uncomment
/*    p.setMachineNameText = function() {
        var name = alteastream.AbstractScene.MACHINE_NAME;
        var machineNameContainer = new createjs.Container();
        var font = 'bold 16px HurmeGeometricSans3';
        var white = "#ffffff";

        var machineNameBg  = new createjs.Shape();
        machineNameBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 230, 44, 5);
        machineNameBg.cache(0, 0, 230, 44);
        var nameText = this.machineNameText = new createjs.Text("MACHINE NAME: " + name, font, white).set({textBaseline:"middle",textAlign:"center"});
        nameText.x = 115;
        nameText.y = 23;

        machineNameContainer.addChild(machineNameBg, nameText);
        this.addChild(machineNameContainer);
        machineNameContainer.x = 20;
        machineNameContainer.y = 10;
        machineNameContainer.mouseEnabled = false;
        machineNameContainer.mouseChildren = false;
    };*/

    p.showNetworkStatus = function(){
        var status = navigator.onLine? "online" : "offline";
        _instance.throwAlert(alteastream.Alert.INFO,"You are "+status,_instance.socketCommunicator.tryReconnect(status));
    };

    p.setMonetaryValueView = function(isEur){
        this.controlBoard.updateTextColorType(isEur);
        //new stakes
        this.controlBoard.fixedStakesPanel.updateMonetary(isEur);
        this.updateInfoPrizes(isEur);
        this.updateCountedTokensView(isEur);
        this.updatePrizesView(isEur);
        if(this.hasBonus === true){
            this.updateWheelPrizes(isEur);
        }
    };

/*    p.addGiveUpButton = function(bool){
        if(bool === true){
            var giveUpButton = this.giveUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
            var width = giveUpButton.width;
            var spacing = width/2;
            giveUpButton.x = alteastream.AbstractScene.GAME_WIDTH - width - spacing;
            giveUpButton.y = 10;
            this.addChild(giveUpButton);

            //giveUpButton.setClickHandler(_instance.quitBtnHandler);// todo proveriti
            giveUpButton.setClickHandler(function () {
                _instance.streamVideo.stop();
                _instance.exitMachineGameplay();
            });
/!*            giveUpButton.addEventListener("click", function () {
                _instance.quitBtnHandler();
                //_instance.streamVideo.stop();
                //_instance.exitMachineGameplay();// nije otvorio soket, ne moze da dobije 85, token itd..mozda http poziv u ovom slucaju
            });*!/
        }else{
            this.removeChild(this.giveUpButton);
            this.giveUpButton = null;
        }
    };*/

    /*    p._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
            return localStorage.getItem("isMobile");
        };*/

    p.bonusFocusIn = function(bool,callback){
        if(bool === true){
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut,x:this.bonusContainer.xPosForAnimation,y:this.bonusContainer.yPosForAnimation,scaleX:0.7,scaleY:0.7,onComplete:function(){
                    callback();
                }});
        }else{
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut, x:this.bonusContainer.startX,y:this.bonusContainer.startY,scaleX:0.5,scaleY:0.5,onComplete:function(){
                    callback();
                }});
        }
    };

    p.addControlBoard = function(){
        this.addChild(this.controlBoard);
        this.controlBoard.visible = false;
    };

    //old socket
    /*p.onSocketConnect = function(frame){ // invalid token fix new
        _instance.socketCommunicator.onGameConnect(frame);
        var response = window.localStorage.getItem('machineParameters');
        var machineResponse = JSON.parse(response);
        _instance.socketCommunicator.startGame(machineResponse);
        _instance._setStreamVideo(machineResponse);
        _instance.setGameParameters(machineResponse);
        _instance.setPayout(machineResponse.payout);
        _instance.setRegularStakes();
        _instance.setCurrency();
        if(Object.keys(machineResponse.bonus).length === 0){
            _instance.streamVideo.activate(function(){_instance.showGame(true);});
        }else{
            _instance._setBonusApplet(machineResponse);
        }
        _instance._setInfo();
        _instance.showGame(false);
        _instance.setRenderer();
        window.localStorage.removeItem('machineParameters');

        var canvas = document.getElementById("screen");
        canvas.focus();
        if(document.activeElement !== canvas)
            canvas.focus();
    };*/

    //new socket
    p.onSocketConnect = function(frame){ // invalid token fix new
        _instance.socketCommunicator.onGameConnect(frame);

        var machineParams = _instance.machineParameters;

        //_instance._setStreamVideo(machineParams);// new stream load comment
        _instance.setGameParameters(machineParams);
        _instance.setPayout(machineParams.payout);
        _instance.setCoinCounter();
        _instance.setRegularStakes();
        //new stakes
        _instance.setFixedStakes();
        _instance.setCurrency();
        _instance.setMonetaryValueView(_instance.controlBoard._toValueView);

        //_instance.checkStreamLoad();// new stream load uncomment

        // new stream load comment block start
/*        if(Object.keys(machineParams.bonus).length === 0){
            _instance.streamVideo.activate(function(){
                _instance.socketCommunicator.confirmGameInit();
            });
        }else{
            _instance._setBonusApplet(machineParams);
        }*/
        // new stream load comment block end

        _instance._setInfo();
        _instance.showGame(false);
        _instance.setRenderer();
        _instance.socketCommunicator.confirmGameInit();

        //window.localStorage.removeItem('machineParameters');

        var canvas = document.getElementById("screen");
        canvas.focus();
        if(document.activeElement !== canvas)
            canvas.focus();
    };
    // new stream load
/*    p.checkStreamLoad = function(){
        if(alteastream.VideoStream.LOADED === true){
            alteastream.VideoStreamPreload.enableSounds();
            _instance._checkIfBonusGame();
        }
        else{
            alteastream.VideoStreamPreload.showPreload(true);
            var loadIntervalCheck = setInterval(function(){
                if(alteastream.VideoStream.LOADED === true){
                    clearInterval(loadIntervalCheck);
                    alteastream.VideoStreamPreload.enableSounds();
                    _instance._checkIfBonusGame();
                }
            },500);
        }
    };*/

    p.showGame = function(bool){
        if(this.hasBonus === true){
            this.bonusContainer.visible = bool;
        }

        if(!this.betManager.canBet(this.credit,this.getSelectedStake())){
            this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance");
        }
/*
        if(bool === true){
            //this.addGiveUpButton(false);
            //temp reminder::
            if(!this.betManager.canBet(this.credit,this.getSelectedStake())){
                this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance");
            }

            if(is.safari()){
                // var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                // if(is.not.ipad() && is.not.ios() && !isiPadOS){
                //     this.streamVideo.setMedia();
                //     alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
                // }
            }else{
                //this.streamVideo.setMedia();
                alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
            }

            var isMobile = localStorage.getItem("isMobile"); // verzija ako hocemo da se ne instancira keyboard listener za mobilni
            if(isMobile === "not"){
                var controller = new alteastream.Controller(this);
                controller.activate();
                this.controlBoard.twinComponent = controller;
            }
        }*/

        this.controlBoard.visible = bool;
        //this.queueInfoContainer.visible = bool;
        this.prizeCounter.visible = bool;
        if(this.coinsCounter)
            this.coinsCounter.visible = bool;
    };

    p.monitorIdleTime = function(bool){// todo OVO MOZDA NEMA LOGIKE DA POSTOJI
        if(bool === true){
            this._kickOutCount = 0;
            clearInterval(this.kickOutTimer);
            this.kickOutTimer = setInterval(function(){
                _instance._kickOutCount++;
                if(_instance._kickOutCount<3){
                    _instance.inactivityWarning(_instance._kickOutCount);
                }
                else{
                    _instance._kickOutCount = 0;
                    clearInterval(_instance.kickOutTimer);
                }
            },this._kickOutPing);
        }else{
            this._kickOutCount = 0;
            clearInterval(this.kickOutTimer);
        }
    }

    p.setPayout = function(response){ // todo temp creating cleanPayouts, response should give us cleaned array
        var tags = Object.keys(response);
        var cleanPayouts = {};
        for(var i = 0; i < tags.length; i++){
            var tag = tags[i];
            var prize = response[tag];

            if(prize !== 0 && tag !== "101"){
                cleanPayouts[tag] = prize;
            }
        }
        //var refund = response["101"] || 0.25;
        var refund = response["101"] || 0;
        refund = refund*this.betManager.getMultiplier();
        this.payoutManager = new alteastream.PayoutManager(cleanPayouts,refund);
    };

    p.onInfo = function(){
        this._info.visible = !this._info.visible;
        this.controlBoard.hideControlls(this._info.visible);
        if(this.hasBonus === true){
            this._setBonuswheelPositionAndForInfo(this._info.visible);
        }

        //this.queueInfoContainer.visible = !this._info.visible;

        alteastream.Assets.playSound("btnDownDouble");
    };

    p.checkIfinfoIsActive = function() {
        if(this._info.visible === true){
            this.onInfo();
        }
    };

    p.onPrizeDetection = function(msgValue){
        var amount = this.payoutManager.getPayoutByPrize(msgValue.prize);
        this.okToShowBalance = false;
        this.setCrd(msgValue.balance);
        this.showWin(amount,msgValue);
    };

/*    p.onQuit = function(){
        //alteastream.VideoStreamPreload.stop();// new stream load uncomment
        //alteastream.VideoStreamPreload.removeVideoElement();// new stream load uncomment
        //this.streamVideo.stop();// new stream load comment
        this.socketCommunicator.quitPlay();
        //moved to SocketCommunicatorGame
        //this.killGame();
        //this.closeGame();
    };*/

/*    p.exitMachineGameplay = function(){
        this.killGame();
        this.closeGame();
    };*/

    //old socket
    /*p.closeGame = function(){
        this.requester.backToLobby(function (res) {
            window.top.manageBgMusic("pause");
            window.top.stopHelperSound("machineNoise");
            //window.localStorage.setItem('backgroundMusicIsMuted', String(_instance.controlBoard.soundToggled));
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
            window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+_instance.socketCommunicator.currentGameUid);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        });
    };*/

    //new socket
/*    p.closeGame = function(){
        window.parent.manageBgMusic("pause");
        //window.parent.stopHelperSound("machineNoise");
        //window.localStorage.setItem('backgroundMusicIsMuted', String(_instance.controlBoard.soundToggled));
        window.parent.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
    };*/

    p.fillToBonus = function(count){
        this.powerUp.fill(count);
    };

    p.onBonusWon = function(msg){
        var bal = msg.vl.balance - msg.vl.money;
        this.setCrd(bal);
        this.okToShowBalance = false;
        this.onBonusAllowed(msg.vl.amount);
    };

    p.onBonusAllowed = function(amount){
        console.log("onBonusAllowed");
        this.checkIfinfoIsActive();
        this.controlBoard.disablePlayButtons(true);
        this.bonusWon = amount;
        this.bonusActive = true;

        this.powerUp.fillAll();
        this.spinForBonus();
    };

    p.spinForBonus = function(){
        this.powerUp.lightsAnimation(false);
        alteastream.Assets.playSound("bonusWinnerSpoken_1",0.3);
        this.bonusFocusIn(true,function(){
            _instance.powerUp.rotationAnimation(true);
            _instance.bonusWheel.spin(_instance.bonusWon);
            alteastream.Assets.playSoundChannelWithOptions("bonusSpinLoop", {loop:-1,vol:0.2});
        });
    };

    p.onBonusPlayed = function(){
        console.log("onBonusPlayed");
        this.bonusHighlightField.visible = true;
        this.bonusActive = false;
        this.bonusWonText.visible = true;
        this.bonusWonText.text = this.bonusWon;
        this.powerUp.rotationAnimation(false);
        this.powerUp.lightsAnimation(true);
        alteastream.Assets.playSound("win1",0.3);
        var bonusWonSpoken = "prize_"+this.bonusWon;
        var appendixWord = "bonusWord";// temp
        this.playSoundChain("youHaveWonSpoken",0.3,function(){
            _instance.playSoundChain(bonusWonSpoken,0.3,function(){
                _instance.playSoundChain(appendixWord,0.3,function(){
                    _instance.powerUp.lightsAnimation(false);
                    _instance._bonusCoinSpawn(_instance.bonusWon);
                });
            });
        });
    };

    p.endBonusFeature = function(){
        this.bonusFocusIn(false,function(){
            _instance.okToShowBalance = true;
            _instance.bonusWonText.text = "";
            _instance.bonusWonText.visible = false;
            _instance.bonusHighlightField.visible = false;
            _instance.bonusWheel.highlightWin(false);
            _instance.powerUp.clear();
            _instance.controlBoard.disablePlayButtons(false);
            _instance.controlBoard.setCrd(_instance.credit);
            _instance.bonusWon = 0;
            alteastream.Assets.stopSoundChannel("bonusSpinLoop");
            alteastream.Assets.playSound("win1",0.3);
            console.log("BONUS END");
        });
    };

/*    p.setQueueInfo = function(){
        var queueInfoContainer = this.queueInfoContainer = new createjs.Container();
        queueInfoContainer.x = 30;
        queueInfoContainer.y = 30;

        var font = '16px Lato';

        var queueInfoBg = this.queueInfoBackground = new createjs.Shape();
        queueInfoBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 140, 45, 10);
        queueInfoBg.alpha = 0.7;
        queueInfoBg.cache(0, 0, 140, 45);

        var queueInfoLabel = this.queueInfoLabel = new createjs.Text("CO-PLAY: ", font, "#ffffff").set({textBaseline:"middle",textAlign:"right"});
        var queueInfoText = this.queueInfoText = new createjs.Text("0", font, "#ffd500").set({textBaseline:"middle",textAlign:"left"});
        queueInfoText.x = queueInfoLabel.x = 102;
        queueInfoText.y = queueInfoLabel.y = 24;

        queueInfoContainer.addChild(queueInfoBg, queueInfoLabel,queueInfoText);
        this.addChild(queueInfoContainer);
        queueInfoContainer.mouseEnabled = false;
        queueInfoContainer.mouseChildren = false;
        queueInfoContainer.visible = false;
    };*/

/*    p.updateQueueInfo = function(queue){
        this.queueInfoText.text = queue || 0;
    };*/

    p.setCoinCounter = function(){
        //if(alteastream.AbstractScene.GAME_TYPE === "pearlpusher")
            //return;
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && this.payoutManager.getRefundValue() > 0){
            this.currentTokensCounted = 0;
            this.currentRefundValue = 0;
            var container = this.coinsCounter = new createjs.Container();
            var font = '16px Lato';
            var yellow = "#ffd500";
            var green = "#00ff00";

            //var back = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images.refundComponentBackground);
            var back = new createjs.Shape();
            back.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 67, 10);
            back.alpha = 0.7;
            back.cache(0, 0, 190, 67);

            var yPos = 35;

            var coinsChargedLabel = this.coinsChargedLabel = new createjs.Text('COINS WIN:', font, "#ffffff").set({textBaseline:"middle",textAlign:"left"});
            coinsChargedLabel.x = 32;
            coinsChargedLabel.y = yPos;

            var coinsNumber  = this.coinsNumber = new createjs.Text("0.00", font, yellow).set({textBaseline:"middle",textAlign:"left"});
            coinsNumber.x = coinsChargedLabel.x + 95;//128
            coinsNumber.y = yPos;

            var eurNumber = this.eurNumber = new createjs.Text("0.00", font, green).set({textBaseline:"middle",textAlign:"left"});
            eurNumber.x = coinsChargedLabel.x + 95;
            eurNumber.y = yPos;
            eurNumber.visible = false;

            container.addChild(back,coinsChargedLabel,coinsNumber, eurNumber);
            this.addChild(container);
            container.x = 337;
            container.y = 987;
            container.pivot = (back.bitmapCache.width*0.5);
            container.mouseEnabled = false;
            container.mouseChildren = false;
            container.visible = false;
        }
    };

    p.setPrizeWinCounter = function() {
        var container = this.prizeCounter = new createjs.Container();
        var font = '16px Lato';
        var yellow = "#ffd500";
        var green = "#00ff00";

        //var back = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images.refundComponentBackground);
        var back = new createjs.Shape();
        back.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 67, 10);
        back.alpha = 0.7;
        back.cache(0, 0, 190, 67);

        var yPos = 35;
        var xCorr = 87;

        var prizeWinLabel = this.prizeWinLabel = new createjs.Text('PRIZE WIN:', font, "#ffffff").set({textBaseline:"middle",textAlign:"left"});
        prizeWinLabel.x = 34;
        prizeWinLabel.y = yPos;

        var prizeNumber  = this.prizeNumber = new createjs.Text("0.00", font, yellow).set({textBaseline:"middle",textAlign:"left"});
        prizeNumber.x = prizeWinLabel.x + 91;
        prizeNumber.y = yPos;

        var prizeEurNumber = this.prizeEurNumber = new createjs.Text("0.00", font, green).set({textBaseline:"middle",textAlign:"left"});
        prizeEurNumber.x = prizeWinLabel.x + 91;
        prizeEurNumber.y = yPos;
        prizeEurNumber.visible = false;

        container.addChild(back, prizeWinLabel,prizeNumber, prizeEurNumber);
        this.addChild(container);
        container.x = 1497- xCorr;
        container.y = 987;
        container.pivot = (back.bitmapCache.width*0.5);
        container.mouseEnabled = false;
        container.mouseChildren = false;
        container.visible = false;
    };

    p.showPrize = function(coins) {
        this._highlightPrizeWin(true);
        this.currentPrize += coins;
        this.currentPrizeEur += coins * this.betManager.getMultiplier();

        this.prizeNumber.text = this.currentPrize;
        var rounderPrizeInEur = gamecore.Utils.NUMBER.roundDecimal(this.currentPrizeEur).toFixed(2);
        this.prizeEurNumber.text = rounderPrizeInEur;
        this.prizeEurNumber.text += gamecore.Utils.NUMBER.addZeros(rounderPrizeInEur,1);

        this.resetPrize();
    };

    p.resetPrize = function() {
        clearTimeout(this.clearPrizeTimer);
        this.clearPrizeTimer = setTimeout(function(){
            _instance.currentPrize =
                _instance.currentPrizeEur = 0;
            _instance.prizeNumber.text = 0;
            _instance.prizeEurNumber.text = "0.00";
            _instance._highlightPrizeWin(false);
        },2000);
    };

    p.refundCoinSpawn = function (tokens) {
        this._refundCoinsCounter += tokens;
        this._highlightCoinsWin(true);
        var refundTokenValue = this.payoutManager.getRefundValue();
        if(this._refundAnimationInterval === null) {
            this._refundAnimationInterval = setInterval(function () {
                console.log("cs");
                _instance._refundCoinsCounter--;
                _instance._coinSpawnAnimation();
                _instance.playSound("refund", 0.8);

                // (0.0015).toFixed(2) = 0.00
                // (0.0015).toFixed(3) = 0.002

                //var coinsValue = gamecore.Utils.NUMBER.floorDecimal(refundTokenValue * (_instance.betManager.getMultiplier() * 100)).toFixed(2);
                //var refundValue = gamecore.Utils.NUMBER.roundDecimal((refundTokenValue * 100) / 100).toFixed(2);

                var coinsValue = gamecore.Utils.NUMBER.floorDecimal(refundTokenValue * (_instance.betManager.getMultiplier() * 100)).toFixed(3);
                var refundValue = gamecore.Utils.NUMBER.roundDecimal((refundTokenValue * 100) / 100).toFixed(3);

                _instance.currentTokensCounted = gamecore.Utils.NUMBER.roundDecimal(_instance.currentTokensCounted + Number(coinsValue));
                _instance.currentRefundValue = gamecore.Utils.NUMBER.roundDecimal(_instance.currentRefundValue + Number(refundValue));

                _instance.coinsNumber.text = _instance.currentTokensCounted + gamecore.Utils.NUMBER.addZeros(_instance.currentTokensCounted);
                _instance.eurNumber.text = _instance.currentRefundValue + gamecore.Utils.NUMBER.addZeros(_instance.currentRefundValue);

                if (_instance._refundCoinsCounter === 0) {
                    clearInterval(_instance._refundAnimationInterval);
                    _instance._refundAnimationInterval = null;
                    _instance._resetTimeout();
                }
            }, 100);
        }
    };

    p.setGameParameters = function(response){
        this.setCrd(response.balance);

        this.minStake = 1;
        this.maxStake = response.maxTokens || 20;
        this.stakeInc = 1;
        this.controlIdleTime = response.controlIdleTime;
        console.log("CONTROL:: "+this.controlIdleTime);
        this._kickOutPing = this.controlIdleTime/3;
        this.setStakeIncrementsMinMax(this.minStake, this.maxStake, this.stakeInc);

        this.currency = response.currency || "N/A";
        //decimal
        //var code = 8364;//resp.user.currency
        //this.currency = String.fromCharCode( code);

        //hex, css
        //var code = '20AC';//20A4 resp.user.currency
        //this.currency = String.fromCharCode( parseInt(code, 16) );

        this.betManager.setMultiplier(response.tokenValue);
    };

    p.setBetTotal = function(){
        this.betManager = new alteastream.BetTotal(this);
    };

    p.setCurrency = function(){
        this.controlBoard.setCurrency(this.currency);
    };

    p.updateInfoPrizes = function(toValue) {
        this._info.updatePrizes(toValue);
    };

    p.updateCountedTokensView = function(bool){
        if(this.coinsNumber){
            this.coinsNumber.visible = !bool;
            this.eurNumber.visible = bool;
        }
    };

    p.updatePrizesView = function(bool) {
        this.prizeNumber.visible = !bool;
        this.prizeEurNumber.visible = bool;
    };

    p.updateWheelPrizes = function(toValue) {
        var multiplier = this.betManager.getMultiplier();
        this.bonusWheel.updatePrizes(toValue, multiplier);
    };

    p.setRegularStakes = function(){
        this.setStake(0);
    };

    p.setFixedStakes = function(){
        this.controlBoard.fixedStakesPanel.plugin(this);
    };

    p.setStake = function(position){
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        this._selectedStake = position;
        this.betManager.setStake();
        this.controlBoard.setBetLabelToMax(this._selectedStake === (this._stakeIncrements.length - 1));
        this.playSound("btnDownDouble");
    };

    p.getSelectedStake = function(){
        return this._stakeIncrements[this._selectedStake];
    };

    p.setStakeIncrements = function(increments){
        this._stakeIncrements = increments;
    };

    p.setStakeIncrementsMinMax = function(min,max,inc){
        var arr = [];
        for(var i = min; i <= max; i = Math.round((i+inc)*100)/100){
            arr.push(i);
        }
        this._stakeIncrements = arr;
    };

    p.setCrd = function(amount){
        this.credit = amount;
        if(this.okToShowBalance){
            this.controlBoard.setCrd(amount);
        }
    };

    p.setWin = function(amount){
        this.controlBoard.setWin(amount);
    };

    //PARAMS TO SEND
    p.getSendBetParams = function(){
        return  {bet: this.betManager.betValue()};
    };

    p.getShooterDirectionParams = function(dir){
        return  {direction: dir};
    };

    //BUTTON HANDLERS
    p.moveShooterBtnHandler = function(e) {
        this.controlBoard.checkIfHiddenMenuIsActive();
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        this.socketCommunicator.moveShooter(e);
        this.playSound("btnDown");

        this.playSoundChannelWithOptions("shooter",{});
    };

    p.stopShooterBtnHandler = function(e) {
        this.socketCommunicator.stopMovingShooter(e);
        this.playSound("btnUp");

        this.stopSoundChannel("shooter");
    };

    p.allowedShooterBtn = function(msg) {
        this.controlBoard.tryShooter();
        this.setCrd(msg.vl);
    };

    p.sendBtnHandler = function(e) {
        this.controlBoard.checkIfHiddenMenuIsActive();
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        if(this.betManager.canBet(this.credit,this.getSelectedStake())){
            if(this.controlBoard.btnSend._disabled)
                return;
            this.controlBoard.blockShooter(true);
            this.socketCommunicator.sendBet(this.getSendBetParams());
            //this.playSound("btnDownDouble");
            this.playSound("insertCoin",0.2);
            this.checkIfinfoIsActive();
        }else{
            this.controlBoard.blockShooter(true);
            var that = this;
            this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance",function(){
                that.controlBoard.tryShooter();
            })
        }
    };

    p.collectBtnHandler = function() {
        this.socketCommunicator.collect(this.onCollectResponse);
    };

    p.incStakeBtnHandler = function(){
        this.controlBoard.checkIfHiddenMenuIsActive();
        this.setStake(this._selectedStake + 1);
    };

    p.decStakeBtnHandler = function(){
        this.controlBoard.checkIfHiddenMenuIsActive();
        this.setStake(this._selectedStake - 1);
    };

    p.infoBtnHandler = function() {
        this.onInfo();
    };

    p.quitBtnHandler = function(){
        _instance.controlBoard.checkIfHiddenMenuIsActive();
        _instance._setQuitPanel();
    };

    p.quitConfirmed = function(){
        //if(_instance.socketCommunicator)
            //_instance.socketCommunicator.disposeCommunication();

        _instance.controlBoard.disablePlayButtons(true);
        _instance.blockControls(true);
        _instance.playSoundChain("seeYouSpoken",0.3,_instance.onQuit,_instance);
    };

    p.quitLimitReached = function(){
        this.controlBoard.disablePlayButtons(true);
        this.blockControls(true);
        this.playSound("byeSpoken",0.2);
        this.throwAlert(alteastream.Alert.INFO,"Non playing time reached...game is closed",function(){
            _instance.exitMachineGameplay();
        });
    };

    p.inactivityWarning = function(n){
        var type = n===1?"First":"Second";
        this.throwAlert(alteastream.Alert.WARNING, type+" warning...return to game");
        this.playSound("continuePlayingSpoken",0.2);
    };

    p.blockControls = function(bool){
        this.controlBoard.mouseEnabled = !bool;
    };

    p.showWin = function(amount,msgValue){
        var prize = msgValue.prize;
        //var win = msgValue.win;
        this.prizeQueue.unshift([amount,prize]);
        if(!this.queuePlaying)
            this.playQueue();
    };

    p.playQueue = function(){
        this.queuePlaying = true;
        var multiplier = this.betManager.getMultiplier();

        var iterateQueue = function(){
            if(_instance.prizeQueue.length>0){
                var lastElement = _instance.prizeQueue.pop();
                //var win = lastElement[2];
                _instance.queuePlaying = true;
                var chip = new alteastream.Chip(lastElement[0],lastElement[1],multiplier);
                var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;
                var prizeCounterPosition = _instance.prizeCounter;
                chip.x = prizeCounterPosition.x+_instance.prizeCounter.pivot;
                chip.y = _height+chip.getSize();
                _instance.addChild(chip);
                var yAnimation = (prizeCounterPosition.y-chip.getSize()*0.5)-5;

                var isEur = _instance.controlBoard._toValueView;

                //console.log("IS BONUS?::  "+_instance.isBonus);
                TweenLite.to(chip,2,{y:yAnimation,onUpdate:function () {
                        var doAnimation = _instance.controlBoard._toValueView !== isEur;
                        chip.updateChip(_instance.controlBoard._toValueView, doAnimation);
                    },onComplete:function(){
                        _instance.removeChild(chip);
                        chip = null;
                        //_instance.setWin(win);
                        if(!_instance.bonusActive){
                            _instance.okToShowBalance = true;
                            _instance.setCrd(_instance.credit);
                        }
                    },ease: Elastic.easeOut.config(0.5, 0.1)});
                _instance.showPrize(lastElement[0]);
                var prizeSpoken = "prize_"+lastElement[0];
                _instance.playSoundChain(prizeSpoken,0.3,function(){// append bonus on next soundChain?
                    _instance.queueCount++;
                    iterateQueue();
                },_instance);
            }else{
                if(_instance.queueCount > 2){
                    var phrase = "";
                    if(_instance.queueCount < 6 ) { phrase = "reallyGoodSpoken";}
                    if(_instance.queueCount > 5 && _instance.queueCount < 9 ) { phrase = "feelDizzySpoken";}
                    if(_instance.queueCount > 8 ) { phrase = "keepGoingSpoken";}
                    _instance.playSoundChain(phrase,0.3,function(){ _instance.queuePlaying = false; iterateQueue();});
                }else{
                    _instance.queuePlaying = false;

                    if(!_instance.bonusActive){
                        _instance.okToShowBalance = true;
                        _instance.setCrd(_instance.credit);
                        _instance.controlBoard.blockShooter(false);
                    }

                    /* if(_instance.bonusWon>0){
                         console.log("onBonusAllowed YES QUEUE");
                         _instance.spinForBonus(_instance.bonusWon);
                     }*/
                }
                _instance.queueCount = 0;
            }
        };
        this.playSound("win1",0.3);
        this.playSoundChain("youHaveWonSpoken",0.3,iterateQueue);
    };

    p.onMachineError = function(err){
        this.throwAlert(alteastream.Alert.ERROR,"Machine error...", function () {
            _instance.onQuit();
        });
        this.playSound("errorSpoken",0.2);
    };

    p.setIosOverlay = function() {
        this.controlBoard.mouseEnabled = false;

        var isMob = window.localStorage.getItem('isMobile') === "yes";
        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var bg = new createjs.Shape();
        bg.graphics.beginFill("#000000").drawRect(0, 0, w, h);

        var qFont = isMob ? "70px Lato" : "90px Lato"
        var qY = isMob ? 80 : 125;
        var question = new createjs.Text("Ready To Play?",qFont,"#ffffff").set({x:w/2, y:qY, textAlign:"center",textBaseline:"middle"});

        var liveShape = new createjs.Shape();
        liveShape.graphics.beginFill("#ff0000").drawRoundRect(0, 0, 76, 40, 5);
        liveShape.regX = 38;
        liveShape.regY = 20;
        liveShape.x = isMob ? 340 : 820;//340
        liveShape.y = isMob ? 230 : 500;

        var arrowsYCorrector = isMob ? 60 : 160;
        var arrowsFont = isMob ? "30px Lato" : "60px Lato";
        var liveTxt = new createjs.Text("LIVE","22px Lato","#fcfcfc").set({x:liveShape.x, y:liveShape.y+2, textAlign:"center", textBaseline:"middle"});
        var streamTxt = new createjs.Text("STREAM","22px Lato","#d10000").set({x:liveTxt.x + 105, y:liveTxt.y, textAlign:"center", textBaseline:"middle"});
        var connectedTxt = new createjs.Text("CONNECTED","22px Lato","#15d510").set({x:streamTxt.x + 130, y:liveTxt.y, textAlign:"center", textBaseline:"middle"});
        var arrowTxt = new createjs.Text(">>",arrowsFont,"#ffffff").set({x:w*0.5, y:liveTxt.y + arrowsYCorrector, textAlign:"center", textBaseline:"middle"});
        arrowTxt.rotation = 90;

        var buttonFont = isMob ? "bold 14px Lato" : "bold 20px Lato";
        var buttonFontY = isMob ? 45 : 65;
        var buttonYCorrector = isMob ? 100 : 145;
        var txt = new createjs.Text("PLAY NOW",buttonFont,"#fcfcfc");
        var playButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,txt);
        playButton.centerText(0,buttonFontY);
        playButton.x = w/2;
        playButton.y = h - buttonYCorrector;

        var overlayClickCallback = function () {
            //_instance.streamVideo.videoElement.setMuted(false);
            //_instance.streamVideo.videoElement.setVolume(0.1);
            //_instance.streamVideo.videoElement.play();
            //_instance.streamVideo.setMedia();
            _instance.controlBoard.mouseEnabled = true;
            alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
        };

        var overlayOptions = {
            background:bg,
            button:playButton,
            graphics:[liveShape],
            texts:[question,liveTxt,streamTxt,connectedTxt,arrowTxt],
            clickCallback:overlayClickCallback
        };

        this.setStartOverlay(overlayOptions);
    };

    p._local_activate = function() {
        /*        this._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
                    //return "yes";
                    return "not";
                };*/
        this.test_BonusFill = function(){
            setTimeout(function(){_instance.powerUp.fill(1);},1000);
            setTimeout(function(){_instance.powerUp.fill(5);},1500);
            setTimeout(function(){_instance.powerUp.fill(6);},2000);
            setTimeout(function(){_instance.powerUp.fill(8);},2500);
            setTimeout(function(){_instance.powerUp.fill(20);},3000);
        };
        this.test_BonusSpin = function(){
            _instance.onBonusAllowed(5);
        };
        this.test_Queue = function(){
            setTimeout(function(){
                var vl = {"prize":"1","amount":50,"money":500,"balance":661000,"win":8};
                _instance.onPrizeDetection(vl)
            },500);
            setTimeout(function(){
                var vl = {"prize":"2","amount":10,"money":1000,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl)
            },1500);
            setTimeout(function(){
                console.log("START SECOND");
                var vl1 = {"prize":"3","amount":15,"money":1500,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl1);
            },2500);
            setTimeout(function(){
                console.log("START THIRD");
                var vl2 = {"prize":"4","amount":45,"money":4500,"balance":661700,"win":8};
                for(var i = 0;i<2;i++){
                    _instance.onPrizeDetection(vl2);
                }
            },3500);
        };

        this.test_BonusSpin = function(n){
            _instance.onBonusAllowed(n);
        };
        this._callInitMethods = function () {};
        alteastream.Assets.setMute(true);
        ///////////////////BONUS TEST ADD//////////////////////
        this._setControlBoard();
        //response sa bonusom
        //var response = {"balance":10000,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"};
        //response bez bonusa
        //var response = {"balance":2055700,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus","tokenValue":100.0};
        //coins "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        //marble "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        //var machineParams = "ws://87.130.112.6:8083/ws";
        //this._setStreamVideo(machineParams);
        var response = {"state":0,"key":"79a2fb56-7c2c-46cf-97d5-35db5661425e","controlIdleTime":60000,"streamendpoint":"ws://87.130.112.6:8889","webrtcurl":"ws://87.130.112.6:8083/ws","balance":9998800,"tokenValue":100.0,"tokens":0,"maxTokens":60,"currency":"EUR","payout":{"1":10,"2":25,"3":50,"4":75,"5":100,"6":250,"7":500,"8":1000,"101":1},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus"};
        this.setGameParameters(response);
        if(Object.keys(response.bonus).length === 0){/*this.streamVideo.activate(function(){this.showGame(true);});*/}
        else{this._setBonusApplet(response);}
        this.bonusWon = 10;//with bonus
        this.setPayout(response.payout);
        this.setRegularStakes();
        //new stakes
        setTimeout(function () {
            _instance.setFixedStakes();
            _instance.controlBoard.fixedStakesPanel.updateMonetary(true);
        },0)
        //this.setFixedStakes();
        this.setCurrency();
        this._addInfo();
        //this.setQueueInfo();
        //this.queueInfoContainer.visible = true;
        this.setCoinCounter();
        this.setPrizeWinCounter();
        //if(alteastream.AbstractScene.GAME_TYPE === "ticketcircus"){
        if(this.coinsCounter){
            this.coinsCounter.visible = true;
        }
        this.prizeCounter.visible = true;
        //this.addGiveUpButton(true);
        //this.giveUpButton.y+=50;
        this._setInfo();
        this._switchInfoAndWheelChildIndexes();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
        this.setMonetaryValueView(this.controlBoard._toValueView);

        this.addChild(this.controlBoard);
        this.setOverlay();
        setTimeout(function(){
            //_instance.addGiveUpButton(false);
            if(_instance._hiddenMenuButton){
                _instance._hiddenMenuButton.visible = true;
                //_instance._showHiddenMenu(true);
            }
        },2000);

/*        setTimeout(function (){
            _instance.setIosOverlay();
        },10);*/

        //_instance.test_Queue();

        // no need for now, there is no bonus wheel
        /*        var button = new createjs.Shape();
                button.graphics.beginFill('#29abe2').drawRoundRect(0,0,170,56,6);
                button.regX = 85;
                button.regY = 28;
                button.x = alteastream.AbstractScene.GAME_WIDTH*0.5;
                button.y = alteastream.AbstractScene.GAME_HEIGHT*0.5;
                this.addChild(button);
                button.cache(0,0,200,100);
                button.on("click", function () {
                    _instance.removeChild(button);
                    ///////////////////BONUS TEST START//////////////////////
                    _instance.test_BonusSpin(_instance.bonusWon);
                    ///////////////////BONUS TEST END//////////////////////
                    ///////////////////QUEUE TEST START//////////////////////
                    //_instance.test_Queue();// includes spin if bonusWon >0
                    ///////////////////QUEUE TEST END//////////////////////
                });*/

        //5 = blue 0.50, tag id 1
        //10 = green 1, tag id 2
        //15 = pink 2, tag id 3
        //45 = orange 5, tag id 4
        //90 = yellow 10, tag id 5

        //NEW PAYOUTS::::::::::::::::COINS::::::::::::::::::::::::
        //one coin = 0.10
        //0.25 = metal 0.025,    tag id 0

        //5 = blue 0.50,         tag id 1
        //10 = green/black 1.00, tag id 2
        //15 = red 15.00,        tag id 3
        //45 = yellow 4.50,      tag id 4


        // "TOKENS DETECTION" msg  - Only available in Cash Circus and Cash Festival machines (COINS)

        //NEW PAYOUTS::::::::::::::::MARBLE::::::::::::::::::::::::
        //one pearl = 0.10
        //5 = blue 0.50,             tag id 1
        //15 = green/black 1.00,     tag id 2
        //40 = red 4.00,             tag id 3
        //100 = yellow 10.00,        tag id 4

        //this.test_Queue();

        ///////TOKENS COUNTED TEST START
        var c = 0;
        var max = 5;
        var tokensFalling = function(){
            _instance.intervalSpawn = setInterval(function () {
                //var c = Math.round(Math.random()*20);
                if(c<max){
                    var n = 1;
                    //_instance.refundCoinSpawn({"ts":1578476179098,"vl":{"tokens":n,"amount":10,"money":1100,"balance":1028000},"tp":6,"cat":50});
                    _instance.refundCoinSpawn(10);
                    c++;
                }else{
                    c = 0;
                    max = 10;
                    clearInterval(_instance.intervalSpawn);
                   setTimeout(function(){
                       console.log("NEW TOKENS FELL");
                       tokensFalling();
                       },500) ;
                }
            },500);
        };
        // show machine name , uncomment
        //this.setMachineNameText();
        //tokensFalling();
        ///////TOKENS COUNTED TEST END

        // BONUS SPAWN TEST START
        //do comment _instance.endBonusFeature();
        //coin.x = 960;
        //coin.y = 600;
        //_instance._bonusCoinSpawn(10);
        // BONUS SPAWN TEST END
    };

// static methods:
// public methods:

    alteastream.CoinPusher = createjs.promote(CoinPusher,"AbstractScene");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusherMobile = function() {
        this.CoinPusher_constructor();
        this.initialize_CoinPusherMobile();
    };
    var p = createjs.extend(CoinPusherMobile, alteastream.CoinPusher);

    // static properties:
    // events:
    // public properties:
    // private properties:

    var _instance = null;

    // constructor:
    p.initialize_CoinPusherMobile = function() {
        _instance = this;

        this._adjustControlBoard();
        //this._adjustCoinsCounter();
        this._adjustPrizeCounter();
        this._adjustQueueInfo();
        //this._adjustFixedStakes();
        //this.showFSClock(true); maybe after audit, now there is a clock from camera
    };
    // static methods:
    CoinPusherMobile.getInstance = function(){
        return _instance;
    };
    // private methods
    p._adjustControlBoard = function() {
        var cBoard = this.controlBoard;
        cBoard.balanceBackground.uncache();
        cBoard.balanceBackground.graphics.clear();
        //cBoard.balanceBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 350, 67, 10);
        cBoard.balanceBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 300, 67, 10);
        cBoard.balanceBackground.alpha = 0.7;
        cBoard.balanceBackground.cache(0, 0, 300, 67);
        cBoard.balanceBackground.x = 30;
        cBoard.balanceBackground.y = 443;
        // verzija ako hocemo da se ne instancira keyboard listener za mobilni
        // kreiranje objekta twinComponent je potrebno posto u _shooterMoveBtnHandler i u _shooterUpBtnHandler postoji provera ,
        // a objekat ne postoji posto nije instancirana Controller klasa u mobile verziji
        //cBoard.twinComponent = {active:false};
/*        cBoard.removeChild(cBoard._textMoveLeft);
        cBoard.removeChild(cBoard._textShooter);
        cBoard.removeChild(cBoard._textMoveRight);
        cBoard.removeChild(cBoard._textDecStake);
        cBoard.removeChild(cBoard._textIncStake);
        cBoard.removeChild(cBoard._textSwitchCurrency);
        cBoard.removeChild(cBoard._textInfoToggle);*/
        //cBoard.removeChild(cBoard.btnFs);
        cBoard.removeChild(cBoard.upperOverlay);
        cBoard.hiddenMenuContainer.removeChild(cBoard.btnFs);

        var leftRightArrowsBackground = cBoard.leftRightArrowsBackground = alteastream.Assets.getImage(alteastream.Assets.images.arrowButtonsBackground);
        leftRightArrowsBackground.x = 30;
        leftRightArrowsBackground.y = 185;
        cBoard.addChildAt(leftRightArrowsBackground,0);

        cBoard.btnShooterLeft.x = 75 - (cBoard.btnShooterLeft._singleImgWidth*0.5);
        cBoard.btnShooterLeft.y = 331 - (cBoard.btnShooterLeft._height*0.5);
        cBoard.btnShooterRight.x = cBoard.btnShooterLeft.x;
        cBoard.btnShooterRight.y = 229 - (cBoard.btnShooterRight._height*0.5);

        var font = "17px Lato";
        cBoard._textCurrencyValue.font = font;
        cBoard._textCurrencyValue.x = 124;
        cBoard._textCurrencyValue.y = 477;
        cBoard._divider.font = font;
        cBoard._divider.x = 157;
        cBoard._divider.y = 476;
        cBoard._textCrd.font = font;
        cBoard._textCrd.x = 168;
        cBoard._textCrd.y = 477;
        cBoard._textCrd.skewX = 0;
        cBoard._textCrdAmount.font = font;
        cBoard._textCrdAmount.x = cBoard._textCrd.x + 90;
        //cBoard._textCrdAmount.y = cBoard._textCurrency.y;
        cBoard._textCrdAmount.y = 477;
        cBoard._textCrdAmount.skewX = 0;

        cBoard.btnSend.x = 855;
        cBoard.btnSend.y = 281;
        cBoard.btnSend.centerText(0,45);
        cBoard.btnSend.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.single.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.burst.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.single.deltaY = 45;
        cBoard.btnSend._presetFonts.burst.deltaY = 45;

        var background = cBoard.stakeBackground;
        background.uncache();
        background.graphics.clear();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 266, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 266, 67);
        var xCorr = 82;
        cBoard.stakeBackground.x = 747-xCorr;
        cBoard.stakeBackground.y = 443;

        cBoard.btnDecStake.x = cBoard.stakeBackground.x + 5;
        cBoard.btnDecStake.y = cBoard.stakeBackground.y + 4;
        cBoard.btnIncStake.x = cBoard.stakeBackground.x + 122;
        cBoard.btnIncStake.y = cBoard.stakeBackground.y + 4;
        cBoard._textStakeAmount.font = font;
        cBoard._textStakeAmount.x = cBoard.stakeBackground.x + 92;
        cBoard._textStakeAmount.y = 477;
        cBoard._textStakeAmount.skewX = 0;

        cBoard._textBet.font = font;
        cBoard._textBet.skewX = 0;
        cBoard._textBet.x = cBoard.stakeBackground.x + 92;
        cBoard._textBet.y = cBoard.stakeBackground.y + 12;

        var menuButton = cBoard._hiddenMenuButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnHiddenMenu),3);
        menuButton.x = 864;
        menuButton.y = 30;
        cBoard.addChild(menuButton);

        var menuButtonClickHandler = function () {
            cBoard._showHiddenMenu(!cBoard.menuIsShown);
        };
        menuButton.setClickHandler(menuButtonClickHandler);

/*        menuButton.addEventListener("click", function () {
            cBoard._showHiddenMenu(!cBoard.menuIsShown);
        });*/

        this.giveUpButton.x = 886;//giveUpButton
        this.giveUpButton.y = this.giveUpButton.y = is.ios() === true ? 100 : 26;

        if(cBoard.btnBetView){
            var btnBetView = cBoard.btnBetView;
            btnBetView.x = 35;
            btnBetView.y = 449;
        }

        cBoard.hiddenMenuContainer.x = 960;
        cBoard.hiddenMenuContainer.y = 136;

        var width = cBoard.btnQuit.width;
        var height = cBoard.btnQuit.height;
        var hitAreaReduce = 30;

        cBoard.btnMachineSound.y = cBoard.btnQuit.y + height;
        cBoard.btnSound.y = cBoard.btnMachineSound.y + height;
        cBoard.btnInfo.y = cBoard.btnSound.y + height;
        cBoard.btnInfo.originalYPosition = cBoard.btnInfo.y;
        cBoard.btnMachineSound.x = cBoard.btnSound.x = cBoard.btnInfo.x = cBoard.btnQuit.x;

        var hitArea = new createjs.Shape();
        hitArea.graphics.beginFill("#fff").drawRect(0, hitAreaReduce * 0.5, width, height-hitAreaReduce);

        cBoard.btnQuit.hitArea = hitArea;
        cBoard.btnMachineSound.hitArea = hitArea;
        cBoard.btnSound.hitArea = hitArea;
        cBoard.btnInfo.hitArea = hitArea;

        cBoard._moveShooterLeftRightAndBtnSendComponents = function(hiddenMenuIsShown){
            cBoard.controlShown = hiddenMenuIsShown;
            var xPosCorrector;
            if(cBoard.controlShown ===true){
                xPosCorrector = -120;
                cBoard.controlShown =false;
            }else{
                xPosCorrector = 120;
                cBoard.controlShown =true;
            }

            createjs.Tween.get(cBoard.leftRightArrowsBackground).to({x:cBoard.leftRightArrowsBackground.x + xPosCorrector},200,createjs.Ease.linear);
            createjs.Tween.get(cBoard.btnShooterLeft).to({x:cBoard.btnShooterLeft.x + xPosCorrector},200,createjs.Ease.linear);
            createjs.Tween.get(cBoard.btnShooterRight).to({x:cBoard.btnShooterRight.x + xPosCorrector},200,createjs.Ease.linear);
            xPosCorrector *= -2;
            createjs.Tween.get(cBoard.btnSend).to({x:cBoard.btnSend.x + xPosCorrector},200,createjs.Ease.linear);
        };

        cBoard.doShowMenu = function(show){
            cBoard._hiddenMenuButton.setDisabled(true);
            var xPos = show === true ? cBoard._hiddenMenuButton.x : alteastream.AbstractScene.GAME_WIDTH;
            cBoard.menuIsShown = show;
            createjs.Tween.get(cBoard.hiddenMenuContainer).to({x:xPos},200,createjs.Ease.linear).call(function () {
                cBoard._hiddenMenuButton.setDisabled(false);
            });
            var scene = alteastream.CoinPusher.getInstance();
            if(scene._info.visible === true && show === false){
                scene.onInfo();
            }
        }
    };

    p._setInfo = function() {
        this._info.setInfo(this.payoutManager,this.hasBonus, this.betManager.getMultiplier());
        this._info.setMaxStakeInfo(this.maxStake);
        this._info.setNonPlayingTimeInfo(this.controlIdleTime);

        this._adjustInfo();
        if(this.hasBonus === true){
            this._info.setBonusInfo(this.powerUp.neededForBonus);
            var bonusDescription = this._info.gameDescriptions[1];
            bonusDescription.font = "14px Impact";
            bonusDescription.x = 480;
            bonusDescription.y = 230;
            bonusDescription.lineWidth = 220;
            bonusDescription.lineHeight = 20;

            var numberNeededForBonus = this._info.gameDescriptions[2];
            numberNeededForBonus.font = "14px Impact";
            numberNeededForBonus.x = 417;
            numberNeededForBonus.y = 230;
        }
        var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
        //if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher"){
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && refundValue > 0){
            //var refundValue = (Math.round(((this.payoutManager.getRefundValue() * 100)/100) * 1e12) / 1e12).toFixed(3);
            //var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
            this._info.setCountedTokensInfo(refundValue);//param metal coinvalue + slicica

            this._info.coinImage.x = 480;
            this._info.coinImage.y = 140;

            var countedTokensDescription = this._info.gameDescriptions[5];
            countedTokensDescription.font = "14px Roboto";
            countedTokensDescription.x = 480;
            countedTokensDescription.y = 199;
            countedTokensDescription.lineWidth = 135;
            countedTokensDescription.lineHeight = 18;

            var refundValueText = this._info.gameDescriptions[6];
            refundValueText.font = "18px Impact";
            refundValueText.lineHeight = 20;
            refundValueText.x = 480;
            refundValueText.y = 132;
        }else{
            this._info.setSunLogo();
            this._info.coinImage.x = 480;
            this._info.coinImage.y = 185;
        }
    };

    p._adjustInfo = function() {
        var numberOfChipsInPaytable = this._info.chips.length;
        var maxChipsInColumn = 4;
        var yPosMultiplier = 0;
        var chipsY = 83;
        var chipsYSpacing = 70;
        var chipsXSpacing = 0;

        var startXPos = numberOfChipsInPaytable > maxChipsInColumn ? 153 : 188;
        for(var i = 0; i < numberOfChipsInPaytable; i++){
            var chip = this._info.chips[i];
            chip.scale = chip.backScaleXForAnimation = 0.68;

            if(yPosMultiplier === maxChipsInColumn){
                chipsXSpacing = 70;
                yPosMultiplier = 0;
            }

            yPosMultiplier++;

            chip.x = startXPos + chipsXSpacing;
            chip.y = chipsY + (chipsYSpacing * yPosMultiplier);

            chip._prizeAmount.font = "bold 22px HurmeGeometricSans3";
        }

        var gameplayDescription = this._info.gameDescriptions[0];
        gameplayDescription.font = "14px Roboto";
        gameplayDescription.x = 770;
        gameplayDescription.y = 131;
        gameplayDescription.lineWidth = 135;
        gameplayDescription.lineHeight = 20;

        var maxStakeLabel = this._info.gameDescriptions[1];
        var maxStakeNumber = this._info.gameDescriptions[2];

        maxStakeLabel.font = "14px Impact";
        maxStakeLabel.x = 773;
        maxStakeLabel.y = 370;

        maxStakeNumber.font = "14px Impact";
        maxStakeNumber.x = 780;
        maxStakeNumber.y = 370;

        var nonPlayingTimeDescription = this._info.gameDescriptions[3];
        var nonPlayingTimeSeconds = this._info.gameDescriptions[4];
        nonPlayingTimeSeconds.lineHeight = 15;

        nonPlayingTimeDescription.font = "14px Roboto";
        nonPlayingTimeDescription.x = 480;
        nonPlayingTimeDescription.y = 389;
        nonPlayingTimeDescription.lineWidth = 150;
        nonPlayingTimeDescription.lineHeight = 15;

        nonPlayingTimeSeconds.font = "18px Impact";
        nonPlayingTimeSeconds.x = 480;
        nonPlayingTimeSeconds.y = 322;
        nonPlayingTimeSeconds.lineHeight = 20;
    };

    p.super_setBonusApplet = p._setBonusApplet;
    p._setBonusApplet = function(options) {
        this.super_setBonusApplet(options);
        this._adjustBonusWheel();
    };

    p._adjustBonusWheel = function() {
        var wheel = this.bonusWheel;
        var fieldsNumber = wheel.numChildren;
        var length = wheel.FIELD_WINS.length;
        for(var i = 1; i < fieldsNumber; i++){// first child is background
            var field = wheel.getChildAt(i);
            field.outerRadiusShrink = field.outerRadiusShrink/2;
            field.innerRadiusShrink = field.innerRadiusShrink/2;
            field.startY = field.startY/2;
            field.sWidth = (field.WIDTH_SUM/length)/2;
            field.sHeight = field.sHeight/2;
            field.itemValue.font = "25px Roboto Black";

            //field.colorSlice(field.defaultSliceColor);

            field.regX = field.sWidth * 0.5; // todo ??????????????????
            field.regY = field.sHeight;
            field.itemValue.x = field.regX;
            field.itemValue.y = field.count % 2===0?field.outerRadiusShrink:field.innerRadiusShrink;
        }

        var powerUpWheel = this.powerUp;
        var radius = 183;
        var lamps = powerUpWheel.numChildren;
        var startAngle = -90 * Math.PI / 180;
        var oneField = powerUpWheel.oneField;
        for(var j = 0; j < lamps; j++){
            var lamp = powerUpWheel.getChildAt(j);
            lamp.x =  Math.cos(startAngle)*radius;
            lamp.y =  Math.sin(startAngle)*radius;
            startAngle += oneField * Math.PI / 180;
        }

        this.bonusWonText.font = "48px Lato";
        this.bonusWonText.x = 33;
        this.bonusWonText.y = 5;

        this.marker.y = -210;

        this.bonusContainer.x = this.bonusContainer.startX = 125;
        this.bonusContainer.y = this.bonusContainer.startY = 130;
        this.bonusContainer.xPosForAnimation = 200;
        this.bonusContainer.yPosForAnimation = 200;
        this.bonusContainer.xPosForInfo = 480;
        this.bonusContainer.yPosForInfo = 115;
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(0.9);
        coin.setVR(-10,10);
        coin.setVX(-1,1);
        coin.setVY(-16,-18);
        coin.animate();
    }

    p._adjustCoinsCounter = function(){
        //if(alteastream.AbstractScene.GAME_TYPE === "pearlpusher")
            //return;
        if(this.coinsCounter){
            var bg = this.coinsCounter.getChildAt(0);
            bg.uncache();
            bg.graphics.clear();
            bg.graphics.beginFill("#000000").drawRoundRect(0, 0, 160, 67, 10);
            bg.alpha = 0.7;
            bg.cache(0, 0, 160, 67);
            var xCorr = 60;
            this.coinsCounter.x = 395-xCorr;
            this.coinsCounter.y = 443;
            this.coinsCounter.pivot = bg.bitmapCache.width*0.5;
            var font = "17px Lato";
            this.coinsChargedLabel.font = font;
            this.coinsNumber.font = font;
            this.eurNumber.font = font;

            this.coinsChargedLabel.x = 10;
            this.coinsNumber.x = this.eurNumber.x = 110;

            this.coinsChargedLabel.y = this.coinsNumber.y = this.eurNumber.y = 34;
        }
    };

    p._adjustPrizeCounter = function() {
        var bg = this.prizeCounter.getChildAt(0);
        bg.uncache();
        bg.graphics.clear();
        bg.graphics.beginFill("#000000").drawRoundRect(0, 0, 160, 67, 10);
        bg.alpha = 0.7;
        bg.cache(0, 0, 160, 67);
        var xCorr = 72;
        this.prizeCounter.x = 572-xCorr;
        this.prizeCounter.y = 443;
        this.prizeCounter.pivot = bg.bitmapCache.width*0.5;
        var font = "17px Lato";
        this.prizeWinLabel.font = font;
        this.prizeNumber.font = font;
        this.prizeEurNumber.font = font;

        this.prizeWinLabel.x = 12;
        this.prizeNumber.x = this.prizeEurNumber.x = 107;

        this.prizeWinLabel.y = this.prizeNumber.y = this.prizeEurNumber.y = 34;
    };

    // show machine name , comment method
    p._adjustQueueInfo = function(){
        this.queueInfoContainer.x = 30;
        this.queueInfoContainer.y = 30;
        this.queueInfoLabel.font = this.queueInfoText.font = "14px Lato";
        this.queueInfoLabel.x = this.queueInfoText.x = this.queueInfoBackground.x + 98;
        this.queueInfoLabel.y = this.queueInfoText.y = this.queueInfoBackground.y + 24;
    };

    p._adjustFixedStakes = function(){
        var cBoard = this.controlBoard;

        cBoard.btnFixedStakes.x = 869;
        cBoard.btnFixedStakes.y = cBoard.stakeBackground.y + 4;

        cBoard.fixedStakesPanel.adjustMobile();
    };

    // public methods
    p.super_showGame = p.showGame;
    p.showGame = function(bool){
        this.super_showGame(bool);
        if(bool === true){
            this.controlBoard._hiddenMenuButton.visible = true;
        }
    };

    p.superSetCoinCounter = p.setCoinCounter;
    p.setCoinCounter = function () {
        this.superSetCoinCounter();
        this._adjustCoinsCounter();
    };

    p.onInfo = function(){
        this._info.visible = !this._info.visible;
        this.controlBoard._hiddenMenuButton.visible =
            this.controlBoard.btnQuit.visible =
                this.controlBoard.btnMachineSound.visible =
                    this.controlBoard.btnSound.visible = !this._info.visible;

        var btnImage = this._info.visible === true ? "btnQuitSS" : "btnInfoSS";
        this.controlBoard.btnInfo.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
        this.controlBoard.btnInfo.x = this._info.visible === true ? -1 : 0;
        this.controlBoard.btnInfo.y = this._info.visible === true ? -114 : this.controlBoard.btnInfo.originalYPosition;

        if(this._info.visible === false){
            this.controlBoard.checkIfHiddenMenuIsActive();
        }
        if(this.hasBonus === true){
            this._setBonuswheelPositionAndForInfo(this._info.visible);
        }
        this.queueInfoContainer.visible = !this._info.visible;
        alteastream.Assets.playSound("btnDownDouble");
    };

    p.super_setFixedStakes = p.setFixedStakes;
    p.setFixedStakes = function(){
        this.super_setFixedStakes();
        this._adjustFixedStakes();
    };

    //p._activateKeyboardListeners = function(){}; // verzija ako hocemo da se ne instancira keyboard listener za mobilni

    alteastream.CoinPusherMobile = createjs.promote(CoinPusherMobile,"CoinPusher");
})();"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function () {

    var ABSField = function(n) {
        this.Container_constructor();
        this.initialize_ABSField(n);
    };
    var p = createjs.extend(ABSField, createjs.Container);

    // events:
    // public properties:
    // static properties:
    // constants
    p.WIDTH_SUM = 2064;
    p.NUM_BONUSES = 0;
    // private properties:
    // constructor:
    p.initialize_ABSField = function(n) {
        this._init(n);
    };
    // static methods:
    // private methods:
    p._init = function(n){
        this.count = n;
        this.mouseEnabled = false;
    };

    p._createSlice = function(color){
        var slice = new createjs.Shape();
        this._draw(slice,color);
        return slice;
    };

    p._draw = function(slice,color){
        var halfWidth = this.sWidth*0.5;
        var halfHeight = this.startY*((this.startY/halfWidth)*0.75);
        slice.graphics.beginFill(color);
        slice.graphics.moveTo(this.startX, this.startY).quadraticCurveTo(halfWidth, halfHeight, this.sWidth, this.startY).lineTo(halfWidth, this.sHeight).lineTo(this.startX, this.startY);
        slice.cache(0,0,this.sWidth,this.sHeight);
    };
    // public methods:
    p.setSliceColor = function(color){
        this.defaultSliceColor = color;
        this.colorSlice(color);
    };

    p.setFontColor = function(color) {
        this.itemValue.color = color;
    };

    p.colorSlice = function(color){
        var slice = this.slice;
        slice.uncache();
        slice.graphics.clear();
        this._draw(slice,color);
    };

    p.highlight = function(bool){
        this.highlightText(bool);
        this.highlightField(bool);
    };

    p.highlightText = function(bool){
        this.itemValue.color = bool === true? this.highlightTextColor: this.defaultTextColor;
        this.itemValue.x = this.regX;
    };

    p.highlightField = function(bool){
        //Mylan: highlight slice-a boja
        //var color = bool === true? "#ffffff": this.defaultSliceColor;  // dynamic draw field ver
        //this.colorSlice(color); // dynamic draw field ver
    };

    alteastream.ABSField = createjs.promote(ABSField,"Container");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var ABSWheel = function(game,bonuses) {
        this.Container_constructor();
        this.initialize_ABSWheel(game,bonuses);
    };
    var p = createjs.extend(ABSWheel, createjs.Container);

    // constants
    p.FULL_CIRCLE = 360;
    p.FIELD_WINS = [];
    // public properties
    p.revolutions = 0;
    p.force = 0;
    p.game = null;
    p.fields = [];
    // constructor
    p.initialize_ABSWheel = function(game,bonuses){
        this.game = game;

        var bonusesReceived = this.bonusesReceived = [];
        for (var key in bonuses)
            bonusesReceived.push(bonuses[key]);

        var minBonus = this.minBonus = Math.min.apply(null, bonusesReceived);
        bonusesReceived.splice(bonusesReceived.indexOf(minBonus),1);
        for(var i = 0;i<bonusesReceived.length;i++){
            this.FIELD_WINS.push(bonusesReceived[i]);
            this.FIELD_WINS.push(minBonus);
        }
        this.oneField = (this.FULL_CIRCLE / this.FIELD_WINS.length);
    };
    // public methods
    p.createWheel = function(fieldClass,lowFieldColor,highFieldColor){
        var cnt = 0;
        var cnt2 = 0;
        var length = this.FIELD_WINS.length;
        for(var i = 0;i<length;i++){
            var field = new alteastream[fieldClass](this.FIELD_WINS[i],i,length);
            this.addChild(field);
            field.mouseEnabled = false;
            field.rotation = i*this.oneField;
            field.id = i;
            if(this.FIELD_WINS[i]===this.minBonus){
                field.name = String(this.minBonus+"_"+cnt);
                field.setFontColor(lowFieldColor);
                cnt++;
            }else{
                field.name = this.FIELD_WINS[i];
                field.setFontColor(highFieldColor);
                cnt2++;
            }
            field.radian = field.rotation;
            this.fields.push(field);
        }
        //add here maybe some highlighterFrame//********?
    };

    p.spin = function(response){
        this.cacheFields(true);
        this.force = 16;
        console.log("response :::::::::::::::: "+response);
        this.revolutions = Math.round(this.force/2)-1;
        var duration = Math.round((this.revolutions/10) * (300/this.force))-5;//-5 4
        var insidePosition = 0;

        var randomGet = String(this.minBonus+"_"+gamecore.Utils.NUMBER.randomRange(0,(this.bonusesReceived.length*0.5)-1));
        this.resultField = response === this.minBonus? randomGet: response;
        var endRadian = this.getChildByName(String(this.resultField)).radian;
        var fieldAngle = endRadian;
        var targetDegree = (insidePosition + this.FULL_CIRCLE * this.revolutions + (this.FULL_CIRCLE - fieldAngle)).toFixed(2);

        var that = this;
        TweenLite.to(this, duration, {ease: Back.easeOut.config(0.2),rotation:targetDegree,onComplete:function(){that.onSpinEnd()}});
    };

    p.onSpinEnd  = function(){
        this.resetDegrees();
        this.highlightWin(true);
        console.log("onSpinEnd:::::::::::::::: ");
        this.game.onBonusPlayed();
        this.cacheFields(false);
    };

    p.resetDegrees  = function(endRadian){
        this.rotation -= (this.revolutions * this.FULL_CIRCLE);
    };

    p.highlightWin = function(bool){
        //this.getChildByName(this.resultField).highlight(bool);
        //this.highlighterFrame.rotation = this.getChildByName(this.resultField).rotation;//********?
    };

    p.cacheFields = function(doCache){
        console.log("radi cache " + doCache);
        for(var i = 0; i < this.fields.length; i++){
            var field = this.fields[i];
            if(doCache === true){
                //gamecore.Utils.DISPLAY.cacheObject(field);
                gamecore.Utils.DISPLAY.cacheText(field.itemValue);
            }else{
                field.itemValue.uncache();
            }
        }
    };
    
    alteastream.ABSWheel = createjs.promote(ABSWheel,"Container");
})();
"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){

    var CoinBonusField = function(n,i,length) {
        this.ABSField_constructor(i,length);
        this.initialize_CoinBonusField(n,length);
    };
    var p = createjs.extend(CoinBonusField, alteastream.ABSField);

    // public methods
    p.outerRadiusShrink = 68;
    p.innerRadiusShrink = 145;
    // constructor
    p.initialize_CoinBonusField = function(n,length){
        this.defaultTextColor = "#fff";
        this.defaultSliceColor = "#00fff5";
        this.highlightTextColor = "#000000";

        this.startX = 0;
        this.startY = 60;
        this.sWidth = this.WIDTH_SUM/length;
        this.sHeight = 380;

        //var slice = this.slice = this._createSlice(this.defaultSliceColor); // dynamic draw field ver
        //this.addChild(slice); // dynamic draw field ver
        //slice.alpha = 0.3;

        this.regX = this.sWidth * 0.5;
        this.regY = this.sHeight;

        var font = '50px Impact';

        var spacingY = this.count % 2===0?this.outerRadiusShrink:this.innerRadiusShrink;
        var itemValue = this.itemValue = new createjs.Text(String(n), font, this.defaultTextColor).set({textBaseline:"top",textAlign:"center",mouseEnabled:false});

        this.addChild(itemValue);
        itemValue.x = this.regX;
        itemValue.y = spacingY;
    };

    alteastream.CoinBonusField = createjs.promote(CoinBonusField,"ABSField");
})();
"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var PowerUpWheel = function(game){
        this.Container_constructor();
        this.initialize_PowerUpWheel(game);
    };
    var p = createjs.extend(PowerUpWheel, createjs.Container);
    // constructor
    p.initialize_PowerUpWheel = function(game){
        this.game = game;
        this.count = 0;
        this.FULL_CIRCLE = 360;
        this._canFill = true;
        this.mouseEnabled = false;
        this.rotTime = 1;
    };
    // private methods
    p._reset = function(){
        for(var i = 0,j=this.numChildren;i<j;i++){
            this.getChildAt(i).image = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]).image
        }
    };

    p._markPair = function(bool){
        var leftover = bool ===true?2:1;
        for(var i = 0,j=this.numChildren;i<j;i++){
            var lamp = this.getChildAt(i);
            lamp.image = i%leftover===0? alteastream.Assets.getImage(alteastream.Assets.images[this.imageOn]).image: alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]).image;
            lamp.visible = i%leftover===0;
        }
    };

    p._rotate = function(bool){
        if(bool ===true)
            TweenMax.to(this, this.rotTime, {rotation:"-360", ease:Linear.easeNone, repeat:-1});
        else{
            TweenMax.killTweensOf(this);
            this.rotation = 0;
        }
    };

    p._mark = function(n){
        var lamp = this.getChildByName(String(n));
        lamp.image = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOn]).image;
        var time = 0.2;
        TweenLite.to(lamp,time,{scaleX:1.5,scaleY:1.5});
        TweenLite.to(lamp,time,{delay:time,scaleX:1,scaleY:1});
    };

    p._animate = function(){
        var that = this;
        var children = this.numChildren;
        var time = 0.2;
        for(var i = 0,j=children;i<j;i++){
            var lamp = this.getChildAt(i);
            TweenLite.to(lamp,time,{scaleX:1.5,scaleY:1.5});
            TweenLite.to(lamp,time,{delay:time,scaleX:1,scaleY:1,onComplete:function(){
                    if(that._doAnimate){
                        that._animate();
                    }
                }});
        }
    };
    // public methods
    p.build = function(imageOff,imageOn,radius,neededForBonus){
        this.radius = radius;
        this.imageOff = imageOff;
        this.imageOn = imageOn;
        var firstLast = 2;
        this.neededForBonus = neededForBonus;
        var bulbs = (neededForBonus*2)-firstLast;
        this.bulbsHalf = bulbs/2;
        var oneField = this.oneField = (this.FULL_CIRCLE / bulbs);
        var startAngle = -90 * Math.PI / 180;
        var lampImg = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]);

        for(var i = 0;i<bulbs;i++){
            var lamp = lampImg.clone();
            this.addChild(lamp);

            lamp.regX = lamp.image.width *0.5;
            lamp.regY = lamp.image.height *0.5;

            lamp.x =  Math.cos(startAngle)*radius;
            lamp.y =  Math.sin(startAngle)*radius;
            startAngle += oneField * Math.PI / 180;
            lamp.mouseEnabled = false;
            lamp.name = i;
        }
    };

    p.fill = function(count){
        if(this._canFill){
            this._reset();
            var length = count<this.neededForBonus? count: this.neededForBonus;
            var last = length === this.neededForBonus;
            var n = last === true? 1: 0;

            for(var i = 0;i<length-n;i++){
                var rightSide = this.bulbsHalf-i;
                var leftSide = this.bulbsHalf+i;
                this._mark(rightSide);
                this._mark(leftSide);
            }
            if(last)
                this.markLast();
            alteastream.Assets.playSound("bonusInc",0.3);
        }
    };

    p.fillAll = function(){
        this.fill(this.neededForBonus);
    };

    p.markLast = function(){// socket event hasBonus dolazi pre last fill..
        this._mark(0);
        this._canFill = false;
        this.lightsAnimation(true);
    };
    
    p.lightsAnimation = function(bool){
        if(bool === true){
            this._animate();
        }
        this._doAnimate = bool;
    };

    p.rotationAnimation = function(bool){
        this._markPair(bool);
        this._rotate(bool);
    };

    p.clear = function(){
        this.rotation = 0;
        this._reset();
        this._canFill = true;
    };

    alteastream.PowerUpWheel = createjs.promote(PowerUpWheel,"Container");
})();
"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var PusherBonusWheel = function(game,bonuses){
        this.ABSWheel_constructor(game,bonuses);
        this.initialize_PusherBonusWheel();
    };
    var p = createjs.extend(PusherBonusWheel, alteastream.ABSWheel);

    // public properties
    p.wheelBackground = null;
    // constructor
    p.initialize_PusherBonusWheel = function(){
        this._local_activate();// local ver
        var bg = this.wheelBackground = alteastream.Assets.getImage(alteastream.Assets.images.wheelBg);
        this.addChild(bg);
        bg.regX = bg.image.width*0.5;
        bg.regY = bg.image.height*0.5;
        // draw field ver
/*        var lowFieldColor = "#f7ec13";
        var fieldColors = [
            "#f8991d",
            "#109a48",
            "#4858a7",
            "#85449a",
            "#ed2024",
            "#f8991d",
            "#109a48",
            "#4858a7",
            "#85449a",
            "#ed2024"
        ];*/
        //this.createWheel("CoinBonusField",lowFieldColor,fieldColors);// draw field ver
        this.createWheel("CoinBonusField", "#000000", "#fff");
    };
    // private methods
    p._isMobile = function() {
        return localStorage.getItem("isMobile");
    };

    p._getImageSrc = function(gameCode, pathAdd, bgImage) {
        return "/gameplay/assets/"+gameCode+pathAdd+"/images/"+ bgImage +".png";
    };

    p._local_activate = function() {
        this._isMobile = function() {
            //return "yes";
            return "not";
        };

        this._getImageSrc = function(gameCode, pathAdd, bgImage) {
            return "../coinpusher/assets/"+gameCode+pathAdd+"/images/"+ bgImage +".png";
        };
    };
    // public methods
    p.setSkin = function(callback) {
        var _this = this;
        var gameCode = starter.getGameCode();
        var isMobile = this._isMobile();
        var pathAdd = isMobile === "not" ? "" : "/mobile";
        var image = new Image();
        var bgImage = "wheel_" + this.bonusesReceived.length * 2 + "_bonus_fields_back";
        image.src = this._getImageSrc(gameCode, pathAdd, bgImage);
        image.onload = function(){
            _this.wheelBackground.image = image;
            callback();
        }
    };

    p.updatePrizes = function(isEur, multiplier) {
        for(var i = 0; i < this.fields.length; i++){
            var field = this.fields[i];
            if(isEur === true){
                field.itemValue.text = (Number(field.itemValue.text) * multiplier).toFixed(2);
            }else{
                field.itemValue.text = Number(field.itemValue.text)*(100 * multiplier);
            }
        }
    };

    alteastream.PusherBonusWheel = createjs.promote(PusherBonusWheel,"ABSWheel");
})();


// namespace:
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var CoinPusherControlBoard = function() {
        this.AbstractControlBoard_constructor();
        this.initialize_CoinPusherControlBoard();
    };
    var p = createjs.extend(CoinPusherControlBoard, alteastream.AbstractControlBoard);

    // static properties:
    // events:
    // private properties:
    p._color_highlighted_text = "";
    p._color_disabled_text = "";
    p._color_enabled_text = "";
    // public properties:
    p.soundMachineToggled = false;

    // constructor:
    p.initialize_CoinPusherControlBoard = function() {
        this._color_highlighted_text = "green";
        this._color_disabled_text = "#0c1828";
        this._color_enabled_text = "white";
        this.textColorCoins = "#ffd500";
        this.textColorMoney = "#00ff00";

        this._setShooterComponent();
        this._setStakeComponent();
        //new stakes
        this._setFixedStakesComponent();
        this._setCreditsComponent();
        this._setBetViewComponent();
        this._setTopButtons();
        //this._setInfoComponent();
    };
    // private methods
    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline, mouseEnabled:false});
        this.addChild(textInstance);
    };

// private methods:
    p._setShooterComponent = function(){
        if(localStorage.getItem("isMobile") === "not"){
            var bg = this._shootComponentBg = alteastream.Assets.getImage(alteastream.Assets.images.controlsBg);
            bg.regX = bg.image.width * 0.5;
            bg.regY = bg.image.height * 0.5;
            bg.x = 960;
            bg.y = 935;
            this.addChild(bg);
        }

        var textBurst = this._buttonYesText = new createjs.Text("BURST","bold 28px Roboto","#fcfcfc");
        var textSingle = this._buttonYesText = new createjs.Text("SINGLE","bold 28px Roboto","#fcfcfc");

        var btnSend = this.btnSend = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,textSingle);
        btnSend.centerText(0,30);

        btnSend.normalTextColor = "#fcfcfc";
        btnSend.higlightedTextColor = "#fcfcfc";
        btnSend.disabledTextColor = "#6d6767";

        btnSend.addTextPreset("burst", textBurst , 0 , 63);
        btnSend.addTextPreset("single", textSingle , 0 , 63);
        btnSend.x = 960;
        btnSend.y = 935;
        btnSend.id = 10;
        this.addChild(btnSend);

        var btnShooterLeft = this.btnShooterLeft = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        btnShooterLeft.x = btnSend.x - 198 - (btnShooterLeft._singleImgWidth * 0.5);
        btnShooterLeft.y = btnSend.y - (btnShooterLeft._height * 0.5);
        btnShooterLeft.id = 7;//left
        this.addChild(btnShooterLeft);

        var btnShooterRight = this.btnShooterRight = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowRightSS),3);
        btnShooterRight.x = btnSend.x + 198 - (btnShooterRight._singleImgWidth * 0.5);
        btnShooterRight.y = btnShooterLeft.y;
        btnShooterRight.id = 8;//right
        this.addChild(btnShooterRight);
    };

    p._setStakeComponent = function(){
        var whitish = "#e4e4e4";
        var yellow = this.textColorCoins;

        //new stakes
        var xCorr = 87;

        var background = this.stakeBackground = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 272, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 272, 67);
        background.x = 1707 - xCorr;
        background.y = 987;
        this.addChild(background);

        var btnDecStake = this.btnDecStake = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnDecSS),3);
        btnDecStake.x = 1712- xCorr;
        btnDecStake.y = 992;
        this.addChild(btnDecStake);

        var btnIncStake = this.btnIncStake = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnIncSS),3);
        btnIncStake.x = 1828- xCorr;
        btnIncStake.y = 992;
        this.addChild(btnIncStake);

        this._createText("_textBet","BET","18px Lato",whitish,{x:1799- xCorr,y:1003,textAlign:"center", textBaseline:"middle"});
        this._createText("_textStakeAmount","0","18px Lato",yellow,{x:1799- xCorr,y:1022,textAlign:"center", textBaseline:"middle"});
    };

    p._setFixedStakesComponent = function(){
        this.fixedStakesPanel = new alteastream.FixedStakes();
        this.addChild(this.fixedStakesPanel);

        var btnFixedStakes = this.btnFixedStakes = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnFixedBet),3);
        btnFixedStakes.x = 1829;//1828
        btnFixedStakes.y = 992;//992
        this.addChild(btnFixedStakes);
    };

    p._setCollectComponent = function(){
        var btnCollect = this.btnCollect = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMedium2SS),3);
        btnCollect.x = 1391;
        btnCollect.y = 952;
        this.addChild(btnCollect);
    };

    p._setCreditsComponent = function(){
        var font = "16px Lato";
        var white = "#fff";
        var yellow = this.textColorCoins;
        var alignLeft = "left";
        var yPos = 1022;

        var background = this.balanceBackground = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 288, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 288, 67);
        background.x = 30;
        background.y = 987;
        this.addChild(background);

        var divider = this._divider = new createjs.Text("|",font,"#ffffff").set({textAlign:"center", textBaseline:"middle", x:154, y:1019});
        this.addChild(divider);

        //this._createText("_textCurrency","CURRENCY: ",font,white,{x:124,y:yPos,textAlign:alignLeft});
        this._createText("_textCurrencyValue","N/A",font,yellow,{x:122,y:yPos,textAlign:"center",textBaseline:"middle"});
        this._createText("_textCrd","BALANCE: ",font,white,{x:163,y:yPos,textAlign:alignLeft,textBaseline:"middle"});
        this._createText("_textCrdAmount","00.00",font,yellow,{x:245,y:yPos,textAlign:alignLeft,textBaseline:"middle"});
    };

    p._setBetViewComponent = function(){
        var btnBetView = this.btnBetView = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBetView),3);
        btnBetView.x = 35;
        btnBetView.y = 992;
        this.addChild(btnBetView);
    };

    p._setTopButtons = function() {
        var assets = alteastream.Assets;

/*        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var width = btnQuit.width;
        var spacing = (width/2)-10;*/

/*        var btnMachineSound = this.btnMachineSound = alteastream.Assets.getImage(assets.images.soundMachineOn);
        var that = this;
        btnMachineSound.on('click', function() {
            that.soundMachineBtnHandler();
        });

        btnMachineSound.x = btnQuit.x - width - spacing;

        assets.setMute(this.soundMachineToggled);*/

/*        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOff);
        this.soundToggled = true;
        var scene = alteastream.CoinPusher.getInstance();

        btnSound.x = btnMachineSound.x - width - spacing;*/

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnInfoSS),3);
        btnInfo.highlightedTextColor  = "#fff";

        //btnInfo.x = btnSound.x - width - spacing;

/*        var btnFs = scene.btnFs = alteastream.Assets.getImage(assets.images.btnFullScreen_On);
        var method;

        btnFs.x = btnInfo.x - width - spacing;

        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", scene[method]);*/

        var hiddenMenuContainer = this.hiddenMenuContainer = new createjs.Container();
        hiddenMenuContainer.x = 1845;
        hiddenMenuContainer.y = 30;

        this.hiddenMenuContainer.addChild(/*btnQuit, btnSound, btnFs, btnMachineSound, */btnInfo);
        this.addChild(this.hiddenMenuContainer);
    };

    p._showHiddenMenu = function(show) {
        this.doShowMenu(show);
        if(this.fixedStakesPanel._active){
            this.fixedStakesPanel.show(false);
        }
        if(this.menuIsShown === true && this.controlShown===true){
            this._moveShooterLeftRightAndBtnSendComponents(true);
        }else if(!this.menuIsShown && this.controlShown===false)
            this._moveShooterLeftRightAndBtnSendComponents(false);
    };

    p._moveShooterLeftRightAndBtnSendComponents = function(){};
    // static methods:
    // public methods:
    p.setBetAmount = function(amount) {
        this._textStakeAmount.text = amount;
    };

    p.hideControlls = function(hide) {
        this._shootComponentBg.visible = this.btnSend.visible = this.btnShooterLeft.visible = this.btnShooterRight.visible = !hide;
    };

    p.setBetLabelToMax = function(setToMax) {
        this._textBet.text = setToMax === true ? "MAX" : "BET";
        this._textBet.color = setToMax === true ? "#f9ff48" : "#fff";
    };

/*    p.checkIfHiddenMenuIsActive = function() {
        if(this.menuIsShown || this.fixedStakesPanel._active){
            this._showHiddenMenu(false);
        }
    };*/

/*    p.doShowMenu = function(show){
        this.menuIsShown = show;
    };*/

/*    p.soundMachineBtnHandler = function(){
        var video = document.getElementById("videoOutput");
        var assets = alteastream.Assets;
        this.soundMachineToggled = !this.soundMachineToggled;
        this.btnMachineSound.image = this.soundMachineToggled === true?
            assets.getImage(assets.images.soundMachineOff).image:
            assets.getImage(assets.images.soundMachineOn).image;

        video.muted = this.soundMachineToggled;
        assets.setMute(this.soundMachineToggled);
        this.checkIfHiddenMenuIsActive();

        window.localStorage.setItem('soundMachineIsToggled', String(this.soundMachineToggled));
    };*/

/*    p._superSoundBtnHandler = p.soundBtnHandler;//todo mozda prebaciit soundBtnHandler iz abs
    p.soundBtnHandler = function() {
        this._superSoundBtnHandler();
        this.checkIfHiddenMenuIsActive();

        window.localStorage.setItem('gameplaySoundIsToggled', String(this.soundToggled));
    };*/

/*    p.restoreMachineSoundState = function (){
        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            this.soundMachineToggled = soundMachineIsToggled === "false";
            this.soundMachineBtnHandler();
        }
    };*/

/*    p.restoreMusicSoundState = function (){
        var gameplaySoundIsToggled = window.localStorage.getItem("gameplaySoundIsToggled");
        if(gameplaySoundIsToggled !== null) {
            this.soundToggled = gameplaySoundIsToggled === "false";
            this.soundBtnHandler();
        }
    };*/

    alteastream.CoinPusherControlBoard = createjs.promote(CoinPusherControlBoard,"AbstractControlBoard");
})();
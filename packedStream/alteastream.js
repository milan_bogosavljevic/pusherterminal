this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var ABSVideoElement = function (options){
        this.initialize_ABSVideoElement(options);
    };

    var p = ABSVideoElement.prototype;

// static properties:
// events:
// public vars:
// private vars:
// constructor:
    p.initialize_ABSVideoElement = function (options){

        var videoID = options.id;

        var isMobile = window.localStorage.getItem('isMobile');
        var videoWidth = isMobile === "not" ? options.width : options.width/2;
        var videoHeight = isMobile === "not" ? options.height : options.height/2;
        var videoTag ="<video id ='"+videoID+"' width = '"+videoWidth+"' height= '"+videoHeight+"' autoplay muted playsinline></video>";

        var htmlVideo = this.htmlVideo = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        document.getElementById("wrapper").insertBefore(document.getElementById(videoID), document.getElementById("wrapper").firstChild);

        var style = htmlVideo.style;
        for(var p in options.style)
            style[p] = options.style[p];
        style.pointerEvents = "none";

        style["video::-webkit-media-controls-panel"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-play-button"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
        style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";
    };

    p.setSource = function(src){
        this.htmlVideo.src = src;
    }

    p.play = function(){
        this.htmlVideo.play();
    }

    p.setMuted = function(bool){
        this.htmlVideo.muted = bool;
    }

    p.setVolume = function(vol){
        this.htmlVideo.volume = vol;
    }

    p.setLoop = function(bool){
        this.htmlVideo.loop = bool;
    }

    p.clear = function(){
        this.htmlVideo.removeAttribute('src');
        this.htmlVideo.load();
    }

    p.recreateVideoElement = function(videoID){
        var canvas = document.getElementById("screen");
        canvas.parentNode.removeChild(this.htmlVideo);
        var htmlVideo = this.htmlVideo = document.createElement("video");
        htmlVideo.id = videoID;
        htmlVideo.autoplay = true;
        canvas.parentNode.insertBefore(htmlVideo,canvas);
        var style = htmlVideo.style;
        style.top = "0px";
        style.left ="0px" ;
        style.width = 100 + "%";
        style.height =  "0 auto";
        style.position = "absolute";
        style.pointerEvents = "none";

        return htmlVideo;
    };

    alteastream.ABSVideoElement = ABSVideoElement;
})();this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var ABSVideoStream = function (scene,options,address){
        this.initialize_ABSVideoStream(scene,options,address);
    };

    var p = ABSVideoStream.prototype;

    p.scene = null;
// static properties:
// events:
// VideoStream was here
// public vars:
// private vars:
// constructor:
    p.initialize_ABSVideoStream = function (scene,options,address){

        this.scene = scene;
        this.videoElement = new alteastream.ABSVideoElement(options);
        var webrtcPlayer = this.webrtcPlayer = new WEBRTCGamePlayer(this.videoElement.htmlVideo);
        this.setPlayer(address);

        webrtcPlayer.onError = function(error) {
            if(error) {
                this.handleError(error);
                //{"id":"error","code":-7,"txt":"User session does not exist"}
                //{"id":"error","code":13,"txt":"User token is expired or does not exist"}
            }
        }.bind(this);
    };

    p.activate = function(callback){
        var that = this;
        //var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
        this.webrtcPlayer.onStreamEvent = function(evt){
            if(evt.type === 'state' && evt.new === 'CONNECTED'){
                console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
            }
            if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                console.log("CONNECTED OK::::::::::::::::::: "+evt);

                that.streamConnected();
                that.setPreloadPoster(false);
                that.concreteActivate();
                //that.setMedia();
                if(callback){
                    callback();
                }
            }
        };

        this.startStreaming();
        this.setPreloadPoster(true);
        this.addSpinner();
        this.addMonitor();
    };

    p.stop = function(){
        this.webrtcPlayer.stop();
        this.clearVideoElement();
    };

    p.reset = function(){

    };

    p.clearVideoElement = function(){
        this.videoElement.clear();
    };

    alteastream.ABSVideoStream = ABSVideoStream;
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MockLoader = function(passedAnim){
        this.Container_constructor();
        this.initMockLoader(passedAnim);
    };

    var p = createjs.extend(MockLoader,createjs.Container);


    p.preloadInfoText = null;
    p.currentAnimation = null;
    p.active = false;
    p._defaultAnimation = true;

    p.initMockLoader = function (passedAnim) {
        if(passedAnim)
            this._defaultAnimation = false;

        this._createDynamicText("preloadInfoText","","bold 22px HurmeGeometricSans3","#FFFFFF",{x:0,y:0,textAlign:"center", textBaseline:"middle"});

        var currentAnimation = this.currentAnimation = this._defaultAnimation===true? alteastream.Assets.getImage(alteastream.Assets.images.spinner): passedAnim;
        this.addChild(currentAnimation);
        this._centerPivot();
        currentAnimation.x = 0;
        //currentAnimation.y = 90;
        var topMarging = 30;
        var spacing = 30;
        currentAnimation.y = this.currentAnimation.regY+topMarging;
        this.preloadInfoText.y = currentAnimation.y-(this._getDimensions().height*0.5)-spacing;
        currentAnimation.mouseEnabled = false;

        this.runPreload(false);
    };

    p.customScaleGfx = function(scale) {
        this.currentAnimation.scale = scale;
    };

    p.customGfxPosition = function(x,y) {
        this.currentAnimation.x = x;
        this.currentAnimation.y = y;
    };

    p.customFont = function(font, align) {
        this.preloadInfoText.font =  font;
        this.preloadInfoText.textAlign = align;
    };

    p.setLabel = function(label,newColor){
        this.preloadInfoText.text = label;
        this.preloadInfoText.color = newColor || "#ffffff";
    };

    p.runPreload = function(bool){
        this.preloadInfoText.visible = bool;
        this.currentAnimation.visible = bool;
        this.active = bool;
        if(bool){
            if(this._defaultAnimation){
                this.currentAnimation.on("tick",function(){this.rotation+=5;});
            }else{
                this.currentAnimation.animate(true);
            }
        }

        else{
            if(this._defaultAnimation){
                this.currentAnimation.removeAllEventListeners("tick");
            }else{
                this.currentAnimation.animate(false);
            }
        }
    };

    p._centerPivot = function(){
        if(this._defaultAnimation === true){
            this.currentAnimation.regX = this.currentAnimation.image.width*0.5;
            this.currentAnimation.regY = this.currentAnimation.image.height*0.5;
        }
    };

    p._getDimensions = function(){
        var dimensions = {};
        if(this._defaultAnimation === true){
            dimensions.width = this.currentAnimation.image.width;
            dimensions.height = this.currentAnimation.image.height;
        }else{
            dimensions.width = this.currentAnimation.getWidth();
            dimensions.height = this.currentAnimation.getHeight();
        }
        return dimensions;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };

    alteastream.MockLoader = createjs.promote(MockLoader,"Container");
})();


/**
 * Created by DACHA on 01-May-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var Renderer = function (game){
        this.initialize(game);
    };

    var p = Renderer.prototype;
    var _instance = null;

// static properties:

// events:

// public vars:

// private vars:
    p._isRendering = null;
    p._stage = null;

// constructor:
    p.initialize = function (game){
        _instance = this;
        createjs.Ticker.removeAllEventListeners("tick");
        this._isRendering = true;
        this.game = game;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this.game.mainStage.update();
        this._setRender(this._isRendering);
    };

// static methods:

// public functions:

    p.stageRendering = function(bool){
        console.log("stageRendering "+bool);
        this._isRendering = bool;
        this._setRender(this._isRendering);
    };

// private functions:
    p._setRender = function(on){
        var eventListener = on === true?"addEventListener":"removeEventListener";
        createjs.Ticker[eventListener]("tick", this._mainLoop);
    };

    p._mainLoop = function(event){
        _instance.game.renderLoop();
        _instance.game.mainStage.update(event);
    };

    alteastream.Renderer = Renderer;
})();/**
 * Created by Dacha on 6/12/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var SSAnimator = function (sheet,w,h,frames){
        this.Container_constructor();
        this.initializeSSAnimator(sheet,w,h,frames);
    };

    var p = createjs.extend(SSAnimator, createjs.Container);


// static properties:
// events:
// public vars:
// private vars:

    p._sprite = null;
    p._image = null;
    p._width = null;
    p._height = null;
    p._count = null;

// constructor:
    p.initializeSSAnimator = function (sheet,w,h,frames){
        this._image = alteastream.Assets.getImageURI(sheet);
        this._width = w;
        this._height = h;
        this._count = frames;
        this._setUp();
    };

// public functions:
    p._setUp = function (){
        var data = new createjs.SpriteSheet({
            images:[this._image],
            frames:{width:this._width, height:this._height, count:this._count},
            animations:{animate:[0, this._count-1, true]}
        });
        this._sprite = new createjs.Sprite(data,"animate");
        this.addChild(this._sprite);
        this._sprite.mouseEnabled = false;
        this._sprite.regX = this._width*0.5;
        this._sprite.regY = this._height*0.5;
        this._sprite.framerate = 30;
    };

    p.animate = function (bool){
        if(bool === true){
            this._sprite.play();

        }else{
            this._sprite.stop();
        }
    };

    p.getWidth = function (){
        return this._width;
    };

    p.getHeight = function (){
        return this._height;
    };

    p.dispose = function(){
        if(this._sprite){
            this.animate(false);
            this.removeAllChildren();
            this._sprite = null;
        }
    };

// private functions:
// static methods:
    alteastream.SSAnimator = createjs.promote(SSAnimator,"Container");
})();this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var StreamMonitor = function (stream){
        this.initialize_StreamMonitor(stream);
    };

    var p = StreamMonitor.prototype;

    p.stream = null;
    p._count = 0;
    p._attempts = 0;
    p._streamEstablished = false;
// static properties:
// events:
// public vars:
// private vars:
// constructor:
    p.initialize_StreamMonitor = function (stream){
         this.stream = stream;
         this._startMonitor();
    };

    p._startMonitor = function(){
        var that = this;
        this.streamEventMonitor = setInterval(function(){
            if(!that._streamEstablished){
                if(that._attempts<5){
                    if(that._count<10){
                        that._count++;
                        console.log("monitoring.. "+that._count);
                    }else{
                        that._attempts++;
                        that._count = 0;
                        that.stream.clearVideoElement();
                        that.stream.startStreaming();//preview/gameplay
                        console.log("attempt.. "+that._attempts);
                    }
                }else{
                    that._stopMonitor();
                    that._attempts = 0;
                    that.stream.onStreamFailed();
                }
            }
        },1000);
    };

    p._stopMonitor = function(){
        this._streamEstablished = false;
        clearInterval(this.streamEventMonitor);
        this._count = 0;
    };

    p.streamConnected = function(){
        this._streamEstablished = true;
        clearInterval(this.streamEventMonitor);
        this._attempts = 0;
        this._count = 0;
    };

    alteastream.StreamMonitor = StreamMonitor;
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var AbstractCounter = function(){
        this.Container_constructor();
        this.initAbstractCounter();
    };

    var p = createjs.extend(AbstractCounter,createjs.Container);

    p.counterInterval = null;
    p.counterCircle = null;
    p.counterCircleBackground = null;
    p.strokeTickness = null;
    p.counterRadius = null;
    p.colors = null;

    var _this;

    p.initAbstractCounter = function () {
        console.log("AbstractCounter initialized:::");
        _this = this;
        this.setLayout();
    };

    AbstractCounter.getInstance = function() {
        return _this;
    };

    p.show = function(bool){
        this.visible = bool;
    };

    p.update = function(waitTime){
        this.clearCounterInterval();
        var machineReadyTime = waitTime || 10;
        //var fullCircle = circleDegree || 360;
        var fullCircle = this.degree;
        var slice = (fullCircle/machineReadyTime)/fullCircle;

        var fillPercent = 1;
        this.counterInterval = setInterval(function () {
            machineReadyTime--;
            fillPercent -= slice;

            this._updateCounter(machineReadyTime);

            var fill = Math.round(fillPercent * fullCircle);
            var color = machineReadyTime>2?this.colors[0]:this.colors[1];
            this.counterCircle.graphics.clear().setStrokeStyle(this.strokeTickness).beginStroke(color).arc(0, 0, this.counterRadius, 0, (Math.PI/180)*fill).endStroke();
        }.bind(this),1000);
    };

    //new socket
    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else
            this.clearCounterInterval();
    };

    p.clearCounterInterval = function(){
        clearInterval(this.counterInterval);
        this.confirmEnterCounter.text = 0;
        this.counterCircle.graphics.clear();
    };

    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };

    alteastream.AbstractCounter = createjs.promote(AbstractCounter,"Container");
})();/**
 * Created by Dacha on 20.04.15..
 */

// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var BaseButton = function(text, width, height) {
        this.Container_constructor();
        this.initializeBaseButton(text, width, height);
    };
    var p = createjs.extend(BaseButton,createjs.Container);

// static properties:
// events:
// public properties:
    p.text = null;
    p.textColor = null;//alpha 1
    p.disabledTextColor = null;//alpha 0.5
    p.higlightedTextColor = null;//"white"
    p.normalTextColor = null;
    p.clickHandler = null;
    //p.cursor = null;
    p.width = null;
    p.height = null;
    p.mouseAreaWidth = null;
    p.mouseAreaHeight = null;
    p.mouseAreaX = null;
    p.mouseAreaY = null;
    p.isImage = null;
    p.prop = null;//new
    p.blinkable = false;
    p.hideOnDisabled = false;

    p.preActionHook = null;
    p.postActionHook = null;
    p.isClicked = false;
    p.value = null;
    p.id = null;

// private properties:
    p._disabled = false;
    p._storedDisabled = false;
    p._localClickHandler = null;
    p._presetFonts = null;
    p._extraLabel = null;


// constructor:
    p.initializeBaseButton = function(text, width, height) {

        this.width = width || this.width;
        this.height = height || this.width;
        this.text = text || new createjs.Text();
        this.mouseAreaWidth = width || this.width;
        this.mouseAreaHeight = height || this.height;


        if(text) {
            this.text = text;
            if(this.textIsImage(this.text)){
                //this.text.image = this.normalTextColor;
                this.prop = "alpha";//new
                this.textColor = 1;
            }else{
                this.prop = "color";//new
                this.textColor = this.text.color;
            }

            this.addChild(this.text);
            // text fix
            this.regX = width*0.5;
            this.regY = height*0.5;

            this.centerText();
        }

        this.preActionHook = function() {
            if(text){
                if (that.textIsImage(that.text)){
                    this.text.image = this.higlightedTextColor;
                }else{
                    this.text.color = this.higlightedTextColor;
                }
            }
        };
        var that = this;
        this.postActionHook = function() {
            if(text){
                if(that.textIsImage(that.text)){
                    that.prop = "alpha";
                    that.text.image = that.normalTextColor;
                    that.textColor = 1;
                }else{
                    that.prop = "color";
                }
                that.text[that.prop] = that._disabled ? that.disabledTextColor : that.textColor;
            }
            that.isClicked = false;
        };
    };

// static methods:
// public methods:
    p.addTextPreset = function(key, text, deltaX, deltaY) {
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;

        if(this._presetFonts === null)
            this._presetFonts = {};

        this._presetFonts[key] = {"text" : text, "deltaX" : deltaX, "deltaY" : deltaY};
    };

    p.selectTextPreset = function(key) {
        var preset = this._presetFonts[key];
        this.removeChild(this.text);
        this.text = preset.text.clone();
        this.addChild(this.text);
        this.textColor = this.text.color;
        this.centerText(preset.deltaX, preset.deltaY);
    };

    p.setClickHandler = function(handler) {
        if(this._localClickHandler !== null)
            this.removeEventListener('click', this._localClickHandler);

        var that = this;
        this._localClickHandler = function(event) {

            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                that.isClicked = true;
                handler(event);

                if (that.preActionHook) that.preActionHook(event);
                if (that.postActionHook) that.postActionHook(event);
            }
        };
        this.addEventListener('click', this._localClickHandler);
    };

    p.setUpHandler = function(handler) {
        if(this._upHandler !== null)
            this.removeEventListener('pressup', this._upHandler);

        this.upHandler = handler;
        var that = this;
        this._upHandler = function(event) {
            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                if(that.isHandled)
                    return;
                that.isDown = false;
                handler(event);
                that.mouseDownPostActionHook();
            }
        };
        this.addEventListener('pressup', this._upHandler);
    };

    p.setHoldDownHandler = function(handler) {
        if(this._downHandler !== null)
            this.removeEventListener('mousedown', this._downHandler);

        var that = this;
        this._downHandler = function(event) {
            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                that.isDown = true;
                that.isHandled = false;
                that.listenMouseLeave(that,event);
                handler(event);
                that.mouseDownPreActionHook(event);
            }
        };
        this.addEventListener('mousedown', this._downHandler);
    };

    p.listenMouseLeave = function(that,event) {
        checkMouseLeave(document, "mouseout", function(e) {
            e = e ? e : window.event;
            var element = e.relatedTarget || e.toElement;
            if (!element || element.nodeName !== "CANVAS") {
                if(that.isDown){
                    that.isDown = false;
                    that.upHandler(event);
                    that.isHandled = true;
                    that.mouseDownPostActionHook();
                }
            }
        });

        function checkMouseLeave(obj, evt, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evt, fn, false);
            }
        }
    };

    p.setDisabled = function(disabled) {
        if(disabled) {
            this._disable();
        } else {
            this._enable();
        }
    };

    p.isDisabled = function() {
        return this._disabled;
    };

    p.storeDisabledState = function() {
        this._storedDisabled = this._disabled;
    };

    p.restoreDisabledState = function() {
        this.setDisabled(this._storedDisabled);
    };

/*    p.centerText = function(deltaX, deltaY) {
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;
        //var width = this.textIsImage(this.text)?0:this.text.bitmapCache.width*0.5;    // bitmap text version
        //var height = this.textIsImage(this.text)?0:this.text.bitmapCache.height*0.5;  // bitmap text version
        var width = this.textIsImage(this.text)?0:this.text.getBounds().width*0.5;
        var height = this.textIsImage(this.text)?0:this.text.getBounds().height*0.5;
        this.text.x = this.width * 0.5 - width + deltaX;
        this.text.y = this.height * 0.5 - height + deltaY+5;
        this.text.textAlign = "center";
        this.text.textBaseline = "middle";
    };*/

    p.centerText = function(deltaX, deltaY) { // text fix
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;
        this.text.x = this.regX + deltaX;
        this.text.y = this.regY + deltaY;
        this.text.textAlign = "center";
        this.text.textBaseline = "middle";
    };

    p.reduceHitArea = function(xPos,yPos,w,h){
        this.mouseAreaWidth = this.mouseAreaWidth - w;
        this.mouseAreaHeight = this.mouseAreaHeight - h;
        this.mouseAreaX = 0 + xPos;
        this.mouseAreaY = 0 + yPos;

    };

    p.setClickListener = function(enabled) {
        if(enabled)
            this.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#f00").drawRect(this.mouseAreaX, this.mouseAreaY, this.mouseAreaWidth, this.mouseAreaHeight));
        else
            this.hitArea = new createjs.Shape();
    };

    p.setClickEnabled = function(enabled) {
        if(enabled)
            this.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#f00").drawRect(this.mouseAreaX,this.mouseAreaY,this.mouseAreaWidth,this.mouseAreaHeight));
        else
            this.hitArea = new createjs.Shape();
    };

    p.textIsImage = function(target){
        return target.toString() === "[Bitmap (name=null)]";
    };

// private methods:
    p._enable = function() {
        this.text[this.prop] = this.textColor;
        this.setClickEnabled(true);
        this._disabled = false;
    };

    p._disable = function() {
        this.text[this.prop] = this.disabledTextColor;
        this.setClickEnabled(false);
        this._disabled = true;
    };

    alteastream.BaseButton = createjs.promote(BaseButton,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BitmapText = function(text,font,color,props) {
        this.Text_constructor(text,font,color);
        this.initialize_BitmapText(text,font,color,props);
    };
    var p = createjs.extend(BitmapText, createjs.Text);

// static properties:
// events:
// public properties:

// private properties:
    p._defaultColor  = "#000";
    p._highlightColor  = "#FFF";

// constructor:
    p.initialize_BitmapText = function(text,font,color,props) {
        this.font = font;
        this.text = text || "...";

        this.textAlign = props.textAlign;
        this.textBaseline = "top";

        this.x = props.x;
        this.y = props.y;
        this.yCacheAdd = props.yCacheAdd || 0;

        if(typeof props.visible !== 'undefined')
            this.visible = props.visible/*===1*/;
        this._defaultColor = this.color = color;
        gamecore.Utils.DISPLAY.cacheText(this);
        this.mouseEnabled = false;
    };
// static methods:
// public methods:
    p.setText = function (text) {
        this.uncache();
        this.text = text;
        gamecore.Utils.DISPLAY.cacheText(this);
    };

    p.updateText = function(prop,change){
        this.uncache();
        this[prop] = change;
        gamecore.Utils.DISPLAY.cacheText(this);
    };

    p.setColor = function (color) {
        this.color = color;
        this.updateCache();
    };

    p.highlight = function(bool){
        this.uncache();
        this.color = bool === true? this._highlightColor: this._defaultColor;
        this.updateCache();
    };


    alteastream.BitmapText = createjs.promote(BitmapText,"Text");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Chip = function(amount,tag,multiplier) {
        this.Container_constructor();
        this.initialize_Chip(amount,tag,multiplier);
    };
    var p = createjs.extend(Chip, createjs.Container);
    // private properties:
    p._frontImage = null;
    p._backImage = null;
    p._frontFont = null;
    p._backFont = null;
    p._prizeImage = null;
    p._prizeAmount = null;
    p._moneyAmount = null;
    p._coinsAmount = null;
    p._isEur = null;
    //p._contrastFontsTags = [4,7,8,10,12];// with new chips
    // public properties:
    p.backScaleXForAnimation = 1;// chipovi za info u mobile verziji se scaliraju  tako da mora da postoji odvojen scaling za flip animaciju
                                 // tj. za mobile info cipove flip animacija scalira X od 0 do 0.68 , a za win na mobile , win na desktop, info na desktop scalira X od 0 do 1

    // static properties:
    // events:
    // constructor:
    p.initialize_Chip = function(amount,tag,multiplier) {
        this._init(amount,tag,multiplier);
    };

    // static methods:
    // private methods:
    p._init = function(amount,tag,multiplier){
        this.tagId = Number(tag);// with new chips

        var frontFont;

        if(amount > 99){
            frontFont = this._isMobile() === "not" ? "bold 24px HurmeGeometricSans3" : "bold 18px HurmeGeometricSans3";
            if(amount > 999){
                frontFont = this._isMobile() === "not" ? "bold 22px HurmeGeometricSans3" : "bold 16px HurmeGeometricSans3";
            }
        }else{
            frontFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";
        }

        //var fontBig = this._frontFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";
        this._frontFont = frontFont;
        this._backFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";

        var alignCenter = "center";
        var alignBaseline = "middle";

        var chipFront = "chip_"+tag+"_back";//new
        var chipBack = "chip_"+tag;//new
        var prizeImage = this._prizeImage = alteastream.Assets.getImage(alteastream.Assets.images[chipFront]);

        //var fontColor = this._frontFontColor = "#FFFFFF";//new
        //this._backFontColor = "#000000";//new
        var fontColor = this._frontFontColor = "#000000";//new
        this._frontImage = alteastream.Assets.getImage(alteastream.Assets.images[chipFront]);
        this._backImage = alteastream.Assets.getImage(alteastream.Assets.images[chipBack]);

        this.regX = prizeImage.image.width*0.5;
        this.regY = prizeImage.image.height*0.5;

        this._coinsAmount = amount;
        this._moneyAmount = (amount * multiplier).toFixed(2);

        var prizeAmount = this._prizeAmount = new createjs.Text(String(amount), frontFont, fontColor).set({textBaseline:alignBaseline,textAlign:alignCenter,alpha:0.8});
        prizeAmount.x = this.regX;
        prizeAmount.y = this.regY;
        this.addChild(prizeImage,prizeAmount);
    };

    p._isMobile = function() {
        return localStorage.getItem("isMobile");
    };

    p._updateProps = function(isEur) {
        if(isEur === true){
            this._prizeImage.image = this._backImage.image;
            this._prizeAmount.font = this._frontFont;
            this._prizeAmount.text = this._moneyAmount;
            //this._prizeAmount.color = this._backFontColor;//new
        }else{
            this._prizeImage.image = this._frontImage.image;
            this._prizeAmount.font = this._backFont;
            this._prizeAmount.text = this._coinsAmount;
            //this._prizeAmount.color = this._contrastFontsTags.indexOf(this.tagId)>-1? this._backFontColor: this._frontFontColor//new
        }
        this._prizeAmount.y = isEur === true? this.regY-10: this.regY;//new
    };

    // public methods:
    p.dispose = function(){
        this.removeAllChildren();
    };
    p.getSize = function(){
        return this._prizeImage.image.width;
    };

    p.updateChip = function(isEur, doAnimation) {
        if(this._isEur !== isEur){
            this._isEur = isEur;
            var method = doAnimation === true ? "flipCoin" : "_updateProps";
            this[method](isEur);
        }
    };

    p.flipCoin = function(isEur) {
        var _this = this;
        createjs.Tween.get(this).to({scaleX:0},100, createjs.Ease.quintInOut).call(function () {
            _this._updateProps(isEur);
            createjs.Tween.get(_this).to({scaleX:_this.backScaleXForAnimation},100,createjs.Ease.quintInOut);
        });
    };

    alteastream.Chip = createjs.promote(Chip,"Container");
})();/**
 * Created by Dacha on 20.04.15..
 */

// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var ImageButton = function(imgSeq, numOfFrames, text) {
        this.BaseButton_constructor();
        this.initializeImageButton(imgSeq, numOfFrames, text);
    };
    var p = createjs.extend(ImageButton,alteastream.BaseButton);

// static properties:
// events:
// public properties:
    p.spriteSheet = null;

// private properties:
    p._imgSeq = null;
    p._singleImgWidth = null;
    p._height = null;
    p._highlighted = false;

// constructor:
    p.Super_initialize = p.initializeBaseButton;
    p.initializeImageButton = function (imgSeq, numOfFrames, text) {

        this._imgSeq = imgSeq;
        var tmpBmp = new createjs.Bitmap(imgSeq);

        this._singleImgWidth = Math.floor(tmpBmp.image.width / numOfFrames);
        this._height = tmpBmp.image.height;
        this.numOfFrames = numOfFrames;

        this.Super_initialize(text, this._singleImgWidth, this._height);
        this._init();
    };

// static methods:
// public methods:
// private methods:

    p._init = function() {
        var data = {
            images: [this._imgSeq],
            frames: {width:this._singleImgWidth,height:this._height},
            animations: {
                normal:{
                    frames:[0]
                },
                highlight:{
                    frames: [1],
                    next: "normal",
                    speed: 0.2//0.2
                },
                disabled:{
                    frames: [2]
                }
            }
        };
        var spriteSheet1 = new createjs.SpriteSheet(data);
        this.spriteSheet = new createjs.Sprite(spriteSheet1, "normal");

        var that = this;
        // Play click animation before the action

        this.super_preActionHook = this.preActionHook;
        /*this.preActionHook = function(event) {// old ok
            that.super_preActionHook(event);
            if(that.spriteSheet){
                that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                that.spriteSheet.gotoAndPlay("highlight");
            }
        };*/
        this.preActionHook = function(event) {// new with _highlighted state remaining
            that.super_preActionHook(event);
            if(that.spriteSheet){
                if(that._highlighted === true){
                    that.spriteSheet.gotoAndStop("highlight");
                }else {
                    that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                    that.spriteSheet.gotoAndPlay("highlight");
                }
            }
        };


        this.mouseDownPreActionHook = function() {
            if(that.spriteSheet){
                that.spriteSheet.gotoAndStop("highlight");
            }
        };

        this.mouseDownPostActionHook = function() {
            if(that.spriteSheet){
                //that.spriteSheet.gotoAndStop("normal");//old
                that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                that.spriteSheet.gotoAndPlay("highlight");
            }
        };

        this.addChildAt(this.spriteSheet, 0);
    };

    p.setHighlighted = function(bool){// new with _highlighted state remaining
        if(this.spriteSheet){
            if(bool === true){
                this._highlighted = true;
                this.spriteSheet.gotoAndStop("highlight");
            }
            else
                this._highlighted = false;
        }
    };

    p._super_enable = p._enable;
    p._enable = function() {
        this._super_enable();
        this.spriteSheet.gotoAndStop("normal");
        this.isClicked = false;
    };

    p._super_disable = p._disable;
    p._disable = function() {
        this._super_disable();

        if(!this.isClicked)                           //stari highlight je bez uslova
            this.spriteSheet.gotoAndStop("disabled");//stari highlight
    };

    p.changeSkin = function(imgSeq){
        this.removeChildAt(0);
        this._imgSeq = imgSeq;
        var tmpBmp = new createjs.Bitmap(imgSeq);

        this._singleImgWidth = Math.floor(tmpBmp.image.width / this.numOfFrames);// HC 3 bilo
        this._height = tmpBmp.image.height;

        var data = {
            images: [this._imgSeq],
            frames: {width:this._singleImgWidth,height:this._height},
            animations: {
                normal:{
                    frames:[0]
                },
                highlight:{
                    frames: [1],
                    next: "normal",
                    speed: 0.2
                },
                disabled:{
                    frames: [2]
                }
            }
        };
        var spriteSheet1 = new createjs.SpriteSheet(data);
        this.spriteSheet = new createjs.Sprite(spriteSheet1, "normal");

        this.addChildAt(this.spriteSheet, 0);
    };

    alteastream.ImageButton = createjs.promote(ImageButton,"BaseButton");
})();


// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseObserver = function(scope,method,type) {
        this.initialize_MouseObserver(scope,method,type);
    };
    var p = MouseObserver.prototype;

// static properties:

// public properties:

// private properties:

// constructor:
    p.initialize_MouseObserver = function(scope,method,type) {
        this.props = {
            eventType:type,
            notify: function(subject) {
                scope[method](subject);
            }
        };
        return this.props;
    };
// static methods:

// public methods:


    alteastream.MouseObserver = MouseObserver;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseSubject = function() {
        this.initialize_MouseSubject();
    };
    var p = MouseSubject.prototype;

// static properties:
// public properties:
// private properties:

// constructor:
    p.initialize_MouseSubject = function() {
        this.observers = [];
    };

// static methods:
// public methods:
    p.subscribe = function(observer){
        this.observers.push(observer);
    };

    p.subscribeToAll = function(observers){

    };

    p.unsubscribe = function(observer){
        var index = this.observers.indexOf(observer);
        if(index > -1) {
            this.observers.splice(index, 1);
        }
    };

    p.unsubscribeAll = function(){
        this.observers = [];
    };

    p.notifyObservers = function(type,subject){
        if(this.observers.length>0)
        for(var i = 0,j = this.observers.length;i<j;i++){
            var observer = this.observers[i].props;
            if(observer.eventType === type)
                observer.notify(subject);
        }
    };

    alteastream.MouseSubject = MouseSubject;
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseWheel = function() {
        this.initialize_MouseWheel();
    };
    var p = MouseWheel.prototype;

// static properties:
// events:
// public properties:
// private properties:
    p._enabled = false;
    p._target = null;
    var that = null;

// constructor:

    p.initialize_MouseWheel = function() {
        that = this;
    };

// static methods:
    MouseWheel.getInstance = function(){
        return that;
    };

// public methods:
    p.enable = function(bool){
        var canvas = document.getElementById("screen");
        var listener = bool === true? "addEventListener": "removeEventListener";
        canvas[listener]("mousewheel", that._onMouseWheel, {passive: true});
        canvas[listener]("DOMMouseScroll", that._onMouseWheel, {passive: true});
        this._enabled = bool;
        console.log("MouseWheel enabled:: "+bool);
    };

    p.setTarget = function(target) {
        this._target = target;
    };

/*    p.setUpDownHandlers = function(up,down){
        p._rollUp = up || this._rollUp;
        p._rollDown = down || this._rollDown;
        if(!this._enabled)
            this.enable(true);
    };*/

// private methods:
/*    p._onMouseWheel = function(e){
        if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0){
            that._rollUp();
        }
        else{
            that._rollDown();
        }
    };*/

    p._onMouseWheel = function(e){
        if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0){
            that._target.onMouseWheel(1);
        }
        else{
            that._target.onMouseWheel(-1);
        }
    };

/*    p._rollUp = function(){
        throw "MouseWheel: No handler defined";
    };

    p._rollDown = function(){
        throw "MouseWheel: No handler defined";
    };*/

    alteastream.MouseWheel = MouseWheel;
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Scroller = function(target) {
        this.initialize_Scroller(target);
    };
    var p = Scroller.prototype;
    var _this = null;
// static properties:
// events:
// public properties:
// private properties:
    p._target = null;
    p._scrollType = null;
    p._scrollLevels = null;
    p._animateScrollCallback = null;
    p._preAnimateScrollMethod = null;
    p._postAnimateScrollMethod = null;
    p._numOfPages = 0;
    p._rowsPerPage = 0;
    p._pagination = null;
    p._scrollDirection = null;
    p.scrollByRowCurrentLevel = 0;
    p.scrollByPageCurrentLevel = 0;
    p.maxScrollLevelByPage = 0;

// constructor:

    p.initialize_Scroller = function(target) {
        console.log("Scroller Initialized");
        _this = this;
        this._target = target;
    };

    p.plugInPagination = function(pagination) {
        this._pagination = pagination;
    };

    p.setScrollType = function(type) {
        this._scrollType = type;
    };

    p.setScrollDirection = function(direction) {
        this._scrollDirection = direction;
    };

    p.setScrollLevels = function(levels) {
        this.scrollByRowCurrentLevel = 0;
        this._scrollLevels = levels;
    };

    p.setRowsPerPage = function(rows) {
        this._rowsPerPage = rows;
    };

    p.setToFirstPage = function() {
        //if(this.scrollByRowCurrentLevel > 0){
        this.scrollByRowCurrentLevel = 0;
        this.scrollByPageCurrentLevel = 0;
        this[this._scrollType](false);
        //}
    };

    p.setPreAnimateScrollMethod = function(method) {
        this._preAnimateScrollMethod = method;
    };

    p.setPostAnimateScrollMethod = function(method) {
        this._postAnimateScrollMethod = method;
    };

    p.getCurrentScrollLevel = function() {
        return this.scrollByRowCurrentLevel;
    };

    p.manageButtonsState = function() {
        if(this._pagination.hasButtons === false){
            return;
        }
        if(this._numOfPages < 2){
            this._pagination.disableButtons(true);
            return;
        }
        if(this.scrollByRowCurrentLevel === 0) {
            this._pagination._upButton.setDisabled(true);
            this._pagination._downButton.setDisabled(false);
        }else if(this.scrollByRowCurrentLevel === this._scrollLevels.length - 1) {
            this._pagination._downButton.setDisabled(true);
            this._pagination._upButton.setDisabled(false);
        }else{
            this._pagination.disableButtons(false);
        }
    };

    p.setNumberOfPages = function(numOfPages) {
        this._numOfPages = numOfPages;
        this.maxScrollLevelByPage = numOfPages - 1;
    };

    p.moveUp = function() {
        if(this.scrollByRowCurrentLevel > 0){
            var currentLevel = this._scrollType + "CurrentLevel";
            this[currentLevel]--;
            this[this._scrollType](true);
            if(this._pagination){this.manageButtonsState();}
        }
    };

    p.moveDown = function() {
        if(this.scrollByRowCurrentLevel < (this._scrollLevels.length - 1)) {
            var currentLevel = this._scrollType + "CurrentLevel";
            this[currentLevel]++;
            this[this._scrollType](true);
            if(this._pagination){this.manageButtonsState();}
        }
    };

    p.scrollByRow = function(doAnimation){
        var position = this._scrollLevels[this.scrollByRowCurrentLevel];
        this.scrollByPageCurrentLevel = Math.ceil(this.scrollByRowCurrentLevel / this._rowsPerPage);
        var animationSpeed = doAnimation === true ? 500 : 0;
        this._animateScroll(position,animationSpeed);
    };

    p.scrollByPage = function(doAnimation) {
        if(this.scrollByPageCurrentLevel === this.maxScrollLevelByPage){
            this.scrollByRowCurrentLevel = this._scrollLevels.length - 1;
        }else{
            this.scrollByRowCurrentLevel = this.scrollByPageCurrentLevel * this._rowsPerPage;
        }
        var position = this._scrollLevels[this.scrollByRowCurrentLevel];
        var animationSpeed = doAnimation === true ? 500 : 0;
        this._animateScroll(position,animationSpeed);
    };

    p.updateCurrentDotPosition = function() {
        this._pagination.moveCurrentDot(this.scrollByPageCurrentLevel);
    };

    p.dotClicked = function(scrollLevel){
        if(this.scrollByPageCurrentLevel !== scrollLevel){
            this.scrollByPageCurrentLevel = scrollLevel;
            this.scrollByPage(true);
        }
    };

    p._animateScroll = function(position,duration) {
        if(this._preAnimateScrollMethod){
            this._preAnimateScrollMethod();
        }
        var animationOptions = {};
        animationOptions.override = true;
        animationOptions[this._scrollDirection] = position;
        createjs.Tween.get(this._target).to(animationOptions , duration , createjs.Ease.quadOut).call(function () {
            if(this._postAnimateScrollMethod){
                this._postAnimateScrollMethod();
            }
        }.bind(this))
    };

    alteastream.Scroller = Scroller;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SpringAnim = function(image) {
        this.Container_constructor();
        this.initialize_SpringAnim(image);
    };
    var p = createjs.extend(SpringAnim, createjs.Container);

    // static properties:
    // events:
    // public properties:
    p.gravity = 1.1;
    // private properties:
    // constructor:
    p.initialize_SpringAnim = function(image) {
        var skin = alteastream.Assets.getImage(alteastream.Assets.images[image]);
        this.addChild(skin);

        this.pivot = skin.image.width*0.5;
        this.regX = this.pivot;
        this.regY = this.pivot;
        this.turn = 0.2;

        this.mouseEnabled = false;
        this.mouseChildren = false;
    };
    // static methods:
    // private methods:
    p._animate = function(){
        this.vy += this.gravity;
        this.x += this.vx;
        this.y += this.vy;
        this.rotation += this.vr;
        this.scaleY += this.turn;

        if(this.scaleY > 0.9)
            this.turn = -0.2;

        else if(this.scaleY < 0.1)
            this.turn = 0.2;
    };
    // public methods:
    p.setGravity = function(g){
        this.gravity = g;
    };

    p.setVX = function(min,max){
        this.vx = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.setVY = function(min,max){
        this.vy = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.setVR = function(min,max){
        this.vr = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.animate = function(){
        this.on("tick", function spawn(){
            this._animate();

            if(this.y - this.pivot > alteastream.AbstractScene.GAME_HEIGHT+this.pivot) {
                this.off("tick",spawn);
                this.dispose();
            }
        });
    };

    p.dispose = function(){
        this.removeAllChildren();
        this.parent.removeChild(this);
    };

    alteastream.SpringAnim = createjs.promote(SpringAnim,"Container");
})();
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var ABSStarter = function(locationSearch,gameId) {
        this.initialize(locationSearch,gameId);
    };
    var p = ABSStarter.prototype;
    var _instance = null;

// static properties:
// events:
// public properties:

// private properties:
    p._url = null;
    p._usr = null;
    p._serverUrl = null;
    p._game = null;

    p._lang = null;
    p._game_id = null;
    p._selectedGame = null;
    p._numOfParams = 0;
    p._params = null;
    p._suffix = "";
    p._gameName = null;

// constructor:
    p.initialize = function(locationSearch,gameId) {
        _instance = this;
        //var _supportedLanguages = ["en","sr"];
        this._game_id = gameId;
        this._protocol = locationSearch.split("?")[0].split("//")[0]+"//";
        this._url = locationSearch.split("?")[0].split("//")[1].split("/")[0];
        this._ipAdress = this._url.split(":")[0];
        this._port = ":"+this._url.split(":")[1];
        this._params = locationSearch.split("?")[1].split("&");
        
        for(var i = 0; i < this._params.length; i++) {
            var param = this._params[i].split("=");
            this["_"+param[0]] = param[1];
        }

        this._selectedGame = this._game_id;

        console.log("protocol = "+this._protocol);
        console.log("ip = "+this._ipAdress);
        console.log("port = "+this._port);
        console.log("url = "+this._url);// not needed
        console.log("full url = "+this._protocol+this._ipAdress+this._port);
        console.log("gameUrl = "+this._protocol+this._ipAdress);
        console.log("serverUrl = "+this._serverUrl);
        console.log("token = "+this._usr);
        console.log("machine name= "+this._machine_name);
        console.log("ID = "+this._game);
        console.log("game name = "+this._gameName);
    };

// static methods:
    ABSStarter.getInstance = function() {
        return _instance;
    };

    p.safariResize = function(){
        var deviceWidth = (window.parent.innerWidth > 0) ? window.parent.innerWidth : screen.width;
        var deviceHeight = (window.parent.innerHeight > 0) ? window.parent.innerHeight : screen.height;

        var innerWidth = deviceWidth + "px";
        var isLandscape = deviceWidth > deviceHeight;
        var topDocument = window.parent.document;

        if(topDocument.getElementById("image") || topDocument.getElementById("scrollDiv")){
            var im = topDocument.getElementById("image");
            var dv = topDocument.getElementById("scrollDiv");
            im.parentNode.removeChild(im);
            dv.parentNode.removeChild(dv);
        }

        if(isLandscape && window.parent.orientation !== 0 ){
            var ratio;
            if(deviceWidth/deviceHeight > 2.1){
                ratio = 19.5/9;
            }else if(deviceWidth/deviceHeight < 1.5){
                ratio = 4/3;
            }else{
                ratio = 16/9;
            }

            if(deviceWidth/deviceHeight > ratio+0.01){
                var div = document.createElement("div");
                div.id = "scrollDiv";

                var img = document.createElement("IMG");
                img.src = "swipeUp.png";
                img.id = "image";

                img.style.cssText = 'position:fixed;top:50%;left:50%;width:384px;height:217px;margin-left:-192px;margin-top:-108px;overflow-y:hidden';
                topDocument.body.appendChild(img);

                topDocument.body.appendChild(div);
                var divHeight = (deviceHeight + 100) + "px";
                div.style.cssText = 'position:absolute;top:50px;left:0px;width:'+innerWidth+';height:'+divHeight;
            }
        }
        // debug log for iphone
        //if(logText){
        //logText.text += "\n";
        //logText.text += "AR W = " + aspect.aW + " AR H = " + aspect.aH+" dwh: "+dwh;
        //logText.text += "ARW = " + aspect.aW + " ARH = " + aspect.aH+" dw: "+deviceWidth+" dh: "+deviceHeight;
        //logText.text += "ARW = " + aspect.aW + " DH = " + deviceHeight + "WinH "+window.top.innerHeight;
        //}
    };

    p.requestFullScreen = function(){
        var targetElement = window.parent.document.documentElement;
        var inFS = window.parent.document.fullscreenElement || window.parent.document.webkitFullscreenElement;
        var requestFS = targetElement.requestFullscreen || targetElement.webkitRequestFullscreen;
        if(!inFS){
            requestFS.call(targetElement);
        }
    };

    p.setResizeListener = function(){
        var that = this;
        if(createjs.Touch.isSupported()) {
            if(is.not.ios()){
                stage.addEventListener("click",function doResize(){
                    stage.removeEventListener("click",doResize);
                    that.requestFullScreen();
                });
                this.checkFS();
            }else{
                if(is.safari()){
                    if(is.ipad()){
                        stage.addEventListener("click",function doResize(){
                            stage.removeEventListener("click",doResize);
                            that.requestFullScreen();
                        });
                        that.checkFS();
                    }else{
                        window.parent.addEventListener("resize",this.safariResize);
                    }
                }
            }
        }
    };

    p.checkFS = function(){
        var that = this;
        $(window.parent.document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e)    {
            if (!window.screenTop && !window.screenY) {
                console.log('not fullscreen');
                stage.addEventListener("click",function doResize(){
                    stage.removeEventListener("click",doResize);
                    that.requestFullScreen();
                });
            } else {
                console.log('fullscreen');
            }
        });
    };

    p.firstCall = function(){
        //window.doLoad();// novo za invalid session
        this.pingServer();
    };

    p.finished = function(){
        //clearInterval(this._timer);
        alteastream.Assets.getQueue().removeAllEventListeners("complete");
        alteastream.Assets.getQueue().removeAllEventListeners("progress");
    };

    p._setLanguage = function(lang,supported){
        if(supported.indexOf(lang)>-1){
            this._lang = supported[supported.indexOf(lang)];
        }else{
            this._lang = "en";
        }
        console.log("loaded gameLanguage: "+this._lang);
    };

    p.getLanguage = function(){
        return this._lang === null? "en" : this._lang;
    };

    p.getGameName = function(){
        return this._selectedGame+this._suffix;
    };

    p.getGameCode = function(){
        return this._gameCode;
    };

    p.getGameAssets = function(){
        return this._selectedGame+"Assets";
    };

    p.pingServer = function(){
        console.log("type:: "+this.getGameName());
        var constVar = alteastream.AbstractScene;
        constVar.GAME_URL = this._protocol+this._url;
        constVar.GAME_ID = this._machine_name || "";//machine_name
        constVar.GAME_NAME = this._game || "";//game
        constVar.GAME_PROTOCOL = this._protocol;
        constVar.SERVER_IP = this._ipAdress;
        constVar.GAME_TOKEN = this._usr;
        var backToHouse = window.localStorage.getItem('backToHouse') || -1;
        constVar.BACK_TO_HOUSE = Number(backToHouse);
        constVar.GAME_QUIT = window.localStorage.getItem('quitUrl');
        constVar.MACHINE_NAME = this._gameName;
    };

// private methods:

    alteastream.ABSStarter = ABSStarter;
})();
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var Assets = function() {};

// static private properties:
    var _queue = null;
    var _texts = {};
    var _game = {};
    var _lang = "";
    var count = 0;
    var soundStorage = [];


// static public properties:

    Assets.VERSION = "2.7";

    //images
    Assets.images = {};
    Assets.images.ticketCircusType = "ticketCircusType";
    Assets.images.cashFestivalType = "cashFestivalType";
    Assets.images.diamondsAndPearlsType = "diamondsAndPearlsType";
    Assets.images.trialSmall = "trialSmall";//temp trial
    Assets.images.trialBig = "trialBig";//temp trial
    Assets.images.preloadingPoster = "preloadingPoster";
    Assets.images.liveIcon = "liveIcon";
    Assets.images.window = "window";
    Assets.images.windowList = "windowList";
    Assets.images.window_big = "window_big";
    Assets.images.beam = "beam";
    Assets.images.flare = "flare";
    Assets.images.thumb = "thumb";
    Assets.images.noThumb = "noThumb";
    Assets.images.machineBg = "machineBg";
    Assets.images.machineOverlay = "machineOverlay";
    Assets.images.spinner = "spinner";
    Assets.images.seatOn = "seatOn";
    Assets.images.seatOff = "seatOff";
    Assets.images.mainBackground = "mainBackground";
    Assets.images.mainBackgroundWithHole = "mainBackgroundWithHole";
    Assets.images.mainBackgroundOverlay = "mainBackgroundOverlay";
    Assets.images.mainBackgroundOverlay2 = "mainBackgroundOverlay2";
    Assets.images.infoBackground = "infoBackground";
    Assets.images.infoPage1Image = "infoPage1Image";
    Assets.images.infoPage2Image = "infoPage2Image";
    Assets.images.infoPage3Image = "infoPage3Image";
    Assets.images.infoBackgroundNoBonus = "infoBackgroundNoBonus";
    Assets.images.floor = "floor";
    Assets.images.houseClosed = "houseClosed";
    Assets.images.smallHouse = "smallHouse";
    Assets.images.smallHouseCurrent = "smallHouseCurrent";
    Assets.images.house = "house";
    Assets.images.houseHighlight = "houseHighlight";
    Assets.images.infoImage = "infoImage";
    Assets.images.tooltipImage = "tooltipImage";
    Assets.images.popupBg = "popupBg";
    Assets.images.introVideo = "introVideo";
    Assets.images.cursorTgt = "cursorTgt";
    Assets.images.mainLogo = "mainLogo";
    Assets.images.refundComponentBackground = "refundComponentBackground";
    // Hajko novo added
    Assets.images.lampYellow = "lampYellow";
    Assets.images.lampYellowDark = "lampYellowDark";
    Assets.images.lampGreen = "lampGreen";
    Assets.images.lampGreenLight = "lampGreenLight";
    Assets.images.lampRed = "lampRed";
    Assets.images.listMachineBackground = "listMachineBackground";

    //buttons
    Assets.images.infoButton = "infoButton";
    Assets.images.infoButtonLeft = "infoButtonLeft";
    Assets.images.infoButtonRight = "infoButtonRight";
    Assets.images.confirmQuitBtn = "confirmQuitBtn";
    Assets.images.btnQuitSS = "btnQuitSS";
    Assets.images.btnInfoSS = "btnInfoSS";
    //Assets.images.btnBackSS = "btnBackSS";
    Assets.images.soundOn = "soundOn";
    Assets.images.soundOff = "soundOff";
    Assets.images.soundMachineOn = "soundMachineOn";
    Assets.images.soundMachineOff = "soundMachineOff";
    Assets.images.btnFullScreen_On = "btnFullScreen_On";
    Assets.images.btnFullScreen_Off = "btnFullScreen_Off";
    Assets.images.btnPlayerActivity = "btnPlayerActivity";
    Assets.images.btnShowAllMachinesActive = "btnShowAllMachinesActive";
    Assets.images.btnShowAllMachinesNotActive = "btnShowAllMachinesNotActive";
    Assets.images.btnShowAvailableMachinesActive = "btnShowAvailableMachinesActive";
    Assets.images.btnShowAvailableMachinesNotActive = "btnShowAvailableMachinesNotActive";
    Assets.images.btnQuitNo = "btnQuitNo";
    Assets.images.btnQuitYes = "btnQuitYes";

    Assets.images.backgroundCB = "backgroundCB";
    //Assets.images.btnMockSS = "btnMockSS";
    Assets.images.btnUp = "btnUp";
    Assets.images.btnDown = "btnDown";
    Assets.images.btnPrev = "btnPrev";
    Assets.images.btnNext = "btnNext";
    Assets.images.btnIncSS = "btnIncSS";
    Assets.images.btnDecSS = "btnDecSS";
    Assets.images.btnFixedBet = "btnFixedBet";
    Assets.images.btnLargeSS = "btnLargeSS";
    Assets.images.btnMediumSS = "btnMediumSS";
    Assets.images.btnMedium2SS = "btnMedium2SS";
    Assets.images.btnArrowLeftSS = "btnArrowLeftSS";
    Assets.images.btnArrowRightSS = "btnArrowRightSS";
    Assets.images.btnBetView = "btnBetView";
    Assets.images.arrowButtonsBackground = "arrowButtonsBackground";
    Assets.images.betBackground = "betBackground";
    Assets.images.hiddenMenuBackground = "hiddenMenuBackground";
    Assets.images.queueInfoBg = "queueInfo_bg";
    Assets.images.controlsBg = "controlsBg";

    Assets.images.btnEnter = "btnEnter";
    Assets.images.btnStart = "btnStart";
    Assets.images.btnStart2 = "btnStart2";
    Assets.images.btnBackToGame = "btnBackToGame";
    Assets.images.btnLeave = "btnLeave";
    Assets.images.btnBack = "btnBack";
    Assets.images.btnBack2 = "btnBack2";
    Assets.images.btnBackForEntranceCounter = "btnBackForEntranceCounter";
    Assets.images.btnExitForQuitCounter = "btnExitForQuitCounter";
    Assets.images.quitCounterBg = "quitCounterBg";
    Assets.images.btnParticipate = "btnParticipate";
    Assets.images.btnStopParticipate = "btnStopParticipate";
    Assets.images.separator = "separator";
    Assets.images.btnStakeDec = "btnStakeDec";
    Assets.images.btnStakeInc = "btnStakeInc";

    Assets.images.btnHiddenMenu = "btnHiddenMenu";
    Assets.images.btnHiddenMenuTransparent = "btnHiddenMenuTransparent";
    Assets.images.btnHiddenMenuTransparentBlack = "btnHiddenMenuTransparentBlack";

    //Assets.images.singleShotTextImg = "singleShotTextImg";
    //Assets.images.burstShotTextImg = "burstShotTextImg";


    //roulette start
    Assets.images.btnStepBackSS = "btnStepBack";
    Assets.images.btnClearSS = "btnClear";
    Assets.images.fieldMini = "fieldMini";
    Assets.images.fieldMiniH = "fieldMiniH";
    Assets.images.fieldSpec = "fieldSpec";
    Assets.images.fieldSpecH = "fieldSpecH";
    Assets.images.fieldSmall = "fieldSmall";
    Assets.images.fieldSmall_red = "fieldSmall_red";
    Assets.images.fieldSmall_black = "fieldSmall_black";
    Assets.images.fieldSmall_green = "fieldSmall_green";
    Assets.images.fieldMedium = "fieldMedium";
    Assets.images.fieldLarge = "fieldLarge";
    Assets.images.fieldZero = "fieldZero";
    Assets.images.fieldSplit = "fieldSplit";
    Assets.images.fieldSplitTest = "fieldSplitTest";// temp dev visibility test only
    Assets.images.fieldSmallH = "fieldSmallH";
    Assets.images.fieldMediumH = "fieldMediumH";
    Assets.images.fieldLargeH = "fieldLargeH";
    Assets.images.fieldZeroH = "fieldZeroH";
    Assets.images.label_red = "label_red";
    Assets.images.label_black = "label_black";
    Assets.images.fieldTrack = "fieldTrack";
    Assets.images.fieldTrack_black = "fieldTrack_black";
    Assets.images.fieldTrack_red = "fieldTrack_red";
    Assets.images.fieldTrack_green = "fieldTrack_green";
    Assets.images.fieldTrackH = "fieldTrackH";
    Assets.images.winningBg_red = "winningBg_red";
    Assets.images.winningBg_black = "winningBg_black";
    Assets.images.winningBg_green = "winningBg_green";

    Assets.images.welcomeBackBackground = "welcomeBackBackground";
    Assets.images.poster = "poster";

    Assets.images.chip0 = "chip0";
    Assets.images.chip1 = "chip1";
    Assets.images.chip2 = "chip2";
    Assets.images.chip3 = "chip3";
    Assets.images.chip4 = "chip4";
    Assets.images.chip5 = "chip5";
    Assets.images.chip6 = "chip6";
    Assets.images.chip7 = "chip7";

    Assets.images.chipBtn0 = "chipBtn0";
    Assets.images.chipBtn1 = "chipBtn1";
    Assets.images.chipBtn2 = "chipBtn2";
    Assets.images.chipBtn3 = "chipBtn3";
    Assets.images.chipBtn4 = "chipBtn4";
    Assets.images.chipBtn5 = "chipBtn5";
    Assets.images.chipBtn6 = "chipBtn6";
    Assets.images.chipBtn7 = "chipBtn7";

    //roulette end

    Assets.images.targetArea = "targetArea";

    Assets.images.metalCoin = "metalCoin";
    Assets.images.sunLogo = "sunLogo";
    Assets.images.coinWin = "coinWin";
    Assets.images.chip_glow = "chip_glow";
    Assets.images.chip_1 = "chip_1";
    Assets.images.chip_2 = "chip_2";
    Assets.images.chip_3 = "chip_3";
    Assets.images.chip_4 = "chip_4";
    Assets.images.chip_5 = "chip_5";
    Assets.images.chip_6 = "chip_6";
    Assets.images.chip_7 = "chip_7";
    Assets.images.chip_8 = "chip_8";
    Assets.images.chip_9 = "chip_9";
    Assets.images.chip_10 = "chip_10";
    Assets.images.chip_11 = "chip_11";
    Assets.images.chip_12 = "chip_12";
    Assets.images.chip_0 = "chip_0";
    Assets.images.chip_1_back = "chip_1_back";
    Assets.images.chip_2_back = "chip_2_back";
    Assets.images.chip_3_back = "chip_3_back";
    Assets.images.chip_4_back = "chip_4_back";
    Assets.images.chip_5_back = "chip_5_back";
    Assets.images.chip_6_back = "chip_6_back";
    Assets.images.chip_7_back = "chip_7_back";
    Assets.images.chip_8_back = "chip_8_back";
    Assets.images.chip_9_back = "chip_9_back";
    Assets.images.chip_10_back = "chip_10_back";
    Assets.images.chip_11_back = "chip_11_back";
    Assets.images.chip_12_back = "chip_12_back";
    Assets.images.chip_0_back = "chip_0_back";
    Assets.images.lampOn = "lampOn";
    Assets.images.lampOff = "lampOff";
    Assets.images.arrow = "arrow";
    Assets.images.wheelBg = "wheelBg";
    Assets.images.wheel_8_bonus_fields_highlight = "wheel_8_bonus_fields_highlight";
    Assets.images.bounsWonBgImage = "bounsWonBgImage";
    Assets.images.bounsWonBgImageRing = "bounsWonBgImageRing";
    Assets.images.animatedLogo = "animatedLogo";
    Assets.images.liveStreamConnecting = "liveStreamConnecting";

    // Sounds
    Assets.sounds = {};
    Assets.sounds.shooter = "shooter";
    Assets.sounds.shooter2 = "shooter2";
    Assets.sounds.insertCoin = "insertCoin";
    Assets.sounds.refund = "refund";
    Assets.sounds.shooterStop = "shooterStop";
    Assets.sounds.bgMusic = "bgMusic";
    Assets.sounds.bgMusicMachine = "bgMusicMachine";
    Assets.sounds.btnUp = "btnUp";
    Assets.sounds.btnDown = "btnDown";
    Assets.sounds.btnDownDouble = "btnDownDouble";
    Assets.sounds.win1 = "win1";
    Assets.sounds.error = "error";
    Assets.sounds.info = "info";
    Assets.sounds.warning = "warning";
    Assets.sounds.exception = "exception";
    Assets.sounds.confirm = "confirm";
    Assets.sounds.welcomeMessageSpoken = "welcomeMessageSpoken";
    Assets.sounds.errorSpoken = "errorSpoken";
    Assets.sounds.continuePlayingSpoken = "continuePlayingSpoken";
    Assets.sounds.byeSpoken = "byeSpoken";
    Assets.sounds.youHaveWonSpoken = "youHaveWonSpoken";
    Assets.sounds.prizeDetect4 = "prizeDetect4";
    Assets.sounds.shootCoinsSpoken = "shootCoinsSpoken";
    Assets.sounds.keepGoingSpoken = "keepGoingSpoken";
    Assets.sounds.feelDizzySpoken = "feelDizzySpoken";
    Assets.sounds.reallyGoodSpoken = "reallyGoodSpoken";
    Assets.sounds.seeYouSpoken = "seeYouSpoken";

    Assets.sounds.prize_1 = "prize_1";
    Assets.sounds.prize_2 = "prize_2";
    Assets.sounds.prize_3 = "prize_3";
    Assets.sounds.prize_5 = "prize_5";
    Assets.sounds.prize_10 = "prize_10";
    Assets.sounds.prize_15 = "prize_15";
    Assets.sounds.prize_20 = "prize_20";
    Assets.sounds.prize_25 = "prize_25";
    Assets.sounds.prize_30 = "prize_30";
    Assets.sounds.prize_35 = "prize_35";
    Assets.sounds.prize_40 = "prize_40";
    Assets.sounds.prize_45 = "prize_45";
    Assets.sounds.prize_50 = "prize_50";
    Assets.sounds.prize_55 = "prize_55";
    Assets.sounds.prize_60 = "prize_60";
    Assets.sounds.prize_65 = "prize_65";
    Assets.sounds.prize_70 = "prize_70";
    Assets.sounds.prize_75 = "prize_75";
    Assets.sounds.prize_80 = "prize_80";
    Assets.sounds.prize_85 = "prize_85";
    Assets.sounds.prize_90 = "prize_90";
    Assets.sounds.prize_95 = "prize_95";
    Assets.sounds.prize_100 = "prize_100";
    Assets.sounds.bonusInc = "bonusInc";
   // Assets.sounds.bonusSpinSound = "bonusSpinSound";
    Assets.sounds.bonusWinnerSpoken_1 = "bonusWinnerSpoken_1";
    Assets.sounds.bonusWinnerSpoken_2 = "bonusWinnerSpoken_2";
    Assets.sounds.bonusSpinLoop = "bonusSpinLoop";
    Assets.sounds.bonusWord = "bonusWord";

    Assets.sounds.buttonClick = "buttonClick";
    Assets.sounds.winSound = "winSound";
    Assets.sounds.winCheer = "winCheer";
    Assets.sounds.buttonCollect = "buttonCollect";
    Assets.sounds.countLoop = "countLoop";
    Assets.sounds.chipDown = "chipDown";

    Assets.sounds.spoken_0 = "spoken_0";
    Assets.sounds.spoken_1 = "spoken_1";
    Assets.sounds.spoken_2 = "spoken_2";
    Assets.sounds.spoken_3 = "spoken_3";
    Assets.sounds.spoken_4 = "spoken_4";
    Assets.sounds.spoken_5 = "spoken_5";
    Assets.sounds.spoken_6 = "spoken_6";
    Assets.sounds.spoken_7 = "spoken_7";
    Assets.sounds.spoken_8 = "spoken_8";
    Assets.sounds.spoken_9 = "spoken_9";
    Assets.sounds.spoken_10 = "spoken_10";
    Assets.sounds.spoken_11 = "spoken_11";
    Assets.sounds.spoken_12 = "spoken_12";
    Assets.sounds.spoken_13 = "spoken_13";
    Assets.sounds.spoken_14 = "spoken_14";
    Assets.sounds.spoken_15 = "spoken_15";
    Assets.sounds.spoken_16 = "spoken_16";
    Assets.sounds.spoken_17 = "spoken_17";
    Assets.sounds.spoken_18 = "spoken_18";
    Assets.sounds.spoken_19 = "spoken_19";
    Assets.sounds.spoken_20 = "spoken_20";
    Assets.sounds.spoken_21 = "spoken_21";
    Assets.sounds.spoken_22 = "spoken_22";
    Assets.sounds.spoken_23 = "spoken_23";
    Assets.sounds.spoken_24 = "spoken_24";
    Assets.sounds.spoken_25 = "spoken_25";
    Assets.sounds.spoken_26 = "spoken_26";
    Assets.sounds.spoken_27 = "spoken_27";
    Assets.sounds.spoken_28 = "spoken_28";
    Assets.sounds.spoken_29 = "spoken_29";
    Assets.sounds.spoken_30 = "spoken_30";
    Assets.sounds.spoken_31 = "spoken_31";
    Assets.sounds.spoken_32 = "spoken_32";
    Assets.sounds.spoken_33 = "spoken_33";
    Assets.sounds.spoken_34 = "spoken_34";
    Assets.sounds.spoken_35 = "spoken_35";
    Assets.sounds.spoken_36 = "spoken_36";
    Assets.sounds.spoken_welcome = "spoken_welcome";
    Assets.sounds.spoken_youWin_1 = "spoken_youWin_1";
    Assets.sounds.spoken_youWin_2 = "spoken_youWin_2";
    Assets.sounds.spoken_youWin_3 = "spoken_youWin_3";
    Assets.sounds.spoken_youWin_4 = "spoken_youWin_4";
    Assets.sounds.spoken_placeBet = "spoken_placeBet";
    Assets.sounds.spoken_placeBet_2 = "spoken_placeBet_2";
    Assets.sounds.spoken_maxReached = "spoken_maxReached";
    Assets.sounds.spoken_clearedBets = "spoken_clearedBets";
    Assets.sounds.spoken_doSpin_1 = "spoken_doSpin_1";
    Assets.sounds.spoken_doSpin_2 = "spoken_doSpin_2";
    Assets.sounds.spoken_doSpin_3 = "spoken_doSpin_3";
    Assets.sounds.spoken_last5 = "spoken_last5";
    Assets.sounds.spoken_winningNumber_1 = "spoken_winningNumber_1";
    Assets.sounds.spoken_winningNumber_2 = "spoken_winningNumber_2";
    Assets.sounds.spoken_noWin_1 = "spoken_noWin_1";
    Assets.sounds.spoken_noWin_2 = "spoken_noWin_2";


    // Texts
    Assets.texts = {};
    Assets.texts.infoText1 = "infoText1";
    Assets.texts.infoText2 = "infoText2";
    Assets.texts.infoText3 = "infoText3";
    Assets.texts.infoText4 = "infoText4";
    Assets.texts.infoText5 = "infoText5";
    Assets.texts.infoText6 = "infoText6";
    Assets.texts.infoText7 = "infoText7";
    Assets.texts.infoText8 = "infoText8";
    Assets.texts.sessionDoesNotExist = "Session does not exist";
    Assets.texts.casinoServiceUnreachable = "CasinoServiceUnreachable";

    //imageTexts
    Assets.imgTexts = {};
    Assets.imgTexts.spinTxtImg = "spinTxtImg";
    Assets.imgTexts.stopTxtImg = "stopTxtImg";
    Assets.imgTexts.powerTxtImg = "powerTxtImg";

// events:
// public properties:
// private properties:
// constructor:
// static methods:
    Assets.getQueue = function(){
        return _queue;
    };

    Assets.setImageLang = function(lang){
        _lang = lang;
    };

    Assets.getImageLang = function() {
        return _lang;
    };

    Assets.setAtlas = function(classIs){
        _game = classIs;
    };

    Assets.getAtlas = function(assetId) {
        return _game[assetId];
    };

    Assets.load = function(loadManifest, handleComplete, handleProgress) {
        _queue = new createjs.LoadQueue(false);
        createjs.Sound.alternateExtensions = ["mp3"];
        _queue.installPlugin(createjs.Sound);

        if(handleComplete)
            _queue.addEventListener("complete", handleComplete);
        if(handleProgress)
            _queue.addEventListener("progress", handleProgress);

        //Assets._listDebug(_queue);

        _queue.loadManifest(loadManifest);
    };

    Assets.getSoundLength = function(sound) {
        return  Math.round(_queue.getResult(sound).duration)*1000;
    };

    Assets.getSoundsLoadedDebug = function(){
        return debugText;
    };

    Assets.setTexts = function(texts) {
        _texts = texts;
    };

    Assets.getText = function(assetId) {
        return _texts[assetId];
    };

    Assets.getLoadProgress = function() {
        return _queue.progress;
    };

    Assets.getAtlasImage = function ( atlas, byName ) {
        var jsonData = atlas+"AtlasData";
        //console.log("trazi Atlas "+jsonData);
        //console.log("sa image_om "+byName);
        var imgName = Assets.chooseImg(byName);
        //console.log("imgName je "+imgName);
        var _atlasData = _queue.getResult(jsonData);
        //console.log("_atlasData "+_atlasData);
        var imageID = _atlasData.animations[ imgName ][0];
        var rectarr = _atlasData.frames[ imageID ];
        var rect = new createjs.Rectangle( );
        rect.x = rectarr[0];
        rect.y = rectarr[1];
        rect.width = rectarr[2];
        rect.height = rectarr[3];

        var currentAtlas = atlas+"Atlas";
        var bmp = new createjs.Bitmap( _queue.getResult(currentAtlas) );
        bmp.sourceRect = rect;
        bmp.width = rect.width;
        bmp.height = rect.height;

        return bmp;
    };

    //our images dir image
    Assets.getThumbnail = function(machineName,holder,w,h,callback){
        var image = new Image();
        image.src = "../images/"+machineName+".jpg?ver="+Math.random();
        //image.src = alteastream.StarterLobby.PREVIEW_SERVER + "/images/" + machineName + ".jpg?ver="+Math.random();
        image.crossOrigin = "";
        holder.hasThumbImage = true;

        image.onerror = function(){
            //image.src = "../images/errThumb.jpg";
            holder.hasThumbImage = false;
            callback();
        };
        image.onload = function(){
            var thumb = new createjs.Bitmap(image);
            //thumb.image.crossOrigin = "Anonymous";
            holder.image = thumb.image;
            callback();
        };
    };

    Assets.chooseImg = function(choose){
        return Assets.getAtlas(choose);
    };

    Assets.getImage = function(assetId) {
        return new createjs.Bitmap(_queue.getResult(assetId));
    };

    Assets.getFont = function(assetId) {
        console.log("asset "+assetId);
        return _queue.getResult(assetId);
    };

    Assets.getFontImage = function(assetId) {
        console.log("asset image "+assetId);
        return _queue.getResult(assetId);
    };

    Assets.getImageURI = function(assetId) {
        return _queue.getResult(assetId);
    };

    //Sound
    Assets.playSound = function(assetId,vol) {
        var sound = createjs.Sound.play(assetId);
        sound.volume = vol||1;
        return sound;
    };

    Assets.playSoundChain = function(assetId,vol,callback,scope) {//temp, just test
        var sound = createjs.Sound.play(assetId);
        sound.volume = vol||1;
        if(sound.duration > 0){
            sound.on("complete", callback, scope);
        }else{// temp fix for prize chips with no sound files
            setTimeout(function (){
                callback();
            },1000);
        }
    };

    Assets.stopSoundChannel = function(instance) {
        for(var i = 0,j=soundStorage.length;i<j;i++){
            if(soundStorage[i].name === instance){
                soundStorage[i].stop();
                soundStorage.splice(i,1);
            }
        }
    };

    Assets.playSoundChannelWithOptions = function(instance, properties) {
        var sound = createjs.Sound.play(instance, properties);
        sound.name = instance;
        sound.volume = properties.vol||1;
        if(soundStorage.length>0){
            for(var i = 0,j=soundStorage.length;i<j;i++){
                if(soundStorage[i].name !== instance){
                    soundStorage.push(sound);
                }
            }
        }else{soundStorage.push(sound);}

        return sound;
    };

    Assets.setMute = function(bool) {
        createjs.Sound.muted = bool;
    };

    Assets.soundVolume = function(sound,vol) {
        for(var i = 0,j=soundStorage.length;i<j;i++){
            if(soundStorage[i].name === sound){
                soundStorage[i].volume = vol;
            }
        }
    };

    Assets.stopSound = function() {
        createjs.Sound.stop();
    };

    Assets.stopAllSounds = function() {
        createjs.Sound.stop();
        soundStorage = [];
    };

// public methods:
// private methods:
    Assets._listDebug = function(_queue) {
        var debugTxt = new createjs.Text("","12px Arial","white").set({x:100});
        window.stage.addChild(debugTxt);

        _queue.on("fileload", function (event) {
            var item = event.item;
            var type = item.type;
            //if (type == createjs.AbstractLoader.VIDEO) {
                //if (type == createjs.LoadQueue.SOUND) {
                //if (type == createjs.LoadQueue.IMAGE) {
                //if (type == createjs.LoadQueue.JSON) {
                var str = String(event.item.src);
                var ext = str.slice(str.length-4,str.length);
                debugTxt.text += "\nFile.. "+String(event.item.id)+ext;
                console.log("File.. "+event.item.id+ext);
           // }
            count++;
            console.log("Total:: "+count);

        }, this);
    };

/*    Assets.setOverlay = function(bg,logo){
        Assets.OVERLAY_BG = bg;
        Assets.OVERLAY_LOGO = logo;
    };*/

/*    Assets.getOverlay = function(game,gameType) {
        var overlay = new createjs.Container();
        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var buttonProps = gameType === "Lobby" ? {w:170,h:56,round:6,f:"24px Arial"} : {w:86,h:28,round:3,f:"12px Arial"};//todo ne postavlja lepo txt za mobile (Y)
        var button = new createjs.Shape();
        button.graphics.setStrokeStyle(2);
        button.graphics.beginStroke("#fff0a4").beginFill('#c69b00').drawRoundRect(0,0,buttonProps.w,buttonProps.h,buttonProps.round);
        button.regX = buttonProps.w/2;
        button.regY = buttonProps.h/2;
        button.x = w/2;
        button.y = h/2 + (buttonProps.h * 2);

        var txt = new createjs.Text("PLAY NOW", buttonProps.f, "#ffffff");
        txt.textAlign = 'center';
        txt.textBaseline = 'middle';
        txt.x = button.x;
        txt.y = button.y;

        overlay.addChild(Assets.OVERLAY_BG,Assets.OVERLAY_LOGO,button,txt);
        overlay.cache(0,0,w,h);

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            game.resetBackgroundMusic();
            overlay.parent.removeChild(overlay);
            overlay = null;
            if(window.localStorage.getItem('isMobile') === "yes"){
                if(is.safari()){
                    var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                    if(is.not.ipad()) {
                        if(!isiPadOS){
                            alteastream.ABSStarter.getInstance().safariResize();
                        }
                    }
                }
            }
        });
        return overlay;
    };*/

/*    Assets.getOverlay = function(options) {
        var overlay = new createjs.Container();

        overlay.addChild(options.background);

        for(var i = 0; i < options.graphics.length; i++){
            overlay.addChild(options.graphics[i]);
        }

        overlay.addChild(options.button);

        for(var j = 0; j < options.texts.length; j++){
            overlay.addChild(options.texts[j]);
        }

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            overlay.parent.removeChild(overlay);
            overlay = null;
            options.clickCallback();
        });

        return overlay;
    };*/

    alteastream.Assets = Assets;
})();

this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var StarterLobby = function(locationSearch,gameId) {
        this.ABSStarter_constructor(locationSearch,gameId);
        this.initialize_StarterLobby();
    };
    var p = createjs.extend(StarterLobby, alteastream.ABSStarter);

// static properties:
    StarterLobby.PREVIEW_SERVER = "";
// public properties:
// private properties:
// static methods:

// constructor:
    p.initialize_StarterLobby = function() {
        StarterLobby.PREVIEW_SERVER = this._ipAdress.split(".")[0] === "staging" ? "https://staging-preview.sungamer.com" : "https://preview.sungamer.com";
    };

// public methods:
    p.pingServer_super = p.pingServer;
    p.pingServer = function(){
        this.pingServer_super();
        //var port = ":8080";//staging
        var port = "";
        //alteastream.AbstractScene.SERVER_URL = this._protocol+this._serverUrl+port;
        //alteastream.AbstractScene.SESSION_TOKEN = jwt_decode(this._usr).token;

        // novo za invalid session start
        if(this._usr === "undefined"){
            var systemAlert = new alteastream.Alert(2,"Invalid Session");
            stage.addChild(systemAlert);

            setTimeout(function(){
                console.warn("dinamicka adresa i putanja za live " + window.location.origin + "/demo");
                window.parent.location.href = decodeURIComponent("https://staging.sungamer.com/demo");
            },3000);
        }else{
            alteastream.AbstractScene.SERVER_URL = this._protocol+this._serverUrl+port;
            alteastream.AbstractScene.SESSION_TOKEN = jwt_decode(this._usr).token;
            window.doLoad();
        }
        // novo za invalid session end
    };

    alteastream.StarterLobby = createjs.promote(StarterLobby,"ABSStarter");
})();
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var StarterMachine = function(locationSearch,gameId) {
        this.ABSStarter_constructor(locationSearch,gameId);
        this.initialize_StarterMachine();
    };
    var p = createjs.extend(StarterMachine, alteastream.ABSStarter);

// static properties:
// public properties:
// private properties:
// static methods:

// constructor:
    p.initialize_StarterMachine = function() {
    };

// public methods:
    p.pingServer_super = p.pingServer;
    p.pingServer = function(){
        window.doLoad();// novo za invalid session
        this.pingServer_super();
        alteastream.AbstractScene.SERVER_URL = this._serverUrl;
        alteastream.AbstractScene.GAME_TYPE = this._gameCode;
        console.log("game code:: "+this._gameCode);
    };

    alteastream.StarterMachine = createjs.promote(StarterMachine,"ABSStarter");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    "use strict";

    var AbstractScene = function(){
        this.Container_constructor();
        this.initialize_AbstractScene();
    };
    var p = createjs.extend(AbstractScene,createjs.Container);


// static properties:
    AbstractScene.GAME_WIDTH = 0;
    AbstractScene.GAME_HEIGHT = 0;
    AbstractScene.GAME_QUIT = "";
    AbstractScene.GAME_TOKEN = "";
    AbstractScene.SESSION_TOKEN = "";
    AbstractScene.GAME_URL = "";
    AbstractScene.GAME_PROTOCOL = "";
    AbstractScene.GAME_ID = 0;
    AbstractScene.GAME_NAME = 0;
    AbstractScene.GAME_CODE = "";
    AbstractScene.DEFAULT_GAME_ID = "";
    AbstractScene.GAME_TYPE = "";
    AbstractScene.MACHINE_NAME = 0;

// events:
// public properties:
    p.requester = null;
    p.renderer = null;
    p.mainStage = null;
    p.isInFS = false;
    p.buttonTriggeredFs = false;
    var _this = null;

// constructor:
    p.initialize_AbstractScene = function(){
        _this = this;
        if(localStorage.getItem("isMobile")==="not"){
            this.fullScreenManager();
        }
    };

// static methods:
// public methods:
    p.setSessionToken = function(){
        AbstractScene.SESSION_TOKEN = jwt_decode(AbstractScene.GAME_TOKEN).token;
    };

    p.monitorNetworkStatus = function(){
        console.log("commented on ICE server");
        window.addEventListener('online',  this.showNetworkStatus);
        window.addEventListener('offline', this.showNetworkStatus);
    };

    p.showNetworkStatus = function(){
        var status = navigator.onLine? "online" : "offline";
        _this.throwAlert(alteastream.Alert.INFO,"You are "+status,_this.socketCommunicator.tryReconnect(status));
        //further options here
    };

    p.setFSClock = function(x,y){
        this.clock = new alteastream.Clock(stage,x,y);
    };

    p.showFSClock = function(bool){
        this.clock.show(bool);
    };

    p.setRenderer = function(){
        this.mainStage = this.parent;
        this.renderer = new alteastream.Renderer(this);
    };

    p.renderLoop = function(event){
        //_this.componentsRender();
        //_this.mainStage.update(event);
    };

    p.globalMouseEnabled = function(bool){
        this.mouseEnabled = bool;
    };

    p.stopAnimation = function(animation){
        this.renderer.stopAnimation(animation);
    };

    p.startAnimation = function(animation){
        this.renderer.startAnimation(animation);
    };

    p.stageRendering = function(bool){
       // this.renderer.stageRendering(bool);
    };

    p.setHttpCommunication = function(){
        this.requester = new alteastream.Requester(this);
    };

    /*p.setSocketCommunication = function(){// local ver
        this.socketCommunicator = new alteastream.SocketCommunicator(this);// local ver
        this.socketCommunicator.activate();// local ver
    };// local ver*/

    //new socket
    p.setSocketCommunication = function(type){
        var socketClass = "SocketCommunicator"+type;
        this.socketCommunicator = new alteastream[socketClass](this);
    };

    p.setStartOverlay = function(options) {
        var overlay = new createjs.Container();

        overlay.addChild(options.background);

        for(var i = 0; i < options.graphics.length; i++){
            overlay.addChild(options.graphics[i]);
        }

        overlay.addChild(options.button);

        for(var j = 0; j < options.texts.length; j++){
            overlay.addChild(options.texts[j]);
        }

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            overlay.parent.removeChild(overlay);
            overlay = null;
            options.clickCallback();
        });

        stage.addChild(overlay);
    };

    p._setQuitPanel = function() {
        var quitPanel = new alteastream.QuitConfirm(this.quitConfirmed);
        if(window.localStorage.getItem('isMobile') === "yes"){
            quitPanel.adjustMobile();
        }
        quitPanel.startUpdate();
        stage.addChild(quitPanel);
    };

    p._setAlert = function(){
        if(this.systemAlert){
            clearTimeout(this.systemAlertTimeout);
            this.systemAlert.dispose();
            stage.removeChild(this.systemAlert);
            this.systemAlert = null;
        }
    };

    p.throwAlert = function(type,exception,callback){
        stage.mouseEnabled = false;
        stage.mouseChildren = false;
        this._setAlert();

        this.systemAlert = new alteastream.Alert(type,exception);
        stage.addChild(this.systemAlert);

        this.removeAlert(3000,callback);
    };

    p.removeAlert = function(delay,callback){
        var that = this;
        this.systemAlertTimeout = setTimeout(function(){
            that.systemAlert.dispose();
            stage.removeChild(that.systemAlert);
            that.systemAlert = null;
            stage.mouseEnabled = true;
            stage.mouseChildren = true;
            if(callback)callback();
        },delay);
    };

    p.throwDialog = function(type,exception){
        this._setAlert();

        this.systemAlert = new alteastream.Alert(type,exception);
        stage.addChild(this.systemAlert);
    };

    p.killGame = function(){//TEMP
        alteastream.Assets.stopAllSounds();
        this.stageRendering(false);
        this.removeAllChildren();
        this.parent.update();
        this.parent.removeChild(this);
    };

    p.killSession = function(msg){//TEMP
        window.stage.mouseEnabled = false;
        window.stage.mouseChildren = false;
        msg = msg || "undefined";
        //window.stage.getChildAt(1).parent.removeChildAt(1);//temp loading overlay
        this.throwAlert(alteastream.Alert.ERROR,msg,function(){
            _this.killGame();
            //window.top.location.href = decodeURIComponent("http://139.59.147.230:81/");//alteastream.AbstractScene.GAME_QUIT
            window.parent.location.href = decodeURIComponent(alteastream.AbstractScene.GAME_QUIT);//alteastream.AbstractScene.GAME_QUIT
        });
    };

    p.playSound = function(sound,vol){
        alteastream.Assets.playSound(sound,vol);
    };

    p.playSoundChain = function(sound,vol,complete,scope){
        alteastream.Assets.playSoundChain(sound,vol,complete,scope);
    };

    p.playSoundChannelWithOptions = function(sound,op){
        alteastream.Assets.playSoundChannelWithOptions(sound,op);
    };

    p.stopSoundChannel = function(sound){
        alteastream.Assets.stopSoundChannel(sound);
    };

    p._addZeros = function(string,max){// todo iz utils
        var fromPoint = String(string);
        var decimals = fromPoint.split('.');
        var zeros = "";
        if(decimals[1] === undefined){
            zeros=".00";
            if(max>1){
                zeros+="0";
            }
        }else{
            if(decimals[1].length===1){
                zeros="00";
            }
            if(decimals[1].length===max){
                zeros="0";
            }
        }
        return zeros;
    };

    p.isRatioFit = function() {
        var deviceWidth = screen.width;
        var deviceHeight = screen.height;
        var deviceRatio = String(deviceWidth/deviceHeight).slice(0, 4);
        var ratioToFit = String(window.parent.widthToHeight).slice(0, 4);
        return deviceRatio === ratioToFit;
    };

    p.fullScreenManager = function(){
        window.parent.addEventListener("fs",function(e){
            _this.isInFS = e.isInFS;

            if(_this.isInFS === true){
                if(_this.buttonTriggeredFs === false){
                    _this.btnFs.mouseEnabled = false;
                    _this.btnFs.alpha = 0.5;
                }
            }else{
                if(_this.btnFs){
                    _this.btnFs.mouseEnabled = true;
                    _this.btnFs.alpha = 1;
                }
            }

            if(_this.btnFs){
                var img = _this.isInFS===true? "btnFullScreen_Off":"btnFullScreen_On";
                _this.btnFs.image = alteastream.Assets.getImage(alteastream.Assets.images[img]).image;
            }

            _this.buttonTriggeredFs = false;
            _this.showFSClock( _this.isInFS);
        });
    };

    p.normalBrowsersFullScreenToggle = function(e) {// CHROME, OPERA, FIREFOX
        //_this.controlBoard.checkIfHiddenMenuIsActive();
        var assets = alteastream.Assets;
        _this.buttonTriggeredFs = true;
        //var element = _this.isRatioFit() === true ? document.documentElement : window.parent.document.documentElement;
        // new version with fixed screen size
        var element = window.parent.document.documentElement;
        if(!window.parent.document.fullscreenElement){
            e.target.image = assets.getImage(assets.images.btnFullScreen_Off).image;
            element.requestFullscreen();
            _this.onFullScreen(true);
        }else{
            e.target.image = assets.getImage(assets.images.btnFullScreen_On).image;
            window.parent.document.exitFullscreen();
            _this.onFullScreen(false);
        }
    };

    p.otherBrowsersFullScreenToggle = function(e) {// EDGE, SAFARI
        //_this.controlBoard.checkIfHiddenMenuIsActive();
        var assets = alteastream.Assets;
        _this.buttonTriggeredFs = true;
        //var element = _this.isRatioFit() === true ? document.documentElement : window.parent.document.documentElement;
        // new version with fixed screen size
        var element = window.parent.document.documentElement;
        if(!window.parent.document.webkitFullscreenElement){ // ovde je drugacija provera posto ovi browseri izadju iz full moda kada krene da se ucitava masina
            e.target.image = assets.getImage(assets.images.btnFullScreen_Off).image;
            element.webkitRequestFullScreen();
            _this.onFullScreen(true);
        }else{
            e.target.image = assets.getImage(assets.images.btnFullScreen_On).image;
            window.parent.document.webkitExitFullscreen();
            _this.onFullScreen(false);
        }
    };

    p.onFullScreen = function(bool){
        this.showFSClock(bool);
    };

    p._addZeros = function(string,max){
        var fromPoint = String(string);
        var decimals = fromPoint.split('.');
        var zeros = "";
        if(decimals[1] === undefined){
            zeros=".00";
            if(max>1){
                zeros+="0";
            }
        }else{
            if(decimals[1].length===1){
                zeros="00";
            }
            if(decimals[1].length===max){
                zeros="0";
            }
        }
        return zeros;
    };

// private methods:

    alteastream.AbstractScene = createjs.promote(AbstractScene,"Container");
})();/**
 * Created by Dacha on 9/25/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var AbstractBet = function(scene) {
        this.initialize(scene);
    };
    var p = AbstractBet.prototype;

// static properties:

// events:
// public properties:
// private properties:
    p._selectedStake = null;
    p._multiplier = 0.10;
    p.scene = null;

// constructor:
    p.initialize = function(scene) {
        this.scene = scene;
    };

// static methods:
// public methods:
    p.betValue = function(){};

    p.setStake = function(){
        this.onStakeChange();
        this.manageStakeButtons();
    };

    p.setMultiplier = function(value){
        this._multiplier = value/1000;
    };

    // value is in cents version
/*    p.setMultiplier = function(value){
        this._multiplier = value/100;
    };*/

    p.getMultiplier = function(){
        return this._multiplier;
    };

    p.rounded = function(value){
        return Math.round(value * 100) / 100;
    };

// private methods:

    alteastream.AbstractBet = AbstractBet;
})();/**
 * Created by Dacha on 9/25/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BetTotal = function(scene) {
        this.initialize(scene);
    };
    var p = BetTotal.prototype = new alteastream.AbstractBet();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.BetTotal_init = p.initialize;

    p.initialize = function(scene) {
        this.BetTotal_init(scene);
    };

// static methods:
// public methods:
    p.onStakeChange = function(){
        //this.scene.controlBoard.setStakeAmount(this.scene._stakeIncrements[this.scene._selectedStake]);
        var stake = this.scene._stakeIncrements[this.scene._selectedStake];
        var multiplier = this.getMultiplier();
        this.scene.controlBoard.setStakeAmount(stake,multiplier);
    };

    p.manageStakeButtons = function(){
        var controlBoard = this.scene.controlBoard;
        var stake = this.scene._selectedStake;

        if(stake === this.scene._stakeIncrements.length - 1){
            controlBoard.btnIncStake.setDisabled(true);
            controlBoard.btnDecStake.setDisabled(false);
        }
        else if(stake === 1){
            controlBoard.btnDecStake.setDisabled(false);
            controlBoard.btnIncStake.setDisabled(false);
        }
        else if(stake === 0){
            controlBoard.btnDecStake.setDisabled(true);
            controlBoard.btnIncStake.setDisabled(false);
        }
        else if(stake === this.scene._stakeIncrements.length - 2){
            controlBoard.btnIncStake.setDisabled(false);
            controlBoard.btnDecStake.setDisabled(false);
        }else{
            controlBoard.btnDecStake.setDisabled(false);
            controlBoard.btnIncStake.setDisabled(false);
        }
        controlBoard.setSingleToBurst(stake>0);
    };

    p.betValue = function(){
        //return this.scene.controlBoard.getStakeAmount();
        return this.scene.getSelectedStake();//Math.min(this.scene.getSelectedStake()*multiplier).toFixed(2) //  coini ili lova da se salje?
    };

    p.canBet = function(have,trying){
        have = have/1000;
        return have >= Math.min(trying*this.getMultiplier()).toFixed(2);
    };

    p.betValueToSend =  function(){
        return this.betValue();
    };

// private methods:

    alteastream.BetTotal = BetTotal;
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSSocketCommunicator = function(game) {
        this.initialize_ABSSocketCommunicator(game);
    };
    var p = ABSSocketCommunicator.prototype;

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    p.currentGameUid = "";
    p.USER_PRESENCE = 0;
    p.subscriptions = [];
    p.intention = false;
    p._isConnected = false;
    var _instance = null;

// constructor:
    p.initialize_ABSSocketCommunicator = function(game) {
        this.game = game;
        _instance = this;
    };

// static methods:
    ABSSocketCommunicator.getInstance = function(){
        return _instance;
    };

// public methods

    p.activate = function(){

    };

    p.unsubscribeFromAll = function(){
        if(this.subscriptions.length>0){
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                this.subscriptions[i].subscription.unsubscribe();
            }
            this.subscriptions = [];
        }
    };

    p.getSubscriptions = function(){
        if(this.subscriptions.length>0){
            console.info("total subscriptions: "+this.subscriptions.length);
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                console.info("subscription_"+i+": "+this.subscriptions[i].name);
            }
        }else{
            console.info("no subscriptions ");
        }
    };

    p.disposeCommunication = function(){
        this.intention = true;
        this.clearHeartBeats();
        this.unsubscribeFromAll();
        //this.gameClient.disconnect( function(){console.log("disconnected");} );
    };

    p.tryReconnect = function(status){
        if(status === "online")
            if(_instance._isConnected === false){
                _instance.connectionLostInterval = setInterval(function(){
                    if(_instance._isConnected === false)
                        _instance.activate();
                    else
                        clearInterval( _instance.connectionLostInterval);
                },2000);
            }else{
                clearInterval( _instance.connectionLostInterval);
            }
    };

    p.onConnectionError = function(message){
        _instance._isConnected = false;
        //_instance.disposeCommunication();
        console.warn("Socket connection lost");
        if(!_instance.intention)
            _instance.game.throwAlert(alteastream.Alert.ERROR,"Socket connection error "+message,function () {
                console.log("try to reconnect");
                _instance.game.onQuit();
            });
    };


    alteastream.ABSSocketCommunicator = ABSSocketCommunicator;
})();/**
 * Created by Dacha on 11.04.14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Alert = function(type,exception) {
        this.Container_constructor();
        this.initialize_Alert(type,exception);
    };
    var p = createjs.extend(Alert, createjs.Container);

// static properties:
    Alert.INFO = 0;
    Alert.WARNING = 1;
    Alert.ERROR = 2;
    Alert.EXCEPTION = 3;

// events:
// public properties:
// private properties:
    p.exceptionObject = null;

// constructor:

    p.initialize_Alert = function(type,exception) {
        this._init(type,exception);
    };
// static methods:
// public methods:
    p.dispose = function(){
        this.removeAllChildren();
    };

// private methods:
    p._init = function(type,exception){
        var setType = 0;
        var strokeColor = "#fff000";
        var typeColor = "#ff0000";
        //var sound = "error1";
        var sound = "error";
        switch(type){
            case Alert.INFO:
                setType = "INFO";
                strokeColor = "#97e5bf";
                typeColor = "#9cd4e5";
                sound = "info";
                break;
            case Alert.EXCEPTION:
                setType = "EXCEPTION";
                strokeColor = "#97e5bf";
                typeColor = "#ffb753";
                sound = "exception";
                break;
            case Alert.WARNING:
                setType = "WARNING";
                typeColor = "#ffb753";
                sound = "warning";
                break;
            case Alert.ERROR:
                setType = "ERROR";
                break;
           }

        this.exceptionObject = {};
        this.exceptionObject.method = setType;//TEMP
        this.exceptionObject.message = exception;

        var fontBig = "bold 20px Verdana";
        var font = "18px Verdana";
        var black = "#000";

        var alignCenter = "center";
        var alignBaseline = "middle";
        var exceptionMethod = this.exceptionObject.method || "message undefined";
        var exceptionMessage = this.exceptionObject.message || "message undefined";

        var _width = alteastream.AbstractScene.GAME_WIDTH || document.getElementById("screen").width;
        var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;

        var bg = new createjs.Shape(new createjs.Graphics().beginFill(black).drawRect(0, 0, _width, _height));
        bg.alpha = 0.6;
        bg.cache(0, 0, _width, _height);

        var infoFieldWidth = 570;
        var infoFieldHeight = 54;
        var infoFieldX = _width/2 - infoFieldWidth/2;
        var infoFieldY = _height/2;

        var infoField = new createjs.Shape(new createjs.Graphics().setStrokeStyle(3).beginStroke(strokeColor).beginFill(black).drawRect(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight));
        infoField.cache(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight);

        var methodTf = new createjs.Text(String(exceptionMethod), fontBig, typeColor).set({textBaseline:alignBaseline,textAlign:alignCenter,mouseEnabled:false});
        var messageTf = new createjs.Text(String(exceptionMessage), font, strokeColor).set({textBaseline:alignBaseline,textAlign:alignCenter,mouseEnabled:false});
        this.addChild(bg,infoField,methodTf,messageTf);

        methodTf.x = (infoFieldX+infoFieldWidth/2);
        methodTf.y = (infoFieldY+infoFieldHeight/2) - 9;

        messageTf.x =  (infoFieldX+infoFieldWidth/2);
        messageTf.y = (infoFieldY+infoFieldHeight/2) + 11;

        alteastream.Assets.playSound(sound);

        this.on("click",function (e){});
    };

    alteastream.Alert = createjs.promote(Alert,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Requester = function(game) {
        this.initialize(game);
    };
    var p = Requester.prototype;

// static properties:
    Requester.MACHINE_PATH = "/machine/";
// events:
// public properties:
    var _instance = null;
    var lokalUrl = "../Ztests/";
// private properties:
    p.gameId = null;
    p.gameToken = null;

    p.GAME_STATE_OK = 0;
    p.ERROR_TOKEN_ALREADY_IN_QUEUE = 1;
    p.ERROR_TOKEN_NOT_IN_QUEUE = 2;
    p.ERROR_QUEUE_DOES_NOT_EXIST = 3;
    p.USER_DID_NOT_START_GAME = 4;
    p.ERROR_MACHINE_DOES_NOT_EXIST = 5;
    p.ERROR_INVALID_TOKEN = 6;
    p.ERROR_TOKEN_ALREADY_ON_MACHINE = 7;
    p.ERROR_TOKEN_ALREADY_IN_GAME = 8;
    p.ERROR_WRONG_GAME_CLASS = 9;
    p.ERROR_MACHINE_CANT_BE_REACHED = 10;
    p.ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN = 11;
    p.ERROR_NON_EXISTING_GAME_TYPE = 12;
    p.ERROR_NO_TOKEN = 13;
    p.ERROR_MACHINE_NOT_AVAILABLE = 14;

// constructor:
    p.initialize = function(game) {
        this.game = game;
        _instance = this;
    };
// static methods:
    Requester.getInstance = function(){
        return _instance;
    };

// public methods
//LOBBY::>>
    /*    p.getHouses = function(callback){
            $.get(lokalUrl+"getHouses.json",function(response){ callback(response); });
        };*/

    p.getGameTypes = function(callback){
        $.get(lokalUrl+"getGameTypes.json",function(response){ callback(response); });
    };

    p.getBalance = function(callback){
        $.get(lokalUrl+"getHousesBalance.json",function(response){ callback(response); });
    };

    // na 5 sec:
/*    p.getMachines = function(house,callback){// sounds/skins per type here
        $.get(lokalUrl+"getMachines.json",function(response){ callback(response); });
    };*/

    // na 5 sec:
    p.getMachinesGt = function(house,callback){// sounds/skins per type here
        $.get(lokalUrl+"getMachinesGt.json",function(response){ callback(response); });
    };

    //na 2 sec:
    p.getCurrentMachine = function(machineId,callback){
        $.get(lokalUrl+"getCurrentMachine.json",function(response){ callback(response); });
    };

    p.getPlayerActivity = function(callback){
        $.get(lokalUrl+"getPlayerActivity.json",function(response){callback(response);});
    };

    //na socket?
    p.enterMachine = function(callback){
        $.get(lokalUrl+"enterqueue.json",function(response){ callback(response); });
    };

    //na socket?
    p.leaveMachine = function(callback){
        $.get(lokalUrl+"leavemachine.json",function(response){ callback(response); });
    };
//LOBBY::<<

//STREAM::>>
    p.startGameStream = function(callbackContinue, callbackStop){
        $.get(lokalUrl+"gamestart.json",function(response){// latest Vlada request
            console.log("STATE:: "+response.state);
            if(response.state === _instance.GAME_STATE_OK){
                callbackContinue(response);
            }else{
                _instance._responseError(_instance._getState(response), callbackStop); // todo promeniti txt
            }
        });
        //gamestart/TOKEN/MASINA?game=rmgame
        //gamestart/TOKEN/MASINA?id=1
    };

    p.backToLobby = function(callback){
        $.get(alteastream.AbstractScene.SERVER_URL +"/"+"quitgame/"+alteastream.AbstractScene.GAME_TOKEN,function(res){
            callback(res);
        });
    };
    
    p._getState = function(res){
        var message = "state: "+res.state;
        switch(res.state){
            case _instance.ERROR_TOKEN_ALREADY_IN_QUEUE:
                message = "Token already in queue";
                //_instance.game.killSession("Token already in game");
                break;
            case _instance.ERROR_TOKEN_NOT_IN_QUEUE:
                message = "Token not in queue";
                break;
            case _instance.ERROR_QUEUE_DOES_NOT_EXIST:
                message = "Queue does not exist";
                break;
            case _instance.USER_DID_NOT_START_GAME:
                message = "User did not start the game";
                break;
            case _instance.ERROR_INVALID_TOKEN:
                message = "Invalid token";
                break;
            case _instance.ERROR_MACHINE_DOES_NOT_EXIST:
                message = "Machine does not exist";
                break;
            case _instance.ERROR_TOKEN_ALREADY_ON_MACHINE:
                message = "Token already on machine";
                break;
            case _instance.ERROR_TOKEN_ALREADY_IN_GAME:
                message = "Token already in game";
                break;
            case _instance.ERROR_WRONG_GAME_CLASS:
                message = "Wrong game class";
                break;
            case _instance.ERROR_MACHINE_CANT_BE_REACHED:
                message = "Machine cant be reached";
                break;
            case _instance.ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN:
                message = "Machine token invalid";
                break;
            case _instance.ERROR_NON_EXISTING_GAME_TYPE:
                message = "Invalid game type";
                break;
            case _instance.ERROR_NO_TOKEN:
                message = "No token";
                break;
            case _instance.ERROR_MACHINE_NOT_AVAILABLE:
                message = "Machine not available";
                break;
        }
        return message;
    };

    p._responseError = function(msg,quitFn){
        _instance.game.throwAlert(alteastream.Alert.ERROR, msg,function(){
            if(quitFn)
                quitFn();
        });
    };

    p._responseException = function(msg){
        _instance.game.throwAlert(alteastream.Alert.EXCEPTION, msg);
    };

    p.fallbackToLobby = function(message){
        _instance.game.throwAlert(alteastream.Alert.ERROR,message,function(){
            _instance.game.killGame();
            _instance.game.closeGame();
        });
    };
//STREAM::<<
    // private methods:
    Requester.getInstance = function(){
        return _instance;
    };

    alteastream.Requester = Requester;
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorGame = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorGame();
    };
    var p = createjs.extend(SocketCommunicatorGame, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var GameMessageType = {
        1:"CONFIRM_GAME_INIT",
        2:"MACHINE_RESET_START",
        3:"MACHINE_RESET_END",
        5:"GAME_IS_LAUNCHED",
        7:"MOVE_SHOOTER_LEFT",
        8:"MOVE_SHOOTER_RIGHT",
        9:"STOP_SHOOTER",
        10:"DISPENSE_TOKENS",
        11:"PRIZE_DETECTION",
        12:"SHOOT_ENABLED",
        40:"NON_PLAYING_TIME_LIMIT_REACHED",
        50:"TOKENS_COUNT_CHANGED",
        60:"BALANCE_CHANGED",
        64:"BONUS_WON",
        70:"MACHINE_ERROR",//USER_KICKED ?
        85:"QUIT",
        100:"HEARTBEAT",
        CONFIRM_GAME_INIT:1,
        MACHINE_RESET_START:2,
        MACHINE_RESET_END:3,
        GAME_IS_LAUNCHED:5,
        MOVE_SHOOTER_LEFT:7,
        MOVE_SHOOTER_RIGHT:8,
        STOP_SHOOTER:9,
        DISPENSE_TOKENS:10,
        PRIZE_DETECTION:11,
        SHOOT_ENABLED:12,
        NON_PLAYING_TIME_LIMIT_REACHED:40,
        TOKENS_COUNT_CHANGED:50,
        BALANCE_CHANGED:60,
        BONUS_WON:64,
        MACHINE_ERROR:70,//USER_KICKED ?
        QUIT:85,
        HEARTBEAT:100
    };

// constructor:
    p.initialize_SocketCommunicatorGame = function() {
        _instance = this;
        this.USER_PRESENCE = 10000;
    };

// static methods:
    SocketCommunicatorGame.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(params){
        var currentGameUid = this.currentGameUid = params.key;
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/wsgame"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&game="+currentGameUid+"&t="+Date.now());
        var gameClient = this.gameClient = Stomp.over(socket);
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onGameConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToMachine();
        _instance.heartbeatGame();
    };

    //iphone debug
    p._subscribeToMachine = function(){
        console.log("socket>> startGame socket Game :::::::::::::::: ");
        var subscriptionObj = {};
        subscriptionObj.name = "gameplay_"+_instance.currentGameUid;
        subscriptionObj.subscription = this.gameClient.subscribe("/topic/"+_instance.currentGameUid+"", function (msg) {
            var mesg = JSON.parse(msg.body);
            _instance.parseGameMessage(mesg);
        });

        this.subscriptions.push(subscriptionObj);

        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.GAME_IS_LAUNCHED,
            tkn:_instance.currentGameUid
        }))
    };

    p.startHeartBeatGame = function () {
        _instance.heartbeatGame();
        this.heartbeatGameInterval = setInterval(function(){
            _instance.heartbeatGame();
        },this.USER_PRESENCE);
    };

    p.heartbeatGame = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/cmd", {}, JSON.stringify({
                val:0,
                tp:GameMessageType.HEARTBEAT,
                tkn:_instance.currentGameUid
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatGameInterval);
    };

    p.sendBet = function(params) {
        this.fireShooter(params.bet);
    };

    p.fireShooter = function(bet) {
        _instance.game.monitorIdleTime(false);
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            val:bet,
            tp:GameMessageType.DISPENSE_TOKENS,
            tkn:_instance.currentGameUid
        }));
    };

    p.moveShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:actionType,
            tkn:_instance.currentGameUid
        }));
    };

    p.stopMovingShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.STOP_SHOOTER,
            tkn:_instance.currentGameUid
        }));
    };

    p.confirmGameInit = function(){
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.CONFIRM_GAME_INIT,
            tkn:_instance.currentGameUid
        }));
    };

    p.quitPlay = function(){
        _instance.game.monitorIdleTime(false);
        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.QUIT,
            tkn:_instance.currentGameUid
        }))
    };

    p.parseGameMessage = function(msg){
        console.log("socket>> parseGameMessage:::::::::::::: ");
        console.log(msg);
        switch(msg.tp){
            case GameMessageType.CONFIRM_GAME_INIT:
                console.info("message>> CONFIRM_GAME_INIT:::::::::::::: "+msg.dsc);
                _instance.game.showGame(true);
                _instance.game.monitorIdleTime(true);
                break;
            case GameMessageType.GAME_IS_LAUNCHED:
                console.log("message>> GAME_IS_LAUNCHED:::::::::::::: "+msg.description);
                _instance.startHeartBeatGame();
                break;
            case GameMessageType.QUIT:
                console.info("message>> QUIT:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.exitMachineGameplay();
                break;
            case GameMessageType.SHOOT_ENABLED:
                if(msg.vl){
                    console.log("message>> SHOOT_ENABLED:::::::::::::: "+msg.vl);
                    _instance.game.allowedShooterBtn(msg);
                    _instance.game.monitorIdleTime(true);
                }
                break;
            case GameMessageType.PRIZE_DETECTION:
                if(msg.vl){
                    _instance.game.onPrizeDetection(msg.vl);
                }
                break;
            case GameMessageType.BALANCE_CHANGED:
                console.log("message>> BALANCE_CHANGED:::::::::::::: ");
                _instance.game.setCrd(msg.bal);
                _instance.game.updateQueueInfo(msg.queueSize);
                /*if(msg.bon){
                    if(msg.bon>0)
                        _instance.game.fillToBonus(msg.bon);
                }*/
                break;
            /*case GameMessageType.BONUS_WON:
                console.log("message>> BONUS_WON "+msg.vl.amount);
                _instance.game.onBonusWon(msg);
                //{"ts":1555571186510,"vl":{"amount":37,"money":3700,"balance":9997290,"prize":2},"tp":6,"cat":64}
                break;*/
            case GameMessageType.TOKENS_COUNT_CHANGED:
                //console.log("socket>> TOKENS_COUNT_CHANGED >> amount: "+msg.vl.amount+" money: "+msg.vl.money);
                console.log("message>> TOKENS_COUNT_CHANGED >> amount: "+msg.vl);
                _instance.game.refundCoinSpawn(msg.vl);
                break;
            case GameMessageType.NON_PLAYING_TIME_LIMIT_REACHED:
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.monitorIdleTime(false);
                _instance.game.quitLimitReached();
                break;
            case GameMessageType.MACHINE_ERROR://USER_KICKED ?
                console.warn("message>> MACHINE_ERROR::::: "+msg.vl);
                _instance.heartbeatGame();
                _instance.game.onMachineError(msg.vl);
                break;
            default:
                console.warn("message>> UNHANDLED MESSAGE TYPE >> "+msg.tp);
        }
    };

    alteastream.SocketCommunicatorGame = createjs.promote(SocketCommunicatorGame,"ABSSocketCommunicator");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorLobby = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorLobby();
    };
    var p = createjs.extend(SocketCommunicatorLobby, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var ErrorMessageType = {
        1:"ERROR_TOKEN_ALREADY_IN_QUEUE",
        2:"ERROR_TOKEN_NOT_IN_QUEUE",
        3:"ERROR_QUEUE_DOES_NOT_EXIST",
        4:"USER_DID_NOT_START_GAME",
        5:"ERROR_MACHINE_DOES_NOT_EXIST",
        6:"ERROR_INVALID_TOKEN",
        7:"ERROR_TOKEN_ALREADY_ON_MACHINE",
        8:"ERROR_TOKEN_ALREADY_IN_GAME",
        9:"ERROR_WRONG_GAME_CLASS",
        10:"ERROR_MACHINE_CANT_BE_REACHED",
        11:"ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN",
        12:"ERROR_NON_EXISTING_GAME_TYPE",
        13:"ERROR_NO_TOKEN",
        14:"ERROR_MACHINE_NOT_AVAILABLE",
        15:"ERROR_NO_FREE_GAMEPLAY_NODE",
        20:"ERROR_QUEUE_SYSTEM",
        ERROR_TOKEN_ALREADY_IN_QUEUE:1,
        ERROR_TOKEN_NOT_IN_QUEUE:2,
        ERROR_QUEUE_DOES_NOT_EXIST:3,
        USER_DID_NOT_START_GAME:4,
        ERROR_MACHINE_DOES_NOT_EXIST:5,
        ERROR_INVALID_TOKEN:6,
        ERROR_TOKEN_ALREADY_ON_MACHINE:7,
        ERROR_TOKEN_ALREADY_IN_GAME:8,
        ERROR_WRONG_GAME_CLASS:9,
        ERROR_MACHINE_CANT_BE_REACHED:10,
        ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN:11,
        ERROR_NON_EXISTING_GAME_TYPE:12,
        ERROR_NO_TOKEN:13,
        ERROR_MACHINE_NOT_AVAILABLE:14,
        ERROR_NO_FREE_GAMEPLAY_NODE:15,
        ERROR_QUEUE_SYSTEM:20
    };

    var SocketMessageType = {
        1:"MACHINE_INFO",
        2:"MACHINE_QUEUE_CHANGED",
        5:"MACHINE_READY_FOR_PLAYER",
        6:"MESSAGE_FROM_GAME",
        10:"EXPIRED_GAME_WAITING",
        20:"QUEUE_TO_MACHINE",
        25:"LEAVE_QUEUE",
        30:"TOKEN_EXPIRED",
        40:"TOKEN_REFRESHED",
        1000:"BETTING_COUNTER",//mp
        1001:"BETTING_EXPIRED",//mp
        90:"BETTING_EVENT",//mp
        110:"BETTING_ENTER_CONFIRMED",//mp
        120:"BETTING_EXIT_CONFIRMED",//mp
        666:"LEFT_GAME",
        MACHINE_INFO:1,
        MACHINE_QUEUE_CHANGED:2,
        MACHINE_READY_FOR_PLAYER:5,
        MESSAGE_FROM_GAME:6,
        EXPIRED_GAME_WAITING:10,
        QUEUE_TO_MACHINE:20,
        LEAVE_QUEUE:25,
        TOKEN_EXPIRED:30,
        TOKEN_REFRESHED:40,
        BETTING_COUNTER:1000,//mp
        BETTING_EXPIRED:1001,//mp
        BETTING_EVENT:90,//mp
        BETTING_ENTER_CONFIRMED:110,//mp
        BETTING_EXIT_CONFIRMED:120,//mp
        LEFT_GAME:666
    };

    var UserMessageType = {
        2:"GAME_COMMAND",
        3:"REGISTER_SOCKET",
        4:"CHECK_USER",
        10:"REQUEST_QUEUE_ENTER",
        15:"REQUEST_QUEUE_LEAVE",
        20:"HEARTBEAT_USER",
        30:"BETTING_START",//mp
        40:"BETTING_STOP",//mp
        GAME_COMMAND:2,
        REGISTER_SOCKET:3,
        CHECK_USER:4,
        REQUEST_QUEUE_ENTER:10,
        REQUEST_QUEUE_LEAVE:15,
        HEARTBEAT_USER:20,
        BETTING_START:30,//mp
        BETTING_STOP:40//mp
    };

// constructor:
    p.initialize_SocketCommunicatorLobby = function() {
        _instance = this;
        this.USER_PRESENCE = 50000;
    };

// static methods:
    SocketCommunicatorLobby.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(){
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/ws"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&t="+Date.now());// /websocket
        var gameClient = this.gameClient = Stomp.over(socket);// sa SockJS
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onLobbyConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToSession();
        _instance._subscribeToPublic();

        _instance.heartbeatUser();
        _instance.startHeartBeatUser();
    };

    p.lobbySwitchSubscriptions = function(previousMachine,newMachine){
        this.unsubscribeFromMachine(previousMachine);
        this._subscribeToMachine(newMachine);
        //test log:
        this.getSubscriptions();
    };

    p.startHeartBeatUser = function () {
        this.heartbeatUserInterval = setInterval(function(){
            _instance.heartbeatUser();
        },this.USER_PRESENCE);
    };

    p.heartbeatUser = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/msg/user", {}, JSON.stringify({
                //val:0,
                tp:UserMessageType.HEARTBEAT_USER,
                tkn:alteastream.AbstractScene.GAME_TOKEN
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatUserInterval);
    };

    p._subscribeToSession = function(){
        var subscriptionObj = {};
        subscriptionObj.name = "session";//+alteastream.AbstractScene.SESSION_TOKEN;//6cfoquoqvd7hfeafcac009s9vq
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/'+alteastream.AbstractScene.SESSION_TOKEN, function (msg) {
            console.log("socket msg: from user >>>>>>>>>>");
            _instance.parseMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p._subscribeToPublic = function(){
        var subscriptionObj = {};
        subscriptionObj.name = "public";
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/public', function (msg) {
            console.log("socket msg: from public >>>>>>>>>>");
            _instance.parsePublicMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p._subscribeToMachine = function(machine_index){
        var subscriptionObj = {};
        subscriptionObj.name = machine_index;
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/'+machine_index, function (msg) {
            console.log("socket msg: from machine >>>>>>>>>> "+machine_index+" "+msg.body);
            _instance.parseMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p.unsubscribeFromMachine = function(machineName){
        if(this.subscriptions.length>0){
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                //if(this.subscriptions[i].name === machineName){
                if(this.subscriptions[i].name !== "session" && this.subscriptions[i].name !== "public"){
                    this.subscriptions[i].subscription.unsubscribe();
                    this.subscriptions.splice(i,1);
                }
            }
        }
    };

    p.enterQueue = function() {
        console.log("socket>> send enterQueue for :::::::::::::::::::::::::: "+alteastream.AbstractScene.GAME_ID);
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:UserMessageType.REQUEST_QUEUE_ENTER,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.leaveQueue = function() {
        console.log("socket>> send leaveQueue for :::::::::::::::::::::::::: "+alteastream.AbstractScene.GAME_ID);
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:UserMessageType.REQUEST_QUEUE_LEAVE,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.doBetting = function(bool){//mp
        console.log("socket>> send betStart :::::::::::::::::::::::::: ");
        var msgType = bool === true? UserMessageType.BETTING_START: UserMessageType.BETTING_STOP;//SPECTATE
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:msgType,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.parseMessage = function(msg){
        var queuePanel = _instance.game.currentLocationMachines.queuePanel;
        var locationMachines = _instance.game.currentLocationMachines;
        switch(msg.tp){
            case SocketMessageType.MACHINE_QUEUE_CHANGED:
                console.log(">>>>> MACHINE_QUEUE_CHANGED::::::::::::::");
                queuePanel.updateQueueInfo(msg.vl);
                locationMachines.updateLoungeComponents();
                break;
            case SocketMessageType.QUEUE_TO_MACHINE:
                console.log(">>>>> QUEUE_TO_MACHINE:::::::::::::::");
                var parsedMsg = JSON.parse(msg.vl);
                if(parsedMsg.code === 0){
                    queuePanel.onQueueEnter(parsedMsg);
                }else{
                    _instance._handleQueueFailed(queuePanel,parsedMsg.code);
                }
                break;
            case SocketMessageType.LEAVE_QUEUE:
                var msgVal = JSON.parse(msg.vl);
                console.log(">>>>> LEAVE_QUEUE:::::::::::::::"+msgVal.machine);
                queuePanel.onQueueLeave();
                break;
            case SocketMessageType.TOKEN_EXPIRED:
                console.warn(">>>>> TOKEN_EXPIRED:::::::::::::::");//heartbeat failed
                _instance.disposeCommunication();
                window.stage.mouseEnabled = false;
                window.stage.mouseChildren = false;
                _instance.game.throwAlert(alteastream.Alert.ERROR,"Token Expired",function() {
                    _instance.game.onQuit();
                });
                break;
            case SocketMessageType.TOKEN_REFRESHED:
                console.info(">>>>> TOKEN_REFRESHED:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;
                break;
            case SocketMessageType.MACHINE_READY_FOR_PLAYER:
                console.log(">>>>> MACHINE_READY_FOR_PLAYER:::::::::::::::");
                var msgParsed;
                    if(queuePanel.previousMachineActive === false){
                        queuePanel.multiPlayerControls.quitBetting();
                        locationMachines.setComponentWhenMachineisFreeToPlay(true);
                        msgParsed = JSON.parse(msg.vl);
                        queuePanel.manageQueueReadyState(msgParsed.wtime);
                    }
                    else{
                        if(queuePanel.spinner.active){
                            console.warn("aktivira spiner a ne treba");
                        }
                        queuePanel.resetDisplay();
                        //_instance.enterQueue();
                    }
                break;
            case SocketMessageType.EXPIRED_GAME_WAITING:
                console.warn(">>>>> EXPIRED_GAME_WAITING:::::::::::::::");
                if(queuePanel.spinner.active){
                    _instance.enterQueue();
                }else{
                    locationMachines.tryLobby();
                }
                break;
            case SocketMessageType.LEFT_GAME:
                console.warn(">>>>> LEFT_GAME:::::::::::::::");
                queuePanel.previousMachineActive = false;
                break;
            case SocketMessageType.BETTING_ENTER_CONFIRMED://mp
                console.log(">>>>> BETTING_ENTER_CONFIRMED:::::::::::::::");
                queuePanel.multiPlayerControls.startBetting();
                break;
            case SocketMessageType.BETTING_EXIT_CONFIRMED://mp
                console.log(">>>>> BETTING_EXIT_CONFIRMED:::::::::::::::");
                queuePanel.multiPlayerControls.stopBetting();
                break;
            case SocketMessageType.BETTING_COUNTER://mp
                console.log(">>>>> BETTING_COUNTER:::::::::::::::");
                var counterParsed = JSON.parse(msg.vl);
                queuePanel.multiPlayerControls.showCounter(counterParsed);
                break;
            case SocketMessageType.BETTING_EXPIRED://mp
                console.error(">>>>> BETTING_EXPIRED:::::::::::::::");
                //queuePanel.multiPlayerControls.stopBetting();
                break;
            case SocketMessageType.BETTING_EVENT://mp
                console.log(">>>>> BETTING_EVENT:::::::::::::::");
                console.log(msg);
                //var betMsg = JSON.parse(msg);
                queuePanel.multiPlayerControls.betEvent(msg);
                break;
            default:
                console.warn("UNHANDLED MESSAGE TYPE>> "+msg.tp);
        }
    };

    p.parsePublicMessage = function(msg){
        //{msg: "machine_free_to_use_update", machineIndex: 1229579567591513, isFreeToPlay: false}
        //if(msg.msg === "machine_free_to_use_update"){
        //delete msg.msg;
        //{msg: "queue_size_changed", machineIndex: 1229579567591513, isFreeToPlay: false, "size":123}
        //{msg: "machine_update","isOnline":false,"isInAutoplay","statusCode" }
        var locationMachines = _instance.game.currentLocationMachines;
        locationMachines._machinesComponent.setMachinesStatus(msg);

        if(locationMachines.queuePanel.machineIsDirty(msg.machineIndex)){
            if(locationMachines.queuePanel.spinner.active){
                locationMachines.queuePanel.resetEntrance(true);
                _instance.enterQueue();
            }
        }
    };

    p._handleQueueFailed = function(queuePanel,code){
        switch (code) {
            case ErrorMessageType.ERROR_TOKEN_ALREADY_IN_QUEUE:// ok desi se: usao, prekinuo counter sa backToLobby, usao ponovo dok je zuta
            case ErrorMessageType.ERROR_TOKEN_ALREADY_ON_MACHINE:// ok desi se: povratak iz masine i udje na zutu u kojoj je bio
            case ErrorMessageType.ERROR_TOKEN_ALREADY_IN_GAME:
                console.warn(">>>>> "+ErrorMessageType[String(code)]);
                // onemogucavanje multiplayera ako se pojavi spiner // todo nije provereno , nije na serveru
                //queuePanel.multiPlayerControls.enable(false);
                //queuePanel.multiPlayerControls.stakeAmountMock.visible = false;
                if(!queuePanel.spinner.active){
                    queuePanel.onQueueDeferred(code);// upali spinner load i cekaj
                }
                break;
            default:
                queuePanel.onQueueError(ErrorMessageType[String(code)]);
        }
        //queuePanel.onQueueError(ErrorMessageType[String(code)]);
    };

    alteastream.SocketCommunicatorLobby = createjs.promote(SocketCommunicatorLobby,"ABSSocketCommunicator");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorRoulette = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorRoulette();
    };
    var p = createjs.extend(SocketCommunicatorRoulette, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var GameMessageType = {
        1:"CONFIRM_GAME_INIT",
        4:"STATUS_CHANGE",
        //2:"MACHINE_RESET_START",
        //3:"MACHINE_RESET_END",
        5:"GAME_IS_LAUNCHED",
        //7:"MOVE_SHOOTER_LEFT",
        //8:"MOVE_SHOOTER_RIGHT",
        //9:"STOP_SHOOTER",
        10:"SEND_BET",// sendBet
        11:"PRIZE_DETECTION",// win
        //12:"SHOOT_ENABLED",
        40:"NON_PLAYING_TIME_LIMIT_REACHED",
        //50:"TOKENS_COUNT_CHANGED",
        60:"BALANCE_CHANGED",
        //64:"BONUS_WON",
        70:"MACHINE_ERROR",
        85:"QUIT",
        100:"HEARTBEAT",
        CONFIRM_GAME_INIT:1,
        STATUS_CHANGE:4,
        //MACHINE_RESET_START:2,
        //MACHINE_RESET_END:3,
        GAME_IS_LAUNCHED:5,
        //MOVE_SHOOTER_LEFT:7,
        //MOVE_SHOOTER_RIGHT:8,
        //STOP_SHOOTER:9,
        SEND_BET:10,
        PRIZE_DETECTION:11,
        //SHOOT_ENABLED:12,
        NON_PLAYING_TIME_LIMIT_REACHED:40,
        //TOKENS_COUNT_CHANGED:50,
        BALANCE_CHANGED:60,
        //BONUS_WON:64,
        MACHINE_ERROR:70,
        QUIT:85,
        HEARTBEAT:100
    };

// constructor:
    p.initialize_SocketCommunicatorRoulette = function() {
        _instance = this;
        this.USER_PRESENCE = 10000;
        _instance.gameIsInitialized = false;//iphone debug
    };

// static methods:
    SocketCommunicatorRoulette.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(params){
        var currentGameUid = this.currentGameUid = params.key;
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/wsgame"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&game="+currentGameUid+"&t="+Date.now());
        var gameClient = this.gameClient = Stomp.over(socket);
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onGameConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToMachine();
        _instance.heartbeatGame();
    };

    p._subscribeToMachine = function(){
        console.log("socket>> startGame socket Game :::::::::::::::: ");
        var subscriptionObj = {};
        subscriptionObj.name = "gameplay_"+_instance.currentGameUid;
        subscriptionObj.subscription = this.gameClient.subscribe("/topic/"+_instance.currentGameUid+"", function (msg) {
            var mesg = JSON.parse(msg.body);
            _instance.parseGameMessage(mesg);
        });

        this.subscriptions.push(subscriptionObj);

        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.GAME_IS_LAUNCHED,
            tkn:_instance.currentGameUid
        }))
    };

    p.startHeartBeatGame = function () {
        _instance.heartbeatGame();
        this.heartbeatGameInterval = setInterval(function(){
            _instance.heartbeatGame();
        },this.USER_PRESENCE);
    };

    p.heartbeatGame = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/cmd", {}, JSON.stringify({
                val:0,
                tp:GameMessageType.HEARTBEAT,
                tkn:_instance.currentGameUid
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatGameInterval);
    };

    p.sendBet = function(bet) {
        console.log("socket>> sendBet:::::::::::::::::: "+bet);
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            val:bet,
            tp:GameMessageType.SEND_BET,
            tkn:_instance.currentGameUid
        }));
    };

    //cameras maybe
    /*
    p.moveShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:actionType,
            tkn:_instance.currentGameUid
        }));
    };

    p.stopMovingShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.STOP_SHOOTER,
            tkn:_instance.currentGameUid
        }));
    };*/

    p.confirmGameInit = function(){
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.CONFIRM_GAME_INIT,
            tkn:_instance.currentGameUid
        }));
    };

    p.quitPlay = function(){
        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.QUIT,
            tkn:_instance.currentGameUid
        }))
    };

    p.parseGameMessage = function(msg){
        console.log("socket>> parseGameMessage:::::::::::::: ");
        console.log(msg);
        switch(msg.tp){
            case GameMessageType.CONFIRM_GAME_INIT:
                console.info("socket>> CONFIRM_GAME_INIT:::::::::::::: "+msg.dsc);
                _instance.game.showGame(true);
                break;
            case GameMessageType.GAME_IS_LAUNCHED:
                console.log("socket>> GAME_IS_LAUNCHED:::::::::::::: "+msg.description);
                //console.log("data:::::::::::::: "+msg.data);
                _instance.startHeartBeatGame();
                break;
            /*case GameMessageType.SHOOT_ENABLED:
                if(msg.vl){
                    console.log("socket>> SHOOT_ENABLED:::::::::::::: "+msg.vl);
                    _instance.game.allowedShooterBtn(msg);
                    _instance.game.monitorIdleTime(true);
                }
                break;*/
            case GameMessageType.STATUS_CHANGE:
                console.log("socket>> STATUS_CHANGE:::::::::::::: ");// 7 statusa {"tp":4,"cat":3,"vl":[1,2,3,4,5,6,7]}
                _instance.game.statusChange(msg.vl);
                break;
            case GameMessageType.PRIZE_DETECTION:
                console.log("socket>> PRIZE_DETECTION:::::::::::::: ");
                _instance.game.onPrizeDetection(msg.vl);////vl = WINNING NUMBER {"tp":11,"cat":3,"vl":{"user_bets":5.0,"win":0,"drawnNo":28,"amount":0,"money":0,"balance":862206}}
                break;
            case GameMessageType.BALANCE_CHANGED:
                console.log("socket>> BALANCE_CHANGED:::::::::::::: ");
                _instance.game.setCrd(msg.bal);
                //_instance.game.updateQueueInfo(msg.queueSize);
                break;
            case GameMessageType.QUIT:
                console.info(">>>>> QUIT:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.exitMachineGameplay();// exitMachineGame
                break;
            case GameMessageType.MACHINE_ERROR:
                _instance.heartbeatGame();
                _instance.game.onMachineError(msg.vl);
                /*switch(msg.vl){
                    case 1:
                        _instance.heartbeatGame();
                        _instance.game.onMachineError(msg.vl);
                        break;
                }*/
                break;
            default:
                console.warn("UNHANDLED MESSAGE TYPE>> "+msg.tp);
        }
    };

    alteastream.SocketCommunicatorRoulette = createjs.promote(SocketCommunicatorRoulette,"ABSSocketCommunicator");
})();/**
 * Created by Dacha on 06-Jul-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var ABSController = function (game) {
        this.initialize_ABSController(game);
    };

    var p = ABSController.prototype;
    var keyboard = null;
    var GameMessageType = {
        7:"MOVE_SHOOTER_LEFT",
        8:"MOVE_SHOOTER_RIGHT",
        9:"STOP_SHOOTER",
        10:"DISPENSE_TOKENS",
        MOVE_SHOOTER_LEFT:7,
        MOVE_SHOOTER_RIGHT:8,
        STOP_SHOOTER:9,
        DISPENSE_TOKENS:10
    };



// static properties:
    p.KEYCODE_SHOOT = 32;
    p.KEYCODE_LEFT = 37;
    p.KEYCODE_RIGHT = 39;
    p.KEYCODE_BET_UP = 38;
    p.KEYCODE_BET_DOWN = 40;
    p.KEYCODE_TO_VALUE_VIEW = 17;
    p.KEYCODE_INFO = 73;

// events:

// public vars:
// private vars:
    p.shootActive = false;
    p.leftActive = false;
    p.betUpActive = false;
    p.betDownActive = false;
    p.rightActive = false;
    p.toViewValueActive = false;
    p.infoActive = false;
    p.active = false;
    p.twinComponent = null;


// constructor:
    p.initialize_ABSController = function (game) {
        this.game = game;
    };

// static methods:

// public functions:
    p.activate = function(){
        keyboard = this;
        window.parent.addEventListener("keydown",this.keyDown,false);
        window.parent.addEventListener("keyup",this.keyUp,false);
        var canvas = document.getElementById("screen");
        canvas.focus();
        window.addEventListener("click",function(){
            if(document.activeElement !== canvas)
                canvas.focus();
        });
    };

    p.keyDown = function(e) {
        e.preventDefault();
       /* if (!e) {
            var e = window.event;
        }*/
        if(keyboard.game.controlBoard.active)
            return;
        keyboard.active = true;
        var game = keyboard.game;
        var currentType = -1;
        switch(e.keyCode){
            case keyboard.KEYCODE_SHOOT:
                if(!keyboard.shootActive) {
                    keyboard.shootActive = true;
                    currentType = 0;
                    keyboard.onShoot(game/*,GameMessageType.DISPENSE_TOKENS*/);
                }
                break;
            case keyboard.KEYCODE_LEFT:
                if(keyboard.rightActive)
                    return;
                if(!keyboard.leftActive) {
                    keyboard.leftActive = true;
                    currentType = 1;
                    keyboard.onShooterAction(game,GameMessageType.MOVE_SHOOTER_LEFT);
                }
                break;
            case keyboard.KEYCODE_RIGHT:
                if(keyboard.leftActive)
                    return;
                if(!keyboard.rightActive) {
                    keyboard.rightActive = true;
                    currentType = 2;
                    keyboard.onShooterAction(game,GameMessageType.MOVE_SHOOTER_RIGHT);
                }
                break;
            case keyboard.KEYCODE_BET_UP:
                if(keyboard.betUpActive)
                    return;
                if(!keyboard.betDownActive) {
                    keyboard.betUpActive = true;
                    currentType = 3;
                    keyboard.onStakeChange(game,"inc");
                }
                break;
            case keyboard.KEYCODE_BET_DOWN:
                if(keyboard.betDownActive)
                    return;
                if(!keyboard.betUpActive) {
                    keyboard.betDownActive = true;
                    currentType = 4;
                    keyboard.onStakeChange(game,"dec");
                }
                break;
            case keyboard.KEYCODE_TO_VALUE_VIEW:
                if(!keyboard.toViewValueActive) {
                    keyboard.toViewValueActive = true;
                    currentType = 5;
                    keyboard.onToValueViewChange(game);
                }
                break;
            case keyboard.KEYCODE_INFO:
                if(!keyboard.infoActive) {
                    keyboard.infoActive = true;
                    currentType = 6;
                    keyboard.onInfo(game);
                }
                break;
        }
        if(currentType>-1){
            game.controlBoard.blockMouse(true);
            game.controlBoard.highlightCurrent(currentType,true);
        }
    };

    p.keyUp = function(e) {
        e.preventDefault();
        /* if (!e) {
         var e = window.event;
         }*/
        if(keyboard.game.controlBoard.active)
            return;
        keyboard.active = false;
        var game = keyboard.game;
        var currentType = -1;
        var type = null;
        switch(e.keyCode){
            case keyboard.KEYCODE_SHOOT:
                keyboard.shootActive = false;
                currentType = 0;
                break;
            case keyboard.KEYCODE_LEFT:
                if(keyboard.rightActive)
                    return;
                keyboard.leftActive = false;
                currentType = 1;
                type = GameMessageType.STOP_SHOOTER;
                break;
            case keyboard.KEYCODE_RIGHT:
                if(keyboard.leftActive)
                    return;
                keyboard.rightActive = false;
                currentType = 2;
                type = GameMessageType.STOP_SHOOTER;
                break;
            case keyboard.KEYCODE_BET_UP:
                if(keyboard.betDownActive)
                    return;
                keyboard.betUpActive = false;
                currentType = 3;
                break;
            case keyboard.KEYCODE_BET_DOWN:
                if(keyboard.betUpActive)
                    return;
                keyboard.betDownActive = false;
                currentType = 4;
                break;
            case keyboard.KEYCODE_TO_VALUE_VIEW:
                keyboard.toViewValueActive = false;
                currentType = 5;
                break;
            case keyboard.KEYCODE_INFO:
                keyboard.infoActive = false;
                currentType = 6;
                break;
        }
        if(currentType>-1){
            game.controlBoard.blockMouse(false);
            game.controlBoard.highlightCurrent(currentType,false);
        }
        keyboard.onUp(keyboard.game,type);
    };

    p.disableLeftRight = function(bool){
        keyboard.leftActive = bool;
        keyboard.rightActive = bool;
    };

    p.disableUpDown = function(bool){
        keyboard.betUpActive = bool;
        keyboard.betDownActive = bool;
    };

    p.disableShoot = function(bool){
        keyboard.shootActive = bool;
    };

// private functions:

    alteastream.ABSController = ABSController;
})();

// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var AbstractControlBoard = function() {
        this.Container_constructor();
        this.initialize_AbstractControlBoard();
    };

    var p = createjs.extend(AbstractControlBoard, createjs.Container);

// static properties:
// events:
// public properties:
    p.background = null;
    p.btnDecStake = null;
    p.btnIncStake = null;
    p.btnSend = null;
    p.btnCollect = null;
    p.btnShooterLeft = null;
    p.btnShooterRight = null;
    p.btnInfo = null;
    //p.btnQuit = null;
    //p.btnSound = null;
    p.btnBack = null;
    p._textStake = null;
    p._textBet = null;
    p._textStakeAmount = null;
    p._textWin = null;
    p._textCrd = null;
    p._textWinAmount = null;
    p._textCrdAmount = null;

    p._stakeBackground = null;
// private properties:

    p.twinComponent = null;

    p._infoBtnHandler = null;
    p._quitBtnHandler = null;
    //p._backBtnHandler = null;
    p._incStakeBtnHandler = null;
    p._decStakeBtnHandler = null;
    p._sendBtnHandler = null;
    p._scene = null;
    p.cbCurrency = null;
    p.currentButtonTextPreset = null;
    p.globalStorage = false;
    p.active = false;
    p._toValueView = true;
    p.textColorCoins = "#ffffff";
    p.textColorMoney = "#ffffff";
    p.controlShown = true;
    p.menuIsShown = false;
    var _this;

// constructor:

    p.initialize_AbstractControlBoard = function() {

    };

// static methods:
// public methods:
    p.plugin = function(scene) {
        _this = this;
        this._scene = scene;
        this._infoBtnHandler =       function() { scene.infoBtnHandler(); };
        this._quitBtnHandler =       function() { scene.quitBtnHandler(); };
        //this._backBtnHandler =       function() { scene.backBtnHandler(); };
        this._incStakeBtnHandler =   function() { scene.incStakeBtnHandler(); };
        this._decStakeBtnHandler =   function() { scene.decStakeBtnHandler(); };
        this._btnBetViewHandler =   function() { _this.onBtnBetViewHandler(); };
        //new stakes
        this._btnFixedStakesHandler =   function() { _this.btnFixedStakesHandler(); };
        this._sendBtnHandler =   function() { scene.sendBtnHandler(); };
        this._collectBtnHandler =   function() { scene.collectBtnHandler(); };
        this._shooterMoveBtnHandler =   function(e) {
            if(_this.twinComponent){
                if(_this.twinComponent.active) return;
            }
            _this.active = true;
            var id = e.currentTarget.id;
            scene.moveShooterBtnHandler(id);
        };
        this._shooterUpBtnHandler =   function(e) {
            if(_this.twinComponent){
                if(_this.twinComponent.active) return;
            }
            _this.active = false;
            /*var id = e.currentTarget.id;*/
            scene.stopShooterBtnHandler(/*id*/);
        };

        //this.btnQuit.setClickHandler(this._quitBtnHandler);
        this.btnInfo.setClickHandler(this._infoBtnHandler);
       // this.btnBack.setClickHandler(this._backBtnHandler);

        if (this.btnSend) this.btnSend.setClickHandler(this._sendBtnHandler);
        //this.btnCollect.setClickHandler(this._collectBtnHandler);

        if (this.btnShooterLeft) this.btnShooterLeft.setHoldDownHandler(this._shooterMoveBtnHandler);
        if (this.btnShooterLeft) this.btnShooterLeft.setUpHandler(this._shooterUpBtnHandler);

        if (this.btnShooterRight) this.btnShooterRight.setHoldDownHandler(this._shooterMoveBtnHandler);
        if (this.btnShooterRight) this.btnShooterRight.setUpHandler(this._shooterUpBtnHandler);

        if (this.btnIncStake) this.btnIncStake.setClickHandler(this._incStakeBtnHandler);
        if (this.btnDecStake) this.btnDecStake.setClickHandler(this._decStakeBtnHandler);

        if (this.btnBetView) this.btnBetView.setClickHandler(this._btnBetViewHandler);

        //new stakes
        if (this.btnFixedStakes) this.btnFixedStakes.setClickHandler(this._btnFixedStakesHandler);

/*        this.btnSound.on('click', function() {
            _this.soundBtnHandler();
        });*/
    };

    p.onBtnBetViewHandler = function(){
        this._toValueView = !this._toValueView;
        this._scene.setCurrency();
        this.setCrd(this._scene.credit);
        this._scene.betManager.onStakeChange();
        this._scene.setMonetaryValueView(this._toValueView);
        this._scene.playSound("btnDownDouble");
    };

    p.btnFixedStakesHandler = function(){
        if(this.menuIsShown===true){
            this.doShowMenu(false);
        }

        this.fixedStakesPanel.show(!this.fixedStakesPanel._active);
        if(this.fixedStakesPanel._active && this.controlShown === true)
            this._moveShooterLeftRightAndBtnSendComponents(true);
        else if(!this.fixedStakesPanel._active && this.controlShown === false)
            this._moveShooterLeftRightAndBtnSendComponents(false);

        this._scene.playSound("btnDownDouble");
    };

    p.blockMouse = function(bool){//if kbd left/right is down
        this.btnShooterLeft.mouseEnabled = !bool;
        this.btnShooterRight.mouseEnabled = !bool;

        this.btnIncStake.mouseEnabled = !bool;
        this.btnDecStake.mouseEnabled = !bool;
    };

    p.tryShooter = function(){
        if(this._scene.okToShowBalance === true){
            this.blockShooter(false);
        }
    };

    p.blockShooter = function(bool){
        this.btnSend.setDisabled(bool);
    };

    p.highlightCurrent = function(type,bool){
        var action = bool === true? "mouseDownPreActionHook":"mouseDownPostActionHook";
        switch(type){
            case 0:
                if(this.btnSend._disabled)
                    return;
                this.btnSend[action]();
                break;
            case 1:
                this.btnShooterLeft[action]();
                break;
            case 2:
                this.btnShooterRight[action]();
                break;
            case 3:
                if(this.btnIncStake._disabled)
                    return;
                this.btnIncStake[action]();
                break;
            case 4:
            if(this.btnDecStake._disabled)
                return;
            this.btnDecStake[action]();
            break;
            case 5:
                this.btnBetView[action]();
                break;
            case 6:
                this.btnInfo[action]();
                break;
        }
    };

    p.onKeyboardStakeChange = function(type){
        if(type === "inc"){
            if(this.btnIncStake._disabled)
                return;

            if(this.btnDecStake._disabled)
                this.btnDecStake.setDisabled(false);
        }

        if(type === "dec"){
            if(this.btnDecStake._disabled)
                return;
            if(this.btnIncStake._disabled)
                this.btnIncStake.setDisabled(false);
        }

        var handler = type + "StakeBtnHandler";
        this._scene[handler]();
    };

    p.setSingleToBurst = function(bool) {
        //if(this.btnSend._disabled)
            //return;
        var type = bool === true?"burst":"single";
        this.btnSend.selectTextPreset(type);

        if(this.btnSend.textIsImage(this.btnSend.text)){
            this.btnSend.normalTextColor = this.btnSend.text.image;
            this.btnSend.text.alpha = this.btnSend._disabled?0.2:1;
        }else{
            this.btnSend.text.color = this.btnSend.isDisabled() ? this.btnSend.disabledTextColor : this.btnSend.normalTextColor;
        }
    };

    p.setCurrency = function(val){
        var pearlsOrCoins = alteastream.AbstractScene.GAME_TYPE === "pearlpusher" ? "PEARLS" : "COINS";
        this._textCurrencyValue.text = this._toValueView === true? val:pearlsOrCoins;
    };

    p.setBetToSend = function(){
        this.setBetAmount(parseFloat(this._scene.betManager.betValue())/*.toFixed(2)*/);
    };

    p.setBetAmount = function(amount) {
        this._textBetAmount.text = amount;
    };

    p.setStakeAmount = function(stakeAmount,multiplier) {
        var betTypeToShow = this._toValueView === true? Math.min(stakeAmount*multiplier).toFixed(2): stakeAmount;
        this._textStakeAmount.text = betTypeToShow;
    };

    p.getStakeAmount = function() {
        var amount = this._textStakeAmount.text;
        return parseFloat(amount);
    };

    p.updateTextColorType = function(isEur){
        var color = isEur === true? this.textColorMoney:this.textColorCoins;
        this._textCrdAmount.color =
            this._textCurrencyValue.color =
                this._textStakeAmount.color = color;
    };

    p.setWin = function(winAmount) {
        this._textWinAmount.text = winAmount;//(winAmount/100).toFixed(2);
    };

    p.setCrd = function(crdAmount) {
        console.log("setCrd good:::::::");
        var crdAmountCents = Math.min(crdAmount/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this._textCrdAmount.text = this._toValueView === true? crdAmountCents: Math.min(crdAmount/100);
    };

    p.getCrd = function() {
        var amount = this._textCrdAmount.text;
        return parseFloat(amount).toFixed(2);
    };

/*    p.soundBtnHandler = function(){
        var assets = alteastream.Assets;
        this.soundToggled = !this.soundToggled;
        this.btnSound.image = this.soundToggled === true? assets.getImage(assets.images.soundOff).image: assets.getImage(assets.images.soundOn).image;
        //assets.setMute(this.soundToggled);

        var action = this.soundToggled===true?"pause":"play";
        window.parent.manageBgMusic(action);
    };*/

    p.disableTextAmountField = function(disabled,textField){
        textField.alpha = disabled === true?0.4:1;
    };

    p.storeButtonStates = function(){
        this.btnIncStake.storeDisabledState();
        this.btnIncStake.setDisabled(true);
        this.btnDecStake.storeDisabledState();
        this.btnDecStake.setDisabled(true);
        this.disableTextAmountField(true, this._textStakeAmount);
    };

    p.restoreButtonStates = function(){
        if (this.btnIncStake) {
            this.btnIncStake.restoreDisabledState();
            this.btnDecStake.restoreDisabledState();
            this.disableTextAmountField(false, this._textStakeAmount);
        }

        if (this.btnIncCoinBet) {
            this.btnIncCoinBet.restoreDisabledState();
            this.btnDecCoinBet.restoreDisabledState();
            this.disableTextAmountField(false, this._textCoinBetAmount);
        }
    };

    p.activateAllButtons = function(bool){
        //this.btnQuit.setDisabled(!bool);
        this.btnInfo.setDisabled(!bool);
    };

    p.initialSetup = function(){
        this.activateAllButtons(false);
        this.btnInfo.setDisabled(false);
        //this.btnQuit.setDisabled(false);
    };

    p.centerRegPoint = function(object){
        object.regX = object.image.width >> 1;
        object.regY = object.image.height >> 1;
    };

    p.disablePlayButtons = function(bool){
        if(bool === true){
            this.storeButtonStates();
        }else{
            this.restoreButtonStates();
        }
        this.blockShooter(bool);
        this.btnShooterLeft.setDisabled(bool);
        this.btnShooterRight.setDisabled(bool);
        this.active = bool;

        this.btnInfo.setDisabled(bool);
        this.btnBetView.setDisabled(bool);
    };

// private methods:
    alteastream.AbstractControlBoard = createjs.promote(AbstractControlBoard,"Container");
})();
/**
 * Created by Dacha on 06-Jul-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var Controller = function (game) {
        this.ABSController_constructor(game);
        this.initialize_Controller();
    };

    var p = createjs.extend(Controller,alteastream.ABSController);


// static properties:


// events:
// public vars:
// private vars:

// constructor:
    p.initialize_Controller = function () {
    };

// static methods:
// public functions:

    p.onShoot = function(game/*,e*/){
        game.sendBtnHandler(/*e*/);
    };

    p.onShooterAction = function(game,e){
        game.moveShooterBtnHandler(e);
    };

    p.onStakeChange = function(game,type){
        game.controlBoard.onKeyboardStakeChange(type);
    };

    p.onToValueViewChange = function(game) {
        game.controlBoard.onBtnBetViewHandler();
    };

    p.onInfo = function(game) {
        game.infoBtnHandler();
    };

    p.onUp = function(game,e){
        if(e !== null)
            game.stopShooterBtnHandler(e);
    };

    // private functions:

    alteastream.Controller = createjs.promote(Controller,"ABSController");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var FixedStakes = function(){
        this.Container_constructor();
        this.initFixedStakes();
    };

    var p = createjs.extend(FixedStakes,createjs.Container);

    var _this;
    p._stakes = [1,5,0,0,0];
    p._shapes = {};
    p._values = {};
    p._active = false;

    p.initFixedStakes = function () {
        console.log("FixedStakes initialized:::");
        _this = this;
        this.visible = false;
    };

    FixedStakes.getInstance = function() {
        return _this;
    };

    p.plugin = function(machine){
        this.machine = machine;
        var maxStake = machine.maxStake;
        this.multiplier = machine.betManager.getMultiplier();
        var startLength = this._stakes.length;

        this._stakes[startLength-1] = maxStake;
        this._stakes[startLength-2] = Math.ceil((maxStake/2)/5)*5;
        this._stakes[startLength-3] = Math.ceil((maxStake/4)/5)*5;
        this.spliceCount = 0;

        this._stakes = this._stakes.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) === index;
        });
        this.spliceCount = startLength-this._stakes.length;

        this._setLayout();
    };

    p._setLayout = function(){
        var fieldSize = this.fieldSize = 68;
        var roundness = 10;
        var totalHeight = fieldSize*this._stakes.length;

        for(var i = 0,j = this._stakes.length;i<j;i++){
            var text = this._stakes[i];
            var tl = 0, tr = 0, br = 0, bl = 0;
            if(i===0){
                bl = roundness; br = roundness;
            }
            if(i===this._stakes.length-1){
                tl = roundness; tr = roundness;
                br = 0; bl =0;
            }
            var fieldShape = this._shapes[i] = new createjs.Shape();
            fieldShape.graphics.beginFill("#101010").drawRoundRectComplex(0, 0, fieldSize, fieldSize,tl,tr,br,bl);
            fieldShape.regX = fieldSize*0.5;
            fieldShape.regY = fieldSize*0.5;
            fieldShape.cache(0, 0, fieldSize, fieldSize);
            fieldShape.alpha = 0.7;

            var textLabel = this._values[i] = new createjs.Text(text,"18px Lato","#000000").set({textAlign:"center",textBaseline:"middle"});
            textLabel.hitArea = fieldShape;

            var fieldContainer = new createjs.Container();
            fieldContainer.addChild(fieldShape,textLabel);
            this.addChild(fieldContainer);
            fieldContainer.name = this._stakes[i];
            fieldContainer.y = totalHeight-(fieldSize*0.5)-(i*fieldSize);
            fieldContainer.on("click", function(e){
                _this._onStakeSelect(e);
            })
        }

        this.inPosition = 1857;
        this.yPosition = 630;
        this._setPosition();
    };

    p.show = function(bool){
        if(bool === true){
            if(!this._active){
                this._active = true;
                this.visible = true;
                createjs.Tween.get(this).to({x:this.inPosition},200,createjs.Ease.linear);
            }
        }else{
            if(this._active){
                this._active = false;
                createjs.Tween.get(this).to({x:alteastream.AbstractScene.GAME_WIDTH+this.fieldSize},200,createjs.Ease.linear).call(function () {
                    _this.visible = false;
                });
            }
        }
    };

    p._setPosition = function(){
        this.y = this.yPosition+(this.fieldSize*this.spliceCount);//647
        this.x = alteastream.AbstractScene.GAME_WIDTH+this.fieldSize;
        this._shapes[0].alpha = 1;
    };

    p._onStakeSelect = function(e){
        var targetName = e.currentTarget.name;
        var targetShape = this._stakes.indexOf(targetName);
        for(var p in this._shapes){
            this._shapes[p].alpha = 0.7;
        }
        this._shapes[targetShape].alpha = 1;

        var position = this.machine._stakeIncrements.indexOf(targetName);
        this.machine.setStake(position);
        this.machine.controlBoard._moveShooterLeftRightAndBtnSendComponents(false);
    };

    p.updateMonetary = function(isEur){
        for(var p in this._values){
            this._values[p].text = isEur===true? Math.min(this._stakes[p]*this.multiplier ).toFixed(2): this._stakes[p];
            this._values[p].color = isEur===true? "#00ff00": "#ffd500";
        }
        //this._values[this._stakes.length-1].text = "MAX"; //they want a value, not text
    };

    p.adjustMobile = function(){
        this.inPosition = 898;
        this.yPosition = 100;

        this._setPosition();
    };

    alteastream.FixedStakes = createjs.promote(FixedStakes,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var AbstractInfo = function(scene) {
    this.Container_constructor();
    this.initialize_AbstractInfo(scene);
};
var p = createjs.extend(AbstractInfo, createjs.Container);

// static properties:

// events:
    /**
     * @event infoScreenChanged
     */

// public properties:
    p.scene = null;

// private properties:

// constructor:

    p.initialize_AbstractInfo = function(scene) {
        this.scene = scene;
    };

    p.showInfo = function(){};

// static methods:
// public methods:



    alteastream.AbstractInfo = createjs.promote(AbstractInfo,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var Clock = function(scene,x,y) {
    this.Container_constructor();
    this.initialize_Clock(scene,x,y);
};
var p = createjs.extend(Clock, createjs.Container);

// static properties:

// public properties:
    p.parent = null;
    p.timer = null;
    p._clockText = null;

// private properties:

// constructor:
    p.initialize_Clock = function(scene,x,y) {
        this.parent = scene;
        this._clockText = new createjs.Text("00000000","bold 15px Arial","#bbb8ba").set({y:8,textAlign:"center",textBaseline:"middle"});

        var clockBg = new createjs.Shape();
        clockBg.graphics.beginFill("#000000").drawRoundRect(-89, -5, 178, 23, 5);
        clockBg.alpha = 0.7;
        clockBg.cache(-89, -5, 178, 23);
        this.addChild(clockBg);

        this.addChild(this._clockText);
        this.parent.addChild(this);
        this.x = x;
        this.y = y;

        this.visible = false;
        this.mouseEnabled = false;
        this.mouseChildren = false;
    };

    p.show = function(bool){
        if(bool){
            var that = this;
            var startTime = function() {
                that._clockText.text = new Date().toLocaleString();
                that.timer = setTimeout(startTime, 500);
            };
            startTime();
        }else{
            clearTimeout(this.timer);
        }
        this.visible = bool;
    };

// static methods:
// public methods:
    alteastream.Clock = createjs.promote(Clock,"Container");
})();// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var QuitConfirm = function(quitMethod) {
        this.Container_constructor();
        this.initialize_QuitConfirm(quitMethod);
    };
    var p = createjs.extend(QuitConfirm, createjs.Container);
    p._quitMethod = null;
    p._questionBackground = null;
    p._buttonYes = null;
    p._buttonYesText = null;
    p._buttonNo = null;
    p._buttonNoText = null;
// static properties:
// events:
// public properties:
// private properties:
// constructor:

    p.initialize_QuitConfirm = function(quitMethod) {
        alteastream.Assets.playSound("info");
        this._quitMethod = quitMethod;
        var _this = this;
        var background = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRect(0, 0, alteastream.AbstractScene.GAME_WIDTH, alteastream.AbstractScene.GAME_HEIGHT);
        background.alpha = 0.6;
        background.cache(0, 0, alteastream.AbstractScene.GAME_WIDTH, alteastream.AbstractScene.GAME_HEIGHT);

        var quitCounter = this._quitCounter = new alteastream.QuitCounter();
        quitCounter.regX = quitCounter.regY = quitCounter.width/2;
        quitCounter.x = alteastream.AbstractScene.GAME_WIDTH/2;
        quitCounter.y = alteastream.AbstractScene.GAME_HEIGHT/2 - 28;
        quitCounter.doneCallback = function(){_this._buttonHandler("yes");}

        var fontBig = "bold 32px Roboto";
        var fontSmall = "24px Roboto";
        var fontButton = "26px Roboto";

        var quitText = this.quitText = new createjs.Text("EXIT GAME?",fontBig,"#ffffff").set({textAlign:"center",textBaseline:"middle", x:quitCounter.x, y:quitCounter.y - 135, mouseEnabled:false});
        var buttonNoText = this._buttonNoText = new createjs.Text("Return to Game",fontButton,"#ffffff");

        var buttonNo = this._buttonNo = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnStart),3,buttonNoText);
        buttonNo.x = quitCounter.x;
        buttonNo.y = quitCounter.y + 224;
        var buttonNoClickHandler = function () {
            _this._buttonHandler("no");
        };
        buttonNo.setClickHandler(buttonNoClickHandler);
/*        buttonNo.on("click", function () {
            _this._buttonHandler("no");
        });*/

        var buttonYesText = this._buttonYesText = new createjs.Text("Exit",fontButton,"#ffffff");

        var buttonYes = this._buttonYes = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnExitForQuitCounter),2,buttonYesText);
        buttonYes.x = quitCounter.x;
        buttonYes.y = quitCounter.y + 310;
        var buttonYesClickHandler = function () {
            _this._buttonHandler("yes");
        };
        buttonYes.setClickHandler(buttonYesClickHandler);
/*        buttonYes.on("click", function () {
            _this._buttonHandler("yes");
        });*/

        this.addChild(background, quitCounter, quitText, buttonNo, buttonYes);
        this.preventClickThrough(true);
    };

    p.startUpdate = function() {
        this._quitCounter.update(10);
    };

    p._buttonHandler = function(quit) {
        if(quit === "yes"){
            this._quitMethod();
        }
        this._quitCounter.clearCounterInterval();
        this.removeAllChildren();
        this.parent.removeChild(this);
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };

    p.adjustMobile = function() {
        /*var background2 = this._questionBackground;
        background2.uncache();
        background2.graphics.clear();
        background2.graphics.beginFill("#303030").drawRoundRect(0, 0, 680, 350, 5);
        background2.cache(0, 0, 680, 350);
        background2.regX = 340;
        background2.regY = 175;
        background2.x = alteastream.AbstractScene.GAME_WIDTH/2;
        background2.y = alteastream.AbstractScene.GAME_HEIGHT/2;*/

        this._quitCounter.adjustMobile();
        this.quitText.font = "bold 26px Roboto";
        this.quitText.y = this._quitCounter.y - 60;

        this._buttonNoText.font = "18px Roboto";
        this._buttonYesText.font = "18px Roboto";

        this._buttonNo.y -= 110;
        this._buttonYes.y -= 120;

    };
// static methods:
// public methods:
// private methods:

    //alteastream.QuitConfirm = QuitConfirm;
    alteastream.QuitConfirm = createjs.promote(QuitConfirm,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var QuitCounter = function(){
        this.AbstractCounter_constructor();
        this.initQuitCounter();
    };

    var p = createjs.extend(QuitCounter,alteastream.AbstractCounter);
    p.thinCircle = null;

    var _this;

    p.initQuitCounter = function () {
        console.log("QuitCounter initialized:::");
        _this = this;
        this.strokeTickness = 19;
        this.counterRadius = 250;
        this.degree = 288;
        this.colors = ["#f9c221", "#ff0000"];
    };

    QuitCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var circleShape = this._circleShape = new createjs.Shape();
        var rad = 320;
        circleShape.graphics.clear().beginFill("#000000").drawCircle(0, 0, rad);
        this.addChild(circleShape);
        circleShape.mouseEnabled = false;
        circleShape.cache(-rad,-rad,rad*2,rad*2);
        circleShape.alpha = 0.7;

        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(19).beginStroke("#555153").arc(0, 0, 250, 0, (Math.PI/180) * 288).endStroke();
        counterCircleBackground.rotation = -234;
        counterCircleBackground.x = 0;
        counterCircleBackground.y = 0;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -234;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        var thinCircle = this.thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 0;
        thinCircle.y = 0;
        thinCircle.mouseEnabled = false;
        this.addChild(thinCircle);

        this._createText("confirmEnterCounter","0","bold 130px HurmeGeometricSans3", "#ffffff",{x:0,y:0,textAlign:"center",textBaseline:"middle"});//1332
    };

    p.adjustMobile = function(){
        this.strokeTickness = 10;
        this.counterRadius = 145;
        this.degree = 268;

        var rad = 200;
        this._circleShape.uncache();
        this._circleShape.graphics.clear().beginFill("#000000").drawCircle(0, 0, rad);
        this._circleShape.cache(-rad,-rad,rad*2,rad*2);

        this.confirmEnterCounter.font = "bold 65px HurmeGeometricSans3";
        this.confirmEnterCounter.x = 0;
        this.confirmEnterCounter.y = 0;

        var counterCircleBackground = this.counterCircleBackground;
        counterCircleBackground.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircleBackground.rotation = -224;
        counterCircleBackground.x = 0;
        counterCircleBackground.y = 0;

        var counterCircle = this.counterCircle;
        counterCircle.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircle.rotation = -224;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;

        var thinCircle = this.thinCircle;
        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = counterCircleBackground.x;
        thinCircle.y = counterCircleBackground.y;
    };

    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else{
            this.clearCounterInterval();
            this.doneCallback();
        }
    };

    alteastream.QuitCounter = createjs.promote(QuitCounter,"AbstractCounter");
})();
"use strict";
(function (win) {
    history.pushState(null, document.title, location.href);
    win.addEventListener('popstate', function (event) {
        history.pushState(null, document.title, location.href);
    });
})(window);
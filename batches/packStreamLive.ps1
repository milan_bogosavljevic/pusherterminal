#Created by Dacha on 24-Oct-19

$versionLabel  = "//Coinpusher v "
$versionNumber  = "1.1.6"
$root  = "C:\wamp64\www\casino\gauselmann"
$ABSStarter = $root + "\stream_core\preloaded\ABSStarter.js"
$RequesterLocal =  $root + "\stream_core\scene\communication\Requester.js"
$RequesterLive =  $root +"\Ztests\RequesterLIVE.txt"
$completeMergePath =  $root +"\batches\complete_merge_stream_new.bat"
$dirLocal =  $root +"\packedStream\"
$dirLive =  $root +"\packedStreamLIVE\"
$dirBackupPath = $root +"\packedStreamVERSIONS\"
$keyword = "// local ver"
$logs = "console.log"
$targetFiles = @("alteastream.js", "coinpusher.js", "loading.js","lobby.js","utils.js","roulette.js")
$dirBackupName = $dirBackupPath+"v "+$versionNumber

function _createRequesterLive{
    Get-ChildItem $RequesterLive |ForEach-Object {
        $NewName = $_.Name -replace "^(LIVE\.)(.*)",'$2'
        $Destination = Join-Path -Path $_.Directory.FullName -ChildPath $NewName
        Copy-Item -Path $_.FullName -Destination $RequesterLocal -Force
    }
}

function _backup{
    echo "backupTo: $dirBackupName"
    $isNewVersion = "yes"
   if ( Test-Path -Path $dirBackupName -PathType Container ){
       $p = $dirBackupName + "\changelog.txt"
       $previousLog = [IO.File]::ReadAllText($p)
       Remove-Item $dirBackupName -Recurse -Confirm:$false
       $isNewVersion = "no"
   }
   Copy-item -Recurse $dirLive -Destination $dirBackupName -Force
    if([string]$isNewVersion -eq "yes"){
        _enterChangeLog
    }else{
        _appendChangeLog $previousLog
    }
}

function _enterChangeLog{
    $changelog = Read-Host -Prompt 'NEW CHANGELOG:: '
    $newLogFile = $dirBackupName + "\changelog.txt"
    New-Item $newLogFile
    if ($changelog) {
        $txt = "* "+$changelog
        Set-Content $newLogFile $txt
    } else {
        _promptError
    }
}

function _appendChangeLog{
    param([string]$previousLog)
    $appendlog = Read-Host -Prompt 'APPEND CHANGELOG:: '
    if ($appendlog) {
        $getLog = $dirBackupName + "\changelog.txt"
        New-Item $getLog
        Set-Content $getLog $previousLog"* "$appendlog
    } else {
        _promptError
    }
}

function _promptError{
    Write-Warning -Message "MUST ENTER CHANGELOG::"
    _enterChangeLog
}

function _runMerge{
    cmd.exe /c $completeMergePath
}

function _copyFromTo{
    param([string]$source,[string]$destination)
    Get-ChildItem -Path $dirLocal -Recurse -Include $targetFiles | Copy-Item -Destination $dirLive
}

function _startPack {
    echo "startPack : "
    $source = Get-ChildItem $dirLocal
    Foreach($file in $source) {
        if($targetFiles.IndexOf([string]$file) -gt -1){
            echo "for live:: $file"
            Get-Content $file.fullname |
                    Where-Object {$_ -notmatch [string]$keyword} |
                    #Where-Object {$_ -notmatch [string]$logs} | # delete console.log also
            Set-Content $dirLive$file
        }
    }
}

function _prependVersion {
    echo "versioning : "
    $sourceVer = Get-ChildItem $dirLive
    Foreach($file in $sourceVer) {
        $versionLabel+$versionNumber+"`r`n" + (Get-Content $file.fullname -Raw) | Set-Content $dirLive$file
    }
}

function _packToLive{
    param([string]$src,[string]$destination)
    $all = "\*.*"
    $clear = "$dirLive$all"
    Remove-Item $clear
    _copyFromTo $src $destination
    _startPack
}

_createRequesterLive
_runMerge
_packToLive $dirLocal $dirLive
_prependVersion
_backup

#$PSVersionTable.PSVersion
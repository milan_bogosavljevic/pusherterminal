this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var CoinPusherStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_CoinPusherStream();
    };

    var p = createjs.extend(CoinPusherStream, alteastream.ABSVideoStream);
// static properties:
// events:
// public vars:
// private vars:
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_CoinPusherStream = function (){
    };

    p.setPlayer = function(address){
        this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);
        this._local_activate();// local ver
    };

    p.handleError = function(error){
        //{"id":"error","code":-7,"txt":"User session does not exist"}
        //{"id":"error","code":13,"txt":"User token is expired or does not exist"}
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            that.scene.socketCommunicator.disposeCommunication();//quitPlay() if started
            that.scene.exitMachineGameplay();
        });
    }

    p.concreteActivate = function(){
        if(is.safari()){
            //var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
            //if(is.ipad() || is.ios() || isiPadOS){
            this.scene.setIosOverlay();
            //}
        }
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;
    };

    p.addSpinner = function(){
        var spinnerBackground = this.spinnerBackground = alteastream.Assets.getImage(alteastream.Assets.images.liveStreamConnecting);
        spinnerBackground.regX = spinnerBackground.image.width*0.5;
        spinnerBackground.regY = spinnerBackground.image.height*0.5;
        spinnerBackground.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinnerBackground.y = alteastream.AbstractScene.GAME_HEIGHT *0.5;

        var spinner = this.spinner = new alteastream.MockLoader();
        spinner.customScaleGfx(0.4);
        this.scene.addChild(spinnerBackground, spinner);
        //spinner.setLabel("Connecting to stream..");
        spinner.x = spinnerBackground.x + 196;
        spinner.y = spinnerBackground.y - 80;
        spinner.runPreload(true);
    }

    p.setMedia = function(){
        window.parent.switchSource("bgMusic");
        //that.scene.controlBoard.soundToggled = backgroundMusicIsMuted === "false";
        //that.scene.controlBoard.soundBtnHandler();

        // hajkova zelja, ako se skloni ovaj blok igra se ponasa tako sto pamti stanje backgroung muzike
        // i na osnovu toga pusta ili ne pusta zvuk kada se udje u masinu ili se vrati u lobi iz masine
        //if(that.scene.controlBoard.soundToggled === false){
        //that.scene.controlBoard.soundBtnHandler();
        //}

        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            soundMachineIsToggled = Boolean(soundMachineIsToggled);
        }else{
            soundMachineIsToggled = false;
        }

        if(soundMachineIsToggled === false){
            this.videoElement.setMuted(false);
            this.videoElement.setVolume(0.1);
            console.log("AUDIO ON:::::");
        }

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    }

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
        this.scene.addChild(poster);
        poster.mouseEnabled = false;*/
    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.startStreaming = function(){
        this.webrtcPlayer.startGameplayStream();//gameplay
    };

    p.onStreamFailed = function(){
        var _this = this;
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed. Exiting game",function(){_this.scene.onQuit();});
    }

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.CoinPusherStream = createjs.promote(CoinPusherStream,"ABSVideoStream");
})();
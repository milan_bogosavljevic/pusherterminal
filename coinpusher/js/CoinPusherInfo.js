
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var CoinPusherInfo = function(){
        this.Container_constructor();
        this.initCoinPusherInfo();
    };

    var p = createjs.extend(CoinPusherInfo,createjs.Container);

    // public properties
    p.prizeImages = [];
    p.prizeValues = [];
    p.prizeDescriptions = [];
    p.labels = [];
    p.gameDescriptions = [];
    p.chips = [];

    // constructor
    p.initCoinPusherInfo = function () {};

    // public methods
    p.setInfo = function(payoutManager, hasBonus, multiplier) {
        // if hasBonus can be true uncomment this two lines block
/*        var backgroundImage = hasBonus === true ? "infoBackground" : "infoBackgroundNoBonus";
        var background = alteastream.Assets.getImage(alteastream.Assets.images[backgroundImage]);*/
        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackgroundNoBonus);
        this.addChild(background);

        var chipsY = 165;
        var chipsYSpacing = 140;
        var chipsXSpacing = 0;

        var payouts = payoutManager.getPayouts();
        var tags = Object.keys(payouts);
        var maxChipsInColumn = 4;
        var yPosMultiplier = 0;
        var startXPos = tags.length > maxChipsInColumn ? 306 : 375;
        for(var i = 0; i < tags.length; i++) {
            var tag = tags[i];
            var prize = payouts[tag];

            if(yPosMultiplier === maxChipsInColumn){
                yPosMultiplier = 0;
                chipsXSpacing = 140;
            }

            yPosMultiplier++;

            var chip = new alteastream.Chip(prize, tag, multiplier);
            chip.x = startXPos + chipsXSpacing;
            chip.y = chipsY + (chipsYSpacing * yPosMultiplier);
            this.addChild(chip);
            chip.updateChip(true, false);
            this.chips.push(chip);
        }

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText2);
        var gameplayDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:1540, y:264, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        gameplayDescription.lineWidth = 270;
        gameplayDescription.lineHeight = 40;
        this.gameDescriptions.push(gameplayDescription);

        this.addChild(gameplayDescription);
    };

/*    p.setKeyboardLabels = function() {
        var ctrl = new createjs.Text("KEY CTRL","18px HurmeGeometricSans3","#cccccc").set({x:62,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var left = new createjs.Text("KEY LEFT","18px HurmeGeometricSans3","#cccccc").set({x:760,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var space = new createjs.Text("KEY SPACE","18px HurmeGeometricSans3","#cccccc").set({x:960,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var right = new createjs.Text("KEY RIGHT","18px HurmeGeometricSans3","#cccccc").set({x:1155,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var down = new createjs.Text("KEY DOWN","18px HurmeGeometricSans3","#cccccc").set({x:1736-87,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var up = new createjs.Text("KEY UP","18px HurmeGeometricSans3","#cccccc").set({x:1857-87,y:1070,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(ctrl,left,space,right,down,up);
    };*/

    p.setNonPlayingTimeInfo = function(time) {
        var seconds = time/1000;
        var textSrc = seconds >= 120? Math.floor(seconds/60) + "\nminutes": seconds + "\nseconds";

        //var textSrc = time/1000 + "\nseconds";
        var nonPlayingTimeSeconds = new createjs.Text(textSrc,"32px Impact","#e2d631").set({x:960, y:658, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        nonPlayingTimeSeconds.lineHeight = 30;

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var nonPlayingTimeDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:960, y:795, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        nonPlayingTimeDescription.lineWidth = 300;
        nonPlayingTimeDescription.lineHeight = 30;

        this.gameDescriptions.push(nonPlayingTimeDescription,nonPlayingTimeSeconds);
        this.addChild(nonPlayingTimeDescription,nonPlayingTimeSeconds);
    };

    p.setCountedTokensInfo = function(refundValue) {
        var coinImage = this.coinImage = alteastream.Assets.getImage(alteastream.Assets.images.metalCoin);
        coinImage.regX = coinImage.image.width*0.5;
        coinImage.regY = coinImage.image.height*0.5;
        coinImage.x = 960;
        coinImage.y = 290;

        textSrc = refundValue + " " + "\n"+this.parent.currency;
        var refundValueText = new createjs.Text(textSrc,"32px Impact","#000000").set({x:coinImage.x+2, y:coinImage.y-17, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        refundValueText.lineHeight = 35;

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText3);
        var countedTokensDescription = new createjs.Text(textSrc,"29px Roboto","#ffffff").set({x:960, y:412, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        countedTokensDescription.lineWidth = 440;
        countedTokensDescription.lineHeight = 30;

        this.gameDescriptions.push(countedTokensDescription,refundValueText);

        this.addChild(coinImage,countedTokensDescription,refundValueText);
    };

    p.setSunLogo = function() {
        var sunLogo = this.coinImage = alteastream.Assets.getImage(alteastream.Assets.images.sunLogo);
        sunLogo.regX = sunLogo.image.width*0.5;
        sunLogo.regY = sunLogo.image.height*0.5;
        sunLogo.x = 960;
        sunLogo.y = 360;
        this.addChild(sunLogo);
    };

    p.setMaxStakeInfo = function(maxStake) {
        //var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var yPos = 736;
        var textSrc = "Max Stake: ";
        var maxStakeDescription = new createjs.Text(textSrc,"29px Impact","#ffffff").set({x:1560, y:yPos, textAlign:"right", textBaseline:"middle", mouseEnabled:false});
        maxStakeDescription.lineWidth = 440;
        maxStakeDescription.lineHeight = 35;
        var stakeType = alteastream.AbstractScene.GAME_TYPE === "pearlpusher" ? " pearls" : " coins";
        var maxStakeText = new createjs.Text(maxStake + stakeType,"29px Impact","#e2d631").set({x:1560, y:yPos, textAlign:"left", textBaseline:"middle", mouseEnabled:false});
        this.gameDescriptions.push(maxStakeDescription,maxStakeText);

        this.addChild(maxStakeDescription,maxStakeText);
    };

    p.setBonusInfo = function(neededForBonus) {
        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText1);
        var bonusWheelDescription = new createjs.Text(textSrc,"29px Impact","#ffffff").set({x:960, y:460, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        bonusWheelDescription.lineWidth = 440;
        bonusWheelDescription.lineHeight = 40;
        this.gameDescriptions.push(bonusWheelDescription);

        var numOfCoinsNeededForBonus = new createjs.Text(neededForBonus,"29px Impact","#fcff00").set({x:830, y:460, textAlign:"center", textBaseline:"middle", mouseEnabled:false});
        this.gameDescriptions.push(numOfCoinsNeededForBonus);

        this.addChild(bonusWheelDescription, numOfCoinsNeededForBonus);
    };

    p.updatePrizes = function(isEur){
        for(var i = 0; i < this.chips.length; i++){
            this.chips[i].flipCoin(isEur);
        }
    };

    alteastream.CoinPusherInfo = createjs.promote(CoinPusherInfo,"Container");
})();
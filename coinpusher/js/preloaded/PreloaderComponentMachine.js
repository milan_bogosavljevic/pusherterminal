
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponentMachine = function(stage, canv) {
    this.initialize(stage, canv);
}
var p = PreloaderComponentMachine.prototype = new alteastream.PreloaderComponent();

    // static properties:
    // events:
    // public properties:
    // private properties:
    // constructor:
    p.PreloaderComponent_initialize = p.initialize;
    p.initialize = function(stage, canv) {
        this.PreloaderComponent_initialize(stage);

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRect(0, 0, canv.width, canv.height);
        shape.cache(0, 0, canv.width, canv.height);
        this.addChild(shape);
    };

    // static methods:
    // public methods:
    // private methods:
    p._getSaturation = function() {
        return (this.getProgress() - 1) * 100;
    };

alteastream.PreloaderComponentMachine = PreloaderComponentMachine;
})();
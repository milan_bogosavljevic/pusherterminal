

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusherAssets = function() {};
    var Assets = alteastream.Assets;

/*    CoinPusherAssets.videoManifest = [
        'assets/sprite_sheets/drum.mp4'
    ];*/

    CoinPusherAssets.count = 0;

    CoinPusherAssets.loadManifest = function(lang,gameCode){
        Assets.setImageLang(lang);
        //var language = Assets.getImageLang();
        //var localePath = typeImages+"/locale/" + language + "/";
        var noCache = "?ver="+Assets.VERSION;

        var typeImages = "assets/"+gameCode+"/images";
        var typeSounds = "assets/"+gameCode+"/sounds";
        //return [
        var manifest =  [
            // Images
            //{id:Assets.images.bounsWonBgImage,src:typeImages+"/bounsWonBgImage.png"+noCache},
            //{id:Assets.images.bounsWonBgImageRing,src:typeImages+"/bounsWonBgImageRing.png"+noCache},
            //{id:Assets.images.wheelBg,src:typeImages+"/wheelBg.png"+noCache},
            //{id:Assets.images.wheel_8_bonus_fields_highlight,src:typeImages+"/wheel_8_bonus_fields_highlight.png"+noCache},
            //{id:Assets.images.lampOn,src:typeImages+"/lampOn.png"+noCache},
            //{id:Assets.images.lampOff,src:typeImages+"/lampOff.png"+noCache},
            //{id:Assets.images.arrow,src:typeImages+"/arrow.png"+noCache},
            //{id:Assets.images.backgroundCB, src:typeImages+"/control_board_bg.png"+noCache},
            {id:Assets.images.preloadingPoster, src:typeImages+"/webrtc.png"+noCache},
            //{id:Assets.images.metalCoin, src:typeImages+"/metalCoin.png"+noCache},
            {id:Assets.images.coinWin, src:typeImages+"/coinWin.png"+noCache},
            {id:Assets.images.chip_glow, src:typeImages+"/chips/chip_glow.png"+noCache},
            {id:Assets.images.chip_1, src:typeImages+"/chips/chip_1.png"+noCache},
            {id:Assets.images.chip_2, src:typeImages+"/chips/chip_2.png"+noCache},
            {id:Assets.images.chip_3, src:typeImages+"/chips/chip_3.png"+noCache},
            {id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache},
            {id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache},
            {id:Assets.images.chip_6, src:typeImages+"/chips/chip_6.png"+noCache},
            {id:Assets.images.chip_7, src:typeImages+"/chips/chip_7.png"+noCache},
            {id:Assets.images.chip_8, src:typeImages+"/chips/chip_8.png"+noCache},
            {id:Assets.images.chip_9, src:typeImages+"/chips/chip_9.png"+noCache},
            {id:Assets.images.chip_10, src:typeImages+"/chips/chip_10.png"+noCache},
            {id:Assets.images.chip_11, src:typeImages+"/chips/chip_11.png"+noCache},
            {id:Assets.images.chip_12, src:typeImages+"/chips/chip_12.png"+noCache},
            {id:Assets.images.chip_0, src:typeImages+"/chips/chip_0.png"+noCache},
            {id:Assets.images.chip_1_back, src:typeImages+"/chips/chip_1_back.png"+noCache},
            {id:Assets.images.chip_2_back, src:typeImages+"/chips/chip_2_back.png"+noCache},
            {id:Assets.images.chip_3_back, src:typeImages+"/chips/chip_3_back.png"+noCache},
            {id:Assets.images.chip_4_back, src:typeImages+"/chips/chip_4_back.png"+noCache},
            {id:Assets.images.chip_5_back, src:typeImages+"/chips/chip_5_back.png"+noCache},
            {id:Assets.images.chip_6_back, src:typeImages+"/chips/chip_6_back.png"+noCache},
            {id:Assets.images.chip_7_back, src:typeImages+"/chips/chip_7_back.png"+noCache},
            {id:Assets.images.chip_8_back, src:typeImages+"/chips/chip_8_back.png"+noCache},
            {id:Assets.images.chip_9_back, src:typeImages+"/chips/chip_9_back.png"+noCache},
            {id:Assets.images.chip_10_back, src:typeImages+"/chips/chip_10_back.png"+noCache},
            {id:Assets.images.chip_11_back, src:typeImages+"/chips/chip_11_back.png"+noCache},
            {id:Assets.images.chip_12_back, src:typeImages+"/chips/chip_12_back.png"+noCache},
            {id:Assets.images.chip_0_back, src:typeImages+"/chips/chip_0_back.png"+noCache},
            //btns
            {id:Assets.images.btnIncSS, src:typeImages+"/buttons/count_btn_plus.png"+noCache},
            {id:Assets.images.btnDecSS, src:typeImages+"/buttons/count_btn_minus.png"+noCache},
            {id:Assets.images.btnFixedBet, src:typeImages+"/buttons/btnFixedBet.png"+noCache},
            {id:Assets.images.btnMediumSS, src:typeImages+"/buttons/middle_btn.png"+noCache},
            {id:Assets.images.btnArrowLeftSS, src:typeImages+"/buttons/left_btn.png"+noCache},
            {id:Assets.images.btnArrowRightSS, src:typeImages+"/buttons/right_btn.png"+noCache},
            {id:Assets.images.btnBetView, src:typeImages+"/buttons/switch_btn.png"+noCache},
            // common
            //{id:Assets.images.poster,src:"assets/common/images/poster.jpg"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/common/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/common/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/common/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/common/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.soundMachineOn,src:"assets/common/images/buttons/sound_icon_machine_default.png"+noCache},
            {id:Assets.images.soundMachineOff,src:"assets/common/images/buttons/sound_icon_machine_disabled.png"+noCache},
            {id:Assets.images.btnFullScreen_On,src:"assets/common/images/buttons/btnFullScreen_On.png"+noCache},
            {id:Assets.images.btnFullScreen_Off,src:"assets/common/images/buttons/btnFullScreen_Off.png"+noCache},
            {id:Assets.images.infoBackground, src:"assets/common/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoBackgroundNoBonus, src:"assets/common/images/infoBackgroundNoBonus.jpg"+noCache},
            {id:Assets.images.spinner, src:"assets/common/images/spinner.png"+noCache},
            {id:Assets.images.liveStreamConnecting, src:"assets/common/images/liveStreamConnecting.png"+noCache},
            {id:Assets.images.btnHiddenMenu, src:"assets/common/images/buttons/hiddenmenu_btn.png"+noCache},
            {id:Assets.images.controlsBg, src:"assets/common/images/controlsBg.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/common/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/common/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.metalCoin,src:"assets/common/images/metalCoin.png"+noCache},
            {id:Assets.images.sunLogo,src:"assets/common/images/sunLogo.png"+noCache},
            // sounds
            {id:Assets.sounds.shooter,src:typeSounds+"/shooter.ogg"+noCache},
            {id:Assets.sounds.shooter2,src:typeSounds+"/shooter2.ogg"+noCache},
            {id:Assets.sounds.insertCoin,src:typeSounds+"/coin_in.ogg"+noCache},
            {id:Assets.sounds.refund,src:typeSounds+"/refund.ogg"+noCache},
            {id:Assets.sounds.shooterStop,src:typeSounds+"/shootStop.ogg"+noCache},
            {id:Assets.sounds.btnUp,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDown,src:typeSounds+"/btnUp.ogg"+noCache},
            {id:Assets.sounds.btnDownDouble,src:typeSounds+"/btnDownDouble.ogg"+noCache},
            {id:Assets.sounds.bgMusicMachine,src:typeSounds+"/bgMusicMachine.ogg"+noCache},
            {id:Assets.sounds.win1,src:typeSounds+"/win1.ogg"+noCache},
            {id:Assets.sounds.error,src:typeSounds+"/error.ogg"+noCache},
            {id:Assets.sounds.info,src:typeSounds+"/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:typeSounds+"/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:typeSounds+"/exception.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:typeSounds+"/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.continuePlayingSpoken,src:typeSounds+"/do_we_continue_playing_01.ogg"+noCache},
            {id:Assets.sounds.byeSpoken,src:typeSounds+"/bye_01.ogg"+noCache},
            {id:Assets.sounds.youHaveWonSpoken,src:typeSounds+"/you_have_won_01.ogg"+noCache},
            {id:Assets.sounds.prizeDetect4,src:typeSounds+"/prize_detection4_01.ogg"+noCache},
            {id:Assets.sounds.shootCoinsSpoken,src:typeSounds+"/press_button_to_shoot_coins_01.ogg"+noCache},
            {id:Assets.sounds.keepGoingSpoken,src:typeSounds+"/keep_going_we_are_in_a_winners_mood_01.ogg"+noCache},
            {id:Assets.sounds.feelDizzySpoken,src:typeSounds+"/wauw_you_make_me_dizzy_01.ogg"+noCache},
            {id:Assets.sounds.reallyGoodSpoken,src:typeSounds+"/this_is_really_good_01.ogg"+noCache},
            {id:Assets.sounds.seeYouSpoken,src:typeSounds+"/see_you_later_01.ogg"+noCache},
            {id:Assets.sounds.prize_1,src:typeSounds+"/prize_1.ogg"+noCache},
            {id:Assets.sounds.prize_2,src:typeSounds+"/prize_2.ogg"+noCache},
            {id:Assets.sounds.prize_3,src:typeSounds+"/prize_3.ogg"+noCache},
            {id:Assets.sounds.prize_5,src:typeSounds+"/prize_5.ogg"+noCache},
            {id:Assets.sounds.prize_10,src:typeSounds+"/prize_10.ogg"+noCache},
            {id:Assets.sounds.prize_15,src:typeSounds+"/prize_15.ogg"+noCache},
            {id:Assets.sounds.prize_20,src:typeSounds+"/prize_20.ogg"+noCache},
            {id:Assets.sounds.prize_25,src:typeSounds+"/prize_25.ogg"+noCache},
            {id:Assets.sounds.prize_30,src:typeSounds+"/prize_30.ogg"+noCache},
            {id:Assets.sounds.prize_35,src:typeSounds+"/prize_35.ogg"+noCache},
            {id:Assets.sounds.prize_40,src:typeSounds+"/prize_40.ogg"+noCache},
            {id:Assets.sounds.prize_45,src:typeSounds+"/prize_45.ogg"+noCache},
            {id:Assets.sounds.prize_50,src:typeSounds+"/prize_50.ogg"+noCache},
            {id:Assets.sounds.prize_55,src:typeSounds+"/prize_55.ogg"+noCache},
            {id:Assets.sounds.prize_60,src:typeSounds+"/prize_60.ogg"+noCache},
            {id:Assets.sounds.prize_65,src:typeSounds+"/prize_65.ogg"+noCache},
            {id:Assets.sounds.prize_70,src:typeSounds+"/prize_70.ogg"+noCache},
            {id:Assets.sounds.prize_75,src:typeSounds+"/prize_75.ogg"+noCache},
            {id:Assets.sounds.prize_80,src:typeSounds+"/prize_80.ogg"+noCache},
            {id:Assets.sounds.prize_85,src:typeSounds+"/prize_85.ogg"+noCache},
            {id:Assets.sounds.prize_90,src:typeSounds+"/prize_90.ogg"+noCache},
            {id:Assets.sounds.prize_95,src:typeSounds+"/prize_95.ogg"+noCache},
            {id:Assets.sounds.prize_100,src:typeSounds+"/prize_100.ogg"+noCache},
            {id:Assets.sounds.bonusInc,src:typeSounds+"/extra_dot.ogg"+noCache},
            //{id:Assets.sounds.bonusSpinSound,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_1,src:typeSounds+"/super_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusWinnerSpoken_2,src:typeSounds+"/yes_we_have_a_bonus_winner.ogg"+noCache},
            {id:Assets.sounds.bonusSpinLoop,src:typeSounds+"/bonus_game_animation.ogg"+noCache},
            {id:Assets.sounds.bonusWord,src:typeSounds+"/bonusWord.ogg"+noCache}//my cut, does not exist

            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"+noCache},
             */

            //images big
            //{id:Assets.images.mainBackground,src:typeImages+"/mainBackground.jpg"+noCache},//1

            //sound
        ];

        var typeMethod = gameCode + "Assets";
        var typeAssets = CoinPusherAssets[typeMethod](typeImages,typeSounds);
        manifest = manifest.concat(typeAssets);
        return manifest;
    };

    CoinPusherAssets.ticketcircusAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_4, src:typeImages+"/chips/chip_4.png"+noCache}
        ]
    };

    CoinPusherAssets.pearlpusherAssets = function(typeImages,typeSounds) {
        return [
            //{id:Assets.images.chip_5, src:typeImages+"/chips/chip_5.png"+noCache}
        ]
    };

    CoinPusherAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/Technology-Bold.ttf",
            "css/impact.ttf",
            "css/Lato-Bold.ttf"
        ];
    };


    /*CoinPusherAssets.atlasImage = {};
    var aI = CoinPusherAssets.atlasImage = {};

    aI[Assets.atlasImage.ss] = "ss";*/

    CoinPusherAssets.texts = {};
    var t = CoinPusherAssets.texts["en"] = {};
    t[Assets.texts.stake] = "BET";
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.crd] = "Cash:";
    t[Assets.texts.bet] = "Bet:";
    t[Assets.texts.infoText1] = "With         coins/marbles fired bonus wheel feature will start and reward you with random prize.\nWhen bonus wheel feature is active playing buttons are disabled.";
    t[Assets.texts.infoText2] = "Use left/right arrows to move shooter.\nUse bet buttons to set your bet amount.\nFire number of coins/marbles set by bet using single/burst button.\nWhile dispensing coins/marbles, playing buttons are disabled.";
    t[Assets.texts.infoText3] = "COINS WON\nWith every metal coin\npushed over the edge\n you also win!  ";
    t[Assets.texts.infoText4] = "of non playing time is allowed\nAfter that your session will be terminated";

    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";

    //t = CoinPusherAssets.texts["sr"] = {};

    alteastream.CoinPusherAssets = CoinPusherAssets;
})();


// namespace:
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var CoinPusherControlBoard = function() {
        this.AbstractControlBoard_constructor();
        this.initialize_CoinPusherControlBoard();
    };
    var p = createjs.extend(CoinPusherControlBoard, alteastream.AbstractControlBoard);

    // static properties:
    // events:
    // private properties:
    p._color_highlighted_text = "";
    p._color_disabled_text = "";
    p._color_enabled_text = "";
    // public properties:
    p.soundMachineToggled = false;

    // constructor:
    p.initialize_CoinPusherControlBoard = function() {
        this._color_highlighted_text = "green";
        this._color_disabled_text = "#0c1828";
        this._color_enabled_text = "white";
        this.textColorCoins = "#ffd500";
        this.textColorMoney = "#00ff00";

        this._setShooterComponent();
        this._setStakeComponent();
        //new stakes
        this._setFixedStakesComponent();
        this._setCreditsComponent();
        this._setBetViewComponent();
        this._setTopButtons();
        //this._setInfoComponent();
    };
    // private methods
    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline, mouseEnabled:false});
        this.addChild(textInstance);
    };

// private methods:
    p._setShooterComponent = function(){
        if(localStorage.getItem("isMobile") === "not"){
            var bg = this._shootComponentBg = alteastream.Assets.getImage(alteastream.Assets.images.controlsBg);
            bg.regX = bg.image.width * 0.5;
            bg.regY = bg.image.height * 0.5;
            bg.x = 960;
            bg.y = 935;
            this.addChild(bg);
        }

        var textBurst = this._buttonYesText = new createjs.Text("BURST","bold 28px Roboto","#fcfcfc");
        var textSingle = this._buttonYesText = new createjs.Text("SINGLE","bold 28px Roboto","#fcfcfc");

        var btnSend = this.btnSend = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,textSingle);
        btnSend.centerText(0,30);

        btnSend.normalTextColor = "#fcfcfc";
        btnSend.higlightedTextColor = "#fcfcfc";
        btnSend.disabledTextColor = "#6d6767";

        btnSend.addTextPreset("burst", textBurst , 0 , 63);
        btnSend.addTextPreset("single", textSingle , 0 , 63);
        btnSend.x = 960;
        btnSend.y = 935;
        btnSend.id = 10;
        this.addChild(btnSend);

        var btnShooterLeft = this.btnShooterLeft = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        btnShooterLeft.x = btnSend.x - 198 - (btnShooterLeft._singleImgWidth * 0.5);
        btnShooterLeft.y = btnSend.y - (btnShooterLeft._height * 0.5);
        btnShooterLeft.id = 7;//left
        this.addChild(btnShooterLeft);

        var btnShooterRight = this.btnShooterRight = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowRightSS),3);
        btnShooterRight.x = btnSend.x + 198 - (btnShooterRight._singleImgWidth * 0.5);
        btnShooterRight.y = btnShooterLeft.y;
        btnShooterRight.id = 8;//right
        this.addChild(btnShooterRight);
    };

    p._setStakeComponent = function(){
        var whitish = "#e4e4e4";
        var yellow = this.textColorCoins;

        //new stakes
        var xCorr = 87;

        var background = this.stakeBackground = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 272, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 272, 67);
        background.x = 1707 - xCorr;
        background.y = 987;
        this.addChild(background);

        var btnDecStake = this.btnDecStake = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnDecSS),3);
        btnDecStake.x = 1712- xCorr;
        btnDecStake.y = 992;
        this.addChild(btnDecStake);

        var btnIncStake = this.btnIncStake = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnIncSS),3);
        btnIncStake.x = 1828- xCorr;
        btnIncStake.y = 992;
        this.addChild(btnIncStake);

        this._createText("_textBet","BET","18px Lato",whitish,{x:1799- xCorr,y:1003,textAlign:"center", textBaseline:"middle"});
        this._createText("_textStakeAmount","0","18px Lato",yellow,{x:1799- xCorr,y:1022,textAlign:"center", textBaseline:"middle"});
    };

    p._setFixedStakesComponent = function(){
        this.fixedStakesPanel = new alteastream.FixedStakes();
        this.addChild(this.fixedStakesPanel);

        var btnFixedStakes = this.btnFixedStakes = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnFixedBet),3);
        btnFixedStakes.x = 1829;//1828
        btnFixedStakes.y = 992;//992
        this.addChild(btnFixedStakes);
    };

    p._setCollectComponent = function(){
        var btnCollect = this.btnCollect = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMedium2SS),3);
        btnCollect.x = 1391;
        btnCollect.y = 952;
        this.addChild(btnCollect);
    };

    p._setCreditsComponent = function(){
        var font = "16px Lato";
        var white = "#fff";
        var yellow = this.textColorCoins;
        var alignLeft = "left";
        var yPos = 1022;

        var background = this.balanceBackground = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 288, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 288, 67);
        background.x = 30;
        background.y = 987;
        this.addChild(background);

        var divider = this._divider = new createjs.Text("|",font,"#ffffff").set({textAlign:"center", textBaseline:"middle", x:154, y:1019});
        this.addChild(divider);

        //this._createText("_textCurrency","CURRENCY: ",font,white,{x:124,y:yPos,textAlign:alignLeft});
        this._createText("_textCurrencyValue","N/A",font,yellow,{x:122,y:yPos,textAlign:"center",textBaseline:"middle"});
        this._createText("_textCrd","BALANCE: ",font,white,{x:163,y:yPos,textAlign:alignLeft,textBaseline:"middle"});
        this._createText("_textCrdAmount","00.00",font,yellow,{x:245,y:yPos,textAlign:alignLeft,textBaseline:"middle"});
    };

    p._setBetViewComponent = function(){
        var btnBetView = this.btnBetView = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBetView),3);
        btnBetView.x = 35;
        btnBetView.y = 992;
        this.addChild(btnBetView);
    };

    p._setTopButtons = function() {
        var assets = alteastream.Assets;

/*        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var width = btnQuit.width;
        var spacing = (width/2)-10;*/

/*        var btnMachineSound = this.btnMachineSound = alteastream.Assets.getImage(assets.images.soundMachineOn);
        var that = this;
        btnMachineSound.on('click', function() {
            that.soundMachineBtnHandler();
        });

        btnMachineSound.x = btnQuit.x - width - spacing;

        assets.setMute(this.soundMachineToggled);*/

/*        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOff);
        this.soundToggled = true;
        var scene = alteastream.CoinPusher.getInstance();

        btnSound.x = btnMachineSound.x - width - spacing;*/

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnInfoSS),3);
        btnInfo.highlightedTextColor  = "#fff";

        //btnInfo.x = btnSound.x - width - spacing;

/*        var btnFs = scene.btnFs = alteastream.Assets.getImage(assets.images.btnFullScreen_On);
        var method;

        btnFs.x = btnInfo.x - width - spacing;

        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", scene[method]);*/

        var hiddenMenuContainer = this.hiddenMenuContainer = new createjs.Container();
        hiddenMenuContainer.x = 1845;
        hiddenMenuContainer.y = 30;

        this.hiddenMenuContainer.addChild(/*btnQuit, btnSound, btnFs, btnMachineSound, */btnInfo);
        this.addChild(this.hiddenMenuContainer);
    };

    p._showHiddenMenu = function(show) {
        this.doShowMenu(show);
        if(this.fixedStakesPanel._active){
            this.fixedStakesPanel.show(false);
        }
        if(this.menuIsShown === true && this.controlShown===true){
            this._moveShooterLeftRightAndBtnSendComponents(true);
        }else if(!this.menuIsShown && this.controlShown===false)
            this._moveShooterLeftRightAndBtnSendComponents(false);
    };

    p._moveShooterLeftRightAndBtnSendComponents = function(){};
    // static methods:
    // public methods:
    p.setBetAmount = function(amount) {
        this._textStakeAmount.text = amount;
    };

    p.hideControlls = function(hide) {
        this._shootComponentBg.visible = this.btnSend.visible = this.btnShooterLeft.visible = this.btnShooterRight.visible = !hide;
    };

    p.setBetLabelToMax = function(setToMax) {
        this._textBet.text = setToMax === true ? "MAX" : "BET";
        this._textBet.color = setToMax === true ? "#f9ff48" : "#fff";
    };

/*    p.checkIfHiddenMenuIsActive = function() {
        if(this.menuIsShown || this.fixedStakesPanel._active){
            this._showHiddenMenu(false);
        }
    };*/

/*    p.doShowMenu = function(show){
        this.menuIsShown = show;
    };*/

/*    p.soundMachineBtnHandler = function(){
        var video = document.getElementById("videoOutput");
        var assets = alteastream.Assets;
        this.soundMachineToggled = !this.soundMachineToggled;
        this.btnMachineSound.image = this.soundMachineToggled === true?
            assets.getImage(assets.images.soundMachineOff).image:
            assets.getImage(assets.images.soundMachineOn).image;

        video.muted = this.soundMachineToggled;
        assets.setMute(this.soundMachineToggled);
        this.checkIfHiddenMenuIsActive();

        window.localStorage.setItem('soundMachineIsToggled', String(this.soundMachineToggled));
    };*/

/*    p._superSoundBtnHandler = p.soundBtnHandler;//todo mozda prebaciit soundBtnHandler iz abs
    p.soundBtnHandler = function() {
        this._superSoundBtnHandler();
        this.checkIfHiddenMenuIsActive();

        window.localStorage.setItem('gameplaySoundIsToggled', String(this.soundToggled));
    };*/

/*    p.restoreMachineSoundState = function (){
        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            this.soundMachineToggled = soundMachineIsToggled === "false";
            this.soundMachineBtnHandler();
        }
    };*/

/*    p.restoreMusicSoundState = function (){
        var gameplaySoundIsToggled = window.localStorage.getItem("gameplaySoundIsToggled");
        if(gameplaySoundIsToggled !== null) {
            this.soundToggled = gameplaySoundIsToggled === "false";
            this.soundBtnHandler();
        }
    };*/

    alteastream.CoinPusherControlBoard = createjs.promote(CoinPusherControlBoard,"AbstractControlBoard");
})();
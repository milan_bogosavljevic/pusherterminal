

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusherMobile = function() {
        this.CoinPusher_constructor();
        this.initialize_CoinPusherMobile();
    };
    var p = createjs.extend(CoinPusherMobile, alteastream.CoinPusher);

    // static properties:
    // events:
    // public properties:
    // private properties:

    var _instance = null;

    // constructor:
    p.initialize_CoinPusherMobile = function() {
        _instance = this;

        this._adjustControlBoard();
        //this._adjustCoinsCounter();
        this._adjustPrizeCounter();
        this._adjustQueueInfo();
        //this._adjustFixedStakes();
        //this.showFSClock(true); maybe after audit, now there is a clock from camera
    };
    // static methods:
    CoinPusherMobile.getInstance = function(){
        return _instance;
    };
    // private methods
    p._adjustControlBoard = function() {
        var cBoard = this.controlBoard;
        cBoard.balanceBackground.uncache();
        cBoard.balanceBackground.graphics.clear();
        //cBoard.balanceBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 350, 67, 10);
        cBoard.balanceBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 300, 67, 10);
        cBoard.balanceBackground.alpha = 0.7;
        cBoard.balanceBackground.cache(0, 0, 300, 67);
        cBoard.balanceBackground.x = 30;
        cBoard.balanceBackground.y = 443;
        // verzija ako hocemo da se ne instancira keyboard listener za mobilni
        // kreiranje objekta twinComponent je potrebno posto u _shooterMoveBtnHandler i u _shooterUpBtnHandler postoji provera ,
        // a objekat ne postoji posto nije instancirana Controller klasa u mobile verziji
        //cBoard.twinComponent = {active:false};
/*        cBoard.removeChild(cBoard._textMoveLeft);
        cBoard.removeChild(cBoard._textShooter);
        cBoard.removeChild(cBoard._textMoveRight);
        cBoard.removeChild(cBoard._textDecStake);
        cBoard.removeChild(cBoard._textIncStake);
        cBoard.removeChild(cBoard._textSwitchCurrency);
        cBoard.removeChild(cBoard._textInfoToggle);*/
        //cBoard.removeChild(cBoard.btnFs);
        cBoard.removeChild(cBoard.upperOverlay);
        cBoard.hiddenMenuContainer.removeChild(cBoard.btnFs);

        var leftRightArrowsBackground = cBoard.leftRightArrowsBackground = alteastream.Assets.getImage(alteastream.Assets.images.arrowButtonsBackground);
        leftRightArrowsBackground.x = 30;
        leftRightArrowsBackground.y = 185;
        cBoard.addChildAt(leftRightArrowsBackground,0);

        cBoard.btnShooterLeft.x = 75 - (cBoard.btnShooterLeft._singleImgWidth*0.5);
        cBoard.btnShooterLeft.y = 331 - (cBoard.btnShooterLeft._height*0.5);
        cBoard.btnShooterRight.x = cBoard.btnShooterLeft.x;
        cBoard.btnShooterRight.y = 229 - (cBoard.btnShooterRight._height*0.5);

        var font = "17px Lato";
        cBoard._textCurrencyValue.font = font;
        cBoard._textCurrencyValue.x = 124;
        cBoard._textCurrencyValue.y = 477;
        cBoard._divider.font = font;
        cBoard._divider.x = 157;
        cBoard._divider.y = 476;
        cBoard._textCrd.font = font;
        cBoard._textCrd.x = 168;
        cBoard._textCrd.y = 477;
        cBoard._textCrd.skewX = 0;
        cBoard._textCrdAmount.font = font;
        cBoard._textCrdAmount.x = cBoard._textCrd.x + 90;
        //cBoard._textCrdAmount.y = cBoard._textCurrency.y;
        cBoard._textCrdAmount.y = 477;
        cBoard._textCrdAmount.skewX = 0;

        cBoard.btnSend.x = 855;
        cBoard.btnSend.y = 281;
        cBoard.btnSend.centerText(0,45);
        cBoard.btnSend.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.single.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.burst.text.font = "bold 20px Roboto";
        cBoard.btnSend._presetFonts.single.deltaY = 45;
        cBoard.btnSend._presetFonts.burst.deltaY = 45;

        var background = cBoard.stakeBackground;
        background.uncache();
        background.graphics.clear();
        background.graphics.beginFill("#000000").drawRoundRect(0, 0, 266, 67, 10);
        background.alpha = 0.7;
        background.cache(0, 0, 266, 67);
        var xCorr = 82;
        cBoard.stakeBackground.x = 747-xCorr;
        cBoard.stakeBackground.y = 443;

        cBoard.btnDecStake.x = cBoard.stakeBackground.x + 5;
        cBoard.btnDecStake.y = cBoard.stakeBackground.y + 4;
        cBoard.btnIncStake.x = cBoard.stakeBackground.x + 122;
        cBoard.btnIncStake.y = cBoard.stakeBackground.y + 4;
        cBoard._textStakeAmount.font = font;
        cBoard._textStakeAmount.x = cBoard.stakeBackground.x + 92;
        cBoard._textStakeAmount.y = 477;
        cBoard._textStakeAmount.skewX = 0;

        cBoard._textBet.font = font;
        cBoard._textBet.skewX = 0;
        cBoard._textBet.x = cBoard.stakeBackground.x + 92;
        cBoard._textBet.y = cBoard.stakeBackground.y + 12;

        var menuButton = cBoard._hiddenMenuButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnHiddenMenu),3);
        menuButton.x = 864;
        menuButton.y = 30;
        cBoard.addChild(menuButton);

        var menuButtonClickHandler = function () {
            cBoard._showHiddenMenu(!cBoard.menuIsShown);
        };
        menuButton.setClickHandler(menuButtonClickHandler);

/*        menuButton.addEventListener("click", function () {
            cBoard._showHiddenMenu(!cBoard.menuIsShown);
        });*/

        this.giveUpButton.x = 886;//giveUpButton
        this.giveUpButton.y = this.giveUpButton.y = is.ios() === true ? 100 : 26;

        if(cBoard.btnBetView){
            var btnBetView = cBoard.btnBetView;
            btnBetView.x = 35;
            btnBetView.y = 449;
        }

        cBoard.hiddenMenuContainer.x = 960;
        cBoard.hiddenMenuContainer.y = 136;

        var width = cBoard.btnQuit.width;
        var height = cBoard.btnQuit.height;
        var hitAreaReduce = 30;

        cBoard.btnMachineSound.y = cBoard.btnQuit.y + height;
        cBoard.btnSound.y = cBoard.btnMachineSound.y + height;
        cBoard.btnInfo.y = cBoard.btnSound.y + height;
        cBoard.btnInfo.originalYPosition = cBoard.btnInfo.y;
        cBoard.btnMachineSound.x = cBoard.btnSound.x = cBoard.btnInfo.x = cBoard.btnQuit.x;

        var hitArea = new createjs.Shape();
        hitArea.graphics.beginFill("#fff").drawRect(0, hitAreaReduce * 0.5, width, height-hitAreaReduce);

        cBoard.btnQuit.hitArea = hitArea;
        cBoard.btnMachineSound.hitArea = hitArea;
        cBoard.btnSound.hitArea = hitArea;
        cBoard.btnInfo.hitArea = hitArea;

        cBoard._moveShooterLeftRightAndBtnSendComponents = function(hiddenMenuIsShown){
            cBoard.controlShown = hiddenMenuIsShown;
            var xPosCorrector;
            if(cBoard.controlShown ===true){
                xPosCorrector = -120;
                cBoard.controlShown =false;
            }else{
                xPosCorrector = 120;
                cBoard.controlShown =true;
            }

            createjs.Tween.get(cBoard.leftRightArrowsBackground).to({x:cBoard.leftRightArrowsBackground.x + xPosCorrector},200,createjs.Ease.linear);
            createjs.Tween.get(cBoard.btnShooterLeft).to({x:cBoard.btnShooterLeft.x + xPosCorrector},200,createjs.Ease.linear);
            createjs.Tween.get(cBoard.btnShooterRight).to({x:cBoard.btnShooterRight.x + xPosCorrector},200,createjs.Ease.linear);
            xPosCorrector *= -2;
            createjs.Tween.get(cBoard.btnSend).to({x:cBoard.btnSend.x + xPosCorrector},200,createjs.Ease.linear);
        };

        cBoard.doShowMenu = function(show){
            cBoard._hiddenMenuButton.setDisabled(true);
            var xPos = show === true ? cBoard._hiddenMenuButton.x : alteastream.AbstractScene.GAME_WIDTH;
            cBoard.menuIsShown = show;
            createjs.Tween.get(cBoard.hiddenMenuContainer).to({x:xPos},200,createjs.Ease.linear).call(function () {
                cBoard._hiddenMenuButton.setDisabled(false);
            });
            var scene = alteastream.CoinPusher.getInstance();
            if(scene._info.visible === true && show === false){
                scene.onInfo();
            }
        }
    };

    p._setInfo = function() {
        this._info.setInfo(this.payoutManager,this.hasBonus, this.betManager.getMultiplier());
        this._info.setMaxStakeInfo(this.maxStake);
        this._info.setNonPlayingTimeInfo(this.controlIdleTime);

        this._adjustInfo();
        if(this.hasBonus === true){
            this._info.setBonusInfo(this.powerUp.neededForBonus);
            var bonusDescription = this._info.gameDescriptions[1];
            bonusDescription.font = "14px Impact";
            bonusDescription.x = 480;
            bonusDescription.y = 230;
            bonusDescription.lineWidth = 220;
            bonusDescription.lineHeight = 20;

            var numberNeededForBonus = this._info.gameDescriptions[2];
            numberNeededForBonus.font = "14px Impact";
            numberNeededForBonus.x = 417;
            numberNeededForBonus.y = 230;
        }
        var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
        //if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher"){
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && refundValue > 0){
            //var refundValue = (Math.round(((this.payoutManager.getRefundValue() * 100)/100) * 1e12) / 1e12).toFixed(3);
            //var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
            this._info.setCountedTokensInfo(refundValue);//param metal coinvalue + slicica

            this._info.coinImage.x = 480;
            this._info.coinImage.y = 140;

            var countedTokensDescription = this._info.gameDescriptions[5];
            countedTokensDescription.font = "14px Roboto";
            countedTokensDescription.x = 480;
            countedTokensDescription.y = 199;
            countedTokensDescription.lineWidth = 135;
            countedTokensDescription.lineHeight = 18;

            var refundValueText = this._info.gameDescriptions[6];
            refundValueText.font = "18px Impact";
            refundValueText.lineHeight = 20;
            refundValueText.x = 480;
            refundValueText.y = 132;
        }else{
            this._info.setSunLogo();
            this._info.coinImage.x = 480;
            this._info.coinImage.y = 185;
        }
    };

    p._adjustInfo = function() {
        var numberOfChipsInPaytable = this._info.chips.length;
        var maxChipsInColumn = 4;
        var yPosMultiplier = 0;
        var chipsY = 83;
        var chipsYSpacing = 70;
        var chipsXSpacing = 0;

        var startXPos = numberOfChipsInPaytable > maxChipsInColumn ? 153 : 188;
        for(var i = 0; i < numberOfChipsInPaytable; i++){
            var chip = this._info.chips[i];
            chip.scale = chip.backScaleXForAnimation = 0.68;

            if(yPosMultiplier === maxChipsInColumn){
                chipsXSpacing = 70;
                yPosMultiplier = 0;
            }

            yPosMultiplier++;

            chip.x = startXPos + chipsXSpacing;
            chip.y = chipsY + (chipsYSpacing * yPosMultiplier);

            chip._prizeAmount.font = "bold 22px HurmeGeometricSans3";
        }

        var gameplayDescription = this._info.gameDescriptions[0];
        gameplayDescription.font = "14px Roboto";
        gameplayDescription.x = 770;
        gameplayDescription.y = 131;
        gameplayDescription.lineWidth = 135;
        gameplayDescription.lineHeight = 20;

        var maxStakeLabel = this._info.gameDescriptions[1];
        var maxStakeNumber = this._info.gameDescriptions[2];

        maxStakeLabel.font = "14px Impact";
        maxStakeLabel.x = 773;
        maxStakeLabel.y = 370;

        maxStakeNumber.font = "14px Impact";
        maxStakeNumber.x = 780;
        maxStakeNumber.y = 370;

        var nonPlayingTimeDescription = this._info.gameDescriptions[3];
        var nonPlayingTimeSeconds = this._info.gameDescriptions[4];
        nonPlayingTimeSeconds.lineHeight = 15;

        nonPlayingTimeDescription.font = "14px Roboto";
        nonPlayingTimeDescription.x = 480;
        nonPlayingTimeDescription.y = 389;
        nonPlayingTimeDescription.lineWidth = 150;
        nonPlayingTimeDescription.lineHeight = 15;

        nonPlayingTimeSeconds.font = "18px Impact";
        nonPlayingTimeSeconds.x = 480;
        nonPlayingTimeSeconds.y = 322;
        nonPlayingTimeSeconds.lineHeight = 20;
    };

    p.super_setBonusApplet = p._setBonusApplet;
    p._setBonusApplet = function(options) {
        this.super_setBonusApplet(options);
        this._adjustBonusWheel();
    };

    p._adjustBonusWheel = function() {
        var wheel = this.bonusWheel;
        var fieldsNumber = wheel.numChildren;
        var length = wheel.FIELD_WINS.length;
        for(var i = 1; i < fieldsNumber; i++){// first child is background
            var field = wheel.getChildAt(i);
            field.outerRadiusShrink = field.outerRadiusShrink/2;
            field.innerRadiusShrink = field.innerRadiusShrink/2;
            field.startY = field.startY/2;
            field.sWidth = (field.WIDTH_SUM/length)/2;
            field.sHeight = field.sHeight/2;
            field.itemValue.font = "25px Roboto Black";

            //field.colorSlice(field.defaultSliceColor);

            field.regX = field.sWidth * 0.5; // todo ??????????????????
            field.regY = field.sHeight;
            field.itemValue.x = field.regX;
            field.itemValue.y = field.count % 2===0?field.outerRadiusShrink:field.innerRadiusShrink;
        }

        var powerUpWheel = this.powerUp;
        var radius = 183;
        var lamps = powerUpWheel.numChildren;
        var startAngle = -90 * Math.PI / 180;
        var oneField = powerUpWheel.oneField;
        for(var j = 0; j < lamps; j++){
            var lamp = powerUpWheel.getChildAt(j);
            lamp.x =  Math.cos(startAngle)*radius;
            lamp.y =  Math.sin(startAngle)*radius;
            startAngle += oneField * Math.PI / 180;
        }

        this.bonusWonText.font = "48px Lato";
        this.bonusWonText.x = 33;
        this.bonusWonText.y = 5;

        this.marker.y = -210;

        this.bonusContainer.x = this.bonusContainer.startX = 125;
        this.bonusContainer.y = this.bonusContainer.startY = 130;
        this.bonusContainer.xPosForAnimation = 200;
        this.bonusContainer.yPosForAnimation = 200;
        this.bonusContainer.xPosForInfo = 480;
        this.bonusContainer.yPosForInfo = 115;
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(0.9);
        coin.setVR(-10,10);
        coin.setVX(-1,1);
        coin.setVY(-16,-18);
        coin.animate();
    }

    p._adjustCoinsCounter = function(){
        //if(alteastream.AbstractScene.GAME_TYPE === "pearlpusher")
            //return;
        if(this.coinsCounter){
            var bg = this.coinsCounter.getChildAt(0);
            bg.uncache();
            bg.graphics.clear();
            bg.graphics.beginFill("#000000").drawRoundRect(0, 0, 160, 67, 10);
            bg.alpha = 0.7;
            bg.cache(0, 0, 160, 67);
            var xCorr = 60;
            this.coinsCounter.x = 395-xCorr;
            this.coinsCounter.y = 443;
            this.coinsCounter.pivot = bg.bitmapCache.width*0.5;
            var font = "17px Lato";
            this.coinsChargedLabel.font = font;
            this.coinsNumber.font = font;
            this.eurNumber.font = font;

            this.coinsChargedLabel.x = 10;
            this.coinsNumber.x = this.eurNumber.x = 110;

            this.coinsChargedLabel.y = this.coinsNumber.y = this.eurNumber.y = 34;
        }
    };

    p._adjustPrizeCounter = function() {
        var bg = this.prizeCounter.getChildAt(0);
        bg.uncache();
        bg.graphics.clear();
        bg.graphics.beginFill("#000000").drawRoundRect(0, 0, 160, 67, 10);
        bg.alpha = 0.7;
        bg.cache(0, 0, 160, 67);
        var xCorr = 72;
        this.prizeCounter.x = 572-xCorr;
        this.prizeCounter.y = 443;
        this.prizeCounter.pivot = bg.bitmapCache.width*0.5;
        var font = "17px Lato";
        this.prizeWinLabel.font = font;
        this.prizeNumber.font = font;
        this.prizeEurNumber.font = font;

        this.prizeWinLabel.x = 12;
        this.prizeNumber.x = this.prizeEurNumber.x = 107;

        this.prizeWinLabel.y = this.prizeNumber.y = this.prizeEurNumber.y = 34;
    };

    // show machine name , comment method
    p._adjustQueueInfo = function(){
        this.queueInfoContainer.x = 30;
        this.queueInfoContainer.y = 30;
        this.queueInfoLabel.font = this.queueInfoText.font = "14px Lato";
        this.queueInfoLabel.x = this.queueInfoText.x = this.queueInfoBackground.x + 98;
        this.queueInfoLabel.y = this.queueInfoText.y = this.queueInfoBackground.y + 24;
    };

    p._adjustFixedStakes = function(){
        var cBoard = this.controlBoard;

        cBoard.btnFixedStakes.x = 869;
        cBoard.btnFixedStakes.y = cBoard.stakeBackground.y + 4;

        cBoard.fixedStakesPanel.adjustMobile();
    };

    // public methods
    p.super_showGame = p.showGame;
    p.showGame = function(bool){
        this.super_showGame(bool);
        if(bool === true){
            this.controlBoard._hiddenMenuButton.visible = true;
        }
    };

    p.superSetCoinCounter = p.setCoinCounter;
    p.setCoinCounter = function () {
        this.superSetCoinCounter();
        this._adjustCoinsCounter();
    };

    p.onInfo = function(){
        this._info.visible = !this._info.visible;
        this.controlBoard._hiddenMenuButton.visible =
            this.controlBoard.btnQuit.visible =
                this.controlBoard.btnMachineSound.visible =
                    this.controlBoard.btnSound.visible = !this._info.visible;

        var btnImage = this._info.visible === true ? "btnQuitSS" : "btnInfoSS";
        this.controlBoard.btnInfo.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
        this.controlBoard.btnInfo.x = this._info.visible === true ? -1 : 0;
        this.controlBoard.btnInfo.y = this._info.visible === true ? -114 : this.controlBoard.btnInfo.originalYPosition;

        if(this._info.visible === false){
            this.controlBoard.checkIfHiddenMenuIsActive();
        }
        if(this.hasBonus === true){
            this._setBonuswheelPositionAndForInfo(this._info.visible);
        }
        this.queueInfoContainer.visible = !this._info.visible;
        alteastream.Assets.playSound("btnDownDouble");
    };

    p.super_setFixedStakes = p.setFixedStakes;
    p.setFixedStakes = function(){
        this.super_setFixedStakes();
        this._adjustFixedStakes();
    };

    //p._activateKeyboardListeners = function(){}; // verzija ako hocemo da se ne instancira keyboard listener za mobilni

    alteastream.CoinPusherMobile = createjs.promote(CoinPusherMobile,"CoinPusher");
})();
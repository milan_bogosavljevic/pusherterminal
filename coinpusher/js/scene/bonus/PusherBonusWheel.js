"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var PusherBonusWheel = function(game,bonuses){
        this.ABSWheel_constructor(game,bonuses);
        this.initialize_PusherBonusWheel();
    };
    var p = createjs.extend(PusherBonusWheel, alteastream.ABSWheel);

    // public properties
    p.wheelBackground = null;
    // constructor
    p.initialize_PusherBonusWheel = function(){
        this._local_activate();// local ver
        var bg = this.wheelBackground = alteastream.Assets.getImage(alteastream.Assets.images.wheelBg);
        this.addChild(bg);
        bg.regX = bg.image.width*0.5;
        bg.regY = bg.image.height*0.5;
        // draw field ver
/*        var lowFieldColor = "#f7ec13";
        var fieldColors = [
            "#f8991d",
            "#109a48",
            "#4858a7",
            "#85449a",
            "#ed2024",
            "#f8991d",
            "#109a48",
            "#4858a7",
            "#85449a",
            "#ed2024"
        ];*/
        //this.createWheel("CoinBonusField",lowFieldColor,fieldColors);// draw field ver
        this.createWheel("CoinBonusField", "#000000", "#fff");
    };
    // private methods
    p._isMobile = function() {
        return localStorage.getItem("isMobile");
    };

    p._getImageSrc = function(gameCode, pathAdd, bgImage) {
        return "/gameplay/assets/"+gameCode+pathAdd+"/images/"+ bgImage +".png";
    };

    p._local_activate = function() {
        this._isMobile = function() {
            //return "yes";
            return "not";
        };

        this._getImageSrc = function(gameCode, pathAdd, bgImage) {
            return "../coinpusher/assets/"+gameCode+pathAdd+"/images/"+ bgImage +".png";
        };
    };
    // public methods
    p.setSkin = function(callback) {
        var _this = this;
        var gameCode = starter.getGameCode();
        var isMobile = this._isMobile();
        var pathAdd = isMobile === "not" ? "" : "/mobile";
        var image = new Image();
        var bgImage = "wheel_" + this.bonusesReceived.length * 2 + "_bonus_fields_back";
        image.src = this._getImageSrc(gameCode, pathAdd, bgImage);
        image.onload = function(){
            _this.wheelBackground.image = image;
            callback();
        }
    };

    p.updatePrizes = function(isEur, multiplier) {
        for(var i = 0; i < this.fields.length; i++){
            var field = this.fields[i];
            if(isEur === true){
                field.itemValue.text = (Number(field.itemValue.text) * multiplier).toFixed(2);
            }else{
                field.itemValue.text = Number(field.itemValue.text)*(100 * multiplier);
            }
        }
    };

    alteastream.PusherBonusWheel = createjs.promote(PusherBonusWheel,"ABSWheel");
})();

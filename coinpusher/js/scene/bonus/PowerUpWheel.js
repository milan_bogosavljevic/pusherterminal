"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var PowerUpWheel = function(game){
        this.Container_constructor();
        this.initialize_PowerUpWheel(game);
    };
    var p = createjs.extend(PowerUpWheel, createjs.Container);
    // constructor
    p.initialize_PowerUpWheel = function(game){
        this.game = game;
        this.count = 0;
        this.FULL_CIRCLE = 360;
        this._canFill = true;
        this.mouseEnabled = false;
        this.rotTime = 1;
    };
    // private methods
    p._reset = function(){
        for(var i = 0,j=this.numChildren;i<j;i++){
            this.getChildAt(i).image = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]).image
        }
    };

    p._markPair = function(bool){
        var leftover = bool ===true?2:1;
        for(var i = 0,j=this.numChildren;i<j;i++){
            var lamp = this.getChildAt(i);
            lamp.image = i%leftover===0? alteastream.Assets.getImage(alteastream.Assets.images[this.imageOn]).image: alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]).image;
            lamp.visible = i%leftover===0;
        }
    };

    p._rotate = function(bool){
        if(bool ===true)
            TweenMax.to(this, this.rotTime, {rotation:"-360", ease:Linear.easeNone, repeat:-1});
        else{
            TweenMax.killTweensOf(this);
            this.rotation = 0;
        }
    };

    p._mark = function(n){
        var lamp = this.getChildByName(String(n));
        lamp.image = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOn]).image;
        var time = 0.2;
        TweenLite.to(lamp,time,{scaleX:1.5,scaleY:1.5});
        TweenLite.to(lamp,time,{delay:time,scaleX:1,scaleY:1});
    };

    p._animate = function(){
        var that = this;
        var children = this.numChildren;
        var time = 0.2;
        for(var i = 0,j=children;i<j;i++){
            var lamp = this.getChildAt(i);
            TweenLite.to(lamp,time,{scaleX:1.5,scaleY:1.5});
            TweenLite.to(lamp,time,{delay:time,scaleX:1,scaleY:1,onComplete:function(){
                    if(that._doAnimate){
                        that._animate();
                    }
                }});
        }
    };
    // public methods
    p.build = function(imageOff,imageOn,radius,neededForBonus){
        this.radius = radius;
        this.imageOff = imageOff;
        this.imageOn = imageOn;
        var firstLast = 2;
        this.neededForBonus = neededForBonus;
        var bulbs = (neededForBonus*2)-firstLast;
        this.bulbsHalf = bulbs/2;
        var oneField = this.oneField = (this.FULL_CIRCLE / bulbs);
        var startAngle = -90 * Math.PI / 180;
        var lampImg = alteastream.Assets.getImage(alteastream.Assets.images[this.imageOff]);

        for(var i = 0;i<bulbs;i++){
            var lamp = lampImg.clone();
            this.addChild(lamp);

            lamp.regX = lamp.image.width *0.5;
            lamp.regY = lamp.image.height *0.5;

            lamp.x =  Math.cos(startAngle)*radius;
            lamp.y =  Math.sin(startAngle)*radius;
            startAngle += oneField * Math.PI / 180;
            lamp.mouseEnabled = false;
            lamp.name = i;
        }
    };

    p.fill = function(count){
        if(this._canFill){
            this._reset();
            var length = count<this.neededForBonus? count: this.neededForBonus;
            var last = length === this.neededForBonus;
            var n = last === true? 1: 0;

            for(var i = 0;i<length-n;i++){
                var rightSide = this.bulbsHalf-i;
                var leftSide = this.bulbsHalf+i;
                this._mark(rightSide);
                this._mark(leftSide);
            }
            if(last)
                this.markLast();
            alteastream.Assets.playSound("bonusInc",0.3);
        }
    };

    p.fillAll = function(){
        this.fill(this.neededForBonus);
    };

    p.markLast = function(){// socket event hasBonus dolazi pre last fill..
        this._mark(0);
        this._canFill = false;
        this.lightsAnimation(true);
    };
    
    p.lightsAnimation = function(bool){
        if(bool === true){
            this._animate();
        }
        this._doAnimate = bool;
    };

    p.rotationAnimation = function(bool){
        this._markPair(bool);
        this._rotate(bool);
    };

    p.clear = function(){
        this.rotation = 0;
        this._reset();
        this._canFill = true;
    };

    alteastream.PowerUpWheel = createjs.promote(PowerUpWheel,"Container");
})();

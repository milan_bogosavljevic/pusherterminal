
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    var ABSWheel = function(game,bonuses) {
        this.Container_constructor();
        this.initialize_ABSWheel(game,bonuses);
    };
    var p = createjs.extend(ABSWheel, createjs.Container);

    // constants
    p.FULL_CIRCLE = 360;
    p.FIELD_WINS = [];
    // public properties
    p.revolutions = 0;
    p.force = 0;
    p.game = null;
    p.fields = [];
    // constructor
    p.initialize_ABSWheel = function(game,bonuses){
        this.game = game;

        var bonusesReceived = this.bonusesReceived = [];
        for (var key in bonuses)
            bonusesReceived.push(bonuses[key]);

        var minBonus = this.minBonus = Math.min.apply(null, bonusesReceived);
        bonusesReceived.splice(bonusesReceived.indexOf(minBonus),1);
        for(var i = 0;i<bonusesReceived.length;i++){
            this.FIELD_WINS.push(bonusesReceived[i]);
            this.FIELD_WINS.push(minBonus);
        }
        this.oneField = (this.FULL_CIRCLE / this.FIELD_WINS.length);
    };
    // public methods
    p.createWheel = function(fieldClass,lowFieldColor,highFieldColor){
        var cnt = 0;
        var cnt2 = 0;
        var length = this.FIELD_WINS.length;
        for(var i = 0;i<length;i++){
            var field = new alteastream[fieldClass](this.FIELD_WINS[i],i,length);
            this.addChild(field);
            field.mouseEnabled = false;
            field.rotation = i*this.oneField;
            field.id = i;
            if(this.FIELD_WINS[i]===this.minBonus){
                field.name = String(this.minBonus+"_"+cnt);
                field.setFontColor(lowFieldColor);
                cnt++;
            }else{
                field.name = this.FIELD_WINS[i];
                field.setFontColor(highFieldColor);
                cnt2++;
            }
            field.radian = field.rotation;
            this.fields.push(field);
        }
        //add here maybe some highlighterFrame//********?
    };

    p.spin = function(response){
        this.cacheFields(true);
        this.force = 16;
        console.log("response :::::::::::::::: "+response);
        this.revolutions = Math.round(this.force/2)-1;
        var duration = Math.round((this.revolutions/10) * (300/this.force))-5;//-5 4
        var insidePosition = 0;

        var randomGet = String(this.minBonus+"_"+gamecore.Utils.NUMBER.randomRange(0,(this.bonusesReceived.length*0.5)-1));
        this.resultField = response === this.minBonus? randomGet: response;
        var endRadian = this.getChildByName(String(this.resultField)).radian;
        var fieldAngle = endRadian;
        var targetDegree = (insidePosition + this.FULL_CIRCLE * this.revolutions + (this.FULL_CIRCLE - fieldAngle)).toFixed(2);

        var that = this;
        TweenLite.to(this, duration, {ease: Back.easeOut.config(0.2),rotation:targetDegree,onComplete:function(){that.onSpinEnd()}});
    };

    p.onSpinEnd  = function(){
        this.resetDegrees();
        this.highlightWin(true);
        console.log("onSpinEnd:::::::::::::::: ");
        this.game.onBonusPlayed();
        this.cacheFields(false);
    };

    p.resetDegrees  = function(endRadian){
        this.rotation -= (this.revolutions * this.FULL_CIRCLE);
    };

    p.highlightWin = function(bool){
        //this.getChildByName(this.resultField).highlight(bool);
        //this.highlighterFrame.rotation = this.getChildByName(this.resultField).rotation;//********?
    };

    p.cacheFields = function(doCache){
        console.log("radi cache " + doCache);
        for(var i = 0; i < this.fields.length; i++){
            var field = this.fields[i];
            if(doCache === true){
                //gamecore.Utils.DISPLAY.cacheObject(field);
                gamecore.Utils.DISPLAY.cacheText(field.itemValue);
            }else{
                field.itemValue.uncache();
            }
        }
    };
    
    alteastream.ABSWheel = createjs.promote(ABSWheel,"Container");
})();

"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function () {

    var ABSField = function(n) {
        this.Container_constructor();
        this.initialize_ABSField(n);
    };
    var p = createjs.extend(ABSField, createjs.Container);

    // events:
    // public properties:
    // static properties:
    // constants
    p.WIDTH_SUM = 2064;
    p.NUM_BONUSES = 0;
    // private properties:
    // constructor:
    p.initialize_ABSField = function(n) {
        this._init(n);
    };
    // static methods:
    // private methods:
    p._init = function(n){
        this.count = n;
        this.mouseEnabled = false;
    };

    p._createSlice = function(color){
        var slice = new createjs.Shape();
        this._draw(slice,color);
        return slice;
    };

    p._draw = function(slice,color){
        var halfWidth = this.sWidth*0.5;
        var halfHeight = this.startY*((this.startY/halfWidth)*0.75);
        slice.graphics.beginFill(color);
        slice.graphics.moveTo(this.startX, this.startY).quadraticCurveTo(halfWidth, halfHeight, this.sWidth, this.startY).lineTo(halfWidth, this.sHeight).lineTo(this.startX, this.startY);
        slice.cache(0,0,this.sWidth,this.sHeight);
    };
    // public methods:
    p.setSliceColor = function(color){
        this.defaultSliceColor = color;
        this.colorSlice(color);
    };

    p.setFontColor = function(color) {
        this.itemValue.color = color;
    };

    p.colorSlice = function(color){
        var slice = this.slice;
        slice.uncache();
        slice.graphics.clear();
        this._draw(slice,color);
    };

    p.highlight = function(bool){
        this.highlightText(bool);
        this.highlightField(bool);
    };

    p.highlightText = function(bool){
        this.itemValue.color = bool === true? this.highlightTextColor: this.defaultTextColor;
        this.itemValue.x = this.regX;
    };

    p.highlightField = function(bool){
        //Mylan: highlight slice-a boja
        //var color = bool === true? "#ffffff": this.defaultSliceColor;  // dynamic draw field ver
        //this.colorSlice(color); // dynamic draw field ver
    };

    alteastream.ABSField = createjs.promote(ABSField,"Container");
})();

/*http://139.59.147.230:82/games/rules*/
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusher = function() {
        this.AbstractScene_constructor();
        this.initialize_CoinPusher();
    };
    var p = createjs.extend(CoinPusher, alteastream.AbstractScene);
    // static properties:
    // events:
    var _instance = null;
    // private properties:
    p.active = false;
    p._info = null;
    p._stakeIncrements = [];
    p._selectedStake = 0;
    p._refundCoinsCounter = 0;
    p._refundAnimationInterval = null;
    // public properties:
    p.bonusWon = 0;
    p.bonusActive = false;
    p.hasBonus = false;
    p.minStake = 0;
    p.maxStake = 0;
    p.stakeInc = 0;
    p.credit = 0;
    p.betManager = null;
    p.prizeQueue = [];
    p.queueCount = 0;
    p.queuePlaying = false;
    p.okToShowBalance = true;
    p.currentTokensCounted = 0;
    p.currentRefundValue = 0;
    p.autoCounter = 0;
    p.coinsCounter = null;
    p.prizeCounter = null;
    p.prizeWinLabel = null;
    p.prizeNumber = null;
    p.prizeEurNumber = null;
    p.currentPrize = 0;
    p.currentPrizeEur = 0;
    p.kickOutTimer = null;

    // constructor:
    p.initialize_CoinPusher = function() {
        stage.enableMouseOver(0);
        _instance = this;

        this._local_activate();// local ver
        this._callInitMethods();

        console.log("initialize_CoinPusher..");
    };
    // static methods
    CoinPusher.getInstance = function(){
        return _instance;
    };
    // private methods
    // new socket
    p._callInitMethods = function() {
        alteastream.Assets.setMute(true);
        this.setHttpCommunication();
        this.setSocketCommunication("Game");

        alteastream.Requester.getInstance().startGameStream(function(response){// todo OVAJ RESPONSE JE machineParameters
            _instance.machineParameters = response;
            _instance.socketCommunicator.activate(_instance.machineParameters);
        });

        /*var machineParams = window.localStorage.getItem('machineParameters');
        this.machineParameters = JSON.parse(machineParams);*/

        this._setControlBoard();
        this._addInfo();
        //this.setQueueInfo();
        //this.setCoinCounter();
        this.setPrizeWinCounter();
        //this.addGiveUpButton(true);//giveUpButton
        this.addControlBoard();// samo 
        this.monitorNetworkStatus();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);

        this.setOverlay();
        //this.setMachineNameText(); // show machine name , uncomment
    };

    p.setOverlay = function() { //todo mozda zvati ovo iz communicatora u CONFIRM_GAME_INIT umestop showGame
        _instance.controlBoard.mouseEnabled = false;
        _instance.showGame(false);

        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var bg = new createjs.Shape();
        bg.graphics.beginFill("#000000").drawRect(0, 0, w, h);

        var qFont = "90px Lato";
        var qY = 125;
        var question = new createjs.Text("Ready To Play?",qFont,"#ffffff").set({x:w/2, y:qY, textAlign:"center",textBaseline:"middle"});

        var buttonFont = "bold 20px Lato";
        var buttonFontY = 65;
        var buttonYCorrector = 145;
        var txt = new createjs.Text("PLAY NOW",buttonFont,"#fcfcfc");
        var playButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,txt);
        playButton.centerText(0,buttonFontY);
        playButton.x = w/2;
        playButton.y = h - buttonYCorrector;

        var overlayClickCallback = function () {
            //_instance.streamVideo.videoElement.setMuted(false);
            //_instance.streamVideo.videoElement.setVolume(0.1);
            //_instance.streamVideo.videoElement.play();
            //_instance.streamVideo.setMedia();
            _instance.controlBoard.mouseEnabled = true;
            _instance.showGame(true);
            _instance.activateFullscreen();
        };

        var overlayOptions = {
            background:bg,
            button:playButton,
            graphics:[],
            texts:[question],
            clickCallback:overlayClickCallback
        };

        this.setStartOverlay(overlayOptions);
    };

    p.activateFullscreen = function () {
        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        _instance[method]();
    };

    p.normalBrowsersFullScreenToggle = function(e) {// CHROME, OPERA, FIREFOX
        var element = window.parent.document.documentElement;
        if(!window.parent.document.fullscreenElement){
            element.requestFullscreen();
        }else{
            window.parent.document.exitFullscreen();
        }
    };

    p.otherBrowsersFullScreenToggle = function(e) {// EDGE, SAFARI
        var element = window.parent.document.documentElement;
        if(!window.parent.document.webkitFullscreenElement){ // ovde je drugacija provera posto ovi browseri izadju iz full moda kada krene da se ucitava masina
            element.webkitRequestFullScreen();
        }else{
            window.parent.document.webkitExitFullscreen();
        }
    };

    p.fullScreenManager = function(){};

    p._addInfo = function() {
        this._info = new alteastream.CoinPusherInfo();
        this._info.visible = false;
        this.addChild(this._info);
    };

    p._setInfo = function() {
        this._info.setInfo(this.payoutManager, this.hasBonus, this.betManager.getMultiplier());
        if(this.hasBonus === true){
            this._info.setBonusInfo(this.powerUp.neededForBonus)
        }
        var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && refundValue > 0){
        //if(refundValue > 0){
            //var refundValue = (Math.round(((this.payoutManager.getRefundValue() * 100)/100) * 1e12) / 1e12).toFixed(3);
            //var refundValue = gamecore.Utils.NUMBER.roundDecimal((this.payoutManager.getRefundValue() * 100)/100).toFixed(3);
            this._info.setCountedTokensInfo(refundValue);//param metal coinvalue + slicica
        }else{
            this._info.setSunLogo();
        }
        this._info.setMaxStakeInfo(this.maxStake);
        this._info.setNonPlayingTimeInfo(this.controlIdleTime);//param from response
        //this._info.setKeyboardLabels();
    };

    p._switchInfoAndWheelChildIndexes = function() {
        this.swapChildren(this._info, this.bonusContainer);
    };

/*    p._setStreamVideo = function(address){
        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideo = new alteastream.CoinPusherStream(this,options,address);
        //this.streamVideo = new alteastream.VideoStream(this,"videoOutput",address);//old
    };*/

    p._setControlBoard = function(){
        var stakeIncrements = [0, 0, 0, 0, 0, 0, 0];
        this.setStakeIncrements(stakeIncrements);
        this.setBetTotal();

        this.controlBoard = new alteastream.CoinPusherControlBoard();
        this.controlBoard.plugin(this);
        this.controlBoard.initialSetup();

        /*var isMobile = localStorage.getItem("isMobile"); // verzija ako hocemo da se ne instancira keyboard listener za mobilni
        if(isMobile === "not"){
            var controller = new alteastream.Controller(this);
            controller.activate();
            this.controlBoard.twinComponent = controller;
        }*/
    };

    p._setBonusApplet = function(options){
        this.hasBonus = true;
        this.bonusContainer = new createjs.Container();

        var wheel = this.bonusWheel = new alteastream.PusherBonusWheel(this,options.bonus);
        this.bonusContainer.addChild(wheel);

        var highlightImg = "wheel_" + wheel.bonusesReceived.length * 2 + "_bonus_fields_highlight";
        var bonusHighlightField = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images[highlightImg]);
        bonusHighlightField.regX = bonusHighlightField.image.width * 0.5;
        bonusHighlightField.regY = bonusHighlightField.image.height;
        bonusHighlightField.visible = false;
        this.bonusContainer.addChild(bonusHighlightField);

        var ring = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImageRing);
        this.bonusContainer.addChild(ring);
        ring.regX = ring.image.width*0.5;
        ring.regY = ring.image.width*0.5;
        ring.mouseEnabled = false;

        var powerUp = this.powerUp = new alteastream.PowerUpWheel(this);
        this.bonusContainer.addChild(powerUp);
        var radius = 366;
        powerUp.build("lampOff","lampOn",radius,options.bonus_limit);

        var bounsWonBgImage = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImage);
        this.bonusContainer.addChild(bounsWonBgImage);
        bounsWonBgImage.regX = bounsWonBgImage.image.width*0.5;
        bounsWonBgImage.regY = bounsWonBgImage.image.height*0.5;

        var bonusWonText = this.bonusWonText = new createjs.Text("", '96px Technology', "#ed2224").set({textBaseline:"middle",textAlign:"right"});
        this.bonusContainer.addChild(bonusWonText);
        bonusWonText.x = 66;
        bonusWonText.y = 9;

        var marker = this.marker = alteastream.Assets.getImage(alteastream.Assets.images.arrow);
        this.bonusContainer.addChild(marker);
        marker.regX = marker.image.width*0.5;
        marker.x = 0;
        marker.y = -420;
        marker.mouseEnabled = false;

        this.addChild(this.bonusContainer);
        this.bonusContainer.x = this.bonusContainer.startX = 250;
        this.bonusContainer.y = this.bonusContainer.startY = 250;
        this.bonusContainer.xPosForAnimation = 400;
        this.bonusContainer.yPosForAnimation = 400;
        this.bonusContainer.xPosForInfo = 960;
        this.bonusContainer.yPosForInfo = 230;
        this.bonusContainer.mouseEnabled = false;
        this.bonusContainer.scaleX = this.bonusContainer.scaleY = 0.5;

        this.bonusWheel.setSkin(function () {
            //_instance.streamVideo.activate(function(){_instance.showGame(true);});
        });

        // new stream load
        /*this.bonusWheel.setSkin(function () {
            _instance.socketCommunicator.confirmGameInit();
        });*/
    };

    p._bonusCoinSpawn = function(n){
        var cnt = 0;
        for(var i = 0; i < n; ++i) {
            setTimeout(function(){
                var coin = new alteastream.SpringAnim('coinWin');
                coin.x = _instance.bonusContainer.xPosForAnimation;
                coin.y = _instance.bonusContainer.yPosForAnimation;
                _instance.addChild(coin);

                coin.setVR(-4,4);
                coin.setVX(-4,4);
                coin.setVY(-15,-30);
                coin.dispose = function(){
                    coin.removeAllChildren();
                    _instance.removeChild(coin);
                    cnt++;
                    if(cnt === n){
                        _instance.endBonusFeature();
                    }
                };
                coin.animate();
                alteastream.Assets.playSound("bonusInc",0.3);
            },i*100);
        }
    };

    p._coinSpawnAnimation = function(){
        var coin = new alteastream.SpringAnim('coinWin');
        coin.x = _instance.coinsCounter.x+_instance.coinsCounter.pivot;
        coin.y = alteastream.AbstractScene.GAME_HEIGHT+coin.pivot;
        _instance.addChild(coin);
        _instance._coinSpawnAnimationParams(coin);
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(1.5);
        coin.setVR(-10,10);
        coin.setVX(-1.5,1.5);
        coin.setVY(-28,-32);
        coin.animate();
    };

    // new stream load
    p._checkIfBonusGame = function(){
        if(Object.keys(_instance.machineParameters.bonus).length === 0){
            _instance.socketCommunicator.confirmGameInit();
        }else{
            _instance._setBonusApplet(_instance.machineParameters);
        }
    };

    p._setBonuswheelPositionAndForInfo = function(infoIsActive) {
        if(infoIsActive === true){
            this.bonusContainer.x = this.bonusContainer.xPosForInfo;
            this.bonusContainer.y = this.bonusContainer.yPosForInfo;
        }else{
            this.bonusContainer.x = this.bonusContainer.startX;
            this.bonusContainer.y = this.bonusContainer.startY;
        }
    };

    p._highlightCoinsWin = function(bool){// just a temp marker for some highlights
        this.coinsCounter.getChildAt(0).alpha =
            this.controlBoard.balanceBackground.alpha = bool === true? 1: 0.7;
    };

    p._highlightPrizeWin = function(bool){// just a temp marker for some highlights
        this.prizeCounter.getChildAt(0).alpha =
            this.controlBoard.balanceBackground.alpha = bool === true? 1: 0.7;
    };

    p._resetTimeout = function(){
        clearTimeout(this.timer);
        this.timer = setTimeout(function(){
                _instance.currentTokensCounted =
                    _instance.currentRefundValue = 0;
                _instance.eurNumber.text =
                    _instance.coinsNumber.text = "0.00";
                _instance._highlightCoinsWin(false);
            }
            ,1200);//adjustable
    };
    // public methods

    // show machine name , replace current methods with these methods
/*    p.setQueueInfo = function(){
        var queueInfoContainer = this.queueInfoContainer = new createjs.Container();
        var font = 'bold 16px HurmeGeometricSans3';
        var white = "#ffffff";

        var queueInfoBg  = new createjs.Shape();
        queueInfoBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 44, 5);
        queueInfoBg.cache(0, 0, 190, 44);

        var queueInfoText = this.queueInfoText = new createjs.Text("CO-PLAY: 0", font, white).set({textBaseline:"middle",textAlign:"center"});
        queueInfoText.x = 95;
        queueInfoText.y = 23;

        queueInfoContainer.addChild(queueInfoBg, queueInfoText);
        this.addChild(queueInfoContainer);
        queueInfoContainer.x = 260;
        queueInfoContainer.y = 10;
        queueInfoContainer.mouseEnabled = false;
        queueInfoContainer.mouseChildren = false;
        queueInfoContainer.visible = false;
    };

    p.updateQueueInfo = function(queue){
        this.queueInfoText.text = "CO-PLAY: " + queue || "CO-PLAY: 0";
    };*/

        // show machine name , uncomment
/*    p.setMachineNameText = function() {
        var name = alteastream.AbstractScene.MACHINE_NAME;
        var machineNameContainer = new createjs.Container();
        var font = 'bold 16px HurmeGeometricSans3';
        var white = "#ffffff";

        var machineNameBg  = new createjs.Shape();
        machineNameBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 230, 44, 5);
        machineNameBg.cache(0, 0, 230, 44);
        var nameText = this.machineNameText = new createjs.Text("MACHINE NAME: " + name, font, white).set({textBaseline:"middle",textAlign:"center"});
        nameText.x = 115;
        nameText.y = 23;

        machineNameContainer.addChild(machineNameBg, nameText);
        this.addChild(machineNameContainer);
        machineNameContainer.x = 20;
        machineNameContainer.y = 10;
        machineNameContainer.mouseEnabled = false;
        machineNameContainer.mouseChildren = false;
    };*/

    p.showNetworkStatus = function(){
        var status = navigator.onLine? "online" : "offline";
        _instance.throwAlert(alteastream.Alert.INFO,"You are "+status,_instance.socketCommunicator.tryReconnect(status));
    };

    p.setMonetaryValueView = function(isEur){
        this.controlBoard.updateTextColorType(isEur);
        //new stakes
        this.controlBoard.fixedStakesPanel.updateMonetary(isEur);
        this.updateInfoPrizes(isEur);
        this.updateCountedTokensView(isEur);
        this.updatePrizesView(isEur);
        if(this.hasBonus === true){
            this.updateWheelPrizes(isEur);
        }
    };

/*    p.addGiveUpButton = function(bool){
        if(bool === true){
            var giveUpButton = this.giveUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
            var width = giveUpButton.width;
            var spacing = width/2;
            giveUpButton.x = alteastream.AbstractScene.GAME_WIDTH - width - spacing;
            giveUpButton.y = 10;
            this.addChild(giveUpButton);

            //giveUpButton.setClickHandler(_instance.quitBtnHandler);// todo proveriti
            giveUpButton.setClickHandler(function () {
                _instance.streamVideo.stop();
                _instance.exitMachineGameplay();
            });
/!*            giveUpButton.addEventListener("click", function () {
                _instance.quitBtnHandler();
                //_instance.streamVideo.stop();
                //_instance.exitMachineGameplay();// nije otvorio soket, ne moze da dobije 85, token itd..mozda http poziv u ovom slucaju
            });*!/
        }else{
            this.removeChild(this.giveUpButton);
            this.giveUpButton = null;
        }
    };*/

    /*    p._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
            return localStorage.getItem("isMobile");
        };*/

    p.bonusFocusIn = function(bool,callback){
        if(bool === true){
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut,x:this.bonusContainer.xPosForAnimation,y:this.bonusContainer.yPosForAnimation,scaleX:0.7,scaleY:0.7,onComplete:function(){
                    callback();
                }});
        }else{
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut, x:this.bonusContainer.startX,y:this.bonusContainer.startY,scaleX:0.5,scaleY:0.5,onComplete:function(){
                    callback();
                }});
        }
    };

    p.addControlBoard = function(){
        this.addChild(this.controlBoard);
        this.controlBoard.visible = false;
    };

    //old socket
    /*p.onSocketConnect = function(frame){ // invalid token fix new
        _instance.socketCommunicator.onGameConnect(frame);
        var response = window.localStorage.getItem('machineParameters');
        var machineResponse = JSON.parse(response);
        _instance.socketCommunicator.startGame(machineResponse);
        _instance._setStreamVideo(machineResponse);
        _instance.setGameParameters(machineResponse);
        _instance.setPayout(machineResponse.payout);
        _instance.setRegularStakes();
        _instance.setCurrency();
        if(Object.keys(machineResponse.bonus).length === 0){
            _instance.streamVideo.activate(function(){_instance.showGame(true);});
        }else{
            _instance._setBonusApplet(machineResponse);
        }
        _instance._setInfo();
        _instance.showGame(false);
        _instance.setRenderer();
        window.localStorage.removeItem('machineParameters');

        var canvas = document.getElementById("screen");
        canvas.focus();
        if(document.activeElement !== canvas)
            canvas.focus();
    };*/

    //new socket
    p.onSocketConnect = function(frame){ // invalid token fix new
        _instance.socketCommunicator.onGameConnect(frame);

        var machineParams = _instance.machineParameters;

        //_instance._setStreamVideo(machineParams);// new stream load comment
        _instance.setGameParameters(machineParams);
        _instance.setPayout(machineParams.payout);
        _instance.setCoinCounter();
        _instance.setRegularStakes();
        //new stakes
        _instance.setFixedStakes();
        _instance.setCurrency();
        _instance.setMonetaryValueView(_instance.controlBoard._toValueView);

        //_instance.checkStreamLoad();// new stream load uncomment

        // new stream load comment block start
/*        if(Object.keys(machineParams.bonus).length === 0){
            _instance.streamVideo.activate(function(){
                _instance.socketCommunicator.confirmGameInit();
            });
        }else{
            _instance._setBonusApplet(machineParams);
        }*/
        // new stream load comment block end

        _instance._setInfo();
        _instance.showGame(false);
        _instance.setRenderer();
        _instance.socketCommunicator.confirmGameInit();

        //window.localStorage.removeItem('machineParameters');

        var canvas = document.getElementById("screen");
        canvas.focus();
        if(document.activeElement !== canvas)
            canvas.focus();
    };
    // new stream load
/*    p.checkStreamLoad = function(){
        if(alteastream.VideoStream.LOADED === true){
            alteastream.VideoStreamPreload.enableSounds();
            _instance._checkIfBonusGame();
        }
        else{
            alteastream.VideoStreamPreload.showPreload(true);
            var loadIntervalCheck = setInterval(function(){
                if(alteastream.VideoStream.LOADED === true){
                    clearInterval(loadIntervalCheck);
                    alteastream.VideoStreamPreload.enableSounds();
                    _instance._checkIfBonusGame();
                }
            },500);
        }
    };*/

    p.showGame = function(bool){
        if(this.hasBonus === true){
            this.bonusContainer.visible = bool;
        }

        if(!this.betManager.canBet(this.credit,this.getSelectedStake())){
            this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance");
        }
/*
        if(bool === true){
            //this.addGiveUpButton(false);
            //temp reminder::
            if(!this.betManager.canBet(this.credit,this.getSelectedStake())){
                this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance");
            }

            if(is.safari()){
                // var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                // if(is.not.ipad() && is.not.ios() && !isiPadOS){
                //     this.streamVideo.setMedia();
                //     alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
                // }
            }else{
                //this.streamVideo.setMedia();
                alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
            }

            var isMobile = localStorage.getItem("isMobile"); // verzija ako hocemo da se ne instancira keyboard listener za mobilni
            if(isMobile === "not"){
                var controller = new alteastream.Controller(this);
                controller.activate();
                this.controlBoard.twinComponent = controller;
            }
        }*/

        this.controlBoard.visible = bool;
        //this.queueInfoContainer.visible = bool;
        this.prizeCounter.visible = bool;
        if(this.coinsCounter)
            this.coinsCounter.visible = bool;
    };

    p.monitorIdleTime = function(bool){// todo OVO MOZDA NEMA LOGIKE DA POSTOJI
        if(bool === true){
            this._kickOutCount = 0;
            clearInterval(this.kickOutTimer);
            this.kickOutTimer = setInterval(function(){
                _instance._kickOutCount++;
                if(_instance._kickOutCount<3){
                    _instance.inactivityWarning(_instance._kickOutCount);
                }
                else{
                    _instance._kickOutCount = 0;
                    clearInterval(_instance.kickOutTimer);
                }
            },this._kickOutPing);
        }else{
            this._kickOutCount = 0;
            clearInterval(this.kickOutTimer);
        }
    }

    p.setPayout = function(response){ // todo temp creating cleanPayouts, response should give us cleaned array
        var tags = Object.keys(response);
        var cleanPayouts = {};
        for(var i = 0; i < tags.length; i++){
            var tag = tags[i];
            var prize = response[tag];

            if(prize !== 0 && tag !== "101"){
                cleanPayouts[tag] = prize;
            }
        }
        //var refund = response["101"] || 0.25;
        var refund = response["101"] || 0;
        refund = refund*this.betManager.getMultiplier();
        this.payoutManager = new alteastream.PayoutManager(cleanPayouts,refund);
    };

    p.onInfo = function(){
        this._info.visible = !this._info.visible;
        this.controlBoard.hideControlls(this._info.visible);
        if(this.hasBonus === true){
            this._setBonuswheelPositionAndForInfo(this._info.visible);
        }

        //this.queueInfoContainer.visible = !this._info.visible;

        alteastream.Assets.playSound("btnDownDouble");
    };

    p.checkIfinfoIsActive = function() {
        if(this._info.visible === true){
            this.onInfo();
        }
    };

    p.onPrizeDetection = function(msgValue){
        var amount = this.payoutManager.getPayoutByPrize(msgValue.prize);
        this.okToShowBalance = false;
        this.setCrd(msgValue.balance);
        this.showWin(amount,msgValue);
    };

/*    p.onQuit = function(){
        //alteastream.VideoStreamPreload.stop();// new stream load uncomment
        //alteastream.VideoStreamPreload.removeVideoElement();// new stream load uncomment
        //this.streamVideo.stop();// new stream load comment
        this.socketCommunicator.quitPlay();
        //moved to SocketCommunicatorGame
        //this.killGame();
        //this.closeGame();
    };*/

/*    p.exitMachineGameplay = function(){
        this.killGame();
        this.closeGame();
    };*/

    //old socket
    /*p.closeGame = function(){
        this.requester.backToLobby(function (res) {
            window.top.manageBgMusic("pause");
            window.top.stopHelperSound("machineNoise");
            //window.localStorage.setItem('backgroundMusicIsMuted', String(_instance.controlBoard.soundToggled));
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
            window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+_instance.socketCommunicator.currentGameUid);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        });
    };*/

    //new socket
/*    p.closeGame = function(){
        window.parent.manageBgMusic("pause");
        //window.parent.stopHelperSound("machineNoise");
        //window.localStorage.setItem('backgroundMusicIsMuted', String(_instance.controlBoard.soundToggled));
        window.parent.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
    };*/

    p.fillToBonus = function(count){
        this.powerUp.fill(count);
    };

    p.onBonusWon = function(msg){
        var bal = msg.vl.balance - msg.vl.money;
        this.setCrd(bal);
        this.okToShowBalance = false;
        this.onBonusAllowed(msg.vl.amount);
    };

    p.onBonusAllowed = function(amount){
        console.log("onBonusAllowed");
        this.checkIfinfoIsActive();
        this.controlBoard.disablePlayButtons(true);
        this.bonusWon = amount;
        this.bonusActive = true;

        this.powerUp.fillAll();
        this.spinForBonus();
    };

    p.spinForBonus = function(){
        this.powerUp.lightsAnimation(false);
        alteastream.Assets.playSound("bonusWinnerSpoken_1",0.3);
        this.bonusFocusIn(true,function(){
            _instance.powerUp.rotationAnimation(true);
            _instance.bonusWheel.spin(_instance.bonusWon);
            alteastream.Assets.playSoundChannelWithOptions("bonusSpinLoop", {loop:-1,vol:0.2});
        });
    };

    p.onBonusPlayed = function(){
        console.log("onBonusPlayed");
        this.bonusHighlightField.visible = true;
        this.bonusActive = false;
        this.bonusWonText.visible = true;
        this.bonusWonText.text = this.bonusWon;
        this.powerUp.rotationAnimation(false);
        this.powerUp.lightsAnimation(true);
        alteastream.Assets.playSound("win1",0.3);
        var bonusWonSpoken = "prize_"+this.bonusWon;
        var appendixWord = "bonusWord";// temp
        this.playSoundChain("youHaveWonSpoken",0.3,function(){
            _instance.playSoundChain(bonusWonSpoken,0.3,function(){
                _instance.playSoundChain(appendixWord,0.3,function(){
                    _instance.powerUp.lightsAnimation(false);
                    _instance._bonusCoinSpawn(_instance.bonusWon);
                });
            });
        });
    };

    p.endBonusFeature = function(){
        this.bonusFocusIn(false,function(){
            _instance.okToShowBalance = true;
            _instance.bonusWonText.text = "";
            _instance.bonusWonText.visible = false;
            _instance.bonusHighlightField.visible = false;
            _instance.bonusWheel.highlightWin(false);
            _instance.powerUp.clear();
            _instance.controlBoard.disablePlayButtons(false);
            _instance.controlBoard.setCrd(_instance.credit);
            _instance.bonusWon = 0;
            alteastream.Assets.stopSoundChannel("bonusSpinLoop");
            alteastream.Assets.playSound("win1",0.3);
            console.log("BONUS END");
        });
    };

/*    p.setQueueInfo = function(){
        var queueInfoContainer = this.queueInfoContainer = new createjs.Container();
        queueInfoContainer.x = 30;
        queueInfoContainer.y = 30;

        var font = '16px Lato';

        var queueInfoBg = this.queueInfoBackground = new createjs.Shape();
        queueInfoBg.graphics.beginFill("#000000").drawRoundRect(0, 0, 140, 45, 10);
        queueInfoBg.alpha = 0.7;
        queueInfoBg.cache(0, 0, 140, 45);

        var queueInfoLabel = this.queueInfoLabel = new createjs.Text("CO-PLAY: ", font, "#ffffff").set({textBaseline:"middle",textAlign:"right"});
        var queueInfoText = this.queueInfoText = new createjs.Text("0", font, "#ffd500").set({textBaseline:"middle",textAlign:"left"});
        queueInfoText.x = queueInfoLabel.x = 102;
        queueInfoText.y = queueInfoLabel.y = 24;

        queueInfoContainer.addChild(queueInfoBg, queueInfoLabel,queueInfoText);
        this.addChild(queueInfoContainer);
        queueInfoContainer.mouseEnabled = false;
        queueInfoContainer.mouseChildren = false;
        queueInfoContainer.visible = false;
    };*/

/*    p.updateQueueInfo = function(queue){
        this.queueInfoText.text = queue || 0;
    };*/

    p.setCoinCounter = function(){
        //if(alteastream.AbstractScene.GAME_TYPE === "pearlpusher")
            //return;
        if(alteastream.AbstractScene.GAME_TYPE !== "pearlpusher" && this.payoutManager.getRefundValue() > 0){
            this.currentTokensCounted = 0;
            this.currentRefundValue = 0;
            var container = this.coinsCounter = new createjs.Container();
            var font = '16px Lato';
            var yellow = "#ffd500";
            var green = "#00ff00";

            //var back = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images.refundComponentBackground);
            var back = new createjs.Shape();
            back.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 67, 10);
            back.alpha = 0.7;
            back.cache(0, 0, 190, 67);

            var yPos = 35;

            var coinsChargedLabel = this.coinsChargedLabel = new createjs.Text('COINS WIN:', font, "#ffffff").set({textBaseline:"middle",textAlign:"left"});
            coinsChargedLabel.x = 32;
            coinsChargedLabel.y = yPos;

            var coinsNumber  = this.coinsNumber = new createjs.Text("0.00", font, yellow).set({textBaseline:"middle",textAlign:"left"});
            coinsNumber.x = coinsChargedLabel.x + 95;//128
            coinsNumber.y = yPos;

            var eurNumber = this.eurNumber = new createjs.Text("0.00", font, green).set({textBaseline:"middle",textAlign:"left"});
            eurNumber.x = coinsChargedLabel.x + 95;
            eurNumber.y = yPos;
            eurNumber.visible = false;

            container.addChild(back,coinsChargedLabel,coinsNumber, eurNumber);
            this.addChild(container);
            container.x = 337;
            container.y = 987;
            container.pivot = (back.bitmapCache.width*0.5);
            container.mouseEnabled = false;
            container.mouseChildren = false;
            container.visible = false;
        }
    };

    p.setPrizeWinCounter = function() {
        var container = this.prizeCounter = new createjs.Container();
        var font = '16px Lato';
        var yellow = "#ffd500";
        var green = "#00ff00";

        //var back = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images.refundComponentBackground);
        var back = new createjs.Shape();
        back.graphics.beginFill("#000000").drawRoundRect(0, 0, 190, 67, 10);
        back.alpha = 0.7;
        back.cache(0, 0, 190, 67);

        var yPos = 35;
        var xCorr = 87;

        var prizeWinLabel = this.prizeWinLabel = new createjs.Text('PRIZE WIN:', font, "#ffffff").set({textBaseline:"middle",textAlign:"left"});
        prizeWinLabel.x = 34;
        prizeWinLabel.y = yPos;

        var prizeNumber  = this.prizeNumber = new createjs.Text("0.00", font, yellow).set({textBaseline:"middle",textAlign:"left"});
        prizeNumber.x = prizeWinLabel.x + 91;
        prizeNumber.y = yPos;

        var prizeEurNumber = this.prizeEurNumber = new createjs.Text("0.00", font, green).set({textBaseline:"middle",textAlign:"left"});
        prizeEurNumber.x = prizeWinLabel.x + 91;
        prizeEurNumber.y = yPos;
        prizeEurNumber.visible = false;

        container.addChild(back, prizeWinLabel,prizeNumber, prizeEurNumber);
        this.addChild(container);
        container.x = 1497- xCorr;
        container.y = 987;
        container.pivot = (back.bitmapCache.width*0.5);
        container.mouseEnabled = false;
        container.mouseChildren = false;
        container.visible = false;
    };

    p.showPrize = function(coins) {
        this._highlightPrizeWin(true);
        this.currentPrize += coins;
        this.currentPrizeEur += coins * this.betManager.getMultiplier();

        this.prizeNumber.text = this.currentPrize;
        var rounderPrizeInEur = gamecore.Utils.NUMBER.roundDecimal(this.currentPrizeEur).toFixed(2);
        this.prizeEurNumber.text = rounderPrizeInEur;
        this.prizeEurNumber.text += gamecore.Utils.NUMBER.addZeros(rounderPrizeInEur,1);

        this.resetPrize();
    };

    p.resetPrize = function() {
        clearTimeout(this.clearPrizeTimer);
        this.clearPrizeTimer = setTimeout(function(){
            _instance.currentPrize =
                _instance.currentPrizeEur = 0;
            _instance.prizeNumber.text = 0;
            _instance.prizeEurNumber.text = "0.00";
            _instance._highlightPrizeWin(false);
        },2000);
    };

    p.refundCoinSpawn = function (tokens) {
        this._refundCoinsCounter += tokens;
        this._highlightCoinsWin(true);
        var refundTokenValue = this.payoutManager.getRefundValue();
        if(this._refundAnimationInterval === null) {
            this._refundAnimationInterval = setInterval(function () {
                console.log("cs");
                _instance._refundCoinsCounter--;
                _instance._coinSpawnAnimation();
                _instance.playSound("refund", 0.8);

                // (0.0015).toFixed(2) = 0.00
                // (0.0015).toFixed(3) = 0.002

                //var coinsValue = gamecore.Utils.NUMBER.floorDecimal(refundTokenValue * (_instance.betManager.getMultiplier() * 100)).toFixed(2);
                //var refundValue = gamecore.Utils.NUMBER.roundDecimal((refundTokenValue * 100) / 100).toFixed(2);

                var coinsValue = gamecore.Utils.NUMBER.floorDecimal(refundTokenValue * (_instance.betManager.getMultiplier() * 100)).toFixed(3);
                var refundValue = gamecore.Utils.NUMBER.roundDecimal((refundTokenValue * 100) / 100).toFixed(3);

                _instance.currentTokensCounted = gamecore.Utils.NUMBER.roundDecimal(_instance.currentTokensCounted + Number(coinsValue));
                _instance.currentRefundValue = gamecore.Utils.NUMBER.roundDecimal(_instance.currentRefundValue + Number(refundValue));

                _instance.coinsNumber.text = _instance.currentTokensCounted + gamecore.Utils.NUMBER.addZeros(_instance.currentTokensCounted);
                _instance.eurNumber.text = _instance.currentRefundValue + gamecore.Utils.NUMBER.addZeros(_instance.currentRefundValue);

                if (_instance._refundCoinsCounter === 0) {
                    clearInterval(_instance._refundAnimationInterval);
                    _instance._refundAnimationInterval = null;
                    _instance._resetTimeout();
                }
            }, 100);
        }
    };

    p.setGameParameters = function(response){
        this.setCrd(response.balance);

        this.minStake = 1;
        this.maxStake = response.maxTokens || 20;
        this.stakeInc = 1;
        this.controlIdleTime = response.controlIdleTime;
        console.log("CONTROL:: "+this.controlIdleTime);
        this._kickOutPing = this.controlIdleTime/3;
        this.setStakeIncrementsMinMax(this.minStake, this.maxStake, this.stakeInc);

        this.currency = response.currency || "N/A";
        //decimal
        //var code = 8364;//resp.user.currency
        //this.currency = String.fromCharCode( code);

        //hex, css
        //var code = '20AC';//20A4 resp.user.currency
        //this.currency = String.fromCharCode( parseInt(code, 16) );

        this.betManager.setMultiplier(response.tokenValue);
    };

    p.setBetTotal = function(){
        this.betManager = new alteastream.BetTotal(this);
    };

    p.setCurrency = function(){
        this.controlBoard.setCurrency(this.currency);
    };

    p.updateInfoPrizes = function(toValue) {
        this._info.updatePrizes(toValue);
    };

    p.updateCountedTokensView = function(bool){
        if(this.coinsNumber){
            this.coinsNumber.visible = !bool;
            this.eurNumber.visible = bool;
        }
    };

    p.updatePrizesView = function(bool) {
        this.prizeNumber.visible = !bool;
        this.prizeEurNumber.visible = bool;
    };

    p.updateWheelPrizes = function(toValue) {
        var multiplier = this.betManager.getMultiplier();
        this.bonusWheel.updatePrizes(toValue, multiplier);
    };

    p.setRegularStakes = function(){
        this.setStake(0);
    };

    p.setFixedStakes = function(){
        this.controlBoard.fixedStakesPanel.plugin(this);
    };

    p.setStake = function(position){
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        this._selectedStake = position;
        this.betManager.setStake();
        this.controlBoard.setBetLabelToMax(this._selectedStake === (this._stakeIncrements.length - 1));
        this.playSound("btnDownDouble");
    };

    p.getSelectedStake = function(){
        return this._stakeIncrements[this._selectedStake];
    };

    p.setStakeIncrements = function(increments){
        this._stakeIncrements = increments;
    };

    p.setStakeIncrementsMinMax = function(min,max,inc){
        var arr = [];
        for(var i = min; i <= max; i = Math.round((i+inc)*100)/100){
            arr.push(i);
        }
        this._stakeIncrements = arr;
    };

    p.setCrd = function(amount){
        this.credit = amount;
        if(this.okToShowBalance){
            this.controlBoard.setCrd(amount);
        }
    };

    p.setWin = function(amount){
        this.controlBoard.setWin(amount);
    };

    //PARAMS TO SEND
    p.getSendBetParams = function(){
        return  {bet: this.betManager.betValue()};
    };

    p.getShooterDirectionParams = function(dir){
        return  {direction: dir};
    };

    //BUTTON HANDLERS
    p.moveShooterBtnHandler = function(e) {
        this.controlBoard.checkIfHiddenMenuIsActive();
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        this.socketCommunicator.moveShooter(e);
        this.playSound("btnDown");

        this.playSoundChannelWithOptions("shooter",{});
    };

    p.stopShooterBtnHandler = function(e) {
        this.socketCommunicator.stopMovingShooter(e);
        this.playSound("btnUp");

        this.stopSoundChannel("shooter");
    };

    p.allowedShooterBtn = function(msg) {
        this.controlBoard.tryShooter();
        this.setCrd(msg.vl);
    };

    p.sendBtnHandler = function(e) {
        this.controlBoard.checkIfHiddenMenuIsActive();
        //new stakes
        this.controlBoard.fixedStakesPanel.show(false);
        if(this.betManager.canBet(this.credit,this.getSelectedStake())){
            if(this.controlBoard.btnSend._disabled)
                return;
            this.controlBoard.blockShooter(true);
            this.socketCommunicator.sendBet(this.getSendBetParams());
            //this.playSound("btnDownDouble");
            this.playSound("insertCoin",0.2);
            this.checkIfinfoIsActive();
        }else{
            this.controlBoard.blockShooter(true);
            var that = this;
            this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance",function(){
                that.controlBoard.tryShooter();
            })
        }
    };

    p.collectBtnHandler = function() {
        this.socketCommunicator.collect(this.onCollectResponse);
    };

    p.incStakeBtnHandler = function(){
        this.controlBoard.checkIfHiddenMenuIsActive();
        this.setStake(this._selectedStake + 1);
    };

    p.decStakeBtnHandler = function(){
        this.controlBoard.checkIfHiddenMenuIsActive();
        this.setStake(this._selectedStake - 1);
    };

    p.infoBtnHandler = function() {
        this.onInfo();
    };

    p.quitBtnHandler = function(){
        _instance.controlBoard.checkIfHiddenMenuIsActive();
        _instance._setQuitPanel();
    };

    p.quitConfirmed = function(){
        //if(_instance.socketCommunicator)
            //_instance.socketCommunicator.disposeCommunication();

        _instance.controlBoard.disablePlayButtons(true);
        _instance.blockControls(true);
        _instance.playSoundChain("seeYouSpoken",0.3,_instance.onQuit,_instance);
    };

    p.quitLimitReached = function(){
        this.controlBoard.disablePlayButtons(true);
        this.blockControls(true);
        this.playSound("byeSpoken",0.2);
        this.throwAlert(alteastream.Alert.INFO,"Non playing time reached...game is closed",function(){
            _instance.exitMachineGameplay();
        });
    };

    p.inactivityWarning = function(n){
        var type = n===1?"First":"Second";
        this.throwAlert(alteastream.Alert.WARNING, type+" warning...return to game");
        this.playSound("continuePlayingSpoken",0.2);
    };

    p.blockControls = function(bool){
        this.controlBoard.mouseEnabled = !bool;
    };

    p.showWin = function(amount,msgValue){
        var prize = msgValue.prize;
        //var win = msgValue.win;
        this.prizeQueue.unshift([amount,prize]);
        if(!this.queuePlaying)
            this.playQueue();
    };

    p.playQueue = function(){
        this.queuePlaying = true;
        var multiplier = this.betManager.getMultiplier();

        var iterateQueue = function(){
            if(_instance.prizeQueue.length>0){
                var lastElement = _instance.prizeQueue.pop();
                //var win = lastElement[2];
                _instance.queuePlaying = true;
                var chip = new alteastream.Chip(lastElement[0],lastElement[1],multiplier);
                var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;
                var prizeCounterPosition = _instance.prizeCounter;
                chip.x = prizeCounterPosition.x+_instance.prizeCounter.pivot;
                chip.y = _height+chip.getSize();
                _instance.addChild(chip);
                var yAnimation = (prizeCounterPosition.y-chip.getSize()*0.5)-5;

                var isEur = _instance.controlBoard._toValueView;

                //console.log("IS BONUS?::  "+_instance.isBonus);
                TweenLite.to(chip,2,{y:yAnimation,onUpdate:function () {
                        var doAnimation = _instance.controlBoard._toValueView !== isEur;
                        chip.updateChip(_instance.controlBoard._toValueView, doAnimation);
                    },onComplete:function(){
                        _instance.removeChild(chip);
                        chip = null;
                        //_instance.setWin(win);
                        if(!_instance.bonusActive){
                            _instance.okToShowBalance = true;
                            _instance.setCrd(_instance.credit);
                        }
                    },ease: Elastic.easeOut.config(0.5, 0.1)});
                _instance.showPrize(lastElement[0]);
                var prizeSpoken = "prize_"+lastElement[0];
                _instance.playSoundChain(prizeSpoken,0.3,function(){// append bonus on next soundChain?
                    _instance.queueCount++;
                    iterateQueue();
                },_instance);
            }else{
                if(_instance.queueCount > 2){
                    var phrase = "";
                    if(_instance.queueCount < 6 ) { phrase = "reallyGoodSpoken";}
                    if(_instance.queueCount > 5 && _instance.queueCount < 9 ) { phrase = "feelDizzySpoken";}
                    if(_instance.queueCount > 8 ) { phrase = "keepGoingSpoken";}
                    _instance.playSoundChain(phrase,0.3,function(){ _instance.queuePlaying = false; iterateQueue();});
                }else{
                    _instance.queuePlaying = false;

                    if(!_instance.bonusActive){
                        _instance.okToShowBalance = true;
                        _instance.setCrd(_instance.credit);
                        _instance.controlBoard.blockShooter(false);
                    }

                    /* if(_instance.bonusWon>0){
                         console.log("onBonusAllowed YES QUEUE");
                         _instance.spinForBonus(_instance.bonusWon);
                     }*/
                }
                _instance.queueCount = 0;
            }
        };
        this.playSound("win1",0.3);
        this.playSoundChain("youHaveWonSpoken",0.3,iterateQueue);
    };

    p.onMachineError = function(err){
        this.throwAlert(alteastream.Alert.ERROR,"Machine error...", function () {
            _instance.onQuit();
        });
        this.playSound("errorSpoken",0.2);
    };

    p.setIosOverlay = function() {
        this.controlBoard.mouseEnabled = false;

        var isMob = window.localStorage.getItem('isMobile') === "yes";
        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var bg = new createjs.Shape();
        bg.graphics.beginFill("#000000").drawRect(0, 0, w, h);

        var qFont = isMob ? "70px Lato" : "90px Lato"
        var qY = isMob ? 80 : 125;
        var question = new createjs.Text("Ready To Play?",qFont,"#ffffff").set({x:w/2, y:qY, textAlign:"center",textBaseline:"middle"});

        var liveShape = new createjs.Shape();
        liveShape.graphics.beginFill("#ff0000").drawRoundRect(0, 0, 76, 40, 5);
        liveShape.regX = 38;
        liveShape.regY = 20;
        liveShape.x = isMob ? 340 : 820;//340
        liveShape.y = isMob ? 230 : 500;

        var arrowsYCorrector = isMob ? 60 : 160;
        var arrowsFont = isMob ? "30px Lato" : "60px Lato";
        var liveTxt = new createjs.Text("LIVE","22px Lato","#fcfcfc").set({x:liveShape.x, y:liveShape.y+2, textAlign:"center", textBaseline:"middle"});
        var streamTxt = new createjs.Text("STREAM","22px Lato","#d10000").set({x:liveTxt.x + 105, y:liveTxt.y, textAlign:"center", textBaseline:"middle"});
        var connectedTxt = new createjs.Text("CONNECTED","22px Lato","#15d510").set({x:streamTxt.x + 130, y:liveTxt.y, textAlign:"center", textBaseline:"middle"});
        var arrowTxt = new createjs.Text(">>",arrowsFont,"#ffffff").set({x:w*0.5, y:liveTxt.y + arrowsYCorrector, textAlign:"center", textBaseline:"middle"});
        arrowTxt.rotation = 90;

        var buttonFont = isMob ? "bold 14px Lato" : "bold 20px Lato";
        var buttonFontY = isMob ? 45 : 65;
        var buttonYCorrector = isMob ? 100 : 145;
        var txt = new createjs.Text("PLAY NOW",buttonFont,"#fcfcfc");
        var playButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMediumSS),3,txt);
        playButton.centerText(0,buttonFontY);
        playButton.x = w/2;
        playButton.y = h - buttonYCorrector;

        var overlayClickCallback = function () {
            //_instance.streamVideo.videoElement.setMuted(false);
            //_instance.streamVideo.videoElement.setVolume(0.1);
            //_instance.streamVideo.videoElement.play();
            //_instance.streamVideo.setMedia();
            _instance.controlBoard.mouseEnabled = true;
            alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
        };

        var overlayOptions = {
            background:bg,
            button:playButton,
            graphics:[liveShape],
            texts:[question,liveTxt,streamTxt,connectedTxt,arrowTxt],
            clickCallback:overlayClickCallback
        };

        this.setStartOverlay(overlayOptions);
    };

    p._local_activate = function() {
        /*        this._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
                    //return "yes";
                    return "not";
                };*/
        this.test_BonusFill = function(){
            setTimeout(function(){_instance.powerUp.fill(1);},1000);
            setTimeout(function(){_instance.powerUp.fill(5);},1500);
            setTimeout(function(){_instance.powerUp.fill(6);},2000);
            setTimeout(function(){_instance.powerUp.fill(8);},2500);
            setTimeout(function(){_instance.powerUp.fill(20);},3000);
        };
        this.test_BonusSpin = function(){
            _instance.onBonusAllowed(5);
        };
        this.test_Queue = function(){
            setTimeout(function(){
                var vl = {"prize":"1","amount":50,"money":500,"balance":661000,"win":8};
                _instance.onPrizeDetection(vl)
            },500);
            setTimeout(function(){
                var vl = {"prize":"2","amount":10,"money":1000,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl)
            },1500);
            setTimeout(function(){
                console.log("START SECOND");
                var vl1 = {"prize":"3","amount":15,"money":1500,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl1);
            },2500);
            setTimeout(function(){
                console.log("START THIRD");
                var vl2 = {"prize":"4","amount":45,"money":4500,"balance":661700,"win":8};
                for(var i = 0;i<2;i++){
                    _instance.onPrizeDetection(vl2);
                }
            },3500);
        };

        this.test_BonusSpin = function(n){
            _instance.onBonusAllowed(n);
        };
        this._callInitMethods = function () {};
        alteastream.Assets.setMute(true);
        ///////////////////BONUS TEST ADD//////////////////////
        this._setControlBoard();
        //response sa bonusom
        //var response = {"balance":10000,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"};
        //response bez bonusa
        //var response = {"balance":2055700,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus","tokenValue":100.0};
        //coins "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        //marble "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        //var machineParams = "ws://87.130.112.6:8083/ws";
        //this._setStreamVideo(machineParams);
        var response = {"state":0,"key":"79a2fb56-7c2c-46cf-97d5-35db5661425e","controlIdleTime":60000,"streamendpoint":"ws://87.130.112.6:8889","webrtcurl":"ws://87.130.112.6:8083/ws","balance":9998800,"tokenValue":100.0,"tokens":0,"maxTokens":60,"currency":"EUR","payout":{"1":10,"2":25,"3":50,"4":75,"5":100,"6":250,"7":500,"8":1000,"101":1},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus"};
        this.setGameParameters(response);
        if(Object.keys(response.bonus).length === 0){/*this.streamVideo.activate(function(){this.showGame(true);});*/}
        else{this._setBonusApplet(response);}
        this.bonusWon = 10;//with bonus
        this.setPayout(response.payout);
        this.setRegularStakes();
        //new stakes
        setTimeout(function () {
            _instance.setFixedStakes();
            _instance.controlBoard.fixedStakesPanel.updateMonetary(true);
        },0)
        //this.setFixedStakes();
        this.setCurrency();
        this._addInfo();
        //this.setQueueInfo();
        //this.queueInfoContainer.visible = true;
        this.setCoinCounter();
        this.setPrizeWinCounter();
        //if(alteastream.AbstractScene.GAME_TYPE === "ticketcircus"){
        if(this.coinsCounter){
            this.coinsCounter.visible = true;
        }
        this.prizeCounter.visible = true;
        //this.addGiveUpButton(true);
        //this.giveUpButton.y+=50;
        this._setInfo();
        this._switchInfoAndWheelChildIndexes();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
        this.setMonetaryValueView(this.controlBoard._toValueView);

        this.addChild(this.controlBoard);
        this.setOverlay();
        setTimeout(function(){
            //_instance.addGiveUpButton(false);
            if(_instance._hiddenMenuButton){
                _instance._hiddenMenuButton.visible = true;
                //_instance._showHiddenMenu(true);
            }
        },2000);

/*        setTimeout(function (){
            _instance.setIosOverlay();
        },10);*/

        //_instance.test_Queue();

        // no need for now, there is no bonus wheel
        /*        var button = new createjs.Shape();
                button.graphics.beginFill('#29abe2').drawRoundRect(0,0,170,56,6);
                button.regX = 85;
                button.regY = 28;
                button.x = alteastream.AbstractScene.GAME_WIDTH*0.5;
                button.y = alteastream.AbstractScene.GAME_HEIGHT*0.5;
                this.addChild(button);
                button.cache(0,0,200,100);
                button.on("click", function () {
                    _instance.removeChild(button);
                    ///////////////////BONUS TEST START//////////////////////
                    _instance.test_BonusSpin(_instance.bonusWon);
                    ///////////////////BONUS TEST END//////////////////////
                    ///////////////////QUEUE TEST START//////////////////////
                    //_instance.test_Queue();// includes spin if bonusWon >0
                    ///////////////////QUEUE TEST END//////////////////////
                });*/

        //5 = blue 0.50, tag id 1
        //10 = green 1, tag id 2
        //15 = pink 2, tag id 3
        //45 = orange 5, tag id 4
        //90 = yellow 10, tag id 5

        //NEW PAYOUTS::::::::::::::::COINS::::::::::::::::::::::::
        //one coin = 0.10
        //0.25 = metal 0.025,    tag id 0

        //5 = blue 0.50,         tag id 1
        //10 = green/black 1.00, tag id 2
        //15 = red 15.00,        tag id 3
        //45 = yellow 4.50,      tag id 4


        // "TOKENS DETECTION" msg  - Only available in Cash Circus and Cash Festival machines (COINS)

        //NEW PAYOUTS::::::::::::::::MARBLE::::::::::::::::::::::::
        //one pearl = 0.10
        //5 = blue 0.50,             tag id 1
        //15 = green/black 1.00,     tag id 2
        //40 = red 4.00,             tag id 3
        //100 = yellow 10.00,        tag id 4

        //this.test_Queue();

        ///////TOKENS COUNTED TEST START
        var c = 0;
        var max = 5;
        var tokensFalling = function(){
            _instance.intervalSpawn = setInterval(function () {
                //var c = Math.round(Math.random()*20);
                if(c<max){
                    var n = 1;
                    //_instance.refundCoinSpawn({"ts":1578476179098,"vl":{"tokens":n,"amount":10,"money":1100,"balance":1028000},"tp":6,"cat":50});
                    _instance.refundCoinSpawn(10);
                    c++;
                }else{
                    c = 0;
                    max = 10;
                    clearInterval(_instance.intervalSpawn);
                   setTimeout(function(){
                       console.log("NEW TOKENS FELL");
                       tokensFalling();
                       },500) ;
                }
            },500);
        };
        // show machine name , uncomment
        //this.setMachineNameText();
        //tokensFalling();
        ///////TOKENS COUNTED TEST END

        // BONUS SPAWN TEST START
        //do comment _instance.endBonusFeature();
        //coin.x = 960;
        //coin.y = 600;
        //_instance._bonusCoinSpawn(10);
        // BONUS SPAWN TEST END
    };

// static methods:
// public methods:

    alteastream.CoinPusher = createjs.promote(CoinPusher,"AbstractScene");
})();
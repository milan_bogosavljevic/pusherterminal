
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PreloaderComponentBar = function(stage,isDesktop,addPercentageText) {
        this.initialize(stage,isDesktop,addPercentageText);
    };

    var p = PreloaderComponentBar.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage,isDesktop,addPercentageText) {
        this.PreloaderComponent_initialize(stage);
        // sredi koordinate
        var props = isDesktop === true ?
            {frameWidth:596, frameHeight:13, frameRoundness:6, font:"14px Arial", txtY:22, txt2Y:50, barWidth:592, barHeight:11, barMargin:2} :
            {frameWidth:298, frameHeight:7, frameRoundness:3, font:"10px Arial", txtY:13, txt2Y:30, barWidth:296, barHeight:6, barMargin:1};

        var shape1 = new createjs.Shape();
        shape1.graphics.beginFill("#ffffff").drawRoundRect(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);
        shape1.cache(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#ffcb05").drawRect(0, 0, 0, 0);
        mask.cache(0, 0, 0, 0);

        var text = new createjs.Text("LOADING...", props.font, "white");
        text.x = props.frameWidth/2;
        text.y = props.txtY;
        text.textAlign = "center";
        var cacheBounds = text.getBounds();
        text.cache(cacheBounds.x, cacheBounds.y, cacheBounds.width, cacheBounds.height);

        var r = 255;
        var g = 203;
        var b = 4;
        var start = 120;

        this.addChild(shape1);
        this.addChild(mask);
        this.addChild(text);

        var that = this;

        if(addPercentageText) {
            var textPercentage = new createjs.Text("%", props.font, "white");
            textPercentage.x = props.frameWidth/2 + 2;
            textPercentage.y = props.txt2Y;
            textPercentage.textAlign = "left";

            var textPercentageNumber = new createjs.Text("0", props.font, "white");
            textPercentageNumber.x = props.frameWidth/2 - 2;
            textPercentageNumber.y = props.txt2Y;
            textPercentageNumber.textAlign = "right";

            this.addChild(textPercentageNumber,textPercentage);

            this.addOnProgressChangeAction(function() {
                textPercentageNumber.text = Math.round(that.getProgressPercent());
            });
        }

        this.addOnProgressChangeAction(function() {
            mask.uncache();
            mask.graphics.clear();
            var progress = props.barWidth*that.getProgress();
            var tmpR = Math.round(start + (r-start) * that.getProgress());
            var tmpG = Math.round(start + (g-start) * that.getProgress());
            var tmpB = Math.round(start + (b-start) * that.getProgress());
            mask.graphics.beginFill("rgb(" + tmpR + "," + tmpG + "," + tmpB + ")").drawRect(props.barMargin, props.barMargin, progress, props.barHeight);
            mask.cache(0,0,progress,props.barHeight);
        });
    };

// static methods:
// public methods:
// private methods:

    alteastream.PreloaderComponentBar = PreloaderComponentBar;
})();
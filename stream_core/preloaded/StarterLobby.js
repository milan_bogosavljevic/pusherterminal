
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var StarterLobby = function(locationSearch,gameId) {
        this.ABSStarter_constructor(locationSearch,gameId);
        this.initialize_StarterLobby();
    };
    var p = createjs.extend(StarterLobby, alteastream.ABSStarter);

// static properties:
    StarterLobby.PREVIEW_SERVER = "";
// public properties:
// private properties:
// static methods:

// constructor:
    p.initialize_StarterLobby = function() {
        StarterLobby.PREVIEW_SERVER = this._ipAdress.split(".")[0] === "staging" ? "https://staging-preview.sungamer.com" : "https://preview.sungamer.com";
    };

// public methods:
    p.pingServer_super = p.pingServer;
    p.pingServer = function(){
        this.pingServer_super();
        //var port = ":8080";//staging
        var port = "";
        //alteastream.AbstractScene.SERVER_URL = this._protocol+this._serverUrl+port;
        //alteastream.AbstractScene.SESSION_TOKEN = jwt_decode(this._usr).token;

        // novo za invalid session start
        if(this._usr === "undefined"){
            var systemAlert = new alteastream.Alert(2,"Invalid Session");
            stage.addChild(systemAlert);

            setTimeout(function(){
                console.warn("dinamicka adresa i putanja za live " + window.location.origin + "/demo");
                window.parent.location.href = decodeURIComponent("https://staging.sungamer.com/demo");
            },3000);
        }else{
            alteastream.AbstractScene.SERVER_URL = this._protocol+this._serverUrl+port;
            alteastream.AbstractScene.SESSION_TOKEN = jwt_decode(this._usr).token;
            window.doLoad();
        }
        // novo za invalid session end
    };

    alteastream.StarterLobby = createjs.promote(StarterLobby,"ABSStarter");
})();
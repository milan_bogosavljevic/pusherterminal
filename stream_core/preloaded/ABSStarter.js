
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var ABSStarter = function(locationSearch,gameId) {
        this.initialize(locationSearch,gameId);
    };
    var p = ABSStarter.prototype;
    var _instance = null;

// static properties:
// events:
// public properties:

// private properties:
    p._url = null;
    p._usr = null;
    p._serverUrl = null;
    p._game = null;

    p._lang = null;
    p._game_id = null;
    p._selectedGame = null;
    p._numOfParams = 0;
    p._params = null;
    p._suffix = "";
    p._gameName = null;

// constructor:
    p.initialize = function(locationSearch,gameId) {
        _instance = this;
        //var _supportedLanguages = ["en","sr"];
        this._game_id = gameId;
        this._protocol = locationSearch.split("?")[0].split("//")[0]+"//";
        this._url = locationSearch.split("?")[0].split("//")[1].split("/")[0];
        this._ipAdress = this._url.split(":")[0];
        this._port = ":"+this._url.split(":")[1];
        this._params = locationSearch.split("?")[1].split("&");
        
        for(var i = 0; i < this._params.length; i++) {
            var param = this._params[i].split("=");
            this["_"+param[0]] = param[1];
        }

        this._selectedGame = this._game_id;

        console.log("protocol = "+this._protocol);
        console.log("ip = "+this._ipAdress);
        console.log("port = "+this._port);
        console.log("url = "+this._url);// not needed
        console.log("full url = "+this._protocol+this._ipAdress+this._port);
        console.log("gameUrl = "+this._protocol+this._ipAdress);
        console.log("serverUrl = "+this._serverUrl);
        console.log("token = "+this._usr);
        console.log("machine name= "+this._machine_name);
        console.log("ID = "+this._game);
        console.log("game name = "+this._gameName);
    };

// static methods:
    ABSStarter.getInstance = function() {
        return _instance;
    };

    p.safariResize = function(){
        var deviceWidth = (window.parent.innerWidth > 0) ? window.parent.innerWidth : screen.width;
        var deviceHeight = (window.parent.innerHeight > 0) ? window.parent.innerHeight : screen.height;

        var innerWidth = deviceWidth + "px";
        var isLandscape = deviceWidth > deviceHeight;
        var topDocument = window.parent.document;

        if(topDocument.getElementById("image") || topDocument.getElementById("scrollDiv")){
            var im = topDocument.getElementById("image");
            var dv = topDocument.getElementById("scrollDiv");
            im.parentNode.removeChild(im);
            dv.parentNode.removeChild(dv);
        }

        if(isLandscape && window.parent.orientation !== 0 ){
            var ratio;
            if(deviceWidth/deviceHeight > 2.1){
                ratio = 19.5/9;
            }else if(deviceWidth/deviceHeight < 1.5){
                ratio = 4/3;
            }else{
                ratio = 16/9;
            }

            if(deviceWidth/deviceHeight > ratio+0.01){
                var div = document.createElement("div");
                div.id = "scrollDiv";

                var img = document.createElement("IMG");
                img.src = "swipeUp.png";
                img.id = "image";

                img.style.cssText = 'position:fixed;top:50%;left:50%;width:384px;height:217px;margin-left:-192px;margin-top:-108px;overflow-y:hidden';
                topDocument.body.appendChild(img);

                topDocument.body.appendChild(div);
                var divHeight = (deviceHeight + 100) + "px";
                div.style.cssText = 'position:absolute;top:50px;left:0px;width:'+innerWidth+';height:'+divHeight;
            }
        }
        // debug log for iphone
        //if(logText){
        //logText.text += "\n";
        //logText.text += "AR W = " + aspect.aW + " AR H = " + aspect.aH+" dwh: "+dwh;
        //logText.text += "ARW = " + aspect.aW + " ARH = " + aspect.aH+" dw: "+deviceWidth+" dh: "+deviceHeight;
        //logText.text += "ARW = " + aspect.aW + " DH = " + deviceHeight + "WinH "+window.top.innerHeight;
        //}
    };

    p.requestFullScreen = function(){
        var targetElement = window.parent.document.documentElement;
        var inFS = window.parent.document.fullscreenElement || window.parent.document.webkitFullscreenElement;
        var requestFS = targetElement.requestFullscreen || targetElement.webkitRequestFullscreen;
        if(!inFS){
            requestFS.call(targetElement);
        }
    };

    p.setResizeListener = function(){
        var that = this;
        if(createjs.Touch.isSupported()) {
            if(is.not.ios()){
                stage.addEventListener("click",function doResize(){
                    stage.removeEventListener("click",doResize);
                    that.requestFullScreen();
                });
                this.checkFS();
            }else{
                if(is.safari()){
                    if(is.ipad()){
                        stage.addEventListener("click",function doResize(){
                            stage.removeEventListener("click",doResize);
                            that.requestFullScreen();
                        });
                        that.checkFS();
                    }else{
                        window.parent.addEventListener("resize",this.safariResize);
                    }
                }
            }
        }
    };

    p.checkFS = function(){
        var that = this;
        $(window.parent.document).on('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e)    {
            if (!window.screenTop && !window.screenY) {
                console.log('not fullscreen');
                stage.addEventListener("click",function doResize(){
                    stage.removeEventListener("click",doResize);
                    that.requestFullScreen();
                });
            } else {
                console.log('fullscreen');
            }
        });
    };

    p.firstCall = function(){
        //window.doLoad();// novo za invalid session
        this.pingServer();
    };

    p.finished = function(){
        //clearInterval(this._timer);
        alteastream.Assets.getQueue().removeAllEventListeners("complete");
        alteastream.Assets.getQueue().removeAllEventListeners("progress");
    };

    p._setLanguage = function(lang,supported){
        if(supported.indexOf(lang)>-1){
            this._lang = supported[supported.indexOf(lang)];
        }else{
            this._lang = "en";
        }
        console.log("loaded gameLanguage: "+this._lang);
    };

    p.getLanguage = function(){
        return this._lang === null? "en" : this._lang;
    };

    p.getGameName = function(){
        return this._selectedGame+this._suffix;
    };

    p.getGameCode = function(){
        return this._gameCode;
    };

    p.getGameAssets = function(){
        return this._selectedGame+"Assets";
    };

    p.pingServer = function(){
        console.log("type:: "+this.getGameName());
        var constVar = alteastream.AbstractScene;
        constVar.GAME_URL = this._protocol+this._url;
        constVar.GAME_ID = this._machine_name || "";//machine_name
        constVar.GAME_NAME = this._game || "";//game
        constVar.GAME_PROTOCOL = this._protocol;
        constVar.SERVER_IP = this._ipAdress;
        constVar.GAME_TOKEN = this._usr;
        var backToHouse = window.localStorage.getItem('backToHouse') || -1;
        constVar.BACK_TO_HOUSE = Number(backToHouse);
        constVar.GAME_QUIT = window.localStorage.getItem('quitUrl');
        constVar.MACHINE_NAME = this._gameName;
    };

// private methods:

    alteastream.ABSStarter = ABSStarter;
})();
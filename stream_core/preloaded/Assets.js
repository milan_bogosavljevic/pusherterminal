
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var Assets = function() {};

// static private properties:
    var _queue = null;
    var _texts = {};
    var _game = {};
    var _lang = "";
    var count = 0;
    var soundStorage = [];


// static public properties:

    Assets.VERSION = "2.7";

    //images
    Assets.images = {};
    Assets.images.ticketCircusType = "ticketCircusType";
    Assets.images.cashFestivalType = "cashFestivalType";
    Assets.images.diamondsAndPearlsType = "diamondsAndPearlsType";
    Assets.images.trialSmall = "trialSmall";//temp trial
    Assets.images.trialBig = "trialBig";//temp trial
    Assets.images.preloadingPoster = "preloadingPoster";
    Assets.images.liveIcon = "liveIcon";
    Assets.images.window = "window";
    Assets.images.windowList = "windowList";
    Assets.images.window_big = "window_big";
    Assets.images.beam = "beam";
    Assets.images.flare = "flare";
    Assets.images.thumb = "thumb";
    Assets.images.noThumb = "noThumb";
    Assets.images.machineBg = "machineBg";
    Assets.images.machineOverlay = "machineOverlay";
    Assets.images.spinner = "spinner";
    Assets.images.seatOn = "seatOn";
    Assets.images.seatOff = "seatOff";
    Assets.images.mainBackground = "mainBackground";
    Assets.images.mainBackgroundWithHole = "mainBackgroundWithHole";
    Assets.images.mainBackgroundOverlay = "mainBackgroundOverlay";
    Assets.images.mainBackgroundOverlay2 = "mainBackgroundOverlay2";
    Assets.images.infoBackground = "infoBackground";
    Assets.images.infoPage1Image = "infoPage1Image";
    Assets.images.infoPage2Image = "infoPage2Image";
    Assets.images.infoPage3Image = "infoPage3Image";
    Assets.images.infoBackgroundNoBonus = "infoBackgroundNoBonus";
    Assets.images.floor = "floor";
    Assets.images.houseClosed = "houseClosed";
    Assets.images.smallHouse = "smallHouse";
    Assets.images.smallHouseCurrent = "smallHouseCurrent";
    Assets.images.house = "house";
    Assets.images.houseHighlight = "houseHighlight";
    Assets.images.infoImage = "infoImage";
    Assets.images.tooltipImage = "tooltipImage";
    Assets.images.popupBg = "popupBg";
    Assets.images.introVideo = "introVideo";
    Assets.images.cursorTgt = "cursorTgt";
    Assets.images.mainLogo = "mainLogo";
    Assets.images.refundComponentBackground = "refundComponentBackground";
    // Hajko novo added
    Assets.images.lampYellow = "lampYellow";
    Assets.images.lampYellowDark = "lampYellowDark";
    Assets.images.lampGreen = "lampGreen";
    Assets.images.lampGreenLight = "lampGreenLight";
    Assets.images.lampRed = "lampRed";
    Assets.images.listMachineBackground = "listMachineBackground";

    //buttons
    Assets.images.infoButton = "infoButton";
    Assets.images.infoButtonLeft = "infoButtonLeft";
    Assets.images.infoButtonRight = "infoButtonRight";
    Assets.images.confirmQuitBtn = "confirmQuitBtn";
    Assets.images.btnQuitSS = "btnQuitSS";
    Assets.images.btnInfoSS = "btnInfoSS";
    //Assets.images.btnBackSS = "btnBackSS";
    Assets.images.soundOn = "soundOn";
    Assets.images.soundOff = "soundOff";
    Assets.images.soundMachineOn = "soundMachineOn";
    Assets.images.soundMachineOff = "soundMachineOff";
    Assets.images.btnFullScreen_On = "btnFullScreen_On";
    Assets.images.btnFullScreen_Off = "btnFullScreen_Off";
    Assets.images.btnPlayerActivity = "btnPlayerActivity";
    Assets.images.btnShowAllMachinesActive = "btnShowAllMachinesActive";
    Assets.images.btnShowAllMachinesNotActive = "btnShowAllMachinesNotActive";
    Assets.images.btnShowAvailableMachinesActive = "btnShowAvailableMachinesActive";
    Assets.images.btnShowAvailableMachinesNotActive = "btnShowAvailableMachinesNotActive";
    Assets.images.btnQuitNo = "btnQuitNo";
    Assets.images.btnQuitYes = "btnQuitYes";

    Assets.images.backgroundCB = "backgroundCB";
    //Assets.images.btnMockSS = "btnMockSS";
    Assets.images.btnUp = "btnUp";
    Assets.images.btnDown = "btnDown";
    Assets.images.btnPrev = "btnPrev";
    Assets.images.btnNext = "btnNext";
    Assets.images.btnIncSS = "btnIncSS";
    Assets.images.btnDecSS = "btnDecSS";
    Assets.images.btnFixedBet = "btnFixedBet";
    Assets.images.btnLargeSS = "btnLargeSS";
    Assets.images.btnMediumSS = "btnMediumSS";
    Assets.images.btnMedium2SS = "btnMedium2SS";
    Assets.images.btnArrowLeftSS = "btnArrowLeftSS";
    Assets.images.btnArrowRightSS = "btnArrowRightSS";
    Assets.images.btnBetView = "btnBetView";
    Assets.images.arrowButtonsBackground = "arrowButtonsBackground";
    Assets.images.betBackground = "betBackground";
    Assets.images.hiddenMenuBackground = "hiddenMenuBackground";
    Assets.images.queueInfoBg = "queueInfo_bg";
    Assets.images.controlsBg = "controlsBg";

    Assets.images.btnEnter = "btnEnter";
    Assets.images.btnStart = "btnStart";
    Assets.images.btnStart2 = "btnStart2";
    Assets.images.btnBackToGame = "btnBackToGame";
    Assets.images.btnLeave = "btnLeave";
    Assets.images.btnBack = "btnBack";
    Assets.images.btnBack2 = "btnBack2";
    Assets.images.btnBackForEntranceCounter = "btnBackForEntranceCounter";
    Assets.images.btnExitForQuitCounter = "btnExitForQuitCounter";
    Assets.images.quitCounterBg = "quitCounterBg";
    Assets.images.btnParticipate = "btnParticipate";
    Assets.images.btnStopParticipate = "btnStopParticipate";
    Assets.images.separator = "separator";
    Assets.images.btnStakeDec = "btnStakeDec";
    Assets.images.btnStakeInc = "btnStakeInc";

    Assets.images.btnHiddenMenu = "btnHiddenMenu";
    Assets.images.btnHiddenMenuTransparent = "btnHiddenMenuTransparent";
    Assets.images.btnHiddenMenuTransparentBlack = "btnHiddenMenuTransparentBlack";

    //Assets.images.singleShotTextImg = "singleShotTextImg";
    //Assets.images.burstShotTextImg = "burstShotTextImg";


    //roulette start
    Assets.images.btnStepBackSS = "btnStepBack";
    Assets.images.btnClearSS = "btnClear";
    Assets.images.fieldMini = "fieldMini";
    Assets.images.fieldMiniH = "fieldMiniH";
    Assets.images.fieldSpec = "fieldSpec";
    Assets.images.fieldSpecH = "fieldSpecH";
    Assets.images.fieldSmall = "fieldSmall";
    Assets.images.fieldSmall_red = "fieldSmall_red";
    Assets.images.fieldSmall_black = "fieldSmall_black";
    Assets.images.fieldSmall_green = "fieldSmall_green";
    Assets.images.fieldMedium = "fieldMedium";
    Assets.images.fieldLarge = "fieldLarge";
    Assets.images.fieldZero = "fieldZero";
    Assets.images.fieldSplit = "fieldSplit";
    Assets.images.fieldSplitTest = "fieldSplitTest";// temp dev visibility test only
    Assets.images.fieldSmallH = "fieldSmallH";
    Assets.images.fieldMediumH = "fieldMediumH";
    Assets.images.fieldLargeH = "fieldLargeH";
    Assets.images.fieldZeroH = "fieldZeroH";
    Assets.images.label_red = "label_red";
    Assets.images.label_black = "label_black";
    Assets.images.fieldTrack = "fieldTrack";
    Assets.images.fieldTrack_black = "fieldTrack_black";
    Assets.images.fieldTrack_red = "fieldTrack_red";
    Assets.images.fieldTrack_green = "fieldTrack_green";
    Assets.images.fieldTrackH = "fieldTrackH";
    Assets.images.winningBg_red = "winningBg_red";
    Assets.images.winningBg_black = "winningBg_black";
    Assets.images.winningBg_green = "winningBg_green";

    Assets.images.welcomeBackBackground = "welcomeBackBackground";
    Assets.images.poster = "poster";

    Assets.images.chip0 = "chip0";
    Assets.images.chip1 = "chip1";
    Assets.images.chip2 = "chip2";
    Assets.images.chip3 = "chip3";
    Assets.images.chip4 = "chip4";
    Assets.images.chip5 = "chip5";
    Assets.images.chip6 = "chip6";
    Assets.images.chip7 = "chip7";

    Assets.images.chipBtn0 = "chipBtn0";
    Assets.images.chipBtn1 = "chipBtn1";
    Assets.images.chipBtn2 = "chipBtn2";
    Assets.images.chipBtn3 = "chipBtn3";
    Assets.images.chipBtn4 = "chipBtn4";
    Assets.images.chipBtn5 = "chipBtn5";
    Assets.images.chipBtn6 = "chipBtn6";
    Assets.images.chipBtn7 = "chipBtn7";

    //roulette end

    Assets.images.targetArea = "targetArea";

    Assets.images.metalCoin = "metalCoin";
    Assets.images.sunLogo = "sunLogo";
    Assets.images.coinWin = "coinWin";
    Assets.images.chip_glow = "chip_glow";
    Assets.images.chip_1 = "chip_1";
    Assets.images.chip_2 = "chip_2";
    Assets.images.chip_3 = "chip_3";
    Assets.images.chip_4 = "chip_4";
    Assets.images.chip_5 = "chip_5";
    Assets.images.chip_6 = "chip_6";
    Assets.images.chip_7 = "chip_7";
    Assets.images.chip_8 = "chip_8";
    Assets.images.chip_9 = "chip_9";
    Assets.images.chip_10 = "chip_10";
    Assets.images.chip_11 = "chip_11";
    Assets.images.chip_12 = "chip_12";
    Assets.images.chip_0 = "chip_0";
    Assets.images.chip_1_back = "chip_1_back";
    Assets.images.chip_2_back = "chip_2_back";
    Assets.images.chip_3_back = "chip_3_back";
    Assets.images.chip_4_back = "chip_4_back";
    Assets.images.chip_5_back = "chip_5_back";
    Assets.images.chip_6_back = "chip_6_back";
    Assets.images.chip_7_back = "chip_7_back";
    Assets.images.chip_8_back = "chip_8_back";
    Assets.images.chip_9_back = "chip_9_back";
    Assets.images.chip_10_back = "chip_10_back";
    Assets.images.chip_11_back = "chip_11_back";
    Assets.images.chip_12_back = "chip_12_back";
    Assets.images.chip_0_back = "chip_0_back";
    Assets.images.lampOn = "lampOn";
    Assets.images.lampOff = "lampOff";
    Assets.images.arrow = "arrow";
    Assets.images.wheelBg = "wheelBg";
    Assets.images.wheel_8_bonus_fields_highlight = "wheel_8_bonus_fields_highlight";
    Assets.images.bounsWonBgImage = "bounsWonBgImage";
    Assets.images.bounsWonBgImageRing = "bounsWonBgImageRing";
    Assets.images.animatedLogo = "animatedLogo";
    Assets.images.liveStreamConnecting = "liveStreamConnecting";

    // Sounds
    Assets.sounds = {};
    Assets.sounds.shooter = "shooter";
    Assets.sounds.shooter2 = "shooter2";
    Assets.sounds.insertCoin = "insertCoin";
    Assets.sounds.refund = "refund";
    Assets.sounds.shooterStop = "shooterStop";
    Assets.sounds.bgMusic = "bgMusic";
    Assets.sounds.bgMusicMachine = "bgMusicMachine";
    Assets.sounds.btnUp = "btnUp";
    Assets.sounds.btnDown = "btnDown";
    Assets.sounds.btnDownDouble = "btnDownDouble";
    Assets.sounds.win1 = "win1";
    Assets.sounds.error = "error";
    Assets.sounds.info = "info";
    Assets.sounds.warning = "warning";
    Assets.sounds.exception = "exception";
    Assets.sounds.confirm = "confirm";
    Assets.sounds.welcomeMessageSpoken = "welcomeMessageSpoken";
    Assets.sounds.errorSpoken = "errorSpoken";
    Assets.sounds.continuePlayingSpoken = "continuePlayingSpoken";
    Assets.sounds.byeSpoken = "byeSpoken";
    Assets.sounds.youHaveWonSpoken = "youHaveWonSpoken";
    Assets.sounds.prizeDetect4 = "prizeDetect4";
    Assets.sounds.shootCoinsSpoken = "shootCoinsSpoken";
    Assets.sounds.keepGoingSpoken = "keepGoingSpoken";
    Assets.sounds.feelDizzySpoken = "feelDizzySpoken";
    Assets.sounds.reallyGoodSpoken = "reallyGoodSpoken";
    Assets.sounds.seeYouSpoken = "seeYouSpoken";

    Assets.sounds.prize_1 = "prize_1";
    Assets.sounds.prize_2 = "prize_2";
    Assets.sounds.prize_3 = "prize_3";
    Assets.sounds.prize_5 = "prize_5";
    Assets.sounds.prize_10 = "prize_10";
    Assets.sounds.prize_15 = "prize_15";
    Assets.sounds.prize_20 = "prize_20";
    Assets.sounds.prize_25 = "prize_25";
    Assets.sounds.prize_30 = "prize_30";
    Assets.sounds.prize_35 = "prize_35";
    Assets.sounds.prize_40 = "prize_40";
    Assets.sounds.prize_45 = "prize_45";
    Assets.sounds.prize_50 = "prize_50";
    Assets.sounds.prize_55 = "prize_55";
    Assets.sounds.prize_60 = "prize_60";
    Assets.sounds.prize_65 = "prize_65";
    Assets.sounds.prize_70 = "prize_70";
    Assets.sounds.prize_75 = "prize_75";
    Assets.sounds.prize_80 = "prize_80";
    Assets.sounds.prize_85 = "prize_85";
    Assets.sounds.prize_90 = "prize_90";
    Assets.sounds.prize_95 = "prize_95";
    Assets.sounds.prize_100 = "prize_100";
    Assets.sounds.bonusInc = "bonusInc";
   // Assets.sounds.bonusSpinSound = "bonusSpinSound";
    Assets.sounds.bonusWinnerSpoken_1 = "bonusWinnerSpoken_1";
    Assets.sounds.bonusWinnerSpoken_2 = "bonusWinnerSpoken_2";
    Assets.sounds.bonusSpinLoop = "bonusSpinLoop";
    Assets.sounds.bonusWord = "bonusWord";

    Assets.sounds.buttonClick = "buttonClick";
    Assets.sounds.winSound = "winSound";
    Assets.sounds.winCheer = "winCheer";
    Assets.sounds.buttonCollect = "buttonCollect";
    Assets.sounds.countLoop = "countLoop";
    Assets.sounds.chipDown = "chipDown";

    Assets.sounds.spoken_0 = "spoken_0";
    Assets.sounds.spoken_1 = "spoken_1";
    Assets.sounds.spoken_2 = "spoken_2";
    Assets.sounds.spoken_3 = "spoken_3";
    Assets.sounds.spoken_4 = "spoken_4";
    Assets.sounds.spoken_5 = "spoken_5";
    Assets.sounds.spoken_6 = "spoken_6";
    Assets.sounds.spoken_7 = "spoken_7";
    Assets.sounds.spoken_8 = "spoken_8";
    Assets.sounds.spoken_9 = "spoken_9";
    Assets.sounds.spoken_10 = "spoken_10";
    Assets.sounds.spoken_11 = "spoken_11";
    Assets.sounds.spoken_12 = "spoken_12";
    Assets.sounds.spoken_13 = "spoken_13";
    Assets.sounds.spoken_14 = "spoken_14";
    Assets.sounds.spoken_15 = "spoken_15";
    Assets.sounds.spoken_16 = "spoken_16";
    Assets.sounds.spoken_17 = "spoken_17";
    Assets.sounds.spoken_18 = "spoken_18";
    Assets.sounds.spoken_19 = "spoken_19";
    Assets.sounds.spoken_20 = "spoken_20";
    Assets.sounds.spoken_21 = "spoken_21";
    Assets.sounds.spoken_22 = "spoken_22";
    Assets.sounds.spoken_23 = "spoken_23";
    Assets.sounds.spoken_24 = "spoken_24";
    Assets.sounds.spoken_25 = "spoken_25";
    Assets.sounds.spoken_26 = "spoken_26";
    Assets.sounds.spoken_27 = "spoken_27";
    Assets.sounds.spoken_28 = "spoken_28";
    Assets.sounds.spoken_29 = "spoken_29";
    Assets.sounds.spoken_30 = "spoken_30";
    Assets.sounds.spoken_31 = "spoken_31";
    Assets.sounds.spoken_32 = "spoken_32";
    Assets.sounds.spoken_33 = "spoken_33";
    Assets.sounds.spoken_34 = "spoken_34";
    Assets.sounds.spoken_35 = "spoken_35";
    Assets.sounds.spoken_36 = "spoken_36";
    Assets.sounds.spoken_welcome = "spoken_welcome";
    Assets.sounds.spoken_youWin_1 = "spoken_youWin_1";
    Assets.sounds.spoken_youWin_2 = "spoken_youWin_2";
    Assets.sounds.spoken_youWin_3 = "spoken_youWin_3";
    Assets.sounds.spoken_youWin_4 = "spoken_youWin_4";
    Assets.sounds.spoken_placeBet = "spoken_placeBet";
    Assets.sounds.spoken_placeBet_2 = "spoken_placeBet_2";
    Assets.sounds.spoken_maxReached = "spoken_maxReached";
    Assets.sounds.spoken_clearedBets = "spoken_clearedBets";
    Assets.sounds.spoken_doSpin_1 = "spoken_doSpin_1";
    Assets.sounds.spoken_doSpin_2 = "spoken_doSpin_2";
    Assets.sounds.spoken_doSpin_3 = "spoken_doSpin_3";
    Assets.sounds.spoken_last5 = "spoken_last5";
    Assets.sounds.spoken_winningNumber_1 = "spoken_winningNumber_1";
    Assets.sounds.spoken_winningNumber_2 = "spoken_winningNumber_2";
    Assets.sounds.spoken_noWin_1 = "spoken_noWin_1";
    Assets.sounds.spoken_noWin_2 = "spoken_noWin_2";


    // Texts
    Assets.texts = {};
    Assets.texts.infoText1 = "infoText1";
    Assets.texts.infoText2 = "infoText2";
    Assets.texts.infoText3 = "infoText3";
    Assets.texts.infoText4 = "infoText4";
    Assets.texts.infoText5 = "infoText5";
    Assets.texts.infoText6 = "infoText6";
    Assets.texts.infoText7 = "infoText7";
    Assets.texts.infoText8 = "infoText8";
    Assets.texts.sessionDoesNotExist = "Session does not exist";
    Assets.texts.casinoServiceUnreachable = "CasinoServiceUnreachable";

    //imageTexts
    Assets.imgTexts = {};
    Assets.imgTexts.spinTxtImg = "spinTxtImg";
    Assets.imgTexts.stopTxtImg = "stopTxtImg";
    Assets.imgTexts.powerTxtImg = "powerTxtImg";

// events:
// public properties:
// private properties:
// constructor:
// static methods:
    Assets.getQueue = function(){
        return _queue;
    };

    Assets.setImageLang = function(lang){
        _lang = lang;
    };

    Assets.getImageLang = function() {
        return _lang;
    };

    Assets.setAtlas = function(classIs){
        _game = classIs;
    };

    Assets.getAtlas = function(assetId) {
        return _game[assetId];
    };

    Assets.load = function(loadManifest, handleComplete, handleProgress) {
        _queue = new createjs.LoadQueue(false);
        createjs.Sound.alternateExtensions = ["mp3"];
        _queue.installPlugin(createjs.Sound);

        if(handleComplete)
            _queue.addEventListener("complete", handleComplete);
        if(handleProgress)
            _queue.addEventListener("progress", handleProgress);

        //Assets._listDebug(_queue);

        _queue.loadManifest(loadManifest);
    };

    Assets.getSoundLength = function(sound) {
        return  Math.round(_queue.getResult(sound).duration)*1000;
    };

    Assets.getSoundsLoadedDebug = function(){
        return debugText;
    };

    Assets.setTexts = function(texts) {
        _texts = texts;
    };

    Assets.getText = function(assetId) {
        return _texts[assetId];
    };

    Assets.getLoadProgress = function() {
        return _queue.progress;
    };

    Assets.getAtlasImage = function ( atlas, byName ) {
        var jsonData = atlas+"AtlasData";
        //console.log("trazi Atlas "+jsonData);
        //console.log("sa image_om "+byName);
        var imgName = Assets.chooseImg(byName);
        //console.log("imgName je "+imgName);
        var _atlasData = _queue.getResult(jsonData);
        //console.log("_atlasData "+_atlasData);
        var imageID = _atlasData.animations[ imgName ][0];
        var rectarr = _atlasData.frames[ imageID ];
        var rect = new createjs.Rectangle( );
        rect.x = rectarr[0];
        rect.y = rectarr[1];
        rect.width = rectarr[2];
        rect.height = rectarr[3];

        var currentAtlas = atlas+"Atlas";
        var bmp = new createjs.Bitmap( _queue.getResult(currentAtlas) );
        bmp.sourceRect = rect;
        bmp.width = rect.width;
        bmp.height = rect.height;

        return bmp;
    };

    //our images dir image
    Assets.getThumbnail = function(machineName,holder,w,h,callback){
        var image = new Image();
        image.src = "../images/"+machineName+".jpg?ver="+Math.random();
        //image.src = alteastream.StarterLobby.PREVIEW_SERVER + "/images/" + machineName + ".jpg?ver="+Math.random();
        image.crossOrigin = "";
        holder.hasThumbImage = true;

        image.onerror = function(){
            //image.src = "../images/errThumb.jpg";
            holder.hasThumbImage = false;
            callback();
        };
        image.onload = function(){
            var thumb = new createjs.Bitmap(image);
            //thumb.image.crossOrigin = "Anonymous";
            holder.image = thumb.image;
            callback();
        };
    };

    Assets.chooseImg = function(choose){
        return Assets.getAtlas(choose);
    };

    Assets.getImage = function(assetId) {
        return new createjs.Bitmap(_queue.getResult(assetId));
    };

    Assets.getFont = function(assetId) {
        console.log("asset "+assetId);
        return _queue.getResult(assetId);
    };

    Assets.getFontImage = function(assetId) {
        console.log("asset image "+assetId);
        return _queue.getResult(assetId);
    };

    Assets.getImageURI = function(assetId) {
        return _queue.getResult(assetId);
    };

    //Sound
    Assets.playSound = function(assetId,vol) {
        var sound = createjs.Sound.play(assetId);
        sound.volume = vol||1;
        return sound;
    };

    Assets.playSoundChain = function(assetId,vol,callback,scope) {//temp, just test
        var sound = createjs.Sound.play(assetId);
        sound.volume = vol||1;
        if(sound.duration > 0){
            sound.on("complete", callback, scope);
        }else{// temp fix for prize chips with no sound files
            setTimeout(function (){
                callback();
            },1000);
        }
    };

    Assets.stopSoundChannel = function(instance) {
        for(var i = 0,j=soundStorage.length;i<j;i++){
            if(soundStorage[i].name === instance){
                soundStorage[i].stop();
                soundStorage.splice(i,1);
            }
        }
    };

    Assets.playSoundChannelWithOptions = function(instance, properties) {
        var sound = createjs.Sound.play(instance, properties);
        sound.name = instance;
        sound.volume = properties.vol||1;
        if(soundStorage.length>0){
            for(var i = 0,j=soundStorage.length;i<j;i++){
                if(soundStorage[i].name !== instance){
                    soundStorage.push(sound);
                }
            }
        }else{soundStorage.push(sound);}

        return sound;
    };

    Assets.setMute = function(bool) {
        createjs.Sound.muted = bool;
    };

    Assets.soundVolume = function(sound,vol) {
        for(var i = 0,j=soundStorage.length;i<j;i++){
            if(soundStorage[i].name === sound){
                soundStorage[i].volume = vol;
            }
        }
    };

    Assets.stopSound = function() {
        createjs.Sound.stop();
    };

    Assets.stopAllSounds = function() {
        createjs.Sound.stop();
        soundStorage = [];
    };

// public methods:
// private methods:
    Assets._listDebug = function(_queue) {
        var debugTxt = new createjs.Text("","12px Arial","white").set({x:100});
        window.stage.addChild(debugTxt);

        _queue.on("fileload", function (event) {
            var item = event.item;
            var type = item.type;
            //if (type == createjs.AbstractLoader.VIDEO) {
                //if (type == createjs.LoadQueue.SOUND) {
                //if (type == createjs.LoadQueue.IMAGE) {
                //if (type == createjs.LoadQueue.JSON) {
                var str = String(event.item.src);
                var ext = str.slice(str.length-4,str.length);
                debugTxt.text += "\nFile.. "+String(event.item.id)+ext;
                console.log("File.. "+event.item.id+ext);
           // }
            count++;
            console.log("Total:: "+count);

        }, this);
    };

/*    Assets.setOverlay = function(bg,logo){
        Assets.OVERLAY_BG = bg;
        Assets.OVERLAY_LOGO = logo;
    };*/

/*    Assets.getOverlay = function(game,gameType) {
        var overlay = new createjs.Container();
        var w = alteastream.AbstractScene.GAME_WIDTH;
        var h = alteastream.AbstractScene.GAME_HEIGHT;

        var buttonProps = gameType === "Lobby" ? {w:170,h:56,round:6,f:"24px Arial"} : {w:86,h:28,round:3,f:"12px Arial"};//todo ne postavlja lepo txt za mobile (Y)
        var button = new createjs.Shape();
        button.graphics.setStrokeStyle(2);
        button.graphics.beginStroke("#fff0a4").beginFill('#c69b00').drawRoundRect(0,0,buttonProps.w,buttonProps.h,buttonProps.round);
        button.regX = buttonProps.w/2;
        button.regY = buttonProps.h/2;
        button.x = w/2;
        button.y = h/2 + (buttonProps.h * 2);

        var txt = new createjs.Text("PLAY NOW", buttonProps.f, "#ffffff");
        txt.textAlign = 'center';
        txt.textBaseline = 'middle';
        txt.x = button.x;
        txt.y = button.y;

        overlay.addChild(Assets.OVERLAY_BG,Assets.OVERLAY_LOGO,button,txt);
        overlay.cache(0,0,w,h);

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            game.resetBackgroundMusic();
            overlay.parent.removeChild(overlay);
            overlay = null;
            if(window.localStorage.getItem('isMobile') === "yes"){
                if(is.safari()){
                    var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                    if(is.not.ipad()) {
                        if(!isiPadOS){
                            alteastream.ABSStarter.getInstance().safariResize();
                        }
                    }
                }
            }
        });
        return overlay;
    };*/

/*    Assets.getOverlay = function(options) {
        var overlay = new createjs.Container();

        overlay.addChild(options.background);

        for(var i = 0; i < options.graphics.length; i++){
            overlay.addChild(options.graphics[i]);
        }

        overlay.addChild(options.button);

        for(var j = 0; j < options.texts.length; j++){
            overlay.addChild(options.texts[j]);
        }

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            overlay.parent.removeChild(overlay);
            overlay = null;
            options.clickCallback();
        });

        return overlay;
    };*/

    alteastream.Assets = Assets;
})();

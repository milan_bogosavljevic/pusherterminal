
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SpringAnim = function(image) {
        this.Container_constructor();
        this.initialize_SpringAnim(image);
    };
    var p = createjs.extend(SpringAnim, createjs.Container);

    // static properties:
    // events:
    // public properties:
    p.gravity = 1.1;
    // private properties:
    // constructor:
    p.initialize_SpringAnim = function(image) {
        var skin = alteastream.Assets.getImage(alteastream.Assets.images[image]);
        this.addChild(skin);

        this.pivot = skin.image.width*0.5;
        this.regX = this.pivot;
        this.regY = this.pivot;
        this.turn = 0.2;

        this.mouseEnabled = false;
        this.mouseChildren = false;
    };
    // static methods:
    // private methods:
    p._animate = function(){
        this.vy += this.gravity;
        this.x += this.vx;
        this.y += this.vy;
        this.rotation += this.vr;
        this.scaleY += this.turn;

        if(this.scaleY > 0.9)
            this.turn = -0.2;

        else if(this.scaleY < 0.1)
            this.turn = 0.2;
    };
    // public methods:
    p.setGravity = function(g){
        this.gravity = g;
    };

    p.setVX = function(min,max){
        this.vx = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.setVY = function(min,max){
        this.vy = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.setVR = function(min,max){
        this.vr = gamecore.Utils.NUMBER.randomRange(min,max);
    };

    p.animate = function(){
        this.on("tick", function spawn(){
            this._animate();

            if(this.y - this.pivot > alteastream.AbstractScene.GAME_HEIGHT+this.pivot) {
                this.off("tick",spawn);
                this.dispose();
            }
        });
    };

    p.dispose = function(){
        this.removeAllChildren();
        this.parent.removeChild(this);
    };

    alteastream.SpringAnim = createjs.promote(SpringAnim,"Container");
})();
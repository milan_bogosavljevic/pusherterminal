// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BitmapText = function(text,font,color,props) {
        this.Text_constructor(text,font,color);
        this.initialize_BitmapText(text,font,color,props);
    };
    var p = createjs.extend(BitmapText, createjs.Text);

// static properties:
// events:
// public properties:

// private properties:
    p._defaultColor  = "#000";
    p._highlightColor  = "#FFF";

// constructor:
    p.initialize_BitmapText = function(text,font,color,props) {
        this.font = font;
        this.text = text || "...";

        this.textAlign = props.textAlign;
        this.textBaseline = "top";

        this.x = props.x;
        this.y = props.y;
        this.yCacheAdd = props.yCacheAdd || 0;

        if(typeof props.visible !== 'undefined')
            this.visible = props.visible/*===1*/;
        this._defaultColor = this.color = color;
        gamecore.Utils.DISPLAY.cacheText(this);
        this.mouseEnabled = false;
    };
// static methods:
// public methods:
    p.setText = function (text) {
        this.uncache();
        this.text = text;
        gamecore.Utils.DISPLAY.cacheText(this);
    };

    p.updateText = function(prop,change){
        this.uncache();
        this[prop] = change;
        gamecore.Utils.DISPLAY.cacheText(this);
    };

    p.setColor = function (color) {
        this.color = color;
        this.updateCache();
    };

    p.highlight = function(bool){
        this.uncache();
        this.color = bool === true? this._highlightColor: this._defaultColor;
        this.updateCache();
    };


    alteastream.BitmapText = createjs.promote(BitmapText,"Text");
})();
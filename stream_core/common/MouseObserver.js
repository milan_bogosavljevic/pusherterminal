

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseObserver = function(scope,method,type) {
        this.initialize_MouseObserver(scope,method,type);
    };
    var p = MouseObserver.prototype;

// static properties:

// public properties:

// private properties:

// constructor:
    p.initialize_MouseObserver = function(scope,method,type) {
        this.props = {
            eventType:type,
            notify: function(subject) {
                scope[method](subject);
            }
        };
        return this.props;
    };
// static methods:

// public methods:


    alteastream.MouseObserver = MouseObserver;
})();
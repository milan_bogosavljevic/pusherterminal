/**
 * Created by Dacha on 20.04.15..
 */

// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var ImageButton = function(imgSeq, numOfFrames, text) {
        this.BaseButton_constructor();
        this.initializeImageButton(imgSeq, numOfFrames, text);
    };
    var p = createjs.extend(ImageButton,alteastream.BaseButton);

// static properties:
// events:
// public properties:
    p.spriteSheet = null;

// private properties:
    p._imgSeq = null;
    p._singleImgWidth = null;
    p._height = null;
    p._highlighted = false;

// constructor:
    p.Super_initialize = p.initializeBaseButton;
    p.initializeImageButton = function (imgSeq, numOfFrames, text) {

        this._imgSeq = imgSeq;
        var tmpBmp = new createjs.Bitmap(imgSeq);

        this._singleImgWidth = Math.floor(tmpBmp.image.width / numOfFrames);
        this._height = tmpBmp.image.height;
        this.numOfFrames = numOfFrames;

        this.Super_initialize(text, this._singleImgWidth, this._height);
        this._init();
    };

// static methods:
// public methods:
// private methods:

    p._init = function() {
        var data = {
            images: [this._imgSeq],
            frames: {width:this._singleImgWidth,height:this._height},
            animations: {
                normal:{
                    frames:[0]
                },
                highlight:{
                    frames: [1],
                    next: "normal",
                    speed: 0.2//0.2
                },
                disabled:{
                    frames: [2]
                }
            }
        };
        var spriteSheet1 = new createjs.SpriteSheet(data);
        this.spriteSheet = new createjs.Sprite(spriteSheet1, "normal");

        var that = this;
        // Play click animation before the action

        this.super_preActionHook = this.preActionHook;
        /*this.preActionHook = function(event) {// old ok
            that.super_preActionHook(event);
            if(that.spriteSheet){
                that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                that.spriteSheet.gotoAndPlay("highlight");
            }
        };*/
        this.preActionHook = function(event) {// new with _highlighted state remaining
            that.super_preActionHook(event);
            if(that.spriteSheet){
                if(that._highlighted === true){
                    that.spriteSheet.gotoAndStop("highlight");
                }else {
                    that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                    that.spriteSheet.gotoAndPlay("highlight");
                }
            }
        };


        this.mouseDownPreActionHook = function() {
            if(that.spriteSheet){
                that.spriteSheet.gotoAndStop("highlight");
            }
        };

        this.mouseDownPostActionHook = function() {
            if(that.spriteSheet){
                //that.spriteSheet.gotoAndStop("normal");//old
                that.spriteSheet.spriteSheet.getAnimation("highlight").next = that._disabled === true? "disabled": "normal";
                that.spriteSheet.gotoAndPlay("highlight");
            }
        };

        this.addChildAt(this.spriteSheet, 0);
    };

    p.setHighlighted = function(bool){// new with _highlighted state remaining
        if(this.spriteSheet){
            if(bool === true){
                this._highlighted = true;
                this.spriteSheet.gotoAndStop("highlight");
            }
            else
                this._highlighted = false;
        }
    };

    p._super_enable = p._enable;
    p._enable = function() {
        this._super_enable();
        this.spriteSheet.gotoAndStop("normal");
        this.isClicked = false;
    };

    p._super_disable = p._disable;
    p._disable = function() {
        this._super_disable();

        if(!this.isClicked)                           //stari highlight je bez uslova
            this.spriteSheet.gotoAndStop("disabled");//stari highlight
    };

    p.changeSkin = function(imgSeq){
        this.removeChildAt(0);
        this._imgSeq = imgSeq;
        var tmpBmp = new createjs.Bitmap(imgSeq);

        this._singleImgWidth = Math.floor(tmpBmp.image.width / this.numOfFrames);// HC 3 bilo
        this._height = tmpBmp.image.height;

        var data = {
            images: [this._imgSeq],
            frames: {width:this._singleImgWidth,height:this._height},
            animations: {
                normal:{
                    frames:[0]
                },
                highlight:{
                    frames: [1],
                    next: "normal",
                    speed: 0.2
                },
                disabled:{
                    frames: [2]
                }
            }
        };
        var spriteSheet1 = new createjs.SpriteSheet(data);
        this.spriteSheet = new createjs.Sprite(spriteSheet1, "normal");

        this.addChildAt(this.spriteSheet, 0);
    };

    alteastream.ImageButton = createjs.promote(ImageButton,"BaseButton");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Scroller = function(target) {
        this.initialize_Scroller(target);
    };
    var p = Scroller.prototype;
    var _this = null;
// static properties:
// events:
// public properties:
// private properties:
    p._target = null;
    p._scrollType = null;
    p._scrollLevels = null;
    p._animateScrollCallback = null;
    p._preAnimateScrollMethod = null;
    p._postAnimateScrollMethod = null;
    p._numOfPages = 0;
    p._rowsPerPage = 0;
    p._pagination = null;
    p._scrollDirection = null;
    p.scrollByRowCurrentLevel = 0;
    p.scrollByPageCurrentLevel = 0;
    p.maxScrollLevelByPage = 0;

// constructor:

    p.initialize_Scroller = function(target) {
        console.log("Scroller Initialized");
        _this = this;
        this._target = target;
    };

    p.plugInPagination = function(pagination) {
        this._pagination = pagination;
    };

    p.setScrollType = function(type) {
        this._scrollType = type;
    };

    p.setScrollDirection = function(direction) {
        this._scrollDirection = direction;
    };

    p.setScrollLevels = function(levels) {
        this.scrollByRowCurrentLevel = 0;
        this._scrollLevels = levels;
    };

    p.setRowsPerPage = function(rows) {
        this._rowsPerPage = rows;
    };

    p.setToFirstPage = function() {
        //if(this.scrollByRowCurrentLevel > 0){
        this.scrollByRowCurrentLevel = 0;
        this.scrollByPageCurrentLevel = 0;
        this[this._scrollType](false);
        //}
    };

    p.setPreAnimateScrollMethod = function(method) {
        this._preAnimateScrollMethod = method;
    };

    p.setPostAnimateScrollMethod = function(method) {
        this._postAnimateScrollMethod = method;
    };

    p.getCurrentScrollLevel = function() {
        return this.scrollByRowCurrentLevel;
    };

    p.manageButtonsState = function() {
        if(this._pagination.hasButtons === false){
            return;
        }
        if(this._numOfPages < 2){
            this._pagination.disableButtons(true);
            return;
        }
        if(this.scrollByRowCurrentLevel === 0) {
            this._pagination._upButton.setDisabled(true);
            this._pagination._downButton.setDisabled(false);
        }else if(this.scrollByRowCurrentLevel === this._scrollLevels.length - 1) {
            this._pagination._downButton.setDisabled(true);
            this._pagination._upButton.setDisabled(false);
        }else{
            this._pagination.disableButtons(false);
        }
    };

    p.setNumberOfPages = function(numOfPages) {
        this._numOfPages = numOfPages;
        this.maxScrollLevelByPage = numOfPages - 1;
    };

    p.moveUp = function() {
        if(this.scrollByRowCurrentLevel > 0){
            var currentLevel = this._scrollType + "CurrentLevel";
            this[currentLevel]--;
            this[this._scrollType](true);
            if(this._pagination){this.manageButtonsState();}
        }
    };

    p.moveDown = function() {
        if(this.scrollByRowCurrentLevel < (this._scrollLevels.length - 1)) {
            var currentLevel = this._scrollType + "CurrentLevel";
            this[currentLevel]++;
            this[this._scrollType](true);
            if(this._pagination){this.manageButtonsState();}
        }
    };

    p.scrollByRow = function(doAnimation){
        var position = this._scrollLevels[this.scrollByRowCurrentLevel];
        this.scrollByPageCurrentLevel = Math.ceil(this.scrollByRowCurrentLevel / this._rowsPerPage);
        var animationSpeed = doAnimation === true ? 500 : 0;
        this._animateScroll(position,animationSpeed);
    };

    p.scrollByPage = function(doAnimation) {
        if(this.scrollByPageCurrentLevel === this.maxScrollLevelByPage){
            this.scrollByRowCurrentLevel = this._scrollLevels.length - 1;
        }else{
            this.scrollByRowCurrentLevel = this.scrollByPageCurrentLevel * this._rowsPerPage;
        }
        var position = this._scrollLevels[this.scrollByRowCurrentLevel];
        var animationSpeed = doAnimation === true ? 500 : 0;
        this._animateScroll(position,animationSpeed);
    };

    p.updateCurrentDotPosition = function() {
        this._pagination.moveCurrentDot(this.scrollByPageCurrentLevel);
    };

    p.dotClicked = function(scrollLevel){
        if(this.scrollByPageCurrentLevel !== scrollLevel){
            this.scrollByPageCurrentLevel = scrollLevel;
            this.scrollByPage(true);
        }
    };

    p._animateScroll = function(position,duration) {
        if(this._preAnimateScrollMethod){
            this._preAnimateScrollMethod();
        }
        var animationOptions = {};
        animationOptions.override = true;
        animationOptions[this._scrollDirection] = position;
        createjs.Tween.get(this._target).to(animationOptions , duration , createjs.Ease.quadOut).call(function () {
            if(this._postAnimateScrollMethod){
                this._postAnimateScrollMethod();
            }
        }.bind(this))
    };

    alteastream.Scroller = Scroller;
})();
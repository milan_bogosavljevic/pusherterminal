/**
 * Created by Dacha on 20.04.15..
 */

// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var BaseButton = function(text, width, height) {
        this.Container_constructor();
        this.initializeBaseButton(text, width, height);
    };
    var p = createjs.extend(BaseButton,createjs.Container);

// static properties:
// events:
// public properties:
    p.text = null;
    p.textColor = null;//alpha 1
    p.disabledTextColor = null;//alpha 0.5
    p.higlightedTextColor = null;//"white"
    p.normalTextColor = null;
    p.clickHandler = null;
    //p.cursor = null;
    p.width = null;
    p.height = null;
    p.mouseAreaWidth = null;
    p.mouseAreaHeight = null;
    p.mouseAreaX = null;
    p.mouseAreaY = null;
    p.isImage = null;
    p.prop = null;//new
    p.blinkable = false;
    p.hideOnDisabled = false;

    p.preActionHook = null;
    p.postActionHook = null;
    p.isClicked = false;
    p.value = null;
    p.id = null;

// private properties:
    p._disabled = false;
    p._storedDisabled = false;
    p._localClickHandler = null;
    p._presetFonts = null;
    p._extraLabel = null;


// constructor:
    p.initializeBaseButton = function(text, width, height) {

        this.width = width || this.width;
        this.height = height || this.width;
        this.text = text || new createjs.Text();
        this.mouseAreaWidth = width || this.width;
        this.mouseAreaHeight = height || this.height;


        if(text) {
            this.text = text;
            if(this.textIsImage(this.text)){
                //this.text.image = this.normalTextColor;
                this.prop = "alpha";//new
                this.textColor = 1;
            }else{
                this.prop = "color";//new
                this.textColor = this.text.color;
            }

            this.addChild(this.text);
            // text fix
            this.regX = width*0.5;
            this.regY = height*0.5;

            this.centerText();
        }

        this.preActionHook = function() {
            if(text){
                if (that.textIsImage(that.text)){
                    this.text.image = this.higlightedTextColor;
                }else{
                    this.text.color = this.higlightedTextColor;
                }
            }
        };
        var that = this;
        this.postActionHook = function() {
            if(text){
                if(that.textIsImage(that.text)){
                    that.prop = "alpha";
                    that.text.image = that.normalTextColor;
                    that.textColor = 1;
                }else{
                    that.prop = "color";
                }
                that.text[that.prop] = that._disabled ? that.disabledTextColor : that.textColor;
            }
            that.isClicked = false;
        };
    };

// static methods:
// public methods:
    p.addTextPreset = function(key, text, deltaX, deltaY) {
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;

        if(this._presetFonts === null)
            this._presetFonts = {};

        this._presetFonts[key] = {"text" : text, "deltaX" : deltaX, "deltaY" : deltaY};
    };

    p.selectTextPreset = function(key) {
        var preset = this._presetFonts[key];
        this.removeChild(this.text);
        this.text = preset.text.clone();
        this.addChild(this.text);
        this.textColor = this.text.color;
        this.centerText(preset.deltaX, preset.deltaY);
    };

    p.setClickHandler = function(handler) {
        if(this._localClickHandler !== null)
            this.removeEventListener('click', this._localClickHandler);

        var that = this;
        this._localClickHandler = function(event) {

            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                that.isClicked = true;
                handler(event);

                if (that.preActionHook) that.preActionHook(event);
                if (that.postActionHook) that.postActionHook(event);
            }
        };
        this.addEventListener('click', this._localClickHandler);
    };

    p.setUpHandler = function(handler) {
        if(this._upHandler !== null)
            this.removeEventListener('pressup', this._upHandler);

        this.upHandler = handler;
        var that = this;
        this._upHandler = function(event) {
            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                if(that.isHandled)
                    return;
                that.isDown = false;
                handler(event);
                that.mouseDownPostActionHook();
            }
        };
        this.addEventListener('pressup', this._upHandler);
    };

    p.setHoldDownHandler = function(handler) {
        if(this._downHandler !== null)
            this.removeEventListener('mousedown', this._downHandler);

        var that = this;
        this._downHandler = function(event) {
            if(event.nativeEvent.button === 2 || event.which === 3){}
            else {
                that.isDown = true;
                that.isHandled = false;
                that.listenMouseLeave(that,event);
                handler(event);
                that.mouseDownPreActionHook(event);
            }
        };
        this.addEventListener('mousedown', this._downHandler);
    };

    p.listenMouseLeave = function(that,event) {
        checkMouseLeave(document, "mouseout", function(e) {
            e = e ? e : window.event;
            var element = e.relatedTarget || e.toElement;
            if (!element || element.nodeName !== "CANVAS") {
                if(that.isDown){
                    that.isDown = false;
                    that.upHandler(event);
                    that.isHandled = true;
                    that.mouseDownPostActionHook();
                }
            }
        });

        function checkMouseLeave(obj, evt, fn) {
            if (obj.addEventListener) {
                obj.addEventListener(evt, fn, false);
            }
        }
    };

    p.setDisabled = function(disabled) {
        if(disabled) {
            this._disable();
        } else {
            this._enable();
        }
    };

    p.isDisabled = function() {
        return this._disabled;
    };

    p.storeDisabledState = function() {
        this._storedDisabled = this._disabled;
    };

    p.restoreDisabledState = function() {
        this.setDisabled(this._storedDisabled);
    };

/*    p.centerText = function(deltaX, deltaY) {
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;
        //var width = this.textIsImage(this.text)?0:this.text.bitmapCache.width*0.5;    // bitmap text version
        //var height = this.textIsImage(this.text)?0:this.text.bitmapCache.height*0.5;  // bitmap text version
        var width = this.textIsImage(this.text)?0:this.text.getBounds().width*0.5;
        var height = this.textIsImage(this.text)?0:this.text.getBounds().height*0.5;
        this.text.x = this.width * 0.5 - width + deltaX;
        this.text.y = this.height * 0.5 - height + deltaY+5;
        this.text.textAlign = "center";
        this.text.textBaseline = "middle";
    };*/

    p.centerText = function(deltaX, deltaY) { // text fix
        var deltaX = deltaX || 0;
        var deltaY = deltaY || 0;
        this.text.x = this.regX + deltaX;
        this.text.y = this.regY + deltaY;
        this.text.textAlign = "center";
        this.text.textBaseline = "middle";
    };

    p.reduceHitArea = function(xPos,yPos,w,h){
        this.mouseAreaWidth = this.mouseAreaWidth - w;
        this.mouseAreaHeight = this.mouseAreaHeight - h;
        this.mouseAreaX = 0 + xPos;
        this.mouseAreaY = 0 + yPos;

    };

    p.setClickListener = function(enabled) {
        if(enabled)
            this.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#f00").drawRect(this.mouseAreaX, this.mouseAreaY, this.mouseAreaWidth, this.mouseAreaHeight));
        else
            this.hitArea = new createjs.Shape();
    };

    p.setClickEnabled = function(enabled) {
        if(enabled)
            this.hitArea = new createjs.Shape(new createjs.Graphics().beginFill("#f00").drawRect(this.mouseAreaX,this.mouseAreaY,this.mouseAreaWidth,this.mouseAreaHeight));
        else
            this.hitArea = new createjs.Shape();
    };

    p.textIsImage = function(target){
        return target.toString() === "[Bitmap (name=null)]";
    };

// private methods:
    p._enable = function() {
        this.text[this.prop] = this.textColor;
        this.setClickEnabled(true);
        this._disabled = false;
    };

    p._disable = function() {
        this.text[this.prop] = this.disabledTextColor;
        this.setClickEnabled(false);
        this._disabled = true;
    };

    alteastream.BaseButton = createjs.promote(BaseButton,"Container");
})();

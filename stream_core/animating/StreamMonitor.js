this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var StreamMonitor = function (stream){
        this.initialize_StreamMonitor(stream);
    };

    var p = StreamMonitor.prototype;

    p.stream = null;
    p._count = 0;
    p._attempts = 0;
    p._streamEstablished = false;
// static properties:
// events:
// public vars:
// private vars:
// constructor:
    p.initialize_StreamMonitor = function (stream){
         this.stream = stream;
         this._startMonitor();
    };

    p._startMonitor = function(){
        var that = this;
        this.streamEventMonitor = setInterval(function(){
            if(!that._streamEstablished){
                if(that._attempts<5){
                    if(that._count<10){
                        that._count++;
                        console.log("monitoring.. "+that._count);
                    }else{
                        that._attempts++;
                        that._count = 0;
                        that.stream.clearVideoElement();
                        that.stream.startStreaming();//preview/gameplay
                        console.log("attempt.. "+that._attempts);
                    }
                }else{
                    that._stopMonitor();
                    that._attempts = 0;
                    that.stream.onStreamFailed();
                }
            }
        },1000);
    };

    p._stopMonitor = function(){
        this._streamEstablished = false;
        clearInterval(this.streamEventMonitor);
        this._count = 0;
    };

    p.streamConnected = function(){
        this._streamEstablished = true;
        clearInterval(this.streamEventMonitor);
        this._attempts = 0;
        this._count = 0;
    };

    alteastream.StreamMonitor = StreamMonitor;
})();
/**
 * Created by DACHA on 01-May-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var Renderer = function (game){
        this.initialize(game);
    };

    var p = Renderer.prototype;
    var _instance = null;

// static properties:

// events:

// public vars:

// private vars:
    p._isRendering = null;
    p._stage = null;

// constructor:
    p.initialize = function (game){
        _instance = this;
        createjs.Ticker.removeAllEventListeners("tick");
        this._isRendering = true;
        this.game = game;

        createjs.Ticker.timingMode = createjs.Ticker.RAF;

        this.game.mainStage.update();
        this._setRender(this._isRendering);
    };

// static methods:

// public functions:

    p.stageRendering = function(bool){
        console.log("stageRendering "+bool);
        this._isRendering = bool;
        this._setRender(this._isRendering);
    };

// private functions:
    p._setRender = function(on){
        var eventListener = on === true?"addEventListener":"removeEventListener";
        createjs.Ticker[eventListener]("tick", this._mainLoop);
    };

    p._mainLoop = function(event){
        _instance.game.renderLoop();
        _instance.game.mainStage.update(event);
    };

    alteastream.Renderer = Renderer;
})();
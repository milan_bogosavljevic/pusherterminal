this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var ABSVideoStream = function (scene,options,address){
        this.initialize_ABSVideoStream(scene,options,address);
    };

    var p = ABSVideoStream.prototype;

    p.scene = null;
// static properties:
// events:
// VideoStream was here
// public vars:
// private vars:
// constructor:
    p.initialize_ABSVideoStream = function (scene,options,address){

        this.scene = scene;
        this.videoElement = new alteastream.ABSVideoElement(options);
        var webrtcPlayer = this.webrtcPlayer = new WEBRTCGamePlayer(this.videoElement.htmlVideo);
        this.setPlayer(address);

        webrtcPlayer.onError = function(error) {
            if(error) {
                this.handleError(error);
                //{"id":"error","code":-7,"txt":"User session does not exist"}
                //{"id":"error","code":13,"txt":"User token is expired or does not exist"}
            }
        }.bind(this);
    };

    p.activate = function(callback){
        var that = this;
        //var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
        this.webrtcPlayer.onStreamEvent = function(evt){
            if(evt.type === 'state' && evt.new === 'CONNECTED'){
                console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
            }
            if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                console.log("CONNECTED OK::::::::::::::::::: "+evt);

                that.streamConnected();
                that.setPreloadPoster(false);
                that.concreteActivate();
                //that.setMedia();
                if(callback){
                    callback();
                }
            }
        };

        this.startStreaming();
        this.setPreloadPoster(true);
        this.addSpinner();
        this.addMonitor();
    };

    p.stop = function(){
        this.webrtcPlayer.stop();
        this.clearVideoElement();
    };

    p.reset = function(){

    };

    p.clearVideoElement = function(){
        this.videoElement.clear();
    };

    alteastream.ABSVideoStream = ABSVideoStream;
})();
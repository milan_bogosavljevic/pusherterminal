this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var ABSVideoElement = function (options){
        this.initialize_ABSVideoElement(options);
    };

    var p = ABSVideoElement.prototype;

// static properties:
// events:
// public vars:
// private vars:
// constructor:
    p.initialize_ABSVideoElement = function (options){

        var videoID = options.id;

        var isMobile = window.localStorage.getItem('isMobile');
        var videoWidth = isMobile === "not" ? options.width : options.width/2;
        var videoHeight = isMobile === "not" ? options.height : options.height/2;
        var videoTag ="<video id ='"+videoID+"' width = '"+videoWidth+"' height= '"+videoHeight+"' autoplay muted playsinline></video>";

        var htmlVideo = this.htmlVideo = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        document.getElementById("wrapper").insertBefore(document.getElementById(videoID), document.getElementById("wrapper").firstChild);

        var style = htmlVideo.style;
        for(var p in options.style)
            style[p] = options.style[p];
        style.pointerEvents = "none";

        style["video::-webkit-media-controls-panel"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-play-button"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
        style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";
    };

    p.setSource = function(src){
        this.htmlVideo.src = src;
    }

    p.play = function(){
        this.htmlVideo.play();
    }

    p.setMuted = function(bool){
        this.htmlVideo.muted = bool;
    }

    p.setVolume = function(vol){
        this.htmlVideo.volume = vol;
    }

    p.setLoop = function(bool){
        this.htmlVideo.loop = bool;
    }

    p.clear = function(){
        this.htmlVideo.removeAttribute('src');
        this.htmlVideo.load();
    }

    p.recreateVideoElement = function(videoID){
        var canvas = document.getElementById("screen");
        canvas.parentNode.removeChild(this.htmlVideo);
        var htmlVideo = this.htmlVideo = document.createElement("video");
        htmlVideo.id = videoID;
        htmlVideo.autoplay = true;
        canvas.parentNode.insertBefore(htmlVideo,canvas);
        var style = htmlVideo.style;
        style.top = "0px";
        style.left ="0px" ;
        style.width = 100 + "%";
        style.height =  "0 auto";
        style.position = "absolute";
        style.pointerEvents = "none";

        return htmlVideo;
    };

    alteastream.ABSVideoElement = ABSVideoElement;
})();
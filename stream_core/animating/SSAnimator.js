/**
 * Created by Dacha on 6/12/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var SSAnimator = function (sheet,w,h,frames){
        this.Container_constructor();
        this.initializeSSAnimator(sheet,w,h,frames);
    };

    var p = createjs.extend(SSAnimator, createjs.Container);


// static properties:
// events:
// public vars:
// private vars:

    p._sprite = null;
    p._image = null;
    p._width = null;
    p._height = null;
    p._count = null;

// constructor:
    p.initializeSSAnimator = function (sheet,w,h,frames){
        this._image = alteastream.Assets.getImageURI(sheet);
        this._width = w;
        this._height = h;
        this._count = frames;
        this._setUp();
    };

// public functions:
    p._setUp = function (){
        var data = new createjs.SpriteSheet({
            images:[this._image],
            frames:{width:this._width, height:this._height, count:this._count},
            animations:{animate:[0, this._count-1, true]}
        });
        this._sprite = new createjs.Sprite(data,"animate");
        this.addChild(this._sprite);
        this._sprite.mouseEnabled = false;
        this._sprite.regX = this._width*0.5;
        this._sprite.regY = this._height*0.5;
        this._sprite.framerate = 30;
    };

    p.animate = function (bool){
        if(bool === true){
            this._sprite.play();

        }else{
            this._sprite.stop();
        }
    };

    p.getWidth = function (){
        return this._width;
    };

    p.getHeight = function (){
        return this._height;
    };

    p.dispose = function(){
        if(this._sprite){
            this.animate(false);
            this.removeAllChildren();
            this._sprite = null;
        }
    };

// private functions:
// static methods:
    alteastream.SSAnimator = createjs.promote(SSAnimator,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorGame = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorGame();
    };
    var p = createjs.extend(SocketCommunicatorGame, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var GameMessageType = {
        1:"CONFIRM_GAME_INIT",
        2:"MACHINE_RESET_START",
        3:"MACHINE_RESET_END",
        5:"GAME_IS_LAUNCHED",
        7:"MOVE_SHOOTER_LEFT",
        8:"MOVE_SHOOTER_RIGHT",
        9:"STOP_SHOOTER",
        10:"DISPENSE_TOKENS",
        11:"PRIZE_DETECTION",
        12:"SHOOT_ENABLED",
        40:"NON_PLAYING_TIME_LIMIT_REACHED",
        50:"TOKENS_COUNT_CHANGED",
        60:"BALANCE_CHANGED",
        64:"BONUS_WON",
        70:"MACHINE_ERROR",//USER_KICKED ?
        85:"QUIT",
        100:"HEARTBEAT",
        CONFIRM_GAME_INIT:1,
        MACHINE_RESET_START:2,
        MACHINE_RESET_END:3,
        GAME_IS_LAUNCHED:5,
        MOVE_SHOOTER_LEFT:7,
        MOVE_SHOOTER_RIGHT:8,
        STOP_SHOOTER:9,
        DISPENSE_TOKENS:10,
        PRIZE_DETECTION:11,
        SHOOT_ENABLED:12,
        NON_PLAYING_TIME_LIMIT_REACHED:40,
        TOKENS_COUNT_CHANGED:50,
        BALANCE_CHANGED:60,
        BONUS_WON:64,
        MACHINE_ERROR:70,//USER_KICKED ?
        QUIT:85,
        HEARTBEAT:100
    };

// constructor:
    p.initialize_SocketCommunicatorGame = function() {
        _instance = this;
        this.USER_PRESENCE = 10000;
    };

// static methods:
    SocketCommunicatorGame.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(params){
        var currentGameUid = this.currentGameUid = params.key;
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/wsgame"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&game="+currentGameUid+"&t="+Date.now());
        var gameClient = this.gameClient = Stomp.over(socket);
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onGameConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToMachine();
        _instance.heartbeatGame();
    };

    //iphone debug
    p._subscribeToMachine = function(){
        console.log("socket>> startGame socket Game :::::::::::::::: ");
        var subscriptionObj = {};
        subscriptionObj.name = "gameplay_"+_instance.currentGameUid;
        subscriptionObj.subscription = this.gameClient.subscribe("/topic/"+_instance.currentGameUid+"", function (msg) {
            var mesg = JSON.parse(msg.body);
            _instance.parseGameMessage(mesg);
        });

        this.subscriptions.push(subscriptionObj);

        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.GAME_IS_LAUNCHED,
            tkn:_instance.currentGameUid
        }))
    };

    p.startHeartBeatGame = function () {
        _instance.heartbeatGame();
        this.heartbeatGameInterval = setInterval(function(){
            _instance.heartbeatGame();
        },this.USER_PRESENCE);
    };

    p.heartbeatGame = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/cmd", {}, JSON.stringify({
                val:0,
                tp:GameMessageType.HEARTBEAT,
                tkn:_instance.currentGameUid
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatGameInterval);
    };

    p.sendBet = function(params) {
        this.fireShooter(params.bet);
    };

    p.fireShooter = function(bet) {
        _instance.game.monitorIdleTime(false);
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            val:bet,
            tp:GameMessageType.DISPENSE_TOKENS,
            tkn:_instance.currentGameUid
        }));
    };

    p.moveShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:actionType,
            tkn:_instance.currentGameUid
        }));
    };

    p.stopMovingShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.STOP_SHOOTER,
            tkn:_instance.currentGameUid
        }));
    };

    p.confirmGameInit = function(){
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.CONFIRM_GAME_INIT,
            tkn:_instance.currentGameUid
        }));
    };

    p.quitPlay = function(){
        _instance.game.monitorIdleTime(false);
        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.QUIT,
            tkn:_instance.currentGameUid
        }))
    };

    p.parseGameMessage = function(msg){
        console.log("socket>> parseGameMessage:::::::::::::: ");
        console.log(msg);
        switch(msg.tp){
            case GameMessageType.CONFIRM_GAME_INIT:
                console.info("message>> CONFIRM_GAME_INIT:::::::::::::: "+msg.dsc);
                _instance.game.showGame(true);
                _instance.game.monitorIdleTime(true);
                break;
            case GameMessageType.GAME_IS_LAUNCHED:
                console.log("message>> GAME_IS_LAUNCHED:::::::::::::: "+msg.description);
                _instance.startHeartBeatGame();
                break;
            case GameMessageType.QUIT:
                console.info("message>> QUIT:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.exitMachineGameplay();
                break;
            case GameMessageType.SHOOT_ENABLED:
                if(msg.vl){
                    console.log("message>> SHOOT_ENABLED:::::::::::::: "+msg.vl);
                    _instance.game.allowedShooterBtn(msg);
                    _instance.game.monitorIdleTime(true);
                }
                break;
            case GameMessageType.PRIZE_DETECTION:
                if(msg.vl){
                    _instance.game.onPrizeDetection(msg.vl);
                }
                break;
            case GameMessageType.BALANCE_CHANGED:
                console.log("message>> BALANCE_CHANGED:::::::::::::: ");
                _instance.game.setCrd(msg.bal);
                _instance.game.updateQueueInfo(msg.queueSize);
                /*if(msg.bon){
                    if(msg.bon>0)
                        _instance.game.fillToBonus(msg.bon);
                }*/
                break;
            /*case GameMessageType.BONUS_WON:
                console.log("message>> BONUS_WON "+msg.vl.amount);
                _instance.game.onBonusWon(msg);
                //{"ts":1555571186510,"vl":{"amount":37,"money":3700,"balance":9997290,"prize":2},"tp":6,"cat":64}
                break;*/
            case GameMessageType.TOKENS_COUNT_CHANGED:
                //console.log("socket>> TOKENS_COUNT_CHANGED >> amount: "+msg.vl.amount+" money: "+msg.vl.money);
                console.log("message>> TOKENS_COUNT_CHANGED >> amount: "+msg.vl);
                _instance.game.refundCoinSpawn(msg.vl);
                break;
            case GameMessageType.NON_PLAYING_TIME_LIMIT_REACHED:
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.monitorIdleTime(false);
                _instance.game.quitLimitReached();
                break;
            case GameMessageType.MACHINE_ERROR://USER_KICKED ?
                console.warn("message>> MACHINE_ERROR::::: "+msg.vl);
                _instance.heartbeatGame();
                _instance.game.onMachineError(msg.vl);
                break;
            default:
                console.warn("message>> UNHANDLED MESSAGE TYPE >> "+msg.tp);
        }
    };

    alteastream.SocketCommunicatorGame = createjs.promote(SocketCommunicatorGame,"ABSSocketCommunicator");
})();
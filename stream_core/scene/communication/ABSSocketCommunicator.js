// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSSocketCommunicator = function(game) {
        this.initialize_ABSSocketCommunicator(game);
    };
    var p = ABSSocketCommunicator.prototype;

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    p.currentGameUid = "";
    p.USER_PRESENCE = 0;
    p.subscriptions = [];
    p.intention = false;
    p._isConnected = false;
    var _instance = null;

// constructor:
    p.initialize_ABSSocketCommunicator = function(game) {
        this.game = game;
        _instance = this;
    };

// static methods:
    ABSSocketCommunicator.getInstance = function(){
        return _instance;
    };

// public methods

    p.activate = function(){

    };

    p.unsubscribeFromAll = function(){
        if(this.subscriptions.length>0){
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                this.subscriptions[i].subscription.unsubscribe();
            }
            this.subscriptions = [];
        }
    };

    p.getSubscriptions = function(){
        if(this.subscriptions.length>0){
            console.info("total subscriptions: "+this.subscriptions.length);
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                console.info("subscription_"+i+": "+this.subscriptions[i].name);
            }
        }else{
            console.info("no subscriptions ");
        }
    };

    p.disposeCommunication = function(){
        this.intention = true;
        this.clearHeartBeats();
        this.unsubscribeFromAll();
        //this.gameClient.disconnect( function(){console.log("disconnected");} );
    };

    p.tryReconnect = function(status){
        if(status === "online")
            if(_instance._isConnected === false){
                _instance.connectionLostInterval = setInterval(function(){
                    if(_instance._isConnected === false)
                        _instance.activate();
                    else
                        clearInterval( _instance.connectionLostInterval);
                },2000);
            }else{
                clearInterval( _instance.connectionLostInterval);
            }
    };

    p.onConnectionError = function(message){
        _instance._isConnected = false;
        //_instance.disposeCommunication();
        console.warn("Socket connection lost");
        if(!_instance.intention)
            _instance.game.throwAlert(alteastream.Alert.ERROR,"Socket connection error "+message,function () {
                console.log("try to reconnect");
                _instance.game.onQuit();
            });
    };


    alteastream.ABSSocketCommunicator = ABSSocketCommunicator;
})();
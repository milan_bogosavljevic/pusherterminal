// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorRoulette = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorRoulette();
    };
    var p = createjs.extend(SocketCommunicatorRoulette, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var GameMessageType = {
        1:"CONFIRM_GAME_INIT",
        4:"STATUS_CHANGE",
        //2:"MACHINE_RESET_START",
        //3:"MACHINE_RESET_END",
        5:"GAME_IS_LAUNCHED",
        //7:"MOVE_SHOOTER_LEFT",
        //8:"MOVE_SHOOTER_RIGHT",
        //9:"STOP_SHOOTER",
        10:"SEND_BET",// sendBet
        11:"PRIZE_DETECTION",// win
        //12:"SHOOT_ENABLED",
        40:"NON_PLAYING_TIME_LIMIT_REACHED",
        //50:"TOKENS_COUNT_CHANGED",
        60:"BALANCE_CHANGED",
        //64:"BONUS_WON",
        70:"MACHINE_ERROR",
        85:"QUIT",
        100:"HEARTBEAT",
        CONFIRM_GAME_INIT:1,
        STATUS_CHANGE:4,
        //MACHINE_RESET_START:2,
        //MACHINE_RESET_END:3,
        GAME_IS_LAUNCHED:5,
        //MOVE_SHOOTER_LEFT:7,
        //MOVE_SHOOTER_RIGHT:8,
        //STOP_SHOOTER:9,
        SEND_BET:10,
        PRIZE_DETECTION:11,
        //SHOOT_ENABLED:12,
        NON_PLAYING_TIME_LIMIT_REACHED:40,
        //TOKENS_COUNT_CHANGED:50,
        BALANCE_CHANGED:60,
        //BONUS_WON:64,
        MACHINE_ERROR:70,
        QUIT:85,
        HEARTBEAT:100
    };

// constructor:
    p.initialize_SocketCommunicatorRoulette = function() {
        _instance = this;
        this.USER_PRESENCE = 10000;
        _instance.gameIsInitialized = false;//iphone debug
    };

// static methods:
    SocketCommunicatorRoulette.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(params){
        var currentGameUid = this.currentGameUid = params.key;
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/wsgame"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&game="+currentGameUid+"&t="+Date.now());
        var gameClient = this.gameClient = Stomp.over(socket);
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onGameConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToMachine();
        _instance.heartbeatGame();
    };

    p._subscribeToMachine = function(){
        console.log("socket>> startGame socket Game :::::::::::::::: ");
        var subscriptionObj = {};
        subscriptionObj.name = "gameplay_"+_instance.currentGameUid;
        subscriptionObj.subscription = this.gameClient.subscribe("/topic/"+_instance.currentGameUid+"", function (msg) {
            var mesg = JSON.parse(msg.body);
            _instance.parseGameMessage(mesg);
        });

        this.subscriptions.push(subscriptionObj);

        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.GAME_IS_LAUNCHED,
            tkn:_instance.currentGameUid
        }))
    };

    p.startHeartBeatGame = function () {
        _instance.heartbeatGame();
        this.heartbeatGameInterval = setInterval(function(){
            _instance.heartbeatGame();
        },this.USER_PRESENCE);
    };

    p.heartbeatGame = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/cmd", {}, JSON.stringify({
                val:0,
                tp:GameMessageType.HEARTBEAT,
                tkn:_instance.currentGameUid
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatGameInterval);
    };

    p.sendBet = function(bet) {
        console.log("socket>> sendBet:::::::::::::::::: "+bet);
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            val:bet,
            tp:GameMessageType.SEND_BET,
            tkn:_instance.currentGameUid
        }));
    };

    //cameras maybe
    /*
    p.moveShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:actionType,
            tkn:_instance.currentGameUid
        }));
    };

    p.stopMovingShooter = function(actionType, callback) {
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.STOP_SHOOTER,
            tkn:_instance.currentGameUid
        }));
    };*/

    p.confirmGameInit = function(){
        _instance.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.CONFIRM_GAME_INIT,
            tkn:_instance.currentGameUid
        }));
    };

    p.quitPlay = function(){
        this.gameClient.send("/cmd", {}, JSON.stringify({
            tp:GameMessageType.QUIT,
            tkn:_instance.currentGameUid
        }))
    };

    p.parseGameMessage = function(msg){
        console.log("socket>> parseGameMessage:::::::::::::: ");
        console.log(msg);
        switch(msg.tp){
            case GameMessageType.CONFIRM_GAME_INIT:
                console.info("socket>> CONFIRM_GAME_INIT:::::::::::::: "+msg.dsc);
                _instance.game.showGame(true);
                break;
            case GameMessageType.GAME_IS_LAUNCHED:
                console.log("socket>> GAME_IS_LAUNCHED:::::::::::::: "+msg.description);
                //console.log("data:::::::::::::: "+msg.data);
                _instance.startHeartBeatGame();
                break;
            /*case GameMessageType.SHOOT_ENABLED:
                if(msg.vl){
                    console.log("socket>> SHOOT_ENABLED:::::::::::::: "+msg.vl);
                    _instance.game.allowedShooterBtn(msg);
                    _instance.game.monitorIdleTime(true);
                }
                break;*/
            case GameMessageType.STATUS_CHANGE:
                console.log("socket>> STATUS_CHANGE:::::::::::::: ");// 7 statusa {"tp":4,"cat":3,"vl":[1,2,3,4,5,6,7]}
                _instance.game.statusChange(msg.vl);
                break;
            case GameMessageType.PRIZE_DETECTION:
                console.log("socket>> PRIZE_DETECTION:::::::::::::: ");
                _instance.game.onPrizeDetection(msg.vl);////vl = WINNING NUMBER {"tp":11,"cat":3,"vl":{"user_bets":5.0,"win":0,"drawnNo":28,"amount":0,"money":0,"balance":862206}}
                break;
            case GameMessageType.BALANCE_CHANGED:
                console.log("socket>> BALANCE_CHANGED:::::::::::::: ");
                _instance.game.setCrd(msg.bal);
                //_instance.game.updateQueueInfo(msg.queueSize);
                break;
            case GameMessageType.QUIT:
                console.info(">>>>> QUIT:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;

                _instance.disposeCommunication();
                _instance.game.exitMachineGameplay();// exitMachineGame
                break;
            case GameMessageType.MACHINE_ERROR:
                _instance.heartbeatGame();
                _instance.game.onMachineError(msg.vl);
                /*switch(msg.vl){
                    case 1:
                        _instance.heartbeatGame();
                        _instance.game.onMachineError(msg.vl);
                        break;
                }*/
                break;
            default:
                console.warn("UNHANDLED MESSAGE TYPE>> "+msg.tp);
        }
    };

    alteastream.SocketCommunicatorRoulette = createjs.promote(SocketCommunicatorRoulette,"ABSSocketCommunicator");
})();
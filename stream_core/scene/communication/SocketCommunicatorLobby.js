// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var SocketCommunicatorLobby = function(game) {
        this.ABSSocketCommunicator_constructor(game);
        this.initialize_SocketCommunicatorLobby();
    };
    var p = createjs.extend(SocketCommunicatorLobby, alteastream.ABSSocketCommunicator);

// static properties:
// events:
// public properties:
    p.gameClient = null;

// private properties:
    var _instance = null;

    var ErrorMessageType = {
        1:"ERROR_TOKEN_ALREADY_IN_QUEUE",
        2:"ERROR_TOKEN_NOT_IN_QUEUE",
        3:"ERROR_QUEUE_DOES_NOT_EXIST",
        4:"USER_DID_NOT_START_GAME",
        5:"ERROR_MACHINE_DOES_NOT_EXIST",
        6:"ERROR_INVALID_TOKEN",
        7:"ERROR_TOKEN_ALREADY_ON_MACHINE",
        8:"ERROR_TOKEN_ALREADY_IN_GAME",
        9:"ERROR_WRONG_GAME_CLASS",
        10:"ERROR_MACHINE_CANT_BE_REACHED",
        11:"ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN",
        12:"ERROR_NON_EXISTING_GAME_TYPE",
        13:"ERROR_NO_TOKEN",
        14:"ERROR_MACHINE_NOT_AVAILABLE",
        15:"ERROR_NO_FREE_GAMEPLAY_NODE",
        20:"ERROR_QUEUE_SYSTEM",
        ERROR_TOKEN_ALREADY_IN_QUEUE:1,
        ERROR_TOKEN_NOT_IN_QUEUE:2,
        ERROR_QUEUE_DOES_NOT_EXIST:3,
        USER_DID_NOT_START_GAME:4,
        ERROR_MACHINE_DOES_NOT_EXIST:5,
        ERROR_INVALID_TOKEN:6,
        ERROR_TOKEN_ALREADY_ON_MACHINE:7,
        ERROR_TOKEN_ALREADY_IN_GAME:8,
        ERROR_WRONG_GAME_CLASS:9,
        ERROR_MACHINE_CANT_BE_REACHED:10,
        ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN:11,
        ERROR_NON_EXISTING_GAME_TYPE:12,
        ERROR_NO_TOKEN:13,
        ERROR_MACHINE_NOT_AVAILABLE:14,
        ERROR_NO_FREE_GAMEPLAY_NODE:15,
        ERROR_QUEUE_SYSTEM:20
    };

    var SocketMessageType = {
        1:"MACHINE_INFO",
        2:"MACHINE_QUEUE_CHANGED",
        5:"MACHINE_READY_FOR_PLAYER",
        6:"MESSAGE_FROM_GAME",
        10:"EXPIRED_GAME_WAITING",
        20:"QUEUE_TO_MACHINE",
        25:"LEAVE_QUEUE",
        30:"TOKEN_EXPIRED",
        40:"TOKEN_REFRESHED",
        1000:"BETTING_COUNTER",//mp
        1001:"BETTING_EXPIRED",//mp
        90:"BETTING_EVENT",//mp
        110:"BETTING_ENTER_CONFIRMED",//mp
        120:"BETTING_EXIT_CONFIRMED",//mp
        666:"LEFT_GAME",
        MACHINE_INFO:1,
        MACHINE_QUEUE_CHANGED:2,
        MACHINE_READY_FOR_PLAYER:5,
        MESSAGE_FROM_GAME:6,
        EXPIRED_GAME_WAITING:10,
        QUEUE_TO_MACHINE:20,
        LEAVE_QUEUE:25,
        TOKEN_EXPIRED:30,
        TOKEN_REFRESHED:40,
        BETTING_COUNTER:1000,//mp
        BETTING_EXPIRED:1001,//mp
        BETTING_EVENT:90,//mp
        BETTING_ENTER_CONFIRMED:110,//mp
        BETTING_EXIT_CONFIRMED:120,//mp
        LEFT_GAME:666
    };

    var UserMessageType = {
        2:"GAME_COMMAND",
        3:"REGISTER_SOCKET",
        4:"CHECK_USER",
        10:"REQUEST_QUEUE_ENTER",
        15:"REQUEST_QUEUE_LEAVE",
        20:"HEARTBEAT_USER",
        30:"BETTING_START",//mp
        40:"BETTING_STOP",//mp
        GAME_COMMAND:2,
        REGISTER_SOCKET:3,
        CHECK_USER:4,
        REQUEST_QUEUE_ENTER:10,
        REQUEST_QUEUE_LEAVE:15,
        HEARTBEAT_USER:20,
        BETTING_START:30,//mp
        BETTING_STOP:40//mp
    };

// constructor:
    p.initialize_SocketCommunicatorLobby = function() {
        _instance = this;
        this.USER_PRESENCE = 50000;
    };

// static methods:
    SocketCommunicatorLobby.getInstance = function(){
        return _instance;
    };

// public methods
    p.activate = function(){
        var socket = new SockJS(alteastream.AbstractScene.SERVER_URL+"/ws"+"?tkn="+alteastream.AbstractScene.GAME_TOKEN+"&t="+Date.now());// /websocket
        var gameClient = this.gameClient = Stomp.over(socket);// sa SockJS
        this.unsubscribeFromAll();
        gameClient.connect({}, _instance.game.onSocketConnect,_instance.onConnectionError);
    };

    p.onLobbyConnect = function(frame){
        console.log(frame);
        _instance._isConnected = true;
        _instance._subscribeToSession();
        _instance._subscribeToPublic();

        _instance.heartbeatUser();
        _instance.startHeartBeatUser();
    };

    p.lobbySwitchSubscriptions = function(previousMachine,newMachine){
        this.unsubscribeFromMachine(previousMachine);
        this._subscribeToMachine(newMachine);
        //test log:
        this.getSubscriptions();
    };

    p.startHeartBeatUser = function () {
        this.heartbeatUserInterval = setInterval(function(){
            _instance.heartbeatUser();
        },this.USER_PRESENCE);
    };

    p.heartbeatUser = function(){
        if(this.gameClient!==null){
            console.log("socket>> heartbeatGame::::::::::::::::::");
            this.gameClient.send("/msg/user", {}, JSON.stringify({
                //val:0,
                tp:UserMessageType.HEARTBEAT_USER,
                tkn:alteastream.AbstractScene.GAME_TOKEN
            }) );
        }
    };

    p.clearHeartBeats = function(){
        clearInterval(this.heartbeatUserInterval);
    };

    p._subscribeToSession = function(){
        var subscriptionObj = {};
        subscriptionObj.name = "session";//+alteastream.AbstractScene.SESSION_TOKEN;//6cfoquoqvd7hfeafcac009s9vq
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/'+alteastream.AbstractScene.SESSION_TOKEN, function (msg) {
            console.log("socket msg: from user >>>>>>>>>>");
            _instance.parseMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p._subscribeToPublic = function(){
        var subscriptionObj = {};
        subscriptionObj.name = "public";
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/public', function (msg) {
            console.log("socket msg: from public >>>>>>>>>>");
            _instance.parsePublicMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p._subscribeToMachine = function(machine_index){
        var subscriptionObj = {};
        subscriptionObj.name = machine_index;
        subscriptionObj.subscription = _instance.gameClient.subscribe('/topic/'+machine_index, function (msg) {
            console.log("socket msg: from machine >>>>>>>>>> "+machine_index+" "+msg.body);
            _instance.parseMessage(JSON.parse(msg.body));
        });
        this.subscriptions.push(subscriptionObj);
    };

    p.unsubscribeFromMachine = function(machineName){
        if(this.subscriptions.length>0){
            for(var i=0,j=this.subscriptions.length;i<j;i++){
                //if(this.subscriptions[i].name === machineName){
                if(this.subscriptions[i].name !== "session" && this.subscriptions[i].name !== "public"){
                    this.subscriptions[i].subscription.unsubscribe();
                    this.subscriptions.splice(i,1);
                }
            }
        }
    };

    p.enterQueue = function() {
        console.log("socket>> send enterQueue for :::::::::::::::::::::::::: "+alteastream.AbstractScene.GAME_ID);
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:UserMessageType.REQUEST_QUEUE_ENTER,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.leaveQueue = function() {
        console.log("socket>> send leaveQueue for :::::::::::::::::::::::::: "+alteastream.AbstractScene.GAME_ID);
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:UserMessageType.REQUEST_QUEUE_LEAVE,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.doBetting = function(bool){//mp
        console.log("socket>> send betStart :::::::::::::::::::::::::: ");
        var msgType = bool === true? UserMessageType.BETTING_START: UserMessageType.BETTING_STOP;//SPECTATE
        _instance.gameClient.send("/msg/machine", {}, JSON.stringify({
            tp:msgType,
            tkn:alteastream.AbstractScene.GAME_TOKEN,
            val:alteastream.AbstractScene.GAME_ID//machine_index
        }));
    };

    p.parseMessage = function(msg){
        var queuePanel = _instance.game.currentLocationMachines.queuePanel;
        var locationMachines = _instance.game.currentLocationMachines;
        switch(msg.tp){
            case SocketMessageType.MACHINE_QUEUE_CHANGED:
                console.log(">>>>> MACHINE_QUEUE_CHANGED::::::::::::::");
                queuePanel.updateQueueInfo(msg.vl);
                locationMachines.updateLoungeComponents();
                break;
            case SocketMessageType.QUEUE_TO_MACHINE:
                console.log(">>>>> QUEUE_TO_MACHINE:::::::::::::::");
                var parsedMsg = JSON.parse(msg.vl);
                if(parsedMsg.code === 0){
                    queuePanel.onQueueEnter(parsedMsg);
                }else{
                    _instance._handleQueueFailed(queuePanel,parsedMsg.code);
                }
                break;
            case SocketMessageType.LEAVE_QUEUE:
                var msgVal = JSON.parse(msg.vl);
                console.log(">>>>> LEAVE_QUEUE:::::::::::::::"+msgVal.machine);
                queuePanel.onQueueLeave();
                break;
            case SocketMessageType.TOKEN_EXPIRED:
                console.warn(">>>>> TOKEN_EXPIRED:::::::::::::::");//heartbeat failed
                _instance.disposeCommunication();
                window.stage.mouseEnabled = false;
                window.stage.mouseChildren = false;
                _instance.game.throwAlert(alteastream.Alert.ERROR,"Token Expired",function() {
                    _instance.game.onQuit();
                });
                break;
            case SocketMessageType.TOKEN_REFRESHED:
                console.info(">>>>> TOKEN_REFRESHED:::::::::::::::");
                alteastream.AbstractScene.GAME_TOKEN = msg.vl;
                break;
            case SocketMessageType.MACHINE_READY_FOR_PLAYER:
                console.log(">>>>> MACHINE_READY_FOR_PLAYER:::::::::::::::");
                var msgParsed;
                    if(queuePanel.previousMachineActive === false){
                        queuePanel.multiPlayerControls.quitBetting();
                        locationMachines.setComponentWhenMachineisFreeToPlay(true);
                        msgParsed = JSON.parse(msg.vl);
                        queuePanel.manageQueueReadyState(msgParsed.wtime);
                    }
                    else{
                        if(queuePanel.spinner.active){
                            console.warn("aktivira spiner a ne treba");
                        }
                        queuePanel.resetDisplay();
                        //_instance.enterQueue();
                    }
                break;
            case SocketMessageType.EXPIRED_GAME_WAITING:
                console.warn(">>>>> EXPIRED_GAME_WAITING:::::::::::::::");
                if(queuePanel.spinner.active){
                    _instance.enterQueue();
                }else{
                    locationMachines.tryLobby();
                }
                break;
            case SocketMessageType.LEFT_GAME:
                console.warn(">>>>> LEFT_GAME:::::::::::::::");
                queuePanel.previousMachineActive = false;
                break;
            case SocketMessageType.BETTING_ENTER_CONFIRMED://mp
                console.log(">>>>> BETTING_ENTER_CONFIRMED:::::::::::::::");
                queuePanel.multiPlayerControls.startBetting();
                break;
            case SocketMessageType.BETTING_EXIT_CONFIRMED://mp
                console.log(">>>>> BETTING_EXIT_CONFIRMED:::::::::::::::");
                queuePanel.multiPlayerControls.stopBetting();
                break;
            case SocketMessageType.BETTING_COUNTER://mp
                console.log(">>>>> BETTING_COUNTER:::::::::::::::");
                var counterParsed = JSON.parse(msg.vl);
                queuePanel.multiPlayerControls.showCounter(counterParsed);
                break;
            case SocketMessageType.BETTING_EXPIRED://mp
                console.error(">>>>> BETTING_EXPIRED:::::::::::::::");
                //queuePanel.multiPlayerControls.stopBetting();
                break;
            case SocketMessageType.BETTING_EVENT://mp
                console.log(">>>>> BETTING_EVENT:::::::::::::::");
                console.log(msg);
                //var betMsg = JSON.parse(msg);
                queuePanel.multiPlayerControls.betEvent(msg);
                break;
            default:
                console.warn("UNHANDLED MESSAGE TYPE>> "+msg.tp);
        }
    };

    p.parsePublicMessage = function(msg){
        //{msg: "machine_free_to_use_update", machineIndex: 1229579567591513, isFreeToPlay: false}
        //if(msg.msg === "machine_free_to_use_update"){
        //delete msg.msg;
        //{msg: "queue_size_changed", machineIndex: 1229579567591513, isFreeToPlay: false, "size":123}
        //{msg: "machine_update","isOnline":false,"isInAutoplay","statusCode" }
        var locationMachines = _instance.game.currentLocationMachines;
        locationMachines._machinesComponent.setMachinesStatus(msg);

        if(locationMachines.queuePanel.machineIsDirty(msg.machineIndex)){
            if(locationMachines.queuePanel.spinner.active){
                locationMachines.queuePanel.resetEntrance(true);
                _instance.enterQueue();
            }
        }
    };

    p._handleQueueFailed = function(queuePanel,code){
        switch (code) {
            case ErrorMessageType.ERROR_TOKEN_ALREADY_IN_QUEUE:// ok desi se: usao, prekinuo counter sa backToLobby, usao ponovo dok je zuta
            case ErrorMessageType.ERROR_TOKEN_ALREADY_ON_MACHINE:// ok desi se: povratak iz masine i udje na zutu u kojoj je bio
            case ErrorMessageType.ERROR_TOKEN_ALREADY_IN_GAME:
                console.warn(">>>>> "+ErrorMessageType[String(code)]);
                // onemogucavanje multiplayera ako se pojavi spiner // todo nije provereno , nije na serveru
                //queuePanel.multiPlayerControls.enable(false);
                //queuePanel.multiPlayerControls.stakeAmountMock.visible = false;
                if(!queuePanel.spinner.active){
                    queuePanel.onQueueDeferred(code);// upali spinner load i cekaj
                }
                break;
            default:
                queuePanel.onQueueError(ErrorMessageType[String(code)]);
        }
        //queuePanel.onQueueError(ErrorMessageType[String(code)]);
    };

    alteastream.SocketCommunicatorLobby = createjs.promote(SocketCommunicatorLobby,"ABSSocketCommunicator");
})();
/**
 * Created by Dacha on 9/25/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var AbstractBet = function(scene) {
        this.initialize(scene);
    };
    var p = AbstractBet.prototype;

// static properties:

// events:
// public properties:
// private properties:
    p._selectedStake = null;
    p._multiplier = 0.10;
    p.scene = null;

// constructor:
    p.initialize = function(scene) {
        this.scene = scene;
    };

// static methods:
// public methods:
    p.betValue = function(){};

    p.setStake = function(){
        this.onStakeChange();
        this.manageStakeButtons();
    };

    p.setMultiplier = function(value){
        this._multiplier = value/1000;
    };

    // value is in cents version
/*    p.setMultiplier = function(value){
        this._multiplier = value/100;
    };*/

    p.getMultiplier = function(){
        return this._multiplier;
    };

    p.rounded = function(value){
        return Math.round(value * 100) / 100;
    };

// private methods:

    alteastream.AbstractBet = AbstractBet;
})();


// namespace
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var AbstractControlBoard = function() {
        this.Container_constructor();
        this.initialize_AbstractControlBoard();
    };

    var p = createjs.extend(AbstractControlBoard, createjs.Container);

// static properties:
// events:
// public properties:
    p.background = null;
    p.btnDecStake = null;
    p.btnIncStake = null;
    p.btnSend = null;
    p.btnCollect = null;
    p.btnShooterLeft = null;
    p.btnShooterRight = null;
    p.btnInfo = null;
    //p.btnQuit = null;
    //p.btnSound = null;
    p.btnBack = null;
    p._textStake = null;
    p._textBet = null;
    p._textStakeAmount = null;
    p._textWin = null;
    p._textCrd = null;
    p._textWinAmount = null;
    p._textCrdAmount = null;

    p._stakeBackground = null;
// private properties:

    p.twinComponent = null;

    p._infoBtnHandler = null;
    p._quitBtnHandler = null;
    //p._backBtnHandler = null;
    p._incStakeBtnHandler = null;
    p._decStakeBtnHandler = null;
    p._sendBtnHandler = null;
    p._scene = null;
    p.cbCurrency = null;
    p.currentButtonTextPreset = null;
    p.globalStorage = false;
    p.active = false;
    p._toValueView = true;
    p.textColorCoins = "#ffffff";
    p.textColorMoney = "#ffffff";
    p.controlShown = true;
    p.menuIsShown = false;
    var _this;

// constructor:

    p.initialize_AbstractControlBoard = function() {

    };

// static methods:
// public methods:
    p.plugin = function(scene) {
        _this = this;
        this._scene = scene;
        this._infoBtnHandler =       function() { scene.infoBtnHandler(); };
        this._quitBtnHandler =       function() { scene.quitBtnHandler(); };
        //this._backBtnHandler =       function() { scene.backBtnHandler(); };
        this._incStakeBtnHandler =   function() { scene.incStakeBtnHandler(); };
        this._decStakeBtnHandler =   function() { scene.decStakeBtnHandler(); };
        this._btnBetViewHandler =   function() { _this.onBtnBetViewHandler(); };
        //new stakes
        this._btnFixedStakesHandler =   function() { _this.btnFixedStakesHandler(); };
        this._sendBtnHandler =   function() { scene.sendBtnHandler(); };
        this._collectBtnHandler =   function() { scene.collectBtnHandler(); };
        this._shooterMoveBtnHandler =   function(e) {
            if(_this.twinComponent){
                if(_this.twinComponent.active) return;
            }
            _this.active = true;
            var id = e.currentTarget.id;
            scene.moveShooterBtnHandler(id);
        };
        this._shooterUpBtnHandler =   function(e) {
            if(_this.twinComponent){
                if(_this.twinComponent.active) return;
            }
            _this.active = false;
            /*var id = e.currentTarget.id;*/
            scene.stopShooterBtnHandler(/*id*/);
        };

        //this.btnQuit.setClickHandler(this._quitBtnHandler);
        this.btnInfo.setClickHandler(this._infoBtnHandler);
       // this.btnBack.setClickHandler(this._backBtnHandler);

        if (this.btnSend) this.btnSend.setClickHandler(this._sendBtnHandler);
        //this.btnCollect.setClickHandler(this._collectBtnHandler);

        if (this.btnShooterLeft) this.btnShooterLeft.setHoldDownHandler(this._shooterMoveBtnHandler);
        if (this.btnShooterLeft) this.btnShooterLeft.setUpHandler(this._shooterUpBtnHandler);

        if (this.btnShooterRight) this.btnShooterRight.setHoldDownHandler(this._shooterMoveBtnHandler);
        if (this.btnShooterRight) this.btnShooterRight.setUpHandler(this._shooterUpBtnHandler);

        if (this.btnIncStake) this.btnIncStake.setClickHandler(this._incStakeBtnHandler);
        if (this.btnDecStake) this.btnDecStake.setClickHandler(this._decStakeBtnHandler);

        if (this.btnBetView) this.btnBetView.setClickHandler(this._btnBetViewHandler);

        //new stakes
        if (this.btnFixedStakes) this.btnFixedStakes.setClickHandler(this._btnFixedStakesHandler);

/*        this.btnSound.on('click', function() {
            _this.soundBtnHandler();
        });*/
    };

    p.onBtnBetViewHandler = function(){
        this._toValueView = !this._toValueView;
        this._scene.setCurrency();
        this.setCrd(this._scene.credit);
        this._scene.betManager.onStakeChange();
        this._scene.setMonetaryValueView(this._toValueView);
        this._scene.playSound("btnDownDouble");
    };

    p.btnFixedStakesHandler = function(){
        if(this.menuIsShown===true){
            this.doShowMenu(false);
        }

        this.fixedStakesPanel.show(!this.fixedStakesPanel._active);
        if(this.fixedStakesPanel._active && this.controlShown === true)
            this._moveShooterLeftRightAndBtnSendComponents(true);
        else if(!this.fixedStakesPanel._active && this.controlShown === false)
            this._moveShooterLeftRightAndBtnSendComponents(false);

        this._scene.playSound("btnDownDouble");
    };

    p.blockMouse = function(bool){//if kbd left/right is down
        this.btnShooterLeft.mouseEnabled = !bool;
        this.btnShooterRight.mouseEnabled = !bool;

        this.btnIncStake.mouseEnabled = !bool;
        this.btnDecStake.mouseEnabled = !bool;
    };

    p.tryShooter = function(){
        if(this._scene.okToShowBalance === true){
            this.blockShooter(false);
        }
    };

    p.blockShooter = function(bool){
        this.btnSend.setDisabled(bool);
    };

    p.highlightCurrent = function(type,bool){
        var action = bool === true? "mouseDownPreActionHook":"mouseDownPostActionHook";
        switch(type){
            case 0:
                if(this.btnSend._disabled)
                    return;
                this.btnSend[action]();
                break;
            case 1:
                this.btnShooterLeft[action]();
                break;
            case 2:
                this.btnShooterRight[action]();
                break;
            case 3:
                if(this.btnIncStake._disabled)
                    return;
                this.btnIncStake[action]();
                break;
            case 4:
            if(this.btnDecStake._disabled)
                return;
            this.btnDecStake[action]();
            break;
            case 5:
                this.btnBetView[action]();
                break;
            case 6:
                this.btnInfo[action]();
                break;
        }
    };

    p.onKeyboardStakeChange = function(type){
        if(type === "inc"){
            if(this.btnIncStake._disabled)
                return;

            if(this.btnDecStake._disabled)
                this.btnDecStake.setDisabled(false);
        }

        if(type === "dec"){
            if(this.btnDecStake._disabled)
                return;
            if(this.btnIncStake._disabled)
                this.btnIncStake.setDisabled(false);
        }

        var handler = type + "StakeBtnHandler";
        this._scene[handler]();
    };

    p.setSingleToBurst = function(bool) {
        //if(this.btnSend._disabled)
            //return;
        var type = bool === true?"burst":"single";
        this.btnSend.selectTextPreset(type);

        if(this.btnSend.textIsImage(this.btnSend.text)){
            this.btnSend.normalTextColor = this.btnSend.text.image;
            this.btnSend.text.alpha = this.btnSend._disabled?0.2:1;
        }else{
            this.btnSend.text.color = this.btnSend.isDisabled() ? this.btnSend.disabledTextColor : this.btnSend.normalTextColor;
        }
    };

    p.setCurrency = function(val){
        var pearlsOrCoins = alteastream.AbstractScene.GAME_TYPE === "pearlpusher" ? "PEARLS" : "COINS";
        this._textCurrencyValue.text = this._toValueView === true? val:pearlsOrCoins;
    };

    p.setBetToSend = function(){
        this.setBetAmount(parseFloat(this._scene.betManager.betValue())/*.toFixed(2)*/);
    };

    p.setBetAmount = function(amount) {
        this._textBetAmount.text = amount;
    };

    p.setStakeAmount = function(stakeAmount,multiplier) {
        var betTypeToShow = this._toValueView === true? Math.min(stakeAmount*multiplier).toFixed(2): stakeAmount;
        this._textStakeAmount.text = betTypeToShow;
    };

    p.getStakeAmount = function() {
        var amount = this._textStakeAmount.text;
        return parseFloat(amount);
    };

    p.updateTextColorType = function(isEur){
        var color = isEur === true? this.textColorMoney:this.textColorCoins;
        this._textCrdAmount.color =
            this._textCurrencyValue.color =
                this._textStakeAmount.color = color;
    };

    p.setWin = function(winAmount) {
        this._textWinAmount.text = winAmount;//(winAmount/100).toFixed(2);
    };

    p.setCrd = function(crdAmount) {
        console.log("setCrd good:::::::");
        var crdAmountCents = Math.min(crdAmount/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this._textCrdAmount.text = this._toValueView === true? crdAmountCents: Math.min(crdAmount/100);
    };

    p.getCrd = function() {
        var amount = this._textCrdAmount.text;
        return parseFloat(amount).toFixed(2);
    };

/*    p.soundBtnHandler = function(){
        var assets = alteastream.Assets;
        this.soundToggled = !this.soundToggled;
        this.btnSound.image = this.soundToggled === true? assets.getImage(assets.images.soundOff).image: assets.getImage(assets.images.soundOn).image;
        //assets.setMute(this.soundToggled);

        var action = this.soundToggled===true?"pause":"play";
        window.parent.manageBgMusic(action);
    };*/

    p.disableTextAmountField = function(disabled,textField){
        textField.alpha = disabled === true?0.4:1;
    };

    p.storeButtonStates = function(){
        this.btnIncStake.storeDisabledState();
        this.btnIncStake.setDisabled(true);
        this.btnDecStake.storeDisabledState();
        this.btnDecStake.setDisabled(true);
        this.disableTextAmountField(true, this._textStakeAmount);
    };

    p.restoreButtonStates = function(){
        if (this.btnIncStake) {
            this.btnIncStake.restoreDisabledState();
            this.btnDecStake.restoreDisabledState();
            this.disableTextAmountField(false, this._textStakeAmount);
        }

        if (this.btnIncCoinBet) {
            this.btnIncCoinBet.restoreDisabledState();
            this.btnDecCoinBet.restoreDisabledState();
            this.disableTextAmountField(false, this._textCoinBetAmount);
        }
    };

    p.activateAllButtons = function(bool){
        //this.btnQuit.setDisabled(!bool);
        this.btnInfo.setDisabled(!bool);
    };

    p.initialSetup = function(){
        this.activateAllButtons(false);
        this.btnInfo.setDisabled(false);
        //this.btnQuit.setDisabled(false);
    };

    p.centerRegPoint = function(object){
        object.regX = object.image.width >> 1;
        object.regY = object.image.height >> 1;
    };

    p.disablePlayButtons = function(bool){
        if(bool === true){
            this.storeButtonStates();
        }else{
            this.restoreButtonStates();
        }
        this.blockShooter(bool);
        this.btnShooterLeft.setDisabled(bool);
        this.btnShooterRight.setDisabled(bool);
        this.active = bool;

        this.btnInfo.setDisabled(bool);
        this.btnBetView.setDisabled(bool);
    };

// private methods:
    alteastream.AbstractControlBoard = createjs.promote(AbstractControlBoard,"Container");
})();


this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var FixedStakes = function(){
        this.Container_constructor();
        this.initFixedStakes();
    };

    var p = createjs.extend(FixedStakes,createjs.Container);

    var _this;
    p._stakes = [1,5,0,0,0];
    p._shapes = {};
    p._values = {};
    p._active = false;

    p.initFixedStakes = function () {
        console.log("FixedStakes initialized:::");
        _this = this;
        this.visible = false;
    };

    FixedStakes.getInstance = function() {
        return _this;
    };

    p.plugin = function(machine){
        this.machine = machine;
        var maxStake = machine.maxStake;
        this.multiplier = machine.betManager.getMultiplier();
        var startLength = this._stakes.length;

        this._stakes[startLength-1] = maxStake;
        this._stakes[startLength-2] = Math.ceil((maxStake/2)/5)*5;
        this._stakes[startLength-3] = Math.ceil((maxStake/4)/5)*5;
        this.spliceCount = 0;

        this._stakes = this._stakes.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) === index;
        });
        this.spliceCount = startLength-this._stakes.length;

        this._setLayout();
    };

    p._setLayout = function(){
        var fieldSize = this.fieldSize = 68;
        var roundness = 10;
        var totalHeight = fieldSize*this._stakes.length;

        for(var i = 0,j = this._stakes.length;i<j;i++){
            var text = this._stakes[i];
            var tl = 0, tr = 0, br = 0, bl = 0;
            if(i===0){
                bl = roundness; br = roundness;
            }
            if(i===this._stakes.length-1){
                tl = roundness; tr = roundness;
                br = 0; bl =0;
            }
            var fieldShape = this._shapes[i] = new createjs.Shape();
            fieldShape.graphics.beginFill("#101010").drawRoundRectComplex(0, 0, fieldSize, fieldSize,tl,tr,br,bl);
            fieldShape.regX = fieldSize*0.5;
            fieldShape.regY = fieldSize*0.5;
            fieldShape.cache(0, 0, fieldSize, fieldSize);
            fieldShape.alpha = 0.7;

            var textLabel = this._values[i] = new createjs.Text(text,"18px Lato","#000000").set({textAlign:"center",textBaseline:"middle"});
            textLabel.hitArea = fieldShape;

            var fieldContainer = new createjs.Container();
            fieldContainer.addChild(fieldShape,textLabel);
            this.addChild(fieldContainer);
            fieldContainer.name = this._stakes[i];
            fieldContainer.y = totalHeight-(fieldSize*0.5)-(i*fieldSize);
            fieldContainer.on("click", function(e){
                _this._onStakeSelect(e);
            })
        }

        this.inPosition = 1857;
        this.yPosition = 630;
        this._setPosition();
    };

    p.show = function(bool){
        if(bool === true){
            if(!this._active){
                this._active = true;
                this.visible = true;
                createjs.Tween.get(this).to({x:this.inPosition},200,createjs.Ease.linear);
            }
        }else{
            if(this._active){
                this._active = false;
                createjs.Tween.get(this).to({x:alteastream.AbstractScene.GAME_WIDTH+this.fieldSize},200,createjs.Ease.linear).call(function () {
                    _this.visible = false;
                });
            }
        }
    };

    p._setPosition = function(){
        this.y = this.yPosition+(this.fieldSize*this.spliceCount);//647
        this.x = alteastream.AbstractScene.GAME_WIDTH+this.fieldSize;
        this._shapes[0].alpha = 1;
    };

    p._onStakeSelect = function(e){
        var targetName = e.currentTarget.name;
        var targetShape = this._stakes.indexOf(targetName);
        for(var p in this._shapes){
            this._shapes[p].alpha = 0.7;
        }
        this._shapes[targetShape].alpha = 1;

        var position = this.machine._stakeIncrements.indexOf(targetName);
        this.machine.setStake(position);
        this.machine.controlBoard._moveShooterLeftRightAndBtnSendComponents(false);
    };

    p.updateMonetary = function(isEur){
        for(var p in this._values){
            this._values[p].text = isEur===true? Math.min(this._stakes[p]*this.multiplier ).toFixed(2): this._stakes[p];
            this._values[p].color = isEur===true? "#00ff00": "#ffd500";
        }
        //this._values[this._stakes.length-1].text = "MAX"; //they want a value, not text
    };

    p.adjustMobile = function(){
        this.inPosition = 898;
        this.yPosition = 100;

        this._setPosition();
    };

    alteastream.FixedStakes = createjs.promote(FixedStakes,"Container");
})();
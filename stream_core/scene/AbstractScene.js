
// namespace:
this.alteastream = this.alteastream || {};

(function(){
    "use strict";

    var AbstractScene = function(){
        this.Container_constructor();
        this.initialize_AbstractScene();
    };
    var p = createjs.extend(AbstractScene,createjs.Container);


// static properties:
    AbstractScene.GAME_WIDTH = 0;
    AbstractScene.GAME_HEIGHT = 0;
    AbstractScene.GAME_QUIT = "";
    AbstractScene.GAME_TOKEN = "";
    AbstractScene.SESSION_TOKEN = "";
    AbstractScene.GAME_URL = "";
    AbstractScene.GAME_PROTOCOL = "";
    AbstractScene.GAME_ID = 0;
    AbstractScene.GAME_NAME = 0;
    AbstractScene.GAME_CODE = "";
    AbstractScene.DEFAULT_GAME_ID = "";
    AbstractScene.GAME_TYPE = "";
    AbstractScene.MACHINE_NAME = 0;

// events:
// public properties:
    p.requester = null;
    p.renderer = null;
    p.mainStage = null;
    p.isInFS = false;
    p.buttonTriggeredFs = false;
    var _this = null;

// constructor:
    p.initialize_AbstractScene = function(){
        _this = this;
        if(localStorage.getItem("isMobile")==="not"){
            this.fullScreenManager();
        }
    };

// static methods:
// public methods:
    p.setSessionToken = function(){
        AbstractScene.SESSION_TOKEN = jwt_decode(AbstractScene.GAME_TOKEN).token;
    };

    p.monitorNetworkStatus = function(){
        console.log("commented on ICE server");
        window.addEventListener('online',  this.showNetworkStatus);
        window.addEventListener('offline', this.showNetworkStatus);
    };

    p.showNetworkStatus = function(){
        var status = navigator.onLine? "online" : "offline";
        _this.throwAlert(alteastream.Alert.INFO,"You are "+status,_this.socketCommunicator.tryReconnect(status));
        //further options here
    };

    p.setFSClock = function(x,y){
        this.clock = new alteastream.Clock(stage,x,y);
    };

    p.showFSClock = function(bool){
        this.clock.show(bool);
    };

    p.setRenderer = function(){
        this.mainStage = this.parent;
        this.renderer = new alteastream.Renderer(this);
    };

    p.renderLoop = function(event){
        //_this.componentsRender();
        //_this.mainStage.update(event);
    };

    p.globalMouseEnabled = function(bool){
        this.mouseEnabled = bool;
    };

    p.stopAnimation = function(animation){
        this.renderer.stopAnimation(animation);
    };

    p.startAnimation = function(animation){
        this.renderer.startAnimation(animation);
    };

    p.stageRendering = function(bool){
       // this.renderer.stageRendering(bool);
    };

    p.setHttpCommunication = function(){
        this.requester = new alteastream.Requester(this);
    };

    /*p.setSocketCommunication = function(){// local ver
        this.socketCommunicator = new alteastream.SocketCommunicator(this);// local ver
        this.socketCommunicator.activate();// local ver
    };// local ver*/

    //new socket
    p.setSocketCommunication = function(type){
        var socketClass = "SocketCommunicator"+type;
        this.socketCommunicator = new alteastream[socketClass](this);
    };

    p.setStartOverlay = function(options) {
        var overlay = new createjs.Container();

        overlay.addChild(options.background);

        for(var i = 0; i < options.graphics.length; i++){
            overlay.addChild(options.graphics[i]);
        }

        overlay.addChild(options.button);

        for(var j = 0; j < options.texts.length; j++){
            overlay.addChild(options.texts[j]);
        }

        overlay.on("click", function () {
            overlay.removeAllEventListeners();
            overlay.removeAllChildren();
            overlay.parent.removeChild(overlay);
            overlay = null;
            options.clickCallback();
        });

        stage.addChild(overlay);
    };

    p._setQuitPanel = function() {
        var quitPanel = new alteastream.QuitConfirm(this.quitConfirmed);
        if(window.localStorage.getItem('isMobile') === "yes"){
            quitPanel.adjustMobile();
        }
        quitPanel.startUpdate();
        stage.addChild(quitPanel);
    };

    p._setAlert = function(){
        if(this.systemAlert){
            clearTimeout(this.systemAlertTimeout);
            this.systemAlert.dispose();
            stage.removeChild(this.systemAlert);
            this.systemAlert = null;
        }
    };

    p.throwAlert = function(type,exception,callback){
        stage.mouseEnabled = false;
        stage.mouseChildren = false;
        this._setAlert();

        this.systemAlert = new alteastream.Alert(type,exception);
        stage.addChild(this.systemAlert);

        this.removeAlert(3000,callback);
    };

    p.removeAlert = function(delay,callback){
        var that = this;
        this.systemAlertTimeout = setTimeout(function(){
            that.systemAlert.dispose();
            stage.removeChild(that.systemAlert);
            that.systemAlert = null;
            stage.mouseEnabled = true;
            stage.mouseChildren = true;
            if(callback)callback();
        },delay);
    };

    p.throwDialog = function(type,exception){
        this._setAlert();

        this.systemAlert = new alteastream.Alert(type,exception);
        stage.addChild(this.systemAlert);
    };

    p.killGame = function(){//TEMP
        alteastream.Assets.stopAllSounds();
        this.stageRendering(false);
        this.removeAllChildren();
        this.parent.update();
        this.parent.removeChild(this);
    };

    p.killSession = function(msg){//TEMP
        window.stage.mouseEnabled = false;
        window.stage.mouseChildren = false;
        msg = msg || "undefined";
        //window.stage.getChildAt(1).parent.removeChildAt(1);//temp loading overlay
        this.throwAlert(alteastream.Alert.ERROR,msg,function(){
            _this.killGame();
            //window.top.location.href = decodeURIComponent("http://139.59.147.230:81/");//alteastream.AbstractScene.GAME_QUIT
            window.parent.location.href = decodeURIComponent(alteastream.AbstractScene.GAME_QUIT);//alteastream.AbstractScene.GAME_QUIT
        });
    };

    p.playSound = function(sound,vol){
        alteastream.Assets.playSound(sound,vol);
    };

    p.playSoundChain = function(sound,vol,complete,scope){
        alteastream.Assets.playSoundChain(sound,vol,complete,scope);
    };

    p.playSoundChannelWithOptions = function(sound,op){
        alteastream.Assets.playSoundChannelWithOptions(sound,op);
    };

    p.stopSoundChannel = function(sound){
        alteastream.Assets.stopSoundChannel(sound);
    };

    p._addZeros = function(string,max){// todo iz utils
        var fromPoint = String(string);
        var decimals = fromPoint.split('.');
        var zeros = "";
        if(decimals[1] === undefined){
            zeros=".00";
            if(max>1){
                zeros+="0";
            }
        }else{
            if(decimals[1].length===1){
                zeros="00";
            }
            if(decimals[1].length===max){
                zeros="0";
            }
        }
        return zeros;
    };

    p.isRatioFit = function() {
        var deviceWidth = screen.width;
        var deviceHeight = screen.height;
        var deviceRatio = String(deviceWidth/deviceHeight).slice(0, 4);
        var ratioToFit = String(window.parent.widthToHeight).slice(0, 4);
        return deviceRatio === ratioToFit;
    };

    p.fullScreenManager = function(){
        window.parent.addEventListener("fs",function(e){
            _this.isInFS = e.isInFS;

            if(_this.isInFS === true){
                if(_this.buttonTriggeredFs === false){
                    _this.btnFs.mouseEnabled = false;
                    _this.btnFs.alpha = 0.5;
                }
            }else{
                if(_this.btnFs){
                    _this.btnFs.mouseEnabled = true;
                    _this.btnFs.alpha = 1;
                }
            }

            if(_this.btnFs){
                var img = _this.isInFS===true? "btnFullScreen_Off":"btnFullScreen_On";
                _this.btnFs.image = alteastream.Assets.getImage(alteastream.Assets.images[img]).image;
            }

            _this.buttonTriggeredFs = false;
            _this.showFSClock( _this.isInFS);
        });
    };

    p.normalBrowsersFullScreenToggle = function(e) {// CHROME, OPERA, FIREFOX
        //_this.controlBoard.checkIfHiddenMenuIsActive();
        var assets = alteastream.Assets;
        _this.buttonTriggeredFs = true;
        //var element = _this.isRatioFit() === true ? document.documentElement : window.parent.document.documentElement;
        // new version with fixed screen size
        var element = window.parent.document.documentElement;
        if(!window.parent.document.fullscreenElement){
            e.target.image = assets.getImage(assets.images.btnFullScreen_Off).image;
            element.requestFullscreen();
            _this.onFullScreen(true);
        }else{
            e.target.image = assets.getImage(assets.images.btnFullScreen_On).image;
            window.parent.document.exitFullscreen();
            _this.onFullScreen(false);
        }
    };

    p.otherBrowsersFullScreenToggle = function(e) {// EDGE, SAFARI
        //_this.controlBoard.checkIfHiddenMenuIsActive();
        var assets = alteastream.Assets;
        _this.buttonTriggeredFs = true;
        //var element = _this.isRatioFit() === true ? document.documentElement : window.parent.document.documentElement;
        // new version with fixed screen size
        var element = window.parent.document.documentElement;
        if(!window.parent.document.webkitFullscreenElement){ // ovde je drugacija provera posto ovi browseri izadju iz full moda kada krene da se ucitava masina
            e.target.image = assets.getImage(assets.images.btnFullScreen_Off).image;
            element.webkitRequestFullScreen();
            _this.onFullScreen(true);
        }else{
            e.target.image = assets.getImage(assets.images.btnFullScreen_On).image;
            window.parent.document.webkitExitFullscreen();
            _this.onFullScreen(false);
        }
    };

    p.onFullScreen = function(bool){
        this.showFSClock(bool);
    };

    p._addZeros = function(string,max){
        var fromPoint = String(string);
        var decimals = fromPoint.split('.');
        var zeros = "";
        if(decimals[1] === undefined){
            zeros=".00";
            if(max>1){
                zeros+="0";
            }
        }else{
            if(decimals[1].length===1){
                zeros="00";
            }
            if(decimals[1].length===max){
                zeros="0";
            }
        }
        return zeros;
    };

// private methods:

    alteastream.AbstractScene = createjs.promote(AbstractScene,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var QuitConfirm = function(quitMethod) {
        this.Container_constructor();
        this.initialize_QuitConfirm(quitMethod);
    };
    var p = createjs.extend(QuitConfirm, createjs.Container);
    p._quitMethod = null;
    p._questionBackground = null;
    p._buttonYes = null;
    p._buttonYesText = null;
    p._buttonNo = null;
    p._buttonNoText = null;
// static properties:
// events:
// public properties:
// private properties:
// constructor:

    p.initialize_QuitConfirm = function(quitMethod) {
        alteastream.Assets.playSound("info");
        this._quitMethod = quitMethod;
        var _this = this;
        var background = new createjs.Shape();
        background.graphics.beginFill("#000000").drawRect(0, 0, alteastream.AbstractScene.GAME_WIDTH, alteastream.AbstractScene.GAME_HEIGHT);
        background.alpha = 0.6;
        background.cache(0, 0, alteastream.AbstractScene.GAME_WIDTH, alteastream.AbstractScene.GAME_HEIGHT);

        var quitCounter = this._quitCounter = new alteastream.QuitCounter();
        quitCounter.regX = quitCounter.regY = quitCounter.width/2;
        quitCounter.x = alteastream.AbstractScene.GAME_WIDTH/2;
        quitCounter.y = alteastream.AbstractScene.GAME_HEIGHT/2 - 28;
        quitCounter.doneCallback = function(){_this._buttonHandler("yes");}

        var fontBig = "bold 32px Roboto";
        var fontSmall = "24px Roboto";
        var fontButton = "26px Roboto";

        var quitText = this.quitText = new createjs.Text("EXIT GAME?",fontBig,"#ffffff").set({textAlign:"center",textBaseline:"middle", x:quitCounter.x, y:quitCounter.y - 135, mouseEnabled:false});
        var buttonNoText = this._buttonNoText = new createjs.Text("Return to Game",fontButton,"#ffffff");

        var buttonNo = this._buttonNo = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnStart),3,buttonNoText);
        buttonNo.x = quitCounter.x;
        buttonNo.y = quitCounter.y + 224;
        var buttonNoClickHandler = function () {
            _this._buttonHandler("no");
        };
        buttonNo.setClickHandler(buttonNoClickHandler);
/*        buttonNo.on("click", function () {
            _this._buttonHandler("no");
        });*/

        var buttonYesText = this._buttonYesText = new createjs.Text("Exit",fontButton,"#ffffff");

        var buttonYes = this._buttonYes = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnExitForQuitCounter),2,buttonYesText);
        buttonYes.x = quitCounter.x;
        buttonYes.y = quitCounter.y + 310;
        var buttonYesClickHandler = function () {
            _this._buttonHandler("yes");
        };
        buttonYes.setClickHandler(buttonYesClickHandler);
/*        buttonYes.on("click", function () {
            _this._buttonHandler("yes");
        });*/

        this.addChild(background, quitCounter, quitText, buttonNo, buttonYes);
        this.preventClickThrough(true);
    };

    p.startUpdate = function() {
        this._quitCounter.update(10);
    };

    p._buttonHandler = function(quit) {
        if(quit === "yes"){
            this._quitMethod();
        }
        this._quitCounter.clearCounterInterval();
        this.removeAllChildren();
        this.parent.removeChild(this);
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };

    p.adjustMobile = function() {
        /*var background2 = this._questionBackground;
        background2.uncache();
        background2.graphics.clear();
        background2.graphics.beginFill("#303030").drawRoundRect(0, 0, 680, 350, 5);
        background2.cache(0, 0, 680, 350);
        background2.regX = 340;
        background2.regY = 175;
        background2.x = alteastream.AbstractScene.GAME_WIDTH/2;
        background2.y = alteastream.AbstractScene.GAME_HEIGHT/2;*/

        this._quitCounter.adjustMobile();
        this.quitText.font = "bold 26px Roboto";
        this.quitText.y = this._quitCounter.y - 60;

        this._buttonNoText.font = "18px Roboto";
        this._buttonYesText.font = "18px Roboto";

        this._buttonNo.y -= 110;
        this._buttonYes.y -= 120;

    };
// static methods:
// public methods:
// private methods:

    //alteastream.QuitConfirm = QuitConfirm;
    alteastream.QuitConfirm = createjs.promote(QuitConfirm,"Container");
})();
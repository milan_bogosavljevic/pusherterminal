
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Requester = function(game) {
        this.initialize(game);
    };
    var p = Requester.prototype;

// static properties:
    Requester.MACHINE_PATH = "/machine/";
// events:
// public properties:
    var _instance = null;
    var lokalUrl = "../Ztests/";
// private properties:
    p.gameId = null;
    p.gameToken = null;

    p.GAME_STATE_OK = 0;
    p.ERROR_TOKEN_ALREADY_IN_QUEUE = 1;
    p.ERROR_TOKEN_NOT_IN_QUEUE = 2;
    p.ERROR_QUEUE_DOES_NOT_EXIST = 3;
    p.USER_DID_NOT_START_GAME = 4;
    p.ERROR_MACHINE_DOES_NOT_EXIST = 5;
    p.ERROR_INVALID_TOKEN = 6;
    p.ERROR_TOKEN_ALREADY_ON_MACHINE = 7;
    p.ERROR_TOKEN_ALREADY_IN_GAME = 8;
    p.ERROR_WRONG_GAME_CLASS = 9;
    p.ERROR_MACHINE_CANT_BE_REACHED = 10;
    p.ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN = 11;
    p.ERROR_NON_EXISTING_GAME_TYPE = 12;
    p.ERROR_NO_TOKEN = 13;
    p.ERROR_MACHINE_NOT_AVAILABLE = 14;

// constructor:
    p.initialize = function(game) {
        this.game = game;
        _instance = this;
    };
// static methods:
    Requester.getInstance = function(){
        return _instance;
    };

// public methods
//LOBBY::>>
/*    p.getHouses = function(callback){
        $.get(lokalUrl+"getHouses.json",function(response){ callback(response); });
    };*/

    p.getGameTypes = function(callback){
        $.get(lokalUrl+"getGameTypes.json",function(response){ callback(response); });
    };

    p.getBalance = function(callback){
        $.get(lokalUrl+"getHousesBalance.json",function(response){ callback(response); });
    };

    // na 5 sec:
    p.getMachines = function(house,callback){// sounds/skins per type here
        $.get(lokalUrl+"getMachines.json",function(response){ callback(response); });
    };

    p.getMachinesGt = function(house,callback){// sounds/skins per type here
        $.get(lokalUrl+"getMachinesGt.json",function(response){ callback(response); });
    };

    //na 2 sec:
    p.getCurrentMachine = function(machineId,callback){
        $.get(lokalUrl+"getCurrentMachine.json",function(response){ callback(response); });
    };

    p.getPlayerActivity = function(callback){
        $.get(lokalUrl+"getPlayerActivity.json",function(response){callback(response);});
    };

    //na socket?
    p.enterMachine = function(callback){
        $.get(lokalUrl+"enterqueue.json",function(response){ callback(response); });
    };

    //na socket?
    p.leaveMachine = function(callback){
        $.get(lokalUrl+"leavemachine.json",function(response){ callback(response); });
    };
//LOBBY::<<

//STREAM::>>
    p.startGameStream = function(callbackContinue, callbackStop){
        $.get(lokalUrl+"gamestart.json",function(response){// latest Vlada request
            console.log("STATE:: "+response.state);
            if(response.state === _instance.GAME_STATE_OK){
                callbackContinue(response);
            }else{
                _instance._responseError(_instance._getState(response), callbackStop); // todo promeniti txt
            }
        });
        //gamestart/TOKEN/MASINA?game=rmgame
        //gamestart/TOKEN/MASINA?id=1
    };

    p.backToLobby = function(callback){
        $.get(alteastream.AbstractScene.SERVER_URL +"/"+"quitgame/"+alteastream.AbstractScene.GAME_TOKEN,function(res){
            callback(res);
        });
    };

    p._getState = function(res){
        var message = "state: "+res.state;
        switch(res.state){
            case _instance.ERROR_TOKEN_ALREADY_IN_QUEUE:
                message = "Token already in queue";
                //_instance.game.killSession("Token already in game");
                break;
            case _instance.ERROR_TOKEN_NOT_IN_QUEUE:
                message = "Token not in queue";
                break;
            case _instance.ERROR_QUEUE_DOES_NOT_EXIST:
                message = "Queue does not exist";
                break;
            case _instance.USER_DID_NOT_START_GAME:
                message = "User did not start the game";
                break;
            case _instance.ERROR_INVALID_TOKEN:
                message = "Invalid token";
                break;
            case _instance.ERROR_MACHINE_DOES_NOT_EXIST:
                message = "Machine does not exist";
                break;
            case _instance.ERROR_TOKEN_ALREADY_ON_MACHINE:
                message = "Token already on machine";
                break;
            case _instance.ERROR_TOKEN_ALREADY_IN_GAME:
                message = "Token already in game";
                break;
            case _instance.ERROR_WRONG_GAME_CLASS:
                message = "Wrong game class";
                break;
            case _instance.ERROR_MACHINE_CANT_BE_REACHED:
                message = "Machine cant be reached";
                break;
            case _instance.ERROR_MACHINE_IN_GAME_W_DIFF_TOKEN:
                message = "Machine token invalid";
                break;
            case _instance.ERROR_NON_EXISTING_GAME_TYPE:
                message = "Invalid game type";
                break;
            case _instance.ERROR_NO_TOKEN:
                message = "No token";
                break;
            case _instance.ERROR_MACHINE_NOT_AVAILABLE:
                message = "Machine not available";
                break;
        }
        return message;
    };

    p._responseError = function(msg,quitFn){
        _instance.game.throwAlert(alteastream.Alert.ERROR, msg,function(){
            if(quitFn)
                quitFn();
        });
    };

    p._responseException = function(msg){
        _instance.game.throwAlert(alteastream.Alert.EXCEPTION, msg);
    };

    p.fallbackToLobby = function(message){
        _instance.game.throwAlert(alteastream.Alert.ERROR,message,function(){
            _instance.game.killGame();
            _instance.game.closeGame();
        });
    };
//STREAM::<<
    // private methods:
    Requester.getInstance = function(){
        return _instance;
    };

    alteastream.Requester = Requester;
})();
function resize(){

    var isMobile = window.localStorage.getItem('isMobile');
/*    var maxWidth = isMobile === "yes"? 960:screen.width;
    var maxHeight = isMobile === "yes"? 540:screen.height;*/
    // new version with fixed screen size
    var maxWidth = isMobile === "yes"? 960:1920;
    var maxHeight = isMobile === "yes"? 540:1080;
    var marginDivider = 2;

    var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
    if(is.ipad() || isiPadOS){
        maxWidth = screen.width*2;
        maxHeight = screen.height*2;
        //marginDivider = 1.35;
    }

    var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var deviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

    if(isMobile === "yes" && window.canShowResizeImage === true) {
        var rotateImgDiv = document.getElementById("warning-message");
        rotateImgDiv.style.display = deviceWidth > deviceHeight ? "none" : "block";
    }

    var newWidth = deviceWidth * 0.98;
    var newHeight = deviceHeight * 0.98;
    if(deviceWidth > maxWidth) {
        newWidth = maxWidth;
        newHeight = maxHeight;
    }

    var marginTop;
    var newWidthToHeight = newWidth/newHeight;
    if (newWidthToHeight > widthToHeight) {
        newWidth = newHeight * widthToHeight;
        marginTop = (deviceHeight - newHeight)/ marginDivider;
        $(".game_container").css({
            "top": "50%",
            "left":"50%",
            "height": newHeight,
            "width": newWidth,
            "marginTop" : marginTop
        });

    } else {
        newHeight = newWidth / widthToHeight;
        marginTop = (deviceHeight - newHeight)/ marginDivider;
        $(".game_container").css({
            "top": "50%",
            "left":"50%",
            "height": newHeight,
            "width": newWidth,
            "marginTop" : marginTop
        });
    }
//FS CHECK
    var maxH = window.screen.height,
        maxW = window.screen.width,
        curH = window.innerHeight,
        curW = window.innerWidth;
    var isInFS = maxW === curW && maxH === curH;
    console.log("FS CHECK "+isInFS);
    var ev = new Event("fs");
    ev.isInFS = maxW === curW && maxH === curH;
//var elem = document.getElementById("screen");
    window.dispatchEvent(ev);
}
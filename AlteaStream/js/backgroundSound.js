"use strict";

var sounds = {};
function loadHelperSounds(callback) {
    var _queue = new createjs.LoadQueue(false);
    createjs.Sound.alternateExtensions = ["mp3"];
    _queue.installPlugin(createjs.Sound);

    _queue.addEventListener("complete", function(){
        sounds.bgMusic = createjs.Sound.createInstance("bgMusic");
        sounds.locationSoundBg = createjs.Sound.createInstance("locationSoundBg");
        sounds.background_music = createjs.Sound.createInstance("background_music");
        //sounds.machineNoise = createjs.Sound.createInstance("machineNoise");
        callback();
    });

    _queue.loadManifest(
        [
            {id:"bgMusic", src:"soundBg/circus2Dacha.ogg"},
            {id:"locationSoundBg", src:"soundBg/locationSoundBg.ogg"},
            {id:"background_music", src:"soundBg/background_music.ogg"}//,
            //{id:"machineNoise", src:"soundBg/machineNoise.ogg"}
        ]
    );
}

function startHelperSound(name,props){
    playHelperSound(name,props);
    sounds.currentSound = sounds[name];
}

function playHelperSound(name,props){
    var sound = sounds[name];
    sound.play();
    sound.volume = props.volume || 1;
    sound.loop = props.loop || 0;
}

function stopHelperSound(name){
    sounds[name].stop();
}

function muteHelperSound(name,bool){
    sounds[name].muted = bool;
}
/**
 * @description
 * Switching current playing background sound.
 * NOTE:
 * @method switchSource
 * @param {String} next Name of the sound to switch to
 * @return {Boolean} desc..
 **/
function switchSource(next){
    sounds.currentSound.stop();
    sounds.currentSound = sounds[next];
}

function manageBgMusic(action){
    if(action ==="play"){
        sounds.currentSound.play();
        sounds.currentSound.loop = -1;
        sounds.currentSound.volume = 0.2;
    }else
        sounds.currentSound.paused = true;
}